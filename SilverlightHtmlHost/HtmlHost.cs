﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
using System.ComponentModel;

namespace SilverlightHtmlHost
{
    public class HtmlHost : ContentControl
    {
        int _screenWidth = 0;
        int _screenHeight = 0;
        int _myCenteredLeft = 0;
        int _myCenteredTop = 0;

        int _myWidth = 0;
        double _widthRatio = 0.9;
        public double WidthRatio
        {
            get { return _widthRatio; }
            set { _widthRatio = value; }
        }

        int _myHeight = 0;
        double _heightRatio = 0.9;
        public double HeightRatio
        {
            get { return _heightRatio; }
            set { _heightRatio = value; }
        }

        int _parentWidth = 0;
        public int ParentWidth
        {
            get { return _parentWidth; }
            set { _parentWidth = value; }
        }

        int _parentHeight = 0;
        public int ParentHeight
        {
            get { return _parentHeight; }
            set { _parentHeight = value; }
        }

        int _htmlControlLeft = 0;
        public int HtmlControlLeft
        {
            get { return _htmlControlLeft; }
            set { _htmlControlLeft = value; }
        }

        int _htmlControlTop=0;
        public int HtmlControlTop
        {
            get { return _htmlControlTop; }
            set { _htmlControlTop = value; }
        }

        //readonly String _htmlControlId = System.Guid.NewGuid().ToString();
        String _htmlControlId = System.Guid.NewGuid().ToString();
        public String HtmlControlId
        {
            get { return _htmlControlId; }
            set { _htmlControlId = value; }
        }

        String _navigationUrl;
        public String NavigationUrl
        {
            get { return _navigationUrl; }
            set { _navigationUrl = value; }
        }

        readonly int _htmlZIndex = 0;
        public int HtmlZIndex
        {
            get { return _htmlZIndex; }
        }

        private HtmlElement divIFrameHost;

        public HtmlHost()
        {
            Loaded += new RoutedEventHandler(HtmlHost_Loaded);
            LayoutUpdated += new EventHandler(HtmlHost_LayoutUpdated);
        }

        void HtmlHost_LayoutUpdated(object sender, EventArgs e)
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                HtmlDocument doc = HtmlPage.Document;
                _screenWidth = (int)((double)doc.Body.GetProperty("clientWidth"));
                _screenHeight = (int)((double)doc.Body.GetProperty("clientHeight"));
                _myWidth = (int)(_parentWidth * _widthRatio);
                _myHeight = (int)(_parentHeight * _heightRatio);
                _myCenteredLeft = (_screenWidth - _myWidth) / 2;
                _myCenteredTop = _htmlControlTop + (_screenHeight - _myHeight) / 2;
                if (divIFrameHost != null)
                {
                    divIFrameHost.SetStyleAttribute("top", _myCenteredTop.ToString() + "px");
                    divIFrameHost.SetStyleAttribute("left", _myCenteredLeft.ToString() + "px");
                }
            }
        }

        void HtmlHost_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeComponent();
        }

        public void InitializeComponent()
        {
            if (!DesignerProperties.IsInDesignTool)
            {
                HtmlDocument doc = HtmlPage.Document;
                _screenWidth = (int)((double)doc.Body.GetProperty("clientWidth"));
                _screenHeight = (int)((double)doc.Body.GetProperty("clientHeight"));
                _myWidth = (int)(_parentWidth * _widthRatio);
                _myHeight = (int)(_parentHeight * _heightRatio);
                _myCenteredLeft = (_screenWidth - _myWidth) / 2;
                _myCenteredTop = _htmlControlTop + (_screenHeight - _myHeight) / 2;
                divIFrameHost = doc.CreateElement("div");
                divIFrameHost.SetStyleAttribute("position", "absolute");
                divIFrameHost.SetAttribute("id", _htmlControlId);
                //divIFrameHost.SetStyleAttribute("height", "80%");
                //divIFrameHost.SetStyleAttribute("width", "80%");
                divIFrameHost.SetStyleAttribute("height", _myHeight.ToString() + "px");
                divIFrameHost.SetStyleAttribute("width", _myWidth.ToString() + "px");
                divIFrameHost.SetStyleAttribute("top", _myCenteredTop.ToString() + "px");
                divIFrameHost.SetStyleAttribute("left", _myCenteredLeft.ToString() + "px");
                //divIFrameHost.SetStyleAttribute("left", _htmlControlLeft.ToString() + "px");
                divIFrameHost.SetStyleAttribute("z-index", _htmlZIndex.ToString());
                divIFrameHost.AppendChild(CreateIFrameControl(doc));
                doc.Body.AppendChild(divIFrameHost);
            }
        }

        private HtmlElement CreateIFrameControl(HtmlDocument doc)
        {
            HtmlElement iFrame = doc.CreateElement("IFRAME");
            iFrame.SetAttribute("src", _navigationUrl);
            //iFrame.SetStyleAttribute("height", "80%");
            //iFrame.SetStyleAttribute("width", "80%");
            iFrame.SetStyleAttribute("height", _myHeight.ToString() + "px");
            iFrame.SetStyleAttribute("width", _myWidth.ToString() + "px");
            iFrame.SetStyleAttribute("left", "0px");
            iFrame.SetStyleAttribute("position", "relative");
            iFrame.SetStyleAttribute("top", "0px");
            iFrame.Id = System.Guid.NewGuid().ToString();
            return iFrame;
        }

    }

}
