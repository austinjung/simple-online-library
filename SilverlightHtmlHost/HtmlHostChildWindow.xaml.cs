﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;

namespace SilverlightHtmlHost
{
    public partial class HtmlHostChildWindow : ChildWindow
    {

        private HtmlDocument doc;
        private HtmlElement HTMLdiv;
        private bool windowError;

        public HtmlHostChildWindow()
        {
            try
            {
                InitializeComponent();
                windowError = false;
            }
            catch (Exception e)
            {
                var txtBox = new TextBox();
                txtBox.FontSize = 12;
                //Create Red Color   
                txtBox.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                txtBox.Text = e.Message;
                LayoutRoot.Children.Clear();
                LayoutRoot.Children.Add(txtBox);
                windowError = true;
            }
        }

        public HtmlHostChildWindow(string url)
        {
            try
            {
                InitializeComponent();
                windowError = false;
                customHost.NavigationUrl = url;
            }
            catch (Exception e)
            {
                var txtBox = new TextBox();
                txtBox.FontSize = 12;
                //Create Red Color   
                txtBox.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));  
                txtBox.Text = e.Message;
                LayoutRoot.Children.Clear();
                LayoutRoot.Children.Add(txtBox);
                windowError = true;
            }
        }

        private void ChildWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (windowError)
            {
                return;
            }
            if (doc == null)
            {
                doc = HtmlPage.Document;
            }
            if (HTMLdiv == null)
            {
                HTMLdiv = doc.GetElementById("HTMLdiv");
            }
            if (HTMLdiv != null)
            {
                try
                {
                    HTMLdiv.Parent.RemoveChild(HTMLdiv);
                }
                catch (exception e)
                {
                }
            }
        }

        private void ChildWindow_GotFocus(object sender, RoutedEventArgs e)
        {
            if (windowError)
            {
                return;
            }
            if (doc == null)
            {
                doc = HtmlPage.Document;
            }
            if (HTMLdiv == null)
            {
                HTMLdiv = doc.GetElementById("HTMLdiv");
            }
            if (HTMLdiv != null)
            {
                HTMLdiv.SetStyleAttribute("z-index", "0");
            }
        }

        private void ChildWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (windowError)
            {
                return;
            }
            if (doc == null)
            {
                doc = HtmlPage.Document;
            }
            if (HTMLdiv == null)
            {
                HTMLdiv = doc.GetElementById("HTMLdiv");
            }
            if (HTMLdiv != null)
            {
                HTMLdiv.SetStyleAttribute("z-index", "0");
            }
        }

    }
}

