﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.Windows
Imports System
Imports Microsoft.Windows.Data.DomainServices

Public Class InvisibleWhenNullConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        If IsNothing(value) Then
            Return Visibility.Collapsed
        ElseIf TypeOf (value) Is String Then
            If value = String.Empty Then
                Return Visibility.Collapsed
            Else
                Return Visibility.Visible
            End If
        ElseIf TypeOf (value) Is Double Then
            If value = 0.0 Then
                Return Visibility.Collapsed
            Else
                Return Visibility.Visible
            End If
        Else
            Return Visibility.Visible
        End If
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function

End Class
