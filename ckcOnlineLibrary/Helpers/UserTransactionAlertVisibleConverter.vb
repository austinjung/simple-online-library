﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.Windows
Imports System

Public Class UserTransactionAlertVisibleConverter
    Implements IValueConverter

    Private userCommented As Boolean = False
    Private userCommentViewed As Boolean = False

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        If TypeOf (value) Is Date And parameter = "UserCommented" Then
            userCommented = True
            Return Visibility.Collapsed
        ElseIf IsNothing(value) And parameter = "UserCommented" Then
            userCommented = False
            Return Visibility.Collapsed
        ElseIf TypeOf (value) Is Boolean And parameter = "UserCommentViewed" Then
            userCommentViewed = CType(value, Boolean)
            If userCommented And Not userCommentViewed Then
                Return Visibility.Visible
            Else
                Return Visibility.Collapsed
            End If
        Else
            Return Visibility.Collapsed
        End If
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function

End Class
