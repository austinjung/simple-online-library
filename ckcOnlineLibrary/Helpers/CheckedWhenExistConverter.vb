﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.ServiceModel.DomainServices.Client

Public Class CheckBookAuthorsConverter
    Implements IValueConverter

    Private _objectList As List(Of Author)


    Public Sub New(ByRef objectList As List(Of Author))
        _objectList = objectList
    End Sub

    Public Sub New()

    End Sub

    Public Property ObjectList As List(Of Author)
        Get
            Return _objectList
        End Get
        Set(value As List(Of Author))
            _objectList = value
        End Set
    End Property

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        If Not IsNothing(_objectList) Then
            Return If(_objectList.Contains(value), True, False)
        End If
        Return False
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function

End Class


Public Class CheckBookCategoriesConverter
    Implements IValueConverter

    Private _objectList As List(Of Category)


    Public Sub New(ByRef objectList As List(Of Category))
        _objectList = objectList
    End Sub

    Public Sub New()

    End Sub

    Public Property ObjectList As List(Of Category)
        Get
            Return _objectList
        End Get
        Set(value As List(Of Category))
            _objectList = value
        End Set
    End Property

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        If Not IsNothing(_objectList) Then
            Return If(_objectList.Contains(value), True, False)
        End If
        Return False
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function

End Class

Public Class CheckBookShelvesConverter
    Implements IValueConverter

    Private _objectList As List(Of Shelf)


    Public Sub New(ByRef objectList As List(Of Shelf))
        _objectList = objectList
    End Sub

    Public Sub New()

    End Sub

    Public Property ObjectList As List(Of Shelf)
        Get
            Return _objectList
        End Get
        Set(value As List(Of Shelf))
            _objectList = value
        End Set
    End Property

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        If Not IsNothing(_objectList) Then
            Return If(_objectList.Contains(value), True, False)
        End If
        Return False
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function

End Class

