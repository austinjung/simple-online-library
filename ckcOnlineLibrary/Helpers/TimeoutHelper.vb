﻿Imports System
Imports System.ServiceModel.DomainServices.Client

Public NotInheritable Class TimeoutHelper

    Private Sub New()
    End Sub

    Public Shared Function HandleAuthenticationTimeout(ex As Exception) As Boolean

        Dim dex As DomainException = TryCast(ex, DomainException)

        If (dex IsNot Nothing) AndAlso (dex.ErrorCode = ErrorCodes.NotAuthenticated) AndAlso WebContext.Current.User.IsAuthenticated Then
            ' Likely a timeout, we'll need to refresh our identity
            WebContext.Current.Authentication.LoadUser()
            Return True
        End If
        Return False

    End Function

End Class
