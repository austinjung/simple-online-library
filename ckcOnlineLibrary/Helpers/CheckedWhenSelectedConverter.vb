﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.ServiceModel.DomainServices.Client

Public Class CheckedWhenSelectedConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        Dim dataGrid As DataGrid = CType(parameter, ViewModelLocator).ManageBooksVMForChildWindow.Categories
        If dataGrid.SelectedItems.Count > 0 Then
            Return If(dataGrid.SelectedItems.Contains(value), True, False)
        End If
        Return False

    End Function

    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function
End Class
