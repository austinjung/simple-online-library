﻿Imports System.Windows.Data

Public Class DateNullValueConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        Dim dateString As String = String.Empty
        Dim transaction As Member_Transactions

        If TypeOf (value) Is DateTime Then
            If Not IsNothing(value) Then
                dateString = String.Format("{0:d}", value)
                'dateString = CType(value, DateTime).ToString("MM/dd/yyyy")
            End If
        ElseIf TypeOf (value) Is Member_Transactions Then
            transaction = CType(value, Member_Transactions)
            If transaction.Expected_Return < Date.Now() And IsNothing(transaction.Returned_Date) Then
                dateString = "Over Due!!!"
            Else
                dateString = String.Format("{0:d}", transaction.Returned_Date)
                'dateString = CType(transaction.Returned_Date, Date).ToString("MM/dd/yyyy")
            End If
        End If
        If dateString.Equals("01/01/0001") Or String.IsNullOrEmpty(dateString) Then
            dateString = "N/A"
        End If
        Return dateString
    End Function

    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack

        Dim returnDate As Date = Nothing

        If TypeOf (value) Is String Then
            Try
                returnDate = Date.Parse(value)
            Catch ex As Exception
                'do nothing
            End Try
        End If

        Return returnDate

    End Function
End Class
