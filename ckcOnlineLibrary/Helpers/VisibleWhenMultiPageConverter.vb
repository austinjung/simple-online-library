﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.Windows
Imports System
Imports Microsoft.Windows.Data.DomainServices

Public Class VisibleWhenMultiPageConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        Dim pageView As DomainCollectionView
        If TypeOf (value) Is DomainCollectionView Then
            pageView = CType(value, DomainCollectionView)
            If pageView.TotalItemCount <= pageView.PageSize Then
                'Return False
                Return Visibility.Collapsed
            Else
                'Return True
                Return Visibility.Visible
            End If
        End If
        'Return(False)
        Return Visibility.Collapsed
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function

End Class
