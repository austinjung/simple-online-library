﻿Imports System.Windows.Data

Public Class DueDateColorConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        If (TypeOf (value) Is Date) And (Not IsNothing(value)) Then
            Dim inputDate As Date = CType(value, Date)
            Dim inputDateString As String
            If IsNothing(inputDate) Then
                inputDateString = "01/01/0001"
            Else
                inputDateString = inputDate.ToString("MM/dd/yyyy")
            End If
            If inputDateString.Equals("01/01/0001") Then
                Return "Black"
            End If
            Dim tomorrow As Date = Date.Today().AddDays(1)
            If CType(value, Date) < tomorrow Then
                Return "Red"
            Else
                Return "Black"
            End If
        End If
        Return "Black"
    End Function

    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotSupportedException()
    End Function
End Class
