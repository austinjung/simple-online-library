﻿Imports System.Windows.Data
Imports System.Windows.Controls

Public Class CommentColorConverter
    Implements IValueConverter


    Public Function Convert(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.Convert
        If (TypeOf (value) Is Boolean) Then
            If CType(value, Boolean) = False Then
                Return "OrangeRed"
            Else
                Return "Black"
            End If
        End If
        Return "Black"
    End Function

    Public Function ConvertBack(value As Object, targetType As System.Type, parameter As Object, culture As System.Globalization.CultureInfo) As Object Implements System.Windows.Data.IValueConverter.ConvertBack
        Throw New NotSupportedException()
    End Function
End Class
