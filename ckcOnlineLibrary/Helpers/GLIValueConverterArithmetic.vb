﻿Imports System.Text.RegularExpressions
Imports System.Windows.Data

''' <summary>
''' Perform Math Operations (Multiply, divide, add, subtract)
''' </summary>
''' <remarks>
''' Snippet source found at: http://learnwpf.com/Posts/Post.aspx?postId=9b745fe8-7d51-4d01-a8c7-f31083c4be94
''' C# converted to VB at: http://www.developerfusion.com/tools/convert/csharp-to-vb/
''' </remarks>

Class GLIValueConverterArithmetic
    Implements IValueConverter
    Private Const ArithmeticParseExpression As String = "([+\-*/]{1,1})\s{0,}(\-?[\d\.]+)"
    Private arithmeticRegex As New Regex(ArithmeticParseExpression)

    Private Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.Convert

        If TypeOf value Is Double AndAlso parameter IsNot Nothing Then
            Dim param As String = parameter.ToString()

            If param.Length > 0 Then
                Dim match As Match = arithmeticRegex.Match(param)
                If match IsNot Nothing AndAlso match.Groups.Count = 3 Then
                    Dim operation As String = match.Groups(1).Value.Trim()
                    Dim numericValue As String = match.Groups(2).Value

                    Dim number As Double = 0
                    If Double.TryParse(numericValue, number) Then
                        ' this should always succeed or our regex is broken
                        Dim valueAsDouble As Double = CDbl(value)
                        Dim returnValue As Double = 0

                        Select Case operation
                            Case "+"
                                returnValue = valueAsDouble + number
                                Exit Select

                            Case "-"
                                If (valueAsDouble - number) < 0 Then
                                    returnValue = 0
                                Else
                                    returnValue = valueAsDouble - number
                                End If
                                Exit Select

                            Case "*"
                                returnValue = valueAsDouble * number
                                Exit Select

                            Case "/"
                                returnValue = valueAsDouble / number
                                Exit Select
                        End Select

                        Return returnValue
                    End If
                End If
            End If
        End If

        Return Nothing
    End Function

    Private Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As System.Globalization.CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New Exception("The method or operation is not implemented.")
    End Function

End Class
