﻿Imports System.Windows.Data
Imports System.Globalization
Imports System.Windows
Imports System
Imports Microsoft.Windows.Data.DomainServices

Public Class VisibleWhenZeroCollectionConverter
    Implements IValueConverter

    Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
        Dim pageView As DomainCollectionView
        If TypeOf (value) Is DomainCollectionView Then
            pageView = CType(value, DomainCollectionView)
            If pageView.IsEmpty Then
                Return Visibility.Visible
            Else
                Return Visibility.Collapsed
            End If
        End If
        Return Visibility.Visible
    End Function

    Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
        Throw New NotImplementedException()
    End Function

End Class
