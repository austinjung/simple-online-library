﻿Public Class DesignEditAuthor

    Public ReadOnly Property EditAuthor As Author
        Get
            Return New Author() With { _
                 .Author_id = 2, _
                 .Author_First_Name = "Austin",
                 .Author_Last_Name = "Jung",
                 .Author_Career = "Career of Author",
                 .IsActive = True
        }
        End Get
    End Property

End Class
