﻿Imports System.Collections.ObjectModel
Imports System

Public Class DesignBooks
    Inherits ObservableCollection(Of DesignBook)

    Public Sub New()
        For i As Integer = 0 To 5
            'Add(CreateBook(i))
            Add(New DesignBook())
        Next
    End Sub

    Public Function CreateBook(i As Integer) As Book
        Dim book = New Book() With { _
            .Book_Language = "English", _
            .Book_Description = "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down.", _
            .Book_Media = "Paperback", _
            .Book_ISBN = "ISBN 000" & i, _
            .Book_Published = New DateTime(2005, 3, 18), _
            .Book_Quantity_Avail = i, _
            .Book_Quantity_Onhand = i, _
            .Book_Title = "Contoso Adventures " & i, _
            .Book_Image_File = "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary.Web/Upload/Book_Images/" & i & ".jpg", _
            .Book_Authors = New DesignAuthors(), _
            .Book_Categories = New DesignCategories(), _
            .Book_Shelves = New DesignShelves(), _
            .IsActive = True
        }
        Return book
    End Function

End Class
