﻿Imports System.Collections.ObjectModel

Public Class DesignShelves
    Inherits ObservableCollection(Of Shelf)

    Public Sub New()
        For i As Integer = 0 To 5
            Add(CreateShelf(i))
        Next
    End Sub

    Public Function CreateShelf(i As Integer) As Shelf
        Dim shelf = New Shelf() With { _
                 .Shelf_id = i, _
                 .Shelf_Code = "Shelf" & i, _
                 .Shelf_Description = "Description of Shelf" & i, _
                 .IsActive = True
        }
        Return shelf
    End Function

End Class
