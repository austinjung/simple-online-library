﻿Imports System.Collections.ObjectModel

Public Class DesignBookDetailHistory

    Public ReadOnly Property Book As DesignBook
        Get
            Return New DesignBook()
        End Get
    End Property

    Public ReadOnly Property Book_Reviews As ObservableCollection(Of Member_Reviews)
        Get
            Return New DesignMemberReviews()
        End Get
    End Property

End Class
