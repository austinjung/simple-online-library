﻿Imports System.Collections.ObjectModel
Imports System

Public Class DesignMemberReviews
    Inherits ObservableCollection(Of Member_Reviews)

    Public ReadOnly Property User_Reviews As ObservableCollection(Of Member_Reviews)
        Get
            Return Me
        End Get
    End Property

    Public Sub New()
        For i As Integer = 0 To 5
            Add(Create_Member_Reviews(i))
            'Add(New DesignMemberReview())
        Next
    End Sub

    Private Function Create_Member_Reviews(i As Integer) As Member_Reviews
        Dim review = New Member_Reviews() With { _
            .Book = New DesignBooks().CreateBook(i), _
            .Review_Date = New DateTime(2005, 3, 18), _
            .Review = "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down.", _
            .Rating = (i + 1) / 6, _
            .Member = New DesignMembers().CreateMember(i)
        }
        Return review
    End Function

End Class
