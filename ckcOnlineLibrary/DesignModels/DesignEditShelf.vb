﻿Public Class DesignEditShelf

    Public ReadOnly Property EditShelf As Shelf
        Get
            Return New Shelf() With { _
                 .Shelf_id = 2, _
                 .Shelf_Code = "A-1",
                 .Shelf_Description = "Upper Shelf",
                 .IsActive = True
        }
        End Get
    End Property

End Class
