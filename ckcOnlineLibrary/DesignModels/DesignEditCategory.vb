﻿Public Class DesignEditCategory

    Public ReadOnly Property EditCategory As Category
        Get
            Return New Category() With { _
                 .Category_id = 2, _
                 .Category_Name = "Programing",
                 .IsActive = True
        }
        End Get
    End Property

End Class
