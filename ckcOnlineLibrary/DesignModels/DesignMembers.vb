﻿Imports System.Collections.ObjectModel
Imports System

Public Class DesignMembers
    Inherits ObservableCollection(Of Member)

    Public Sub New()
        For i As Integer = 0 To 5
            Add(CreateMember(i))
        Next
    End Sub

    Public ReadOnly Property Members As ObservableCollection(Of Member)
        Get
            Return Me
        End Get
    End Property

    Public ReadOnly Property Selected_Member As Member
        Get
            Return Me(2)
        End Get
    End Property

    Public ReadOnly Property MemberLogin As String
        Get
            Return Me(2).MemberLogin
        End Get
    End Property

    Public ReadOnly Property FullName As String
        Get
            Return Me(2).FullName
        End Get
    End Property

    Public ReadOnly Property Email As String
        Get
            Return Me(2).Email
        End Get
    End Property

    Public ReadOnly Property Role As String
        Get
            Return Me(2).Role
        End Get
    End Property

    Public ReadOnly Property MemberActive As Boolean
        Get
            Return Me(2).MemberActive
        End Get
    End Property

    Public ReadOnly Property MemberSince As Date
        Get
            Return Me(2).MemberSince
        End Get
    End Property

    Public ReadOnly Property MemberDeactivatedFrom As Date
        Get
            Return Me(2).MemberDeactivatedFrom
        End Get
    End Property

    Public ReadOnly Property Address As String
        Get
            Return Me(2).Address_Line_1 & " " & Me(2).Address_Line_2 & ", " & Me(2).City & ", " & Me(2).Province & "  " & Me(2).PostalCode
        End Get
    End Property

    Public Function CreateMember(i As Integer) As Member
        Dim member = New Member() With { _
            .Address_Line_1 = i & " Willoughby Avenue", _
            .Address_Line_2 = "", _
            .AllowEmails = True, _
            .City = "Langley", _
            .Country = "Canada", _
            .Email = "user@email.com", _
            .FullName = "Member " & i, _
            .Library_PIN = "PIN 000" & i, _
            .MemberActive = True, _
            .MemberLogin = "Member " & i, _
            .MemberSince = New Date(2005, 3, 18), _
            .MemberDeactivatedFrom = Nothing, _
            .Name = "Member " & i, _
            .Password = "*******", _
            .Phone = "(604) 123-4567", _
            .PostalCode = "V5C 7Y7", _
            .Province = "BC", _
            .Role = "User", _
            .Roles = New String() {"User", "Manager"}
        }
        Return member
    End Function

End Class
