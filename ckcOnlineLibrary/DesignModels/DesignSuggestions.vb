﻿Imports System.Collections.ObjectModel

Public Class DesignSuggestions
    Inherits ObservableCollection(Of NewBookRequest)

    Public Sub New()
        For i As Integer = 0 To 5
            Add(CreateNewBookRequest(i))
        Next
    End Sub

    Public Function CreateNewBookRequest(i As Integer) As NewBookRequest
        Dim bookRequest = New NewBookRequest() With { _
                 .NewBookRequestID = i, _
                 .Member = New Member With {.MemberLogin = "User" & i}, _
                 .RequestContent = "I heard 'The Bible' is a very good for us.", _
                 .RequestDate = New DateTime(2012, 10, 18), _
                 .RequestViewed = True,
                 .Reviews = "We are processing now.",
                 .LastReviewDate = New DateTime(2012, 10, 20), _
                 .RequestClosed = False
        }
        Return bookRequest
    End Function

    Public ReadOnly Property SelectedSuggestion As NewBookRequest
        Get
            Return Me.Item(0)
        End Get
    End Property

    Public ReadOnly Property SuggestionsTree As ObservableCollection(Of TopicTree)
        Get
            Dim trees As ObservableCollection(Of TopicTree) = New ObservableCollection(Of TopicTree)
            For Each sg As NewBookRequest In Me
                Dim suggestion As TopicTree
                Dim review As TopicTree
                suggestion = New TopicTree With {
                    .Title = sg.RequestContent & " - " & sg.Member.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
                }
                review = New TopicTree With {
                    .Title = sg.Reviews & " - Admin : " & sg.RequestDate.ToString("MM/dd/yyyy")
                }
                Dim reviews As ObservableCollection(Of TopicTree) = New ObservableCollection(Of TopicTree)
                reviews.Add(review)
                suggestion.ChildTopics = reviews
                trees.Add(suggestion)
            Next
            Return trees
        End Get
    End Property

    Public ReadOnly Property TemplateAnswers As ObservableCollection(Of AnswerTemplate)
        Get
            Dim trees As ObservableCollection(Of AnswerTemplate) = New ObservableCollection(Of AnswerTemplate)
            For i As Integer = 0 To 5
                Dim answer As AnswerTemplate
                answer = New AnswerTemplate With {
                    .AnswerTemplate1 = "We will buy some soon. We will buy some soon. We will buy some soon." & _
                                        "We will buy some soon. We will buy some soon. We will buy some soon." & _
                                        "We will buy some soon. We will buy some soon. We will buy some soon." & _
                                        "We will buy some soon. We will buy some soon. We will buy some soon." & _
                                        "We will buy some soon. We will buy some soon. We will buy some soon." & vbCrLf & "Please be patient"
                }
                trees.Add(answer)
            Next
            Return trees
        End Get
    End Property

End Class
