﻿Public Class DesignBookRequest

    Public ReadOnly Property Book As DesignBook
        Get
            Return New DesignBook()
        End Get
    End Property

    Public ReadOnly Property RequestExpectedDate As Date
        Get
            Return Today.AddDays(1)
        End Get
    End Property

    Public ReadOnly Property RequestComment As String
        Get
            Return "I would like to pick-up at Noon tomorrow."
        End Get
    End Property

    Public ReadOnly Property My_Previous_Pending_Request As Member_Requests
        Get
            Return New DesignMemberRequests().Create_Member_Requests(1)
        End Get
    End Property

    Public ReadOnly Property Selected_Request As Member_Requests
        Get
            Return New DesignMemberRequests().Create_Member_Requests(1)
        End Get
    End Property

    Public ReadOnly Property Selected_Transaction As Member_Transactions
        Get
            Return New DesignMemberTransactions().Create_Member_Transactions(1)
        End Get
    End Property

End Class
