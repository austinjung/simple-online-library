﻿Imports System.Collections.ObjectModel
Imports System

Public Class DesignMemberDetailTransactions
    Inherits ObservableCollection(Of Transactions_Detail)

    Public ReadOnly Property User_Returned_Transactions_Detail As ObservableCollection(Of Transactions_Detail)
        Get
            Return Me
        End Get
    End Property

    Public Sub New()
        For i As Integer = 0 To 5
            Add(Create_Member_Transactions_Detail(i))
        Next
    End Sub

    Private Function Create_Member_Transactions_Detail(i As Integer) As Transactions_Detail
        Dim transaction = New Transactions_Detail() With { _
            .Book_Title = "Contoso Adventures" & i, _
            .Book_Language = "English", _
            .Book_Description = "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down.", _
            .Book_Media = "Paperback", _
            .Book_ISBN = "ISBN 000" & i, _
            .Authors = "Austin Jung", _
            .Categories = "Programming", _
            .Book_Published = New DateTime(2005, 3, 18), _
            .Review = "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down.", _
            .Issued_Date = New DateTime(2005, 3, 18), _
            .Rating = (i + 1) * 15 / 100, _
            .Review_Date = New DateTime(2005, 3, 18), _
            .Returned_Date = New DateTime(2005, 3, 18), _
            .Book_Image_File = "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary.Web/Upload/Book_Images/" & i & ".jpg"
        }
        Return transaction
    End Function

End Class
