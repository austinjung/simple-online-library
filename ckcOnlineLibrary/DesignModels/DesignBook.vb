﻿Public Class DesignBook

    Public ReadOnly Property Book_Title As String
        Get
            Return "Contoso Adventures"
        End Get
    End Property

    Public ReadOnly Property Book_Language As String
        Get
            Return "English"
        End Get
    End Property

    Public ReadOnly Property Book_Description As String
        Get
            Return "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down."
        End Get
    End Property

    Public ReadOnly Property Book_Media As String
        Get
            Return "Paperback"
        End Get
    End Property

    Public ReadOnly Property Book_ISBN As String
        Get
            Return "ISBN 0001"
        End Get
    End Property

    Public ReadOnly Property Book_Published As DateTime
        Get
            Return New DateTime(2005, 3, 18)
        End Get
    End Property

    Public ReadOnly Property Book_Quantity_Avail As Integer
        Get
            Return 3
        End Get
    End Property

    Public ReadOnly Property Book_Quantity_Onhand As Integer
        Get
            Return 1
        End Get
    End Property

    Public ReadOnly Property Book_Image_File As String
        Get
            'Return "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary/Assets/Images/1.jpg"
            Return "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary.Web/Upload/Book_Images/1.jpg"
        End Get
    End Property
    Public ReadOnly Property ImageSource As String
        Get
            Return Book_Image_File
        End Get
    End Property

    Public ReadOnly Property Book_Categories_List As String
        Get
            Return "Programming"
        End Get
    End Property

    Public ReadOnly Property Book_Authors_List As String
        Get
            Return "Austin Jung"
        End Get
    End Property

    Public ReadOnly Property Book_Shelves_List As String
        Get
            Return "Shelf:A-1"
        End Get
    End Property

    Public ReadOnly Property Total_Readers As Integer
        Get
            Return 33
        End Get
    End Property

    Public ReadOnly Property Total_Reviews As Integer
        Get
            Return 12
        End Get
    End Property

    Public ReadOnly Property Pending_Requests As Integer
        Get
            Return 1
        End Get
    End Property

    Public ReadOnly Property Average_Rating As Double
        Get
            Return 89.9
        End Get
    End Property

End Class
