﻿Public Class DesignDetailTransaction

    Public ReadOnly Property Book_Title As String
        Get
            Return "Contoso Adventures"
        End Get
    End Property

    Public ReadOnly Property Book_Language As String
        Get
            Return "English"
        End Get
    End Property

    Public ReadOnly Property Book_Description As String
        Get
            Return "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down."
        End Get
    End Property

    Public ReadOnly Property Book_Media As String
        Get
            Return "Paperback"
        End Get
    End Property

    Public ReadOnly Property Book_ISBN As String
        Get
            Return "ISBN 0001"
        End Get
    End Property

    Public ReadOnly Property Book_Published As DateTime
        Get
            Return New DateTime(2005, 3, 18)
        End Get
    End Property

    Public ReadOnly Property Issued_Date As DateTime
        Get
            Return New DateTime(2012, 3, 18)
        End Get
    End Property

    Public ReadOnly Property Returned_Date As DateTime
        Get
            Return New DateTime(2012, 3, 31)
        End Get
    End Property

    Public ReadOnly Property Review_Date As DateTime
        Get
            Return New DateTime(2012, 3, 31)
        End Get
    End Property

    Public ReadOnly Property Book_Image_File As String
        Get
            'Return "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary/Assets/Images/1.jpg"
            Return "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary.Web/Upload/Book_Images/1.jpg"
        End Get
    End Property
    Public ReadOnly Property ImageSource As String
        Get
            Return Book_Image_File
        End Get
    End Property

    Public ReadOnly Property Categories As String
        Get
            Return "Programming"
        End Get
    End Property

    Public ReadOnly Property Authors As String
        Get
            Return "Austin Jung"
        End Get
    End Property

    Public ReadOnly Property Review As String
        Get
            Return "The best book ever created by man!"
        End Get
    End Property

    Public ReadOnly Property Rating As Double
        Get
            Return 80.0
        End Get
    End Property

    Public ReadOnly Property UserInputComment As String
        Get
            Return "The best book ever created by man!"
        End Get
    End Property

    Public ReadOnly Property UserInputRating As Double
        Get
            Return 80.0
        End Get
    End Property

End Class
