﻿Imports System.Collections.ObjectModel

Public Class DesignTopReviews
    Inherits ObservableCollection(Of DesignTopReviewBook)

    Public ReadOnly Property Top_Reviews As ObservableCollection(Of DesignTopReviewBook)
        Get
            Return Me
        End Get
    End Property

    Public Sub New()
        For i As Integer = 0 To 5
            'Add(CreateTopReviewsBooks(i))
            Add(New DesignTopReviewBook())
        Next
    End Sub

    Public Function CreateTopReviewsBooks(i As Integer) As Books_By_Reviews
        Dim book = New Books_By_Reviews() With { _
            .Book_id = i, _
            .Book_Description = "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down.", _
            .Book_Quantity_Onhand = i, _
            .Book_Title = "Contoso Adventures " & i, _
            .Book_Image_File = "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary/Assets/Images/" & i & ".jpg", _
            .Average_Rate = (i + 1) / 6
        }
        Return book
    End Function

End Class
