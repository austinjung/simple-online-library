﻿Imports System.Windows.Media.Imaging

Public Class DesignTopReviewBook

    Public ReadOnly Property Book_Title As String
        Get
            Return "Contoso Adventures"
        End Get
    End Property

    Public ReadOnly Property Book_Description As String
        Get
            Return "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down."
        End Get
    End Property

    Public ReadOnly Property Average_Rate As Double
        Get
            Return 3.7 / 5
        End Get
    End Property

    Public ReadOnly Property Book_Quantity_Onhand As Integer
        Get
            Return 1
        End Get
    End Property

    Public ReadOnly Property Book_Image_Source As ImageSource
        Get
            'Return New BitmapImage(New Uri(Application.Current.Host.Source, "/Assets/Images/1.jpg"))
            Dim image As Image = New Image()
            image.Source = New BitmapImage(New Uri("file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary/Assets/Images/1.jpg"))
            Return image.Source
        End Get
    End Property

    Public ReadOnly Property Book_Image_File As String
        Get
            Return "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary/Assets/Images/1.jpg"
        End Get
    End Property

    Public ReadOnly Property ImageSource As String
        Get
            Return Me.Book_Image_File
        End Get
    End Property
End Class
