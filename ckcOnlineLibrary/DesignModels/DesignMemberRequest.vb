﻿Imports System.Windows.Media.Imaging

Public Class DesignMemberRequest

    Public ReadOnly Property Book As DesignBook
        Get
            Return New DesignBook()
        End Get
    End Property

    Public ReadOnly Property Member As Member
        Get
            Return New DesignMembers().CreateMember(4)
        End Get
    End Property

    Public ReadOnly Property Expected_Date As DateTime
        Get
            Return New DateTime(2005, 3, 18)
        End Get
    End Property

    Public ReadOnly Property Comitted_Date As DateTime
        Get
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property Comment As String
        Get
            Return "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down."
        End Get
    End Property

    Public ReadOnly Property Request_Status As Boolean
        Get
            Return True
        End Get
    End Property

End Class
