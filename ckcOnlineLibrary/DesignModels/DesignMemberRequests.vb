﻿Imports System.Collections.ObjectModel
Imports System

Public Class DesignMemberRequests
    Inherits ObservableCollection(Of Member_Requests)

    Public ReadOnly Property User_Pending_Requests As ObservableCollection(Of Member_Requests)
        Get
            Return Me
        End Get
    End Property

    Public Sub New()
        For i As Integer = 0 To 5
            Add(Create_Member_Requests(i))
            'Add(New DesignMemberRequest())
        Next
    End Sub

    Public Function Create_Member_Requests(i As Integer) As Member_Requests
        Dim request = New Member_Requests() With { _
            .Book = New DesignBooks().CreateBook(i), _
            .Comitted_Date = New DateTime(2005, 3, 18), _
            .Comment = "austinjung : I'll pick-up at noon - 4/12/2012.", _
            .Expected_Date = New DateTime(2005, 3, 18), _
            .Member = New DesignMembers().CreateMember(i), _
            .Request_Status = True
        }
        Return request
    End Function

End Class
