﻿Imports System.Collections.ObjectModel

Public Class DesignCategories
    Inherits ObservableCollection(Of Category)

    Public Sub New()
        For i As Integer = 0 To 5
            Add(CreateCategory(i))
        Next
    End Sub

    Public Function CreateCategory(i As Integer) As Category
        Dim category = New Category() With { _
                 .Category_id = i, _
                 .Category_Name = "Technical " & i, _
                 .IsActive = True
        }
        Return category
    End Function


End Class
