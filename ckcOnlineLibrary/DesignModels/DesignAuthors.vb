﻿Imports System.Collections.ObjectModel

Public Class DesignAuthors
    Inherits ObservableCollection(Of Author)

    Public Sub New()
        For i As Integer = 0 To 5
            Add(CreateAuthor(i))
        Next
    End Sub

    Public Function CreateAuthor(i As Integer) As Author
        Dim author = New Author() With { _
                 .Author_id = i, _
                 .Author_First_Name = "Fname" & i, _
                 .Author_Last_Name = "Author" & i, _
                 .Author_Career = "Career of Author" & i, _
                 .IsActive = True
        }
        Return author
    End Function

End Class
