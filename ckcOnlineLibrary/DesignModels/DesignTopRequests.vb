﻿Imports System.Collections.ObjectModel

Public Class DesignTopRequests
    Inherits ObservableCollection(Of DesignTopRequestBook)

    Public ReadOnly Property Top_Requests As ObservableCollection(Of DesignTopRequestBook)
        Get
            Return Me
        End Get
    End Property

    Public Sub New()
        For i As Integer = 0 To 5
            'Add(CreateTopRequestBooks(i))
            Add(New DesignTopRequestBook())
        Next
    End Sub

    Public Function CreateTopRequestBooks(i As Integer) As Books_By_Requests
        Dim book = New Books_By_Requests() With { _
            .Book_id = i, _
            .Book_Description = "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down.", _
            .Book_Quantity_Onhand = i, _
            .Book_Title = "Contoso Adventures " & i, _
            .Book_Image_File = "file:///C:/Documents%20and%20Settings/austinjung/My%20Documents/CK%20Library/ckcOnlineLibrary/ckcOnlineLibrary/Assets/Images/" & i & ".jpg", _
            .Request_Count = i
        }
        Return book
    End Function

End Class
