﻿Imports System.Collections.ObjectModel
Imports System

Public Class DesignMemberTransactions
    Inherits ObservableCollection(Of Member_Transactions)

    Public ReadOnly Property User_Transactions As ObservableCollection(Of Member_Transactions)
        Get
            Return Me
        End Get
    End Property

    Public Sub New()
        For i As Integer = 0 To 5
            Add(Create_Member_Transactions(i))
            'Add(New DesignMemberTransaction())
        Next
    End Sub

    Public Function Create_Member_Transactions(i As Integer) As Member_Transactions
        Dim transaction = New Member_Transactions() With { _
            .Book = New DesignBooks().CreateBook(i), _
            .Expected_Return = New DateTime(2005, 3, 18), _
            .Comment = "The best book ever created by man! Don't believe me? Well, ask anyone. Seriously. Once you pick it up, you won't be able to put it down.", _
            .Issued_Date = New DateTime(2005, 3, 18), _
            .Member = New DesignMembers().CreateMember(i), _
            .Requested_Date = New DateTime(2005, 3, 18), _
            .Returned_Date = New DateTime(2005, 3, 18)
        }
        Return transaction
    End Function

End Class
