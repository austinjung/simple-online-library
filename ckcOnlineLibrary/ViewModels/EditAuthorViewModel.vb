﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class EditAuthorViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'Book Properties to edit
    Private _editAuthor As Author = Nothing
    Private _authorBeforeEdit As Author = Nothing

    Private _authors As List(Of Author) = Nothing
    Private _authorsVisibility As Visibility = Visibility.Collapsed

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False
    Private _isNewAuthorEdited As Boolean = True
    Private _isChecked As Boolean = False
    Private _isCheckedVisibility As Visibility = Visibility.Collapsed

    'Commands for save & cancel
    Private m_SaveAuthorChangesCommand As RelayCommand
    Private m_CancelAuthorChangesCommand As RelayCommand
    Private m_SelectedAuthorChangedCommand As RelayCommand(Of DataGrid)

#End Region

#Region "Private Method : Property changed event"

    Private Sub editAuthor_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        Select Case e.PropertyName
            Case "Author_First_Name"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveAuthorChangesCommand.RaiseCanExecuteChanged()
                End If
            Case "Author_Last_Name"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveAuthorChangesCommand.RaiseCanExecuteChanged()
                End If
            Case "Author_Career"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveAuthorChangesCommand.RaiseCanExecuteChanged()
                End If
            Case "IsActive"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveAuthorChangesCommand.RaiseCanExecuteChanged()
                End If
        End Select
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"


    Public Property EditAuthor() As Author
        Get
            Return _editAuthor
        End Get
        Set(value As Author)
            _editAuthor = value
            If IsNothing(value) Then
                IsNewAuthorEdited = False
            ElseIf _editAuthor.Author_id > 0 Then
                IsNewAuthorEdited = False
            Else
                IsNewAuthorEdited = True
            End If
            SaveAuthorBeforeEdit()
            Me.RaisePropertyChanged("EditAuthor")
        End Set
    End Property

    Public Property IsNewAuthorEdited As Boolean
        Get
            Return _isNewAuthorEdited
        End Get
        Set(value As Boolean)
            _isNewAuthorEdited = value
            If value Then
                AuthorsVisibility = Visibility.Collapsed
            Else
                AuthorsVisibility = Visibility.Visible
            End If
        End Set
    End Property

    Public Property Authors As List(Of Author)
        Get
            Return _authors
        End Get
        Set(value As List(Of Author))
            _authors = value
        End Set
    End Property

    Public Property AuthorsVisibility As Visibility
        Get
            Return _authorsVisibility
        End Get
        Set(value As Visibility)
            _authorsVisibility = value
            If value = Visibility.Visible Then
                IsCheckedVisibility = Visibility.Collapsed
            Else
                IsCheckedVisibility = Visibility.Visible
            End If
            Me.RaisePropertyChanged("AuthorsVisibility")
        End Set
    End Property

    Public Property IsCheckedVisibility As Visibility
        Get
            Return _isCheckedVisibility
        End Get
        Set(value As Visibility)
            _isCheckedVisibility = value
            Me.RaisePropertyChanged("IsCheckedVisibility")
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   authors As List(Of Author),
                   editAuthor As Author)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        Me.Authors = authors
        Me.EditAuthor = editAuthor

        ''''''''''''''''''''''''''''
        'Don't refresh Domain Data Context, 
        'Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''

        'Set Property Changed Event Handler

        If Not IsNothing(Me.EditAuthor) Then
            AddHandler Me._editAuthor.PropertyChanged, AddressOf editAuthor_PropertyChanged
        End If

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SaveAuthorChangesCommand() As RelayCommand
        Get
            Return m_SaveAuthorChangesCommand
        End Get
        Set(value As RelayCommand)
            m_SaveAuthorChangesCommand = value
        End Set
    End Property

    Public Property CancelAuthorChangesCommand() As RelayCommand
        Get
            Return m_CancelAuthorChangesCommand
        End Get
        Set(value As RelayCommand)
            m_CancelAuthorChangesCommand = value
        End Set
    End Property

    Public Property SeletedAuthorChangedCommand As RelayCommand(Of DataGrid)
        Get
            Return m_SelectedAuthorChangedCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectedAuthorChangedCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SaveAuthorChangesCommand = New RelayCommand(AddressOf OnSaveAuthorChanges, Function() HasChanges)
        CancelAuthorChangesCommand = New RelayCommand(AddressOf OnCancelAuthorChanges)
        SeletedAuthorChangedCommand = New RelayCommand(Of DataGrid)(AddressOf OnSelectedAuthorChanged)
    End Sub

    Protected Sub UnregisterCommands()
        SaveAuthorChangesCommand = Nothing
        CancelAuthorChangesCommand = Nothing
        SeletedAuthorChangedCommand = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If Not IsNothing(Me._editAuthor) Then
            RemoveHandler Me._editAuthor.PropertyChanged, AddressOf editAuthor_PropertyChanged
        End If

        UnregisterCommands()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If Not IsNothing(Me._editAuthor) Then
            RemoveHandler Me._editAuthor.PropertyChanged, AddressOf editAuthor_PropertyChanged
        End If

        UnregisterCommands()

    End Sub
#End Region

#Region "Private Methods : Save & Cancel"

    Private Sub OnSaveAuthorChanges()
        If Me.EditAuthor.HasValidationErrors Then
            Return
        End If
        If Me.HasChanges Then
            _isChecked = _editAuthor.IsChecked
            If IsNewAuthorEdited Then
                Me.LibraryDataService.DomainContext.Authors.Add(EditAuthor)
            Else
                Dim author = (From a In Me.LibraryDataService.DomainContext.Authors
                              Where a.Author_id = EditAuthor.Author_id
                              Select a).FirstOrDefault
                author.Author_First_Name = EditAuthor.Author_First_Name
                author.Author_Last_Name = EditAuthor.Author_Last_Name
                author.Author_Career = EditAuthor.Author_Career
                author.IsActive = EditAuthor.IsActive
            End If
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveAuthorChanges, Nothing)
        End If
    End Sub

    Private Sub AfterSaveAuthorChanges(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedAuthorDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim savedAuthor As Author = Nothing

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "Add"
                For Each e As Object In result.ChangeSet.AddedEntities
                    If TypeOf (e) Is Author Then
                        savedAuthor = CType(e, Author)
                        resultMessage &= savedAuthor.Author_Full_Name & " was added successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Author Then
                        savedAuthor = CType(e, Author)
                        resultMessage &= savedAuthor.Author_Full_Name & " was updated successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.RemovedEntities.Count > 0 Then
                action = "Delete"
                For Each e As Object In result.ChangeSet.RemovedEntities
                    If TypeOf (e) Is Author Then
                        savedAuthor = CType(e, Author)
                        resultMessage &= savedAuthor.Author_Full_Name & " was deleted successfully." & vbCrLf
                    End If
                Next
            End If
            savedAuthor.IsChecked = _isChecked
        End If

        dialogMessage = New SavedAuthorDialogMessage(dialogType, action, resultMessage, savedAuthor)
        Messenger.[Default].Send(dialogMessage)
        Dim myMessageBox = New MyMessageBox(dialogMessage.Type, dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button)
        myMessageBox.Show()

    End Sub

    Private Sub OnCancelAuthorChanges()
        RestoreAuthorBeforeEdit()
        Me.LibraryDataService.DomainContext.RejectChanges()
    End Sub

    Private Sub OnSelectedAuthorChanged(sender As DataGrid)
        If Not (TypeOf (sender) Is DataGrid) Then
            Return
        End If
        If Not IsNothing(Me.EditAuthor) Then
            RemoveHandler Me._editAuthor.PropertyChanged, AddressOf editAuthor_PropertyChanged
        End If

        EditAuthor = sender.SelectedItem

        If Not IsNothing(Me.EditAuthor) Then
            AddHandler Me._editAuthor.PropertyChanged, AddressOf editAuthor_PropertyChanged
        End If
    End Sub

    Private Sub SaveAuthorBeforeEdit()
        If Not IsNothing(EditAuthor) Then
            _authorBeforeEdit = New Author() With {
                                            .Author_id = EditAuthor.Author_id,
                                            .Author_First_Name = EditAuthor.Author_First_Name,
                                            .Author_Last_Name = EditAuthor.Author_Last_Name,
                                            .IsActive = EditAuthor.IsActive,
                                            .IsChecked = EditAuthor.IsChecked
                                            }
        Else
            _authorBeforeEdit = Nothing
        End If
    End Sub

    Private Sub RestoreAuthorBeforeEdit()
        If Not IsNothing(_authorBeforeEdit) Then
            EditAuthor.Author_First_Name = _authorBeforeEdit.Author_First_Name
            EditAuthor.Author_Last_Name = _authorBeforeEdit.Author_Last_Name
            EditAuthor.Author_Career = _authorBeforeEdit.Author_Career
            EditAuthor.IsChecked = _authorBeforeEdit.IsChecked
            EditAuthor.IsActive = _authorBeforeEdit.IsActive
        End If
    End Sub

#End Region

End Class
