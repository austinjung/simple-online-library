﻿Imports Papa.Common

Public Class CategoryWithCheckedFlag
    Inherits NotifiableObject

    Private _category As Category
    Public Property Category As Category
        Get
            Return _category
        End Get
        Set(value As Category)
            _category = value
            RaisePropertyChanged("Category_Name")
        End Set
    End Property

    Private _isChecked As Boolean
    Public Property IsChecked As Boolean
        Get
            Return _isChecked
        End Get
        Set(value As Boolean)
            _isChecked = value
            RaisePropertyChanged("IsChecked")
        End Set
    End Property

    Public ReadOnly Property Category_Name As String
        Get
            Return _category.Category_Name
        End Get
    End Property

End Class
