﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class SearchBookForManagerViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"
    'Searched Books
    Private _searched_Books As DomainCollectionView(Of Book)
    Private _booksPageSize As Integer = 6

    'Filter Books 
    Private _filter_Authors As DomainCollectionView(Of Author)
    Private _filter_Categories As DomainCollectionView(Of Category)
    Private _selected_Authors As List(Of Author)
    Private _selected_Categories As List(Of Category)
    Private _filter_Keyword As String = String.Empty
    Private _filter_Targets As ObservableCollection(Of KeywordFilterTarget) = KeywordFilterTargets.GetFilterTargets

    'Edited Book
    Private _editedBook As Book
    Private _isNewBookEdited As Boolean

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Current DataGrid variables
    Private SortingGridByChecked As Boolean = False
    Private workingDataGrid As DataGrid = Nothing

    'Commands for book properties datagrids
    Private m_SelectGridRowCommand As RelayCommand(Of DataGrid)
    Private m_SelectWorkingGridCommand As RelayCommand(Of DataGrid)
    Private m_SelectGridSortCommand As RelayCommand(Of MouseButtonEventArgs)
    Private m_GridFilterChangedCommand As RelayCommand(Of TextBox)
    Private m_SearchBookCommand As RelayCommand(Of ComboBox)
    Private _sortDescriptorChangeCommand As RelayCommand(Of ComboBox)
    Private _sortDirectorChangeCommand As RelayCommand(Of ComboBox)

    'Sort Directions/ Sorter
    Private sortDirection As Integer = ListSortDirection.Ascending
    Private sorter As String = "Sort By Title"

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "AuthorsLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("Filter_Authors")
            InitializeCheckValueOfFilterAuthors()
            Me._filter_Authors.Refresh()
        ElseIf e.PropertyName = "CategoriesLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("Filter_Categories")
            InitializeCheckValueOfFilterCategories()
            Me._filter_Categories.Refresh()
        ElseIf e.PropertyName = "UpdateAuthorsOfBook" Then
            HasChanges = True
            Me.RaisePropertyChanged("Book_Authors_List")
            Me._searched_Books.Refresh()
        ElseIf e.PropertyName = "UpdateCategoriesOfBook" Then
            HasChanges = True
            Me.RaisePropertyChanged("Book_Categories_List")
            Me._searched_Books.Refresh()
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public Property EditedBook() As Book
        Get
            Return _editedBook
        End Get
        Set(value As Book)
            _editedBook = value
        End Set
    End Property

    Public Property IsNewBookEdited As Boolean
        Get
            Return _isNewBookEdited
        End Get
        Set(value As Boolean)
            _isNewBookEdited = value
        End Set
    End Property

    Public ReadOnly Property Filter_Authors As ICollectionView
        Get
            If IsNothing(Me._filter_Authors) Then
                Me._filter_Authors = LibraryDataService.BookAuthorFilterCollectionView
            End If
            Return Me._filter_Authors
        End Get
    End Property
    Public ReadOnly Property Filter_Authors_Loader As StaticFilterAutoCompleteViewLoader(Of Author)
        Get
            Return Me.LibraryDataService.BookAuthorFilterCollectionLoader
        End Get
    End Property

    Public ReadOnly Property Filter_Categories As ICollectionView
        Get
            If Me._filter_Categories Is Nothing Then
                Me._filter_Categories = LibraryDataService.BookCategoryFilterCollectionView
            End If
            Return Me._filter_Categories
        End Get
    End Property
    Public ReadOnly Property Filter_Categories_Loader As StaticFilterAutoCompleteViewLoader(Of Category)
        Get
            Return Me.LibraryDataService.BookCategoryFilterCollectionLoader
        End Get
    End Property

    Public Property Filter_Keyword As String
        Get
            Return Me._filter_Keyword
        End Get
        Set(value As String)
            Me._filter_Keyword = value
        End Set
    End Property

    Public ReadOnly Property Filter_Targets As ObservableCollection(Of KeywordFilterTarget)
        Get
            Return Me._filter_Targets
        End Get
    End Property

    Public ReadOnly Property Searched_Books As ICollectionView
        Get
            If Not IsNothing(Me._searched_Books) Then
                Me._searched_Books = LibraryDataService.BookCollectionView
            End If
            Return Me._searched_Books
        End Get
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService

        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context, we need to update Authors & Categories Collection data 
        Me.LibraryDataService.RefreshBookFilterCollectionViews()
        Me.LibraryDataService.FilterBookByAuthors = Nothing
        Me.LibraryDataService.FilterBookByCategories = Nothing
        Me.LibraryDataService.FilterBookDescription = Nothing
        Me.LibraryDataService.FilterBookISBN = Nothing
        Me.LibraryDataService.FilterBookLanguage = Nothing
        Me.LibraryDataService.FilterBookTitle = Nothing
        Me.LibraryDataService.FilterBookMedia = Nothing
        Me.LibraryDataService.FilterActiveBook = True
        ''''''''''''''''''''''''''''
        Me._searched_Books = libraryDataService.BookCollectionView

        'Set Property Changed Event Handler
        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        Me.LibraryDataService.LoadAuthors()
        Me.LibraryDataService.LoadCategories()
        Me.LibraryDataService.LoadBooks()
        Me.LibraryDataService.LoadShelves()

        Using (Me._searched_Books.DeferRefresh())
            Me._searched_Books.PageSize = _booksPageSize
            Me._searched_Books.SetTotalItemCount(-1)
            Me._searched_Books.MoveToFirstPage()
        End Using

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SelectGridRowCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_SelectGridRowCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectGridRowCommand = value
        End Set
    End Property

    Public Property SelectWorkingGridCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_SelectWorkingGridCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectWorkingGridCommand = value
        End Set
    End Property

    Public Property SelectGridSortCommand() As RelayCommand(Of MouseButtonEventArgs)
        Get
            Return m_SelectGridSortCommand
        End Get
        Set(value As RelayCommand(Of MouseButtonEventArgs))
            m_SelectGridSortCommand = value
        End Set
    End Property

    Public Property GridFilterChangedCommand() As RelayCommand(Of TextBox)
        Get
            Return m_GridFilterChangedCommand
        End Get
        Set(value As RelayCommand(Of TextBox))
            m_GridFilterChangedCommand = value
        End Set
    End Property

    Public Property SearchBookCommand() As RelayCommand(Of ComboBox)
        Get
            Return m_SearchBookCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            m_SearchBookCommand = value
        End Set
    End Property

    Public Property SortDescriptorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDescriptorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDescriptorChangeCommand = value
        End Set
    End Property

    Public Property SortDirectorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDirectorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDirectorChangeCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands & Messages"

    Protected Overrides Sub RegisterCommands()
        SelectGridRowCommand = New RelayCommand(Of DataGrid)(AddressOf OnSelectGridRow,
                                                            Function(sender) Not IsNothing(sender))
        SelectWorkingGridCommand = New RelayCommand(Of DataGrid)(AddressOf OnSelectWorkingGrid,
                                                            Function(sender) Not IsNothing(sender))
        SelectGridSortCommand = New RelayCommand(Of MouseButtonEventArgs)(AddressOf OnSelectGridSort,
                                                                           Function(e) Not IsNothing(e))
        GridFilterChangedCommand = New RelayCommand(Of TextBox)(AddressOf OnGridFilterChanged)
        SearchBookCommand = New RelayCommand(Of ComboBox)(AddressOf OnSearchBook)
        SortDescriptorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDescriptorChange,
                                              Function() CType(Searched_Books, DomainCollectionView).ItemCount > 0)
        SortDirectorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDirectorChange,
                                              Function() CType(Searched_Books, DomainCollectionView).ItemCount > 0)
    End Sub

    Protected Overrides Sub RegisterMessages()

        Messenger.[Default].Register(Of SavedBookDialogMessage)(Me, AddressOf OnSavedBookDialogMessageReceived)
        Messenger.[Default].Register(Of SavedAuthorDialogMessage)(Me, AddressOf OnSavedAuthorDialogMessageReceived)
        Messenger.[Default].Register(Of SavedCategoryDialogMessage)(Me, AddressOf OnSavedCategoryDialogMessageReceived)

    End Sub

    Private Sub OnSavedBookDialogMessageReceived(msg As SavedBookDialogMessage)

        If msg.Content = "Add" Then
            If Not IsNothing(Me._searched_Books) And Not IsNothing(Me.m_LibraryDataService) Then
                Dim currentPage = Me._searched_Books.PageIndex
                Using (Me._searched_Books.DeferRefresh())
                    Me._searched_Books.SetTotalItemCount(-1)
                    Me._searched_Books.MoveToPage(currentPage)
                End Using
            End If
        ElseIf msg.Content = "Update" Or msg.Content = "Delete" Then
            If Not IsNothing(Me._searched_Books) And Not IsNothing(Me.m_LibraryDataService) Then
                Dim currentPosition = Me._searched_Books.CurrentPosition
                Dim currentPage = Me._searched_Books.PageIndex
                Dim pageTotal = CType(Me._searched_Books.SourceCollection, EntityList(Of Book)).Count - 1 'total items in current page
                If Me.LibraryDataService.FilterActiveBook And msg.SavedBook.IsActive = False Then
                    Using (Me._searched_Books.DeferRefresh())
                        Me._searched_Books.SetTotalItemCount(-1)
                        If currentPosition = 0 And pageTotal = currentPosition And currentPage > 0 Then
                            Me._searched_Books.MoveToPreviousPage()
                        Else
                            Me._searched_Books.MoveToPage(currentPage)
                        End If
                    End Using
                Else
                    Me._searched_Books.Refresh()
                End If
            End If
        End If

    End Sub

    Private Sub OnSavedAuthorDialogMessageReceived(msg As SavedAuthorDialogMessage)

        If msg.Content = "Add" Then
            If Not IsNothing(Me._filter_Authors) And Not IsNothing(Me.m_LibraryDataService) Then
                CType(Me.Filter_Authors_Loader, StaticFilterAutoCompleteViewLoader(Of Author)).AddAuthor(msg.SavedAuthor)
            End If
        ElseIf msg.Content = "Update" Or msg.Content = "Delete" Then
            If Not IsNothing(Me._searched_Books) And Not IsNothing(Me.m_LibraryDataService) Then
                Me._searched_Books.Refresh()
            End If
        End If

    End Sub

    Private Sub OnSavedCategoryDialogMessageReceived(msg As SavedCategoryDialogMessage)

        If msg.Content = "Add" Then
            If Not IsNothing(Me._filter_Categories) And Not IsNothing(Me.m_LibraryDataService) Then
                CType(Me.Filter_Categories_Loader, StaticFilterAutoCompleteViewLoader(Of Category)).AddCategory(msg.SavedCategory)
            End If
        ElseIf msg.Content = "Update" Or msg.Content = "Delete" Then
            If Not IsNothing(Me._searched_Books) And Not IsNothing(Me.m_LibraryDataService) Then
                Me._searched_Books.Refresh()
            End If
        End If

    End Sub

    Protected Sub UnregisterMessages()

        Messenger.[Default].Unregister(Of SavedBookDialogMessage)(Me, AddressOf OnSavedBookDialogMessageReceived)
        Messenger.[Default].Unregister(Of SavedAuthorDialogMessage)(Me, AddressOf OnSavedAuthorDialogMessageReceived)
        Messenger.[Default].Unregister(Of SavedCategoryDialogMessage)(Me, AddressOf OnSavedCategoryDialogMessageReceived)

    End Sub

    Protected Sub UnregisterCommands()
        SelectGridRowCommand = Nothing
        SelectWorkingGridCommand = Nothing
        SelectGridSortCommand = Nothing
        GridFilterChangedCommand = Nothing
        SearchBookCommand = Nothing
        SortDescriptorChangeCommand = Nothing
        SortDirectorChangeCommand = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
            Me.LibraryDataService.RefreshBookPropertiesCollectionViews()
            Me.LibraryDataService.FilterBookByAuthors = Nothing
            Me.LibraryDataService.FilterBookByCategories = Nothing
            Me.LibraryDataService.FilterBookDescription = Nothing
            Me.LibraryDataService.FilterBookISBN = Nothing
            Me.LibraryDataService.FilterBookLanguage = Nothing
            Me.LibraryDataService.FilterBookTitle = Nothing
            Me.LibraryDataService.FilterBookMedia = Nothing
            Me.LibraryDataService.FilterActiveBook = True
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub
#End Region

#Region "Private Methods : Initialize Check Value for Books Link Grid"

    Private Sub InitializeCheckValueOfFilterAuthors()
        If Not Me.LibraryDataService.BookAuthorFilterCollectionLoader Is Nothing Then
            _selected_Authors = New List(Of Author)
            _selected_Authors.Add(New Author With {.Author_First_Name = "- Select All"})
            Me.LibraryDataService.BookAuthorFilterCollectionLoader.SelectedList = _selected_Authors
        End If
    End Sub

    Private Sub InitializeCheckValueOfFilterCategories()
        If Not Me.LibraryDataService.BookCategoryFilterCollectionLoader Is Nothing Then
            _selected_Categories = New List(Of Category)
            _selected_Categories.Add(New Category With {.Category_Name = "- Select All"})
            Me.LibraryDataService.BookCategoryFilterCollectionLoader.SelectedList = _selected_Categories
        End If
    End Sub

#End Region

#Region "Private Methods : Handling Event of DataGrid & Filter TextBox"

    'When Datagrid selection mode is "single"
    Private Sub OnSelectGridRow(sender As DataGrid)

        If Not (TypeOf (sender) Is DataGrid) Then
            Return
        End If
        If TypeOf (sender.SelectedItem) Is Author Then
            OnSelectAuthor(sender)
        ElseIf TypeOf (sender.SelectedItem) Is Category Then
            OnSelectCategory(sender)
        Else
            Return
        End If

    End Sub

    Private Sub OnSelectAuthor(dataGrid As DataGrid)

        Dim selectedAuthor As Author = dataGrid.SelectedItem

        If (Not IsNothing(selectedAuthor)) And (Not SortingGridByChecked) Then

            Dim selectedAuthors As List(Of Author) = New List(Of Author)
            If selectedAuthor.IsChecked Then
                selectedAuthor.IsChecked = False
            Else
                selectedAuthor.IsChecked = True
            End If

            Dim orderedSelectedAuthors = From a In dataGrid.ItemsSource
                                         Order By a.Author_Full_Name
                                         Select a

            If selectedAuthor.Author_First_Name = "- Select All" Then
                If selectedAuthor.IsChecked Then
                    For Each c In orderedSelectedAuthors
                        selectedAuthors.Add(c)
                    Next
                Else
                    For Each c In orderedSelectedAuthors
                        c.IsChecked = False
                    Next
                End If
            Else
                For Each a In orderedSelectedAuthors
                    If a.IsChecked Then
                        selectedAuthors.Add(a)
                    End If
                Next
            End If

            _selected_Authors = selectedAuthors

            'Sort by selected
            SortingGridByChecked = True
            Dim collectionView = CType(dataGrid.ItemsSource, DomainCollectionView)
            If collectionView.SortDescriptions.Count > 0 Then
                If collectionView.SortDescriptions(0).PropertyName = "IsChecked" Then
                    collectionView.SortDescriptions.Clear()
                    dataGrid.ItemsSource = orderedSelectedAuthors
                End If
            End If
            Dim selectedIndex = dataGrid.SelectedIndex
            If Not IsNothing(Me.Filter_Authors_Loader) Then
                Me.Filter_Authors_Loader.SelectedList = selectedAuthors
            End If
            SortingGridByChecked = True
            dataGrid.SelectedIndex = selectedIndex
            dataGrid.ScrollIntoView(dataGrid.ItemsSource(selectedIndex), dataGrid.Columns.First())

        End If

        'change selected index to get mouse click event without selection change
        dataGrid.SelectedIndex = -1
        SortingGridByChecked = False

    End Sub

    Private Sub OnSelectCategory(dataGrid As DataGrid)

        Dim selectedCategory As Category = dataGrid.SelectedItem

        If (Not IsNothing(selectedCategory)) And (Not SortingGridByChecked) Then

            Dim selectedCategories As List(Of Category) = New List(Of Category)
            If selectedCategory.IsChecked Then
                selectedCategory.IsChecked = False
            Else
                selectedCategory.IsChecked = True
            End If

            Dim orderedSelectedCategories = From c In dataGrid.ItemsSource
                                            Order By c.Category_Name
                                            Select c

            If selectedCategory.Category_Name = "- Select All" Then
                If selectedCategory.IsChecked Then
                    For Each c In orderedSelectedCategories
                        selectedCategories.Add(c)
                    Next
                Else
                    For Each c In orderedSelectedCategories
                        c.IsChecked = False
                    Next
                End If
            Else
                For Each c In orderedSelectedCategories
                    If c.IsChecked Then
                        selectedCategories.Add(c)
                    End If
                Next
            End If

            _selected_Categories = selectedCategories

            'Sort by selected
            SortingGridByChecked = True
            Dim collectionView = CType(dataGrid.ItemsSource, DomainCollectionView)
            If collectionView.SortDescriptions.Count > 0 Then
                If collectionView.SortDescriptions(0).PropertyName = "IsChecked" Then
                    collectionView.SortDescriptions.Clear()
                    dataGrid.ItemsSource = orderedSelectedCategories
                End If
            End If
            Dim selectedIndex = dataGrid.SelectedIndex
            If Not IsNothing(Me.Filter_Categories_Loader) Then
                Me.Filter_Categories_Loader.SelectedList = selectedCategories
            End If
            SortingGridByChecked = True
            dataGrid.SelectedIndex = selectedIndex
            dataGrid.ScrollIntoView(dataGrid.ItemsSource(selectedIndex), dataGrid.Columns.First())

        End If

        'change selected index to get mouse click event without selection change
        dataGrid.SelectedIndex = -1
        SortingGridByChecked = False

    End Sub

    Private Sub OnSelectWorkingGrid(sender As DataGrid)

        If Not (TypeOf (sender) Is DataGrid) Then
            Return
        End If
        If sender.Name = "FilterAuthorDataGrid" Then
            workingDataGrid = sender
        ElseIf sender.Name = "FilterCategoryDataGrid" Then
            workingDataGrid = sender
        Else
            Return
        End If

    End Sub

    Private Sub OnSelectGridSort(e As MouseButtonEventArgs)

        If Not (TypeOf (e) Is MouseButtonEventArgs) Then
            Return
        End If
        If IsNothing(workingDataGrid) Then
            Return
        End If

        'Dim header As DataGridColumnHeader = VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(Me), Me).OfType(Of DataGridColumnHeader)().SingleOrDefault()
        Dim fe As FrameworkElement = DirectCast(e.OriginalSource, FrameworkElement)
        Dim column As DataGridColumn = DirectCast(DataGridColumn.GetColumnContainingElement(fe), DataGridColumn)

        Dim collectionView = CType(workingDataGrid.ItemsSource, DomainCollectionView)

        If Not (TypeOf (column) Is DataGridTextColumn) Then
            e.Handled = True
            collectionView.SortDescriptions.Clear()
            collectionView.Refresh()
        ElseIf column.Header <> "Authors" And column.Header <> "Categories" Then
            e.Handled = True
            collectionView.SortDescriptions.Clear()
            collectionView.Refresh()
        End If

    End Sub

    Private Sub OnGridFilterChanged(filter As TextBox)

        If filter.Name = "Book_Authors_FilterTextBox" Then
            Me.Filter_Authors_Loader.Filter = filter.Text
            Me.Filter_Authors.Refresh()
        ElseIf filter.Name = "Book_Categories_FilterTextBox" Then
            Me.Filter_Categories_Loader.Filter = filter.Text
            Me.Filter_Categories.Refresh()
        End If

    End Sub

    Private Sub OnSearchBook(searchTargetsCombo As ComboBox)

        If Me._selected_Authors.Count < 1 And Me._selected_Categories.Count < 1 Then
            ErrorWindow.CreateNew("Please select authors and categories to search.")
            Return
        End If
        If Me._selected_Authors.Count < 1 Then
            ErrorWindow.CreateNew("Please select authors to search.")
            Return
        End If
        If Me._selected_Categories.Count < 1 Then
            ErrorWindow.CreateNew("Please select categories to search.")
            Return
        End If
        If Me._selected_Authors.Count = 1 And Me._selected_Authors(0).Author_First_Name = "- Select All" Then
            Me.LibraryDataService.FilterBookByAuthors = Nothing
        Else
            Me.LibraryDataService.FilterBookByAuthors = Me._selected_Authors
        End If
        If Me._selected_Categories.Count = 1 And Me._selected_Categories(0).Category_Name = "- Select All" Then
            Me.LibraryDataService.FilterBookByCategories = Nothing
        Else
            Me.LibraryDataService.FilterBookByCategories = Me._selected_Categories
        End If

        'For Each target In Me.Filter_Targets
        For Each target As KeywordFilterTarget In searchTargetsCombo.Items
            If target.Target.Contains("Media") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterBookMedia = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterBookMedia = Nothing
                End If
            ElseIf target.Target.Contains("Title") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterBookTitle = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterBookTitle = Nothing
                End If
            ElseIf target.Target.Contains("Description") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterBookDescription = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterBookDescription = Nothing
                End If
            ElseIf target.Target.Contains("ISBN") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterBookISBN = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterBookISBN = Nothing
                End If
            ElseIf target.Target.Contains("Language") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterBookLanguage = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterBookLanguage = Nothing
                End If
            End If
        Next

        Using (Me._searched_Books.DeferRefresh())
            Me._searched_Books.SetTotalItemCount(-1)
            Me._searched_Books.MoveToFirstPage()
        End Using

    End Sub

    Private Sub OnSortDirectorChange(sortDirector As ComboBox)

        If sortDirector.SelectedItem.Content.contains("Asc") Then
            sortDirection = ListSortDirection.Ascending
        Else
            sortDirection = ListSortDirection.Descending
        End If

        SortBooks()

    End Sub

    Private Sub OnSortDescriptorChange(sortDescriptor As ComboBox)

        sorter = sortDescriptor.SelectedItem.Content
        SortBooks()

    End Sub

    Private Sub SortBooks()
        If sorter.Contains("Title") Then
            Searched_Books.SortDescriptions.Clear()
            Searched_Books.SortDescriptions.Add(New SortDescription("Book_Title", sortDirection))
        ElseIf sorter.Contains("ISBN") Then
            Searched_Books.SortDescriptions.Clear()
            Searched_Books.SortDescriptions.Add(New SortDescription("Book_ISBN", sortDirection))
        ElseIf sorter.Contains("Published") Then
            Searched_Books.SortDescriptions.Clear()
            Searched_Books.SortDescriptions.Add(New SortDescription("Book_Published", sortDirection))
        ElseIf sorter.Contains("Media") Then
            Searched_Books.SortDescriptions.Clear()
            Searched_Books.SortDescriptions.Add(New SortDescription("Book_Media", sortDirection))
        ElseIf sorter.Contains("Language") Then
            Searched_Books.SortDescriptions.Clear()
            Searched_Books.SortDescriptions.Add(New SortDescription("Book_Language", sortDirection))
        End If
        Searched_Books.Refresh()
    End Sub

#End Region

End Class
