﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class UserDashboardViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    Private ReadOnly _categories As DomainCollectionView(Of Category)
    Private ReadOnly _authors As DomainCollectionView(Of Author)

    'user requests
    Private _user_Pending_Requests As DomainCollectionView(Of Member_Requests) = Nothing
    'user transactions
    Private _user_Expected_Returns As DomainCollectionView(Of Member_Transactions) = Nothing
    Private _user_ReturnedTransactions As DomainCollectionView(Of Member_Transactions) = Nothing

    Private _user_Transactions_View As DomainCollectionView(Of Member_Transactions)
    Private _user_Transactions_Loader As StaticViewLoader(Of Member_Transactions)
    Private _user_Transactions_Source As EntityList(Of Member_Transactions)
    'user reviews
    Private _user_Reviews As DomainCollectionView(Of Member_Reviews) = Nothing
    Private _unreviewdReturnedBookInPastTwoMonths = 0

    'popular requests & reviews
    Private _top_Requests As DomainCollectionView(Of Books_By_Requests) = Nothing
    Private _top_Reviews As DomainCollectionView(Of Books_By_Reviews) = Nothing

    'selected book ID
    Private _selectedBook_ID As Integer

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Commands
    Private m_SelectPendingRequestCommand As RelayCommand(Of DataGrid)
    Private m_SelectTransactionCommand As RelayCommand(Of DataGrid)
#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)

        If e.PropertyName = "TopRequestsLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("TopRequestsLoaded")
        ElseIf e.PropertyName = "TopReviewsLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("TopReviewsLoaded")
        ElseIf e.PropertyName = "RequestsLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("User_Pending_Requests")
        ElseIf e.PropertyName = "TransactionsLoaded" Then
            Me._user_Transactions_Source = New EntityList(Of Member_Transactions)(Me.m_LibraryDataService.DomainContext.Member_Transactions)
            Me._user_Transactions_Loader = New StaticViewLoader(Of Member_Transactions)(
                                                Me._user_Transactions_Source,
                                                Me._user_Expected_Returns.SourceCollection.AsQueryable
                                           )
            Me._user_Transactions_View = New DomainCollectionView(Of Member_Transactions)(Me._user_Transactions_Loader, Me._user_Transactions_Source)
            Dim totalTransactionsCount As Integer = Me._user_Transactions_Loader.Source.Count
            'copy returned transaction
            For Each t In Me._user_ReturnedTransactions.SourceCollection
                Me._user_Transactions_Loader.AddEntityTransaction(t)
                totalTransactionsCount += 1
            Next
            Dim pendingRequestsCount As Integer = 0
            For Each t In Me._user_Pending_Requests.SourceCollection
                pendingRequestsCount += 1
            Next
            Dim extra As Integer = pendingRequestsCount + totalTransactionsCount - 6
            If extra > 0 Then
                Dim lastIndex As Integer = Me._user_Transactions_Loader.Source.Count - 1
                For i As Integer = 1 To extra Step 1
                    Me._user_Transactions_Loader.RemoveEntityTransactionAt(lastIndex)
                    lastIndex -= 1
                Next
            End If
            'Me._user_Transactions_Source.Source = Me._user_Transactions_Loader.Source
            Me._user_Transactions_View.CollectionViewLoader.Load(Nothing)
            HasChanges = True
            Me.RaisePropertyChanged("User_Transactions")
        End If

    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property User_Pending_Requests As ICollectionView
        Get
            Return _user_Pending_Requests
        End Get
    End Property

    Public ReadOnly Property User_Expected_Returns As ICollectionView
        Get
            Return _user_Expected_Returns
        End Get
    End Property

    Public ReadOnly Property User_Transactions As ICollectionView
        Get
            If IsNothing(Me._user_Transactions_View) Then
                Me._user_Transactions_Source = New EntityList(Of Member_Transactions)(Me.m_LibraryDataService.DomainContext.Member_Transactions)
                Me._user_Transactions_Loader = New StaticViewLoader(Of Member_Transactions)(
                                                    Me._user_Transactions_Source,
                                                    Me._user_Expected_Returns.SourceCollection.AsQueryable
                                          )
                Me._user_Transactions_View = New DomainCollectionView(Of Member_Transactions)(Me._user_Transactions_Loader, Me._user_Transactions_Source)
            End If
            Return _user_Transactions_View
        End Get
    End Property

    Public Property User_Transaction_Take_Size As Integer
        Get
            Return LibraryDataService.SelectTopTransactions
        End Get
        Set(value As Integer)
            LibraryDataService.SelectTopTransactions = value
            Me.LibraryDataService.LoadCurrentMemberTransactions()
        End Set
    End Property

    Public ReadOnly Property User_Recent_Reviews As ICollectionView
        Get
            Return _user_Reviews
        End Get
    End Property

    Public ReadOnly Property Top_Requests As ICollectionView
        Get
            Return _top_Requests
        End Get
    End Property

    Public ReadOnly Property Top_Reviews As ICollectionView
        Get
            Return _top_Reviews
        End Get
    End Property

    Public ReadOnly Property UnreviewdReturnedBookInPastTwoMonths As Integer
        Get
            Return _unreviewdReturnedBookInPastTwoMonths
        End Get
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context to manage compact : every page will cause a new 
        Me.LibraryDataService.RefreshLibraryDataService()
        Me.LibraryDataService.SelectTopTransactions = 6
        ''''''''''''''''''''''''''''

        Me._user_Pending_Requests = libraryDataService.RequestCollectionView
        Me._user_Expected_Returns = libraryDataService.ExpectedCurrentMemberReturnCollectionView
        Me._user_ReturnedTransactions = libraryDataService.TransactionCollectionView
        Me._user_Reviews = libraryDataService.ReviewCollectionView
        Me._top_Requests = libraryDataService.TopRequestsCollectionView
        Me._top_Reviews = libraryDataService.TopReviewsCollectionView

        Me._categories = libraryDataService.CategoryCollectionView
        Me._authors = libraryDataService.AuthorCollectionView

        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        Me._top_Requests.CollectionViewLoader.Load(Nothing)
        Me._top_Reviews.CollectionViewLoader.Load(Nothing)

        Me.LibraryDataService.LoadCurrentMemberPendingRequests()

        Me.LibraryDataService.DomainContext.Count_Unreviewed_Transactions_Of_Member(WebContext.Current.User.MemberID, _
                                                                                    AddressOf CountUnreviewedTransactionsOfMemberCallBack, Nothing)

        Me.LibraryDataService.LoadCategories()
        Me.LibraryDataService.LoadAuthors()

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SelectPendingRequestCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_SelectPendingRequestCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectPendingRequestCommand = value
        End Set
    End Property

    Public Property SelectTransactionCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_SelectTransactionCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectTransactionCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SelectPendingRequestCommand = New RelayCommand(Of DataGrid)(AddressOf SelectPendingRequest)
        SelectTransactionCommand = New RelayCommand(Of DataGrid)(AddressOf SelectTransaction,
                                                            Function(sender) Not IsNothing(sender))
    End Sub

    Protected Sub UnregisterCommands()
        SelectPendingRequestCommand = Nothing
        SelectTransactionCommand = Nothing
    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If
        'Me.LibraryDataService.RefreshBookPropertiesCollectionViews()
        UnregisterCommands()
        UnregisterMessages()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub
#End Region

#Region "Register Message & Handle"

    Protected Overrides Sub RegisterMessages()
        Messenger.Reset()
        Messenger.[Default].Register(Of SavedRequestBookDialogMessage)(Me, AddressOf OnSavedRequestBookDialogMessageReceived)
        Messenger.[Default].Register(Of ExtendReturnBookResultMessage)(Me, AddressOf OnExtendReturnBookResultMessageReceived)
        'Messenger.[Default].Register(Of ReadCommentDialogMessage)(Me, AddressOf OnReadCommentDialogMessageReceived)
    End Sub

    Protected Sub UnregisterMessages()
        Messenger.[Default].Unregister(Of SavedRequestBookDialogMessage)(Me, AddressOf OnSavedRequestBookDialogMessageReceived)
        Messenger.[Default].Unregister(Of ExtendReturnBookResultMessage)(Me, AddressOf OnExtendReturnBookResultMessageReceived)
        'Messenger.[Default].Unregister(Of ReadCommentDialogMessage)(Me, AddressOf OnReadCommentDialogMessageReceived)
    End Sub

    'Private Sub OnReadCommentDialogMessageReceived(msg As ReadCommentDialogMessage)
    '    If msg.DialogResult.Equals("OpenAddComment") Then
    '       Dim editBookRequestWindow = New RequestBookChildWindow()
    '       editBookRequestWindow.Title = "Modify my request"
    '       editBookRequestWindow.Show()
    '    End If
    'End Sub

    Private Sub OnExtendReturnBookResultMessageReceived(msg As ExtendReturnBookResultMessage)
        Dim myMessageBox = New MyMessageBox(msg.Type, msg.Content, msg.Caption, msg.Button)
        myMessageBox.Show()
        Me.LibraryDataService.ReloadCurrentMemberTransactions(CType(Me.User_Pending_Requests.SourceCollection, EntityList(Of Member_Requests)).Count)
        HasChanges = True
        Me.RaisePropertyChanged("Extend_User_Transactions")
    End Sub

    Private Sub OnSavedRequestBookDialogMessageReceived(msg As SavedRequestBookDialogMessage)
        If Not msg.Content.Contains("Update Comment View Flag") Then
            Dim myMessageBox = New MyMessageBox(msg.Type, msg.Content, msg.Caption, msg.Button)
            myMessageBox.Show()
        End If
        HasChanges = True
        Me.RaisePropertyChanged("User_Pending_Requests")
        Me.User_Pending_Requests.Refresh()
    End Sub

#End Region

#Region "Private Methods : Handling Commands"

    Private Sub SelectPendingRequest(dataGrid As DataGrid)

        Dim selectedRequest As Member_Requests = dataGrid.SelectedItem

    End Sub

    Private Sub SelectTransaction(dataGrid As DataGrid)

        Dim selectedTransaction As Member_Transactions = dataGrid.SelectedItem

    End Sub

#End Region

#Region "Public Methods : Load Data"

    Public Sub LoadCurrentMemberTransactions()
        Me.LibraryDataService.LoadCurrentMemberTransactions()
    End Sub

    Private Sub CountUnreviewedTransactionsOfMemberCallBack(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of Integer))
        If result.HasError Then
            _unreviewdReturnedBookInPastTwoMonths = 0
        Else
            _unreviewdReturnedBookInPastTwoMonths = result.Value
        End If
    End Sub

#End Region

End Class
