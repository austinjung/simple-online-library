﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class EditCategoryViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'Book Properties to edit
    Private _editCategory As Category = Nothing
    Private _categoryBeforeEdit As Category = Nothing

    Private _categories As List(Of Category) = Nothing
    Private _categoriesVisibility As Visibility = Visibility.Collapsed

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False
    Private _isNewCategoryEdited As Boolean = True
    Private _isChecked As Boolean = False
    Private _isCheckedVisibility As Visibility = Visibility.Collapsed

    'Commands for save & cancel
    Private m_SaveCategoryChangesCommand As RelayCommand
    Private m_CancelCategoryChangesCommand As RelayCommand
    Private m_SelectedCategoryChangedCommand As RelayCommand(Of DataGrid)

#End Region

#Region "Private Method : Property changed event"

    Private Sub editCategory_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        Select Case e.PropertyName
            Case "Category_Name"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveCategoryChangesCommand.RaiseCanExecuteChanged()
                End If
            Case "IsActive"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveCategoryChangesCommand.RaiseCanExecuteChanged()
                End If
        End Select
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"


    Public Property EditCategory() As Category
        Get
            Return _editCategory
        End Get
        Set(value As Category)
            _editCategory = value
            If IsNothing(value) Then
                IsNewCategoryEdited = False
            ElseIf _editCategory.Category_id > 0 Then
                IsNewCategoryEdited = False
            Else
                IsNewCategoryEdited = True
            End If
            SaveCategoryBeforeEdit()
            Me.RaisePropertyChanged("EditCategory")
        End Set
    End Property

    Public Property IsNewCategoryEdited As Boolean
        Get
            Return _isNewCategoryEdited
        End Get
        Set(value As Boolean)
            _isNewCategoryEdited = value
            If value Then
                CategoriesVisibility = Visibility.Collapsed
            Else
                CategoriesVisibility = Visibility.Visible
            End If
        End Set
    End Property

    Public Property Categories As List(Of Category)
        Get
            Return _categories
        End Get
        Set(value As List(Of Category))
            _categories = value
        End Set
    End Property

    Public Property CategoriesVisibility As Visibility
        Get
            Return _categoriesVisibility
        End Get
        Set(value As Visibility)
            _categoriesVisibility = value
            If value = Visibility.Visible Then
                IsCheckedVisibility = Visibility.Collapsed
            Else
                IsCheckedVisibility = Visibility.Visible
            End If
            Me.RaisePropertyChanged("CategoriesVisibility")
        End Set
    End Property

    Public Property IsCheckedVisibility As Visibility
        Get
            Return _isCheckedVisibility
        End Get
        Set(value As Visibility)
            _isCheckedVisibility = value
            Me.RaisePropertyChanged("IsCheckedVisibility")
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   categories As List(Of Category),
                   editCategory As Category)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        Me.Categories = categories
        Me.EditCategory = editCategory

        ''''''''''''''''''''''''''''
        'Don't refresh Domain Data Context, 
        'Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''

        'Set Property Changed Event Handler

        If Not IsNothing(Me.EditCategory) Then
            AddHandler Me._editCategory.PropertyChanged, AddressOf editCategory_PropertyChanged
        End If

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SaveCategoryChangesCommand() As RelayCommand
        Get
            Return m_SaveCategoryChangesCommand
        End Get
        Set(value As RelayCommand)
            m_SaveCategoryChangesCommand = value
        End Set
    End Property

    Public Property CancelCategoryChangesCommand() As RelayCommand
        Get
            Return m_CancelCategoryChangesCommand
        End Get
        Set(value As RelayCommand)
            m_CancelCategoryChangesCommand = value
        End Set
    End Property

    Public Property SeletedCategoryChangedCommand As RelayCommand(Of DataGrid)
        Get
            Return m_SelectedCategoryChangedCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectedCategoryChangedCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SaveCategoryChangesCommand = New RelayCommand(AddressOf OnSaveCategoryChanges, Function() HasChanges)
        CancelCategoryChangesCommand = New RelayCommand(AddressOf OnCancelCategoryChanges)
        SeletedCategoryChangedCommand = New RelayCommand(Of DataGrid)(AddressOf OnSelectedCategoryChanged)
    End Sub

    Protected Sub UnregisterCommands()
        SaveCategoryChangesCommand = Nothing
        CancelCategoryChangesCommand = Nothing
        SeletedCategoryChangedCommand = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If Not IsNothing(Me._editCategory) Then
            RemoveHandler Me._editCategory.PropertyChanged, AddressOf editCategory_PropertyChanged
        End If

        UnregisterCommands()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If Not IsNothing(Me._editCategory) Then
            RemoveHandler Me._editCategory.PropertyChanged, AddressOf editCategory_PropertyChanged
        End If

        UnregisterCommands()

    End Sub
#End Region

#Region "Private Methods : Save & Cancel"

    Private Sub OnSaveCategoryChanges()
        If Me.EditCategory.HasValidationErrors Then
            Return
        End If
        If Me.HasChanges Then
            _isChecked = _editCategory.IsChecked
            If IsNewCategoryEdited Then
                Me.LibraryDataService.DomainContext.Categories.Add(EditCategory)
            Else
                Dim category = (From c In Me.LibraryDataService.DomainContext.Categories
                                Where c.Category_id = EditCategory.Category_id
                                Select c).FirstOrDefault
                category.Category_Name = EditCategory.Category_Name
                category.IsActive = EditCategory.IsActive
            End If
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveCategoryChanges, Nothing)
        End If
    End Sub

    Private Sub AfterSaveCategoryChanges(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedCategoryDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim savedCategory As Category = Nothing

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "Add"
                For Each e As Object In result.ChangeSet.AddedEntities
                    If TypeOf (e) Is Category Then
                        savedCategory = CType(e, Category)
                        resultMessage &= savedCategory.Category_Name & " was added successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Category Then
                        savedCategory = CType(e, Category)
                        resultMessage &= savedCategory.Category_Name & " was updated successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.RemovedEntities.Count > 0 Then
                action = "Delete"
                For Each e As Object In result.ChangeSet.RemovedEntities
                    If TypeOf (e) Is Category Then
                        savedCategory = CType(e, Category)
                        resultMessage &= savedCategory.Category_Name & " was deleted successfully." & vbCrLf
                    End If
                Next
            End If
            savedCategory.IsChecked = _isChecked
        End If

        dialogMessage = New SavedCategoryDialogMessage(dialogType, action, resultMessage, savedCategory)
        Messenger.[Default].Send(dialogMessage)
        Dim myMessageBox = New MyMessageBox(dialogMessage.Type, dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button)
        myMessageBox.Show()

    End Sub

    Private Sub OnCancelCategoryChanges()
        RestoreCategoryBeforeEdit()
        Me.LibraryDataService.DomainContext.RejectChanges()
    End Sub

    Private Sub OnSelectedCategoryChanged(sender As DataGrid)
        If Not (TypeOf (sender) Is DataGrid) Then
            Return
        End If
        If Not IsNothing(Me.EditCategory) Then
            RemoveHandler Me._editCategory.PropertyChanged, AddressOf editCategory_PropertyChanged
        End If
        EditCategory = sender.SelectedItem
        If Not IsNothing(Me.EditCategory) Then
            AddHandler Me._editCategory.PropertyChanged, AddressOf editCategory_PropertyChanged
        End If
    End Sub

    Private Sub SaveCategoryBeforeEdit()
        If Not IsNothing(EditCategory) Then
            _categoryBeforeEdit = New Category() With {
                                            .Category_Name = EditCategory.Category_Name,
                                            .IsActive = EditCategory.IsActive,
                                            .IsChecked = EditCategory.IsChecked
                                            }
        Else
            _categoryBeforeEdit = Nothing
        End If
    End Sub

    Private Sub RestoreCategoryBeforeEdit()
        If Not IsNothing(_categoryBeforeEdit) Then
            EditCategory.Category_Name = _categoryBeforeEdit.Category_Name
            EditCategory.IsChecked = _categoryBeforeEdit.IsChecked
            EditCategory.IsActive = _categoryBeforeEdit.IsActive
        End If
    End Sub

#End Region

End Class
