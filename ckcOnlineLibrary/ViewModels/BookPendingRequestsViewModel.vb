﻿Imports Papa.Common
Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client

Public Class BookPendingRequestsViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'Request Book
    Private _book_id As Integer = -1
    'Request Member : Default all member's pending requests on _book_id
    Private _member_id As Integer = -1

    'Review Collection views
    Private _book_Requests As DomainCollectionView(Of Member_Requests)
    Private _book_Requests_Source As EntityList(Of Member_Requests)
    Private _book_Requests_Loader As DomainCollectionViewLoader(Of Member_Requests)

    Private _request_Member As DomainCollectionView(Of Member)
    Private _request_Member_Source As EntityList(Of Member)
    Private _request_Member_Loader As DomainCollectionViewLoader(Of Member)

    Private _requestProperties As DomainCollectionModels = New DomainCollectionModels()
    Private requestPropertiesLoadQueue As Queue(Of EntityQuery) = New Queue(Of EntityQuery)

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Collection View Default Page Size : No Page
    Private _page_Size As Integer = 0

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            If LibraryDataService.DomainContext.Members.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Members.HasChanges
                Me.RaisePropertyChanged("RequestMember")
            ElseIf LibraryDataService.DomainContext.Books.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Books.HasChanges
                Me.RaisePropertyChanged("RequestBook")
            ElseIf LibraryDataService.DomainContext.Member_Requests.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Member_Requests.HasChanges
                Me.RaisePropertyChanged("Request")
            End If
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Book_Pending_Requests As DomainCollectionView(Of Member_Requests)
        Get
            Return Me._book_Requests
        End Get
    End Property

    Public Property Page_Size As Integer
        Get
            Return _page_Size
        End Get
        Set(value As Integer)
            _page_Size = value
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Private Sub new_Common(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService

        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        loadPendingRequestsOnBook()

        Using Me.Book_Pending_Requests.DeferRefresh()
            Me.Book_Pending_Requests.MoveToFirstPage()
        End Using

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   book_id As Integer)

        Me._book_id = book_id

        new_Common(pageConductor, libraryDataService)

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   book_id As Integer,
                   member_id As Integer)

        Me._book_id = book_id
        Me._member_id = member_id

        new_Common(pageConductor, libraryDataService)

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   book_id As Integer,
                   member_id As Integer,
                   pageSize As Integer)

        Me._book_id = book_id
        Me._member_id = member_id
        Me._page_Size = Page_Size

        new_Common(pageConductor, libraryDataService)

    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        Me._book_id = -1
        Me._book_Requests = Nothing
        Me._book_Requests_Source = Nothing
        Me._book_Requests_Loader = Nothing

        Me._request_Member = Nothing
        Me._request_Member_Source = Nothing
        Me._request_Member_Loader = Nothing

        Me._requestProperties = New DomainCollectionModels()
        Me.requestPropertiesLoadQueue = New Queue(Of EntityQuery)

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

    End Sub
#End Region

#Region "Private Methods : Load Data"

    Private Sub loadPendingRequestsOnBook()

        'Initialize Review Member Collection
        Me._request_Member_Source = New EntityList(Of Member)(Me.LibraryDataService.DomainContext.Members)
        Me._request_Member_Loader = New DomainCollectionViewLoader(Of Member)(
                                        AddressOf Me.LoadRequestMember,
                                        AddressOf Me.OnLoadRequestMemberCompleted)
        Me._request_Member = New DomainCollectionView(Of Member)(_request_Member_Loader, _request_Member_Source)
        Me._requestProperties(GetType(Member)) = New DomainCollectionModel("Members", GetType(Member),
                                       Me._request_Member, Me._request_Member_Source, Me._request_Member_Loader)

        'Initialize Book Review Collection
        Me._book_Requests_Source = New EntityList(Of Member_Requests)(Me.LibraryDataService.DomainContext.Member_Requests)
        Me._book_Requests_Loader = New DomainCollectionViewLoader(Of Member_Requests)(
                                        AddressOf Me.LoadRequestEntities,
                                        AddressOf Me.OnLoadRequestEntitiesCompleted)
        Me._book_Requests = New DomainCollectionView(Of Member_Requests)(_book_Requests_Loader, _book_Requests_Source)

        'Set Page size
        Me._book_Requests.PageSize = Me._page_Size
        'Load data
        Me._book_Requests.CollectionViewLoader.Load(Nothing)

    End Sub

    Private Function LoadRequestEntities() As LoadOperation(Of Member_Requests)
        Me.CanLoad = False
        Dim query As EntityQuery(Of Member_Requests)
        If Me._member_id > 0 Then
            'query = Me.LibraryDataService.DomainContext.Get_Current_Member_Pending_Requests_On_BookQuery(Me._book_id)
            query = Me.LibraryDataService.DomainContext.Get_Member_Pending_Requests_On_BookQuery(Me._member_id, Me._book_id)
        Else
            query = Me.LibraryDataService.DomainContext.GetAllMember_Pending_Requests_On_BookQuery(Me._book_id)
        End If
        Return Me.LibraryDataService.DomainContext.Load(query.SortAndPageBy(Me._book_Requests))
    End Function

    Private Sub OnLoadRequestEntitiesCompleted(op As LoadOperation(Of Member_Requests))
        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Me._book_Requests_Source.Source = op.Entities
            If op.TotalEntityCount <> -1 Then
                Me._book_Requests.SetTotalItemCount(op.TotalEntityCount)
            End If
            Me.RaisePropertyChanged("BookRequestsLoaded")
            If Me._member_id <= 0 Then
                For Each request As Member_Requests In CType(op.Entities, IEnumerable(Of Member_Requests))
                    Me.LoadMemberOfRequest(request.Request_id)
                Next
            End If
            End If
    End Sub

    Private Sub LoadMemberOfRequest(ByVal request_id As Integer)
        If Me._requestProperties.ContainsKey(GetType(Member)) Then
            Me._requestProperties(GetType(Member)).Query = Me.LibraryDataService.DomainContext.GetMember_Of_ReviewQuery(request_id)
            Me._requestProperties(GetType(Member)).Loader.Load(Nothing)
        End If
    End Sub

    Private Function LoadRequestMember(Of T As Entity)() As LoadOperation(Of T)

        Dim collectionModel As DomainCollectionModel = CType(Me._requestProperties(GetType(T)), DomainCollectionModel)
        Dim entityQuery As EntityQuery(Of T) = collectionModel.Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        Me.CanLoad = False
        Return Me.LibraryDataService.DomainContext.Load(entityQuery.SortAndPageBy(collectionModel.View))

    End Function

    Private Sub OnLoadRequestMemberCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Try
                Dim domainCollectionModel As DomainCollectionModel =
                        CType(Me._requestProperties(GetType(T)), DomainCollectionModel)
                CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
                If op.AllEntities.Count <> -1 Then
                    If op.TotalEntityCount <> -1 Then
                        CType(Me._requestProperties(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                    End If

                    Dim request_id = op.EntityQuery.Parameters("request_id")
                    Dim request = From r In Me._book_Requests_Source.Source
                                  Where r.Request_id = request_id
                                  Select r
                    Dim requestRef = request.FirstOrDefault
                    If Not requestRef Is Nothing Then
                        If GetType(T) Is GetType(Member) Then
                            requestRef.Member = CType(domainCollectionModel.Source, EntityList(Of Member)).Source.ElementAtOrDefault(0)
                        End If
                    End If
                End If
            Catch ex As Exception
                'do nothing
            End Try
        Else
            'If query was cancelled, do it again to get all books' properties
            requestPropertiesLoadQueue.Enqueue(op.EntityQuery)
        End If

        'If there are pending query of review properties, do it
        If requestPropertiesLoadQueue.Count > 0 Then
            Dim entityQuery As EntityQuery = requestPropertiesLoadQueue.Dequeue()
            If TypeOf (entityQuery) Is EntityQuery(Of Member) Then
                Me._requestProperties(GetType(Member)).Query = entityQuery
                Me._requestProperties(GetType(Member)).Loader.Load(Nothing)
            End If
        End If

    End Sub

#End Region

End Class
