﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class UserReviewBookViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'user transactions
    Private _user_Returned_Transactions_Detail As DomainCollectionView(Of Transactions_Detail) = Nothing

    'transaction page size
    Private _transactionsPageSize As Integer = 6

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Filter Books 
    Private _filter_Keyword As String = String.Empty
    Private _filter_Targets As ObservableCollection(Of KeywordFilterTarget) = KeywordFilterTargets.GetFilterTargets

    'Current DataGrid variables
    Private SortingGridByChecked As Boolean = False

    'Sort Directions/ Sorter
    Private sortDirection As Integer = ListSortDirection.Ascending
    Private sorter As String = "Sort By Returned"

    'Commands
    Private m_SearchTransactionCommand As RelayCommand(Of ComboBox)
    Private _sortDescriptorChangeCommand As RelayCommand(Of ComboBox)
    Private _sortDirectorChangeCommand As RelayCommand(Of ComboBox)

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "TransactionsLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("User_Returned_Transactions_Detail")
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property User_Returned_Transactions_Detail As ICollectionView
        Get
            Return _user_Returned_Transactions_Detail
        End Get
    End Property

    Public Property Filter_Keyword As String
        Get
            Return Me._filter_Keyword
        End Get
        Set(value As String)
            Me._filter_Keyword = value
        End Set
    End Property

    Public ReadOnly Property Filter_Targets As ObservableCollection(Of KeywordFilterTarget)
        Get
            Return Me._filter_Targets
        End Get
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context to manage compact : every page will cause a new 
        Me.LibraryDataService.RefreshLibraryDataService()
        Me.LibraryDataService.SelectTopTransactions = 0
        ''''''''''''''''''''''''''''
        Me._user_Returned_Transactions_Detail = libraryDataService.TransactionDetailCollectionView

        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf Me.libraryDataService_PropertyChanged
        End If

        Me.LibraryDataService.LoadCurrentMemberAllTransactions()

        Using (Me._user_Returned_Transactions_Detail.DeferRefresh())
            Me._user_Returned_Transactions_Detail.PageSize = _transactionsPageSize
            Me._user_Returned_Transactions_Detail.SetTotalItemCount(-1)
            Me._user_Returned_Transactions_Detail.MoveToFirstPage()
        End Using

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SearchTransactionCommand() As RelayCommand(Of ComboBox)
        Get
            Return m_SearchTransactionCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            m_SearchTransactionCommand = value
        End Set
    End Property

    Public Property SortDescriptorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDescriptorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDescriptorChangeCommand = value
        End Set
    End Property

    Public Property SortDirectorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDirectorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDirectorChangeCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SearchTransactionCommand = New RelayCommand(Of ComboBox)(AddressOf SearchTransaction,
                                                            Function(sender) Not IsNothing(sender))
        SortDescriptorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDescriptorChange,
                                              Function() CType(_user_Returned_Transactions_Detail, DomainCollectionView).ItemCount > 0)
        SortDirectorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDirectorChange,
                                              Function() CType(_user_Returned_Transactions_Detail, DomainCollectionView).ItemCount > 0)
    End Sub

    Protected Sub UnregisterCommands()
        SearchTransactionCommand = Nothing
        SortDescriptorChangeCommand = Nothing
        SortDirectorChangeCommand = Nothing
    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub
#End Region

#Region "Register Message & Handle"

    Protected Overrides Sub RegisterMessages()
        Messenger.Reset()
        Messenger.[Default].Register(Of SavedReviewDialogMessage)(Me, AddressOf OnSavedReviewBookDialogMessageReceived)
    End Sub

    Protected Sub UnregisterMessages()
        Messenger.[Default].Unregister(Of SavedReviewDialogMessage)(Me, AddressOf OnSavedReviewBookDialogMessageReceived)
    End Sub

    Private Sub OnSavedReviewBookDialogMessageReceived(msg As SavedReviewDialogMessage)

        Dim myMessageBox = New MyMessageBox(msg.Type, msg.Content, msg.Caption, msg.Button)
        myMessageBox.Show()

        Dim currentPage = Me._user_Returned_Transactions_Detail.PageIndex
        Using (Me._user_Returned_Transactions_Detail.DeferRefresh())
            Me._user_Returned_Transactions_Detail.SetTotalItemCount(-1)
            Me._user_Returned_Transactions_Detail.MoveToPage(currentPage)
        End Using

    End Sub

#End Region

#Region "Private Methods : Handling Commands"

    Private Sub SearchTransaction(searchTargetsCombo As ComboBox)

        'For Each target In Me.Filter_Targets
        For Each target As KeywordFilterTarget In searchTargetsCombo.Items
            If target.Target.Contains("Media") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionBookMedia = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionBookMedia = Nothing
                End If
            ElseIf target.Target.Contains("Title") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionBookTitle = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionBookTitle = Nothing
                End If
            ElseIf target.Target.Contains("Description") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionBookDescription = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionBookDescription = Nothing
                End If
            ElseIf target.Target.Contains("ISBN") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionBookISBN = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionBookISBN = Nothing
                End If
            ElseIf target.Target.Contains("Language") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionBookLanguage = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionBookLanguage = Nothing
                End If
            End If
        Next

        'Me.LibraryDataService.LoadBooks()
        Using (Me._user_Returned_Transactions_Detail.DeferRefresh())
            Me._user_Returned_Transactions_Detail.SetTotalItemCount(-1)
            Me._user_Returned_Transactions_Detail.MoveToFirstPage()
        End Using


    End Sub

    Private Sub OnSortDirectorChange(sortDirector As ComboBox)

        If sortDirector.SelectedItem.Content.contains("Asc") Then
            sortDirection = ListSortDirection.Ascending
        Else
            sortDirection = ListSortDirection.Descending
        End If

        SortBooks()

    End Sub

    Private Sub OnSortDescriptorChange(sortDescriptor As ComboBox)

        sorter = sortDescriptor.SelectedItem.Content
        SortBooks()

    End Sub

    Private Sub SortBooks()
        If sorter.Contains("Title") Then
            _user_Returned_Transactions_Detail.SortDescriptions.Clear()
            _user_Returned_Transactions_Detail.SortDescriptions.Add(New SortDescription("Book_Title", sortDirection))
        ElseIf sorter.Contains("ISBN") Then
            _user_Returned_Transactions_Detail.SortDescriptions.Clear()
            _user_Returned_Transactions_Detail.SortDescriptions.Add(New SortDescription("Book_ISBN", sortDirection))
        ElseIf sorter.Contains("Published") Then
            _user_Returned_Transactions_Detail.SortDescriptions.Clear()
            _user_Returned_Transactions_Detail.SortDescriptions.Add(New SortDescription("Book_Published", sortDirection))
        ElseIf sorter.Contains("Media") Then
            _user_Returned_Transactions_Detail.SortDescriptions.Clear()
            _user_Returned_Transactions_Detail.SortDescriptions.Add(New SortDescription("Book_Media", sortDirection))
        ElseIf sorter.Contains("Language") Then
            _user_Returned_Transactions_Detail.SortDescriptions.Clear()
            _user_Returned_Transactions_Detail.SortDescriptions.Add(New SortDescription("Book_Language", sortDirection))
        ElseIf sorter.Contains("Returned") Then
            _user_Returned_Transactions_Detail.SortDescriptions.Clear()
            _user_Returned_Transactions_Detail.SortDescriptions.Add(New SortDescription("Returned_Date", sortDirection))
        ElseIf sorter.Contains("Reviewed") Then
            _user_Returned_Transactions_Detail.SortDescriptions.Clear()
            _user_Returned_Transactions_Detail.SortDescriptions.Add(New SortDescription("Review_Date", sortDirection))
        End If
        _user_Returned_Transactions_Detail.Refresh()
    End Sub

#End Region

End Class
