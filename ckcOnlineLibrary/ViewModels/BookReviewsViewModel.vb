﻿Imports Papa.Common
Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports System.Collections.ObjectModel

Public Class BookReviewsViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'Book
    Private _book_id As Integer = -1

    'Review Collection views
    Private _book_Reviews As DomainCollectionView(Of Member_Reviews)
    Private _book_Reviews_Source As EntityList(Of Member_Reviews)
    Private _book_Reviews_Loader As DomainCollectionViewLoader(Of Member_Reviews)

    Private _review_Member As DomainCollectionView(Of Member)
    Private _review_Member_Source As EntityList(Of Member)
    Private _review_Member_Loader As DomainCollectionViewLoader(Of Member)

    Private _reviewProperties As DomainCollectionModels = New DomainCollectionModels()
    Private reviewPropertiesLoadQueue As Queue(Of EntityQuery) = New Queue(Of EntityQuery)

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Collection View Default Page Size
    Private _page_Size As Integer = 3

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            If LibraryDataService.DomainContext.Members.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Members.HasChanges
                Me.RaisePropertyChanged("ReviewMember")
            ElseIf LibraryDataService.DomainContext.Books.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Books.HasChanges
                Me.RaisePropertyChanged("ReviewBook")
            ElseIf LibraryDataService.DomainContext.Member_Reviews.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Member_Reviews.HasChanges
                Me.RaisePropertyChanged("Review")
            End If
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Book_Reviews As DomainCollectionView(Of Member_Reviews)
        Get
            Return Me._book_Reviews
        End Get
    End Property

    Public Property Page_Size As Integer
        Get
            Return _page_Size
        End Get
        Set(value As Integer)
            _page_Size = value
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService, book_id As Integer)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService

        Me._book_id = book_id

        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        loadBookReview()

        Using Me.Book_Reviews.DeferRefresh()
            Me.Book_Reviews.MoveToFirstPage()
        End Using

    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        Me._book_id = -1
        Me._book_Reviews = Nothing
        Me._book_Reviews_Source = Nothing
        Me._book_Reviews_Loader = Nothing

        Me._review_Member = Nothing
        Me._review_Member_Source = Nothing
        Me._review_Member_Loader = Nothing

        Me._reviewProperties = New DomainCollectionModels()
        Me.reviewPropertiesLoadQueue = New Queue(Of EntityQuery)

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

    End Sub
#End Region

#Region "Private Methods : Load Data"

    Private Sub loadBookReview()

        'Initialize Review Member Collection
        Me._review_Member_Source = New EntityList(Of Member)(Me.LibraryDataService.DomainContext.Members)
        Me._review_Member_Loader = New DomainCollectionViewLoader(Of Member)(
                                        AddressOf Me.LoadReviewMember,
                                        AddressOf Me.OnLoadReviewMemberCompleted)
        Me._review_Member = New DomainCollectionView(Of Member)(_review_Member_Loader, _review_Member_Source)
        Me._reviewProperties(GetType(Member)) = New DomainCollectionModel("Members", GetType(Member),
                                       Me._review_Member, Me._review_Member_Source, Me._review_Member_Loader)

        'Initialize Book Review Collection
        Me._book_Reviews_Source = New EntityList(Of Member_Reviews)(Me.LibraryDataService.DomainContext.Member_Reviews)
        Me._book_Reviews_Loader = New DomainCollectionViewLoader(Of Member_Reviews)(
                                        AddressOf Me.LoadReviewEntities,
                                        AddressOf Me.OnLoadReviewEntitiesCompleted)
        Me._book_Reviews = New DomainCollectionView(Of Member_Reviews)(_book_Reviews_Loader, _book_Reviews_Source)

        'Set Page size
        Me._book_Reviews.PageSize = Me._page_Size
        'Load data
        Me._book_Reviews.CollectionViewLoader.Load(Nothing)

    End Sub

    Private Function LoadReviewEntities() As LoadOperation(Of Member_Reviews)
        Me.CanLoad = False
        Dim query As EntityQuery(Of Member_Reviews) = Me.LibraryDataService.DomainContext.Get_Reviews_By_BookQuery(Me._book_id, 0)
        Return Me.LibraryDataService.DomainContext.Load(query.SortAndPageBy(Me._book_Reviews))
    End Function

    Private Sub OnLoadReviewEntitiesCompleted(op As LoadOperation(Of Member_Reviews))
        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Me._book_Reviews_Source.Source = op.Entities
            If op.TotalEntityCount <> -1 Then
                Me._book_Reviews.SetTotalItemCount(op.TotalEntityCount)
            End If
            Me.RaisePropertyChanged("BookReviewsLoaded")
            For Each review As Member_Reviews In CType(op.Entities, IEnumerable(Of Member_Reviews))
                Me.LoadMemberOfReview(review.Review_id)
            Next
        End If
    End Sub

    Private Sub LoadMemberOfReview(ByVal review_id As Integer)
        If Me._reviewProperties.ContainsKey(GetType(Member)) Then
            Me._reviewProperties(GetType(Member)).Query = Me.LibraryDataService.DomainContext.GetMember_Of_ReviewQuery(review_id)
            Me._reviewProperties(GetType(Member)).Loader.Load(Nothing)
        End If
    End Sub

    Private Function LoadReviewMember(Of T As Entity)() As LoadOperation(Of T)

        Dim collectionModel As DomainCollectionModel = CType(Me._reviewProperties(GetType(T)), DomainCollectionModel)
        Dim entityQuery As EntityQuery(Of T) = collectionModel.Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        Me.CanLoad = False
        Return Me.LibraryDataService.DomainContext.Load(entityQuery.SortAndPageBy(collectionModel.View))

    End Function

    Private Sub OnLoadReviewMemberCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Try
                Dim domainCollectionModel As DomainCollectionModel =
                        CType(Me._reviewProperties(GetType(T)), DomainCollectionModel)
                CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
                If op.AllEntities.Count <> -1 Then
                    If op.TotalEntityCount <> -1 Then
                        CType(Me._reviewProperties(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                    End If

                    Dim review_id = op.EntityQuery.Parameters("review_id")
                    Dim review = From r In Me._book_Reviews_Source.Source
                                 Where r.Review_id = review_id
                                 Select r
                    Dim reviewRef = review.FirstOrDefault
                    If Not reviewRef Is Nothing Then
                        If GetType(T) Is GetType(Member) Then
                            reviewRef.Member = CType(domainCollectionModel.Source, EntityList(Of Member)).Source.ElementAtOrDefault(0)
                        End If
                    End If
                End If
            Catch ex As Exception
                'do nothing
            End Try
        Else
            'If query was cancelled, do it again to get all books' properties
            reviewPropertiesLoadQueue.Enqueue(op.EntityQuery)
        End If

        'If there are pending query of review properties, do it
        If reviewPropertiesLoadQueue.Count > 0 Then
            Dim entityQuery As EntityQuery = reviewPropertiesLoadQueue.Dequeue()
            If TypeOf (entityQuery) Is EntityQuery(Of Member) Then
                Me._reviewProperties(GetType(Member)).Query = entityQuery
                Me._reviewProperties(GetType(Member)).Loader.Load(Nothing)
            End If
        End If

    End Sub

#End Region

End Class
