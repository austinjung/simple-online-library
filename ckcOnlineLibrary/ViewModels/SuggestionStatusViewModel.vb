﻿Imports Papa.Common
Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports System.Collections.ObjectModel
Imports GalaSoft.MvvmLight.Command
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging

Public Class SuggestionStatusViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Public Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'Book Collection views
    Private _suggestions As DomainCollectionView(Of NewBookRequest)
    Private _templateAnswers As DomainCollectionView(Of AnswerTemplate)

    Private _isVisible As Visibility = Visibility.Collapsed

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False
    Private _isAnswered As Boolean = False

    Private m_AnswerSelectedSuggestionCommand As RelayCommand
    Private m_AddTemplateToReviewCommand As RelayCommand(Of AnswerTemplate)
    Private m_SaveAnswerSuggestionCommand As RelayCommand
    Private m_CancelAnswerSuggestionCommand As RelayCommand

    Private _originalReview As String
    Private _reviewTextBox As TextBox

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "NewBookRequestsLoaded" Then
            Me.HasChanges = True
            Me.RaisePropertyChanged("Suggestions")
            If LibraryDataService.NewBookRequestCollectionView.IsEmpty Then
                Me.IsVisible = Visibility.Collapsed
            Else
                Me.IsVisible = Visibility.Visible
                Me.RaisePropertyChanged("IsVisible")
            End If
        ElseIf e.PropertyName = "TemplateAnswersLoaded" Then
            Me.HasChanges = True
            Me.RaisePropertyChanged("TemplateAnswers")
        ElseIf e.PropertyName = "NewBookRequestMemberLoaded" Then
            Me.HasChanges = True
            Me.RaisePropertyChanged("SuggestionsTree")
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

    Public Property IsAnswered() As Boolean
        Get
            Return _isAnswered
        End Get
        Set(value As Boolean)
            _isAnswered = value
            Me.RaisePropertyChanged("IsAnswered")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public Property ReviewTextBox As TextBox
        Get
            Return Me._reviewTextBox
        End Get
        Set(value As TextBox)
            Me._reviewTextBox = value
        End Set
    End Property

    Public ReadOnly Property OriginalReview As String
        Get
            Return Me._originalReview
        End Get
    End Property

    Public ReadOnly Property TemplateAnswers As DomainCollectionView(Of AnswerTemplate)
        Get
            Return Me._templateAnswers
        End Get
    End Property

    Public ReadOnly Property Suggestions As DomainCollectionView(Of NewBookRequest)
        Get
            Return _suggestions
        End Get
    End Property

    Public ReadOnly Property SuggestionsTree As ObservableCollection(Of TopicTree)
        Get
            Dim trees As ObservableCollection(Of TopicTree) = New ObservableCollection(Of TopicTree)
            For Each sg As NewBookRequest In _suggestions
                Dim suggestion As TopicTree
                Dim review As TopicTree
                Dim reviews As ObservableCollection(Of TopicTree) = New ObservableCollection(Of TopicTree)
                Dim dateString As String

                suggestion = New TopicTree With {
                    .NewBookRequestID = sg.NewBookRequestID
                }
                suggestion.Title = sg.RequestContent
                If String.IsNullOrEmpty(suggestion.Title) Then
                    If Not String.IsNullOrEmpty(sg.BookTitle) Then
                        suggestion.Title = "Book Title '" & sg.BookTitle & "'."
                    ElseIf Not String.IsNullOrEmpty(sg.ISBN) Then
                        suggestion.Title = "Book ISBN '" & sg.ISBN & "'."
                    End If
                    sg.RequestContent = suggestion.Title
                End If
                If WebContext.Current.User.Roles.Contains("User") Then

                    If IsNothing(sg.LastReviewDate) Then
                        dateString = "01/01/0001"
                    Else
                        dateString = CType(sg.LastReviewDate, Date).ToString("MM/dd/yyyy")
                    End If

                    If (sg.ReviewViewed = False) And (Not dateString.Equals("01/01/0001")) Then
                        suggestion.Title = suggestion.Title & vbCrLf & "** New Status **                          - " & _
                                         WebContext.Current.User.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
                    Else
                        suggestion.Title = suggestion.Title & vbCrLf & "                                                       - " & _
                                         WebContext.Current.User.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
                    End If
                Else
                    If IsNothing(sg.Member) Then
                        If sg.RequestViewed = False Then
                            suggestion.Title = suggestion.Title & vbCrLf & "** New Suggection **                  - " & _
                                             "          " & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
                        Else
                            suggestion.Title = suggestion.Title & vbCrLf & "                                                       - " & _
                                             "          " & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
                        End If
                    Else
                        If sg.RequestViewed = False Then
                            suggestion.Title = suggestion.Title & vbCrLf & "** New Suggection **                  - " & _
                                             sg.Member.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
                        Else
                            suggestion.Title = suggestion.Title & vbCrLf & "                                                       - " & _
                                             sg.Member.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
                        End If
                    End If
                End If

                If Not String.IsNullOrEmpty(sg.BookTitle) Then
                    review = New TopicTree With {
                        .NewBookRequestID = sg.NewBookRequestID, _
                        .Title = "    - Book Title : '" & sg.BookTitle & "'."
                    }
                    reviews.Add(review)
                ElseIf Not String.IsNullOrEmpty(sg.ISBN) Then
                    review = New TopicTree With {
                        .NewBookRequestID = sg.NewBookRequestID, _
                        .Title = "    - Book ISBN : '" & sg.ISBN & "'."
                    }
                    reviews.Add(review)
                End If
                If String.IsNullOrEmpty(sg.Reviews) Then
                    review = New TopicTree With {
                        .NewBookRequestID = sg.NewBookRequestID, _
                        .Title = sg.Reviews & "Admin has not read yet."
                    }
                Else
                    review = New TopicTree With {
                        .NewBookRequestID = sg.NewBookRequestID, _
                        .Title = sg.Reviews & vbCrLf & "                                                  - " & _
                        "Admin : " & sg.RequestDate.ToString("MM/dd/yyyy")
                    }
                End If
                reviews.Add(review)
                suggestion.ChildTopics = reviews
                trees.Add(suggestion)
            Next
            Return trees
        End Get
    End Property

    Private _selectedSuggestion As NewBookRequest
    Public Property SelectedSuggection As NewBookRequest
        Get
            Return _selectedSuggestion
        End Get
        Set(value As NewBookRequest)
            _selectedSuggestion = value
            _originalReview = value.Reviews
            Me.RaisePropertyChanged("SelectedSuggection")
        End Set
    End Property

    Public Property IsVisible As Visibility
        Get
            Return _isVisible
        End Get
        Set(value As Visibility)
            _isVisible = value
            Me.RaisePropertyChanged("IsVisible")
        End Set
    End Property

#End Region

#Region "Commands Properties"

    Public Property SaveAnswerSuggestionCommand As RelayCommand
        Get
            Return m_SaveAnswerSuggestionCommand
        End Get
        Set(value As RelayCommand)
            m_SaveAnswerSuggestionCommand = value
        End Set
    End Property

    Public Property CancelAnswerSuggestionCommand As RelayCommand
        Get
            Return m_CancelAnswerSuggestionCommand
        End Get
        Set(value As RelayCommand)
            m_CancelAnswerSuggestionCommand = value
        End Set
    End Property

    Public Property AnswerSelectedSuggestionCommand As RelayCommand
        Get
            Return m_AnswerSelectedSuggestionCommand
        End Get
        Set(value As RelayCommand)
            m_AnswerSelectedSuggestionCommand = value
        End Set
    End Property

    Public Property AddTemplateToReviewCommand As RelayCommand(Of AnswerTemplate)
        Get
            Return m_AddTemplateToReviewCommand
        End Get
        Set(value As RelayCommand(Of AnswerTemplate))
            m_AddTemplateToReviewCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        AnswerSelectedSuggestionCommand = New RelayCommand(AddressOf AnswerSelectedSuggestion)
        AddTemplateToReviewCommand = New RelayCommand(Of AnswerTemplate)(AddressOf AddTemplateToReview)
        SaveAnswerSuggestionCommand = New RelayCommand(AddressOf SaveAnswerSuggestion)
        CancelAnswerSuggestionCommand = New RelayCommand(AddressOf CancelAnswerSuggestion)
    End Sub

    Protected Sub UnregisterCommands()
        AnswerSelectedSuggestionCommand = Nothing
        AddTemplateToReviewCommand = Nothing
        SaveAnswerSuggestionCommand = Nothing
        CancelAnswerSuggestionCommand = Nothing
    End Sub

#End Region

#Region "Constructors & Public Methods"

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService

        AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged

        Me.IsVisible = Visibility.Collapsed
        If Not IsNothing(WebContext.Current.User.Roles) Then
            If WebContext.Current.User.Roles.Contains("Manager") Then
                Me._suggestions = Me.LibraryDataService.NewBookRequestCollectionView
                Me.LibraryDataService.LoadAllMemberOpenSuggestions()
            Else
                Me._suggestions = Me.LibraryDataService.CurrentUserNewBookRequestCollectionView
                Me.LibraryDataService.LoadMyOpenSuggestions()
            End If
        End If

    End Sub

    Public Sub Refresh()

        Me.IsVisible = Visibility.Collapsed
        If Not IsNothing(WebContext.Current.User.Roles) Then
            If WebContext.Current.User.Roles.Contains("Manager") Then
                Me._suggestions = Me.LibraryDataService.NewBookRequestCollectionView
                Me.LibraryDataService.LoadAllMemberOpenSuggestions()
            Else
                Me._suggestions = Me.LibraryDataService.CurrentUserNewBookRequestCollectionView
                Me.LibraryDataService.LoadMyOpenSuggestions()
            End If
        End If

    End Sub

    Public Sub ReLoad()

        Me.IsVisible = Visibility.Collapsed
        If Not IsNothing(WebContext.Current.User.Roles) Then
            If WebContext.Current.User.Roles.Contains("Manager") Then
                Me.LibraryDataService.LoadAllMemberOpenSuggestions()
            Else
                Me.LibraryDataService.LoadMyOpenSuggestions()
            End If
        End If

    End Sub

    Public Function UpdateSuggestionTree(ByVal sg As NewBookRequest) As TopicTree

        Dim review As TopicTree
        Dim reviews As ObservableCollection(Of TopicTree) = New ObservableCollection(Of TopicTree)

        Dim savedSuggestionID As Integer = sg.NewBookRequestID
        Dim suggestion As TopicTree = (From t In SuggestionsTree
                                       Where t.NewBookRequestID = savedSuggestionID
                                       Select t).FirstOrDefault

        suggestion.Title = sg.RequestContent
        If String.IsNullOrEmpty(suggestion.Title) Then
            If Not String.IsNullOrEmpty(sg.BookTitle) Then
                suggestion.Title = "Book Title '" & sg.BookTitle & "'."
            ElseIf Not String.IsNullOrEmpty(sg.ISBN) Then
                suggestion.Title = "Book ISBN '" & sg.ISBN & "'."
            End If
            sg.RequestContent = suggestion.Title
        End If
        If WebContext.Current.User.Roles.Contains("User") Then
            Dim DateString As String
            If IsNothing(sg.LastReviewDate) Then
                DateString = "01/01/0001"
            Else
                DateString = CType(sg.LastReviewDate, Date).ToString("MM/dd/yyyy")
            End If

            If (sg.ReviewViewed = False) And (Not DateString.Equals("01/01/0001")) Then
                suggestion.Title = suggestion.Title & vbCrLf & "** New Status **                          - " & _
                                 sg.Member.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
            Else
                suggestion.Title = suggestion.Title & vbCrLf & "                                                       - " & _
                                 sg.Member.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
            End If
        Else
            If sg.RequestViewed = False Then
                suggestion.Title = suggestion.Title & vbCrLf & "** New Suggection **                  - " & _
                                 sg.Member.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
            Else
                suggestion.Title = suggestion.Title & vbCrLf & "                                                       - " & _
                                 sg.Member.MemberLogin & " : " & sg.RequestDate.ToString("MM/dd/yyyy")
            End If
        End If

        If Not String.IsNullOrEmpty(sg.BookTitle) Then
            review = New TopicTree With {
                .NewBookRequestID = sg.NewBookRequestID, _
                .Title = "    - Book Title : '" & sg.BookTitle & "'."
            }
            reviews.Add(review)
        ElseIf Not String.IsNullOrEmpty(sg.ISBN) Then
            review = New TopicTree With {
                .NewBookRequestID = sg.NewBookRequestID, _
                .Title = "    - Book ISBN : '" & sg.ISBN & "'."
            }
            reviews.Add(review)
        End If
        If String.IsNullOrEmpty(sg.Reviews) Then
            review = New TopicTree With {
                .NewBookRequestID = sg.NewBookRequestID, _
                .Title = sg.Reviews & "Admin has not read yet."
            }
        Else
            review = New TopicTree With {
                .NewBookRequestID = sg.NewBookRequestID, _
                .Title = sg.Reviews & vbCrLf & "                                                  - " & _
                "Admin : " & sg.RequestDate.ToString("MM/dd/yyyy")
            }
        End If
        reviews.Add(review)
        suggestion.ChildTopics = reviews

        Return suggestion

    End Function

    Public Sub RaisePropertyChange(ByVal prop As String)
        Me.RaisePropertyChanged(prop)
    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()

    End Sub
#End Region

    Private Sub AnswerSelectedSuggestion()
        Me._templateAnswers = Me.LibraryDataService.TemplateAnswerCollectionView
        Me.LibraryDataService.LoadAnswerTemplates()

        Dim answerDialog = New AnswerSuggestionChildWindow()
        answerDialog.Show()
    End Sub

    Private Sub AddTemplateToReview(template As AnswerTemplate)

        Dim s = Me.ReviewTextBox.SelectionStart
        If (s = 0) Then
            Me.SelectedSuggection.Reviews = template.AnswerTemplate1 + Me.SelectedSuggection.Reviews
        ElseIf (s = Me.ReviewTextBox.Text.Length) Then
            Me.SelectedSuggection.Reviews += template.AnswerTemplate1
        Else
            Dim head = Me.ReviewTextBox.Text.Substring(0, s)
            Dim tail = Me.ReviewTextBox.Text.Substring(s, Me.ReviewTextBox.Text.Length - s)
            Me.SelectedSuggection.Reviews = head + template.AnswerTemplate1 + tail
        End If

    End Sub

    Private Sub SaveAnswerSuggestion()
        Me._isAnswered = False
        Me._selectedSuggestion.ReviewSequence += 1
        Me._selectedSuggestion.LastReviewDate = Now()
        Me._selectedSuggestion.ReviewViewed = False
        Me.LibraryDataService.SubmitChanges(AddressOf AfterSaveAnswer, Nothing)
    End Sub

    Private Sub CancelAnswerSuggestion()
        Me._isAnswered = False
        Me.LibraryDataService.DomainContext.RejectChanges()
    End Sub

    Private Sub AfterSaveAnswer(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedAnswerSuggestionDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
        Else
            dialogType = "Success"
        End If
        If result.ChangeSet.ModifiedEntities.Count > 0 Then
            For Each e As Object In result.ChangeSet.ModifiedEntities
                If TypeOf (e) Is NewBookRequest Then
                    Me._selectedSuggestion = e
                    Exit For
                End If
            Next
            action = "Update"
            resultMessage = ""
        End If
        If (result.[Error] IsNot Nothing) Then
            dialogMessage = New SavedAnswerSuggestionDialogMessage("Error", "Save", "Save was unsuccessful.", Me._selectedSuggestion)
            Dim myMessageBox = New MyMessageBox("Error", "Save", "Save was unsuccessful.", MessageBoxButton.OK)
            myMessageBox.Show()
        Else
            dialogMessage = New SavedAnswerSuggestionDialogMessage("Success", "Save", "Save was successful.", Me._selectedSuggestion)
            Dim myMessageBox = New MyMessageBox("Success", "Save", "Save was successful.", MessageBoxButton.OK)
            myMessageBox.Show()
        End If
        Messenger.[Default].Send(dialogMessage)

    End Sub

End Class
