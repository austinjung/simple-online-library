﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight
Imports ckcOnlineLibrary.DateTimeExtensions

Public Class BookDetailHistoryViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    Private _book_id As Integer = -1
    Private _newOnHandQuantity As Integer = -1
    Private _comittedDate As Date = Nothing
    Private _preComittedDate As String
    Private _isAllocated As Boolean
    Private noDay As String = "01/01/0001"

    Private _qualificationForNewComitte As Boolean

    'Book Request Information
    Private _requestExpectedDate As Date = CalculateBusinessDaysFromInputDate(Today, 1) ' Default expected day : next day
    Private _requestComitteMaxDate As Date = CalculateBusinessDaysFromInputDate(Today, 5) ' Maximum day to be comitted : 5 business days
    Private _requestComment As String = String.Empty
    Private _myPendingRequest As Member_Requests
    Private _isNewRequest As Boolean = False
    Private _requestExpectedDateChanged As Boolean = True

    'View Models
    Private _bookByBookIDVM As BookByBookIDViewModel
    Private _bookReviewsVM As BookReviewsViewModel
    Private _bookPendingRequestVM As BookPendingRequestsViewModel

    'Book Collection views
    Private _books As DomainCollectionView(Of Book)
    Private _books_Source As EntityList(Of Book)
    Private _books_Loader As DomainCollectionViewLoader(Of Book)
    Private _editableBook As Book

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Command Properties
    Private _sortDescriptorChangeCommand As RelayCommand(Of ComboBox)
    Private _openRequestBookCommand As RelayCommand
    Private _saveRequestBookCommand As RelayCommand
    Private _cancelPendingRequestBookCommand As RelayCommand

#End Region

#Region "Private Method : Property changed event"

    Private Sub bookViewModel_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            Me.HasChanges = _bookByBookIDVM.HasChanges
        ElseIf e.PropertyName = "CanLoad" Then
            Me.CanLoad = _bookByBookIDVM.CanLoad
        Else
            Me.RaisePropertyChanged("Book")
        End If
    End Sub

    Private Sub bookReviewViewModel_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            Me.HasChanges = _bookReviewsVM.HasChanges
        ElseIf e.PropertyName = "CanLoad" Then
            Me.CanLoad = _bookReviewsVM.CanLoad
        Else
            Me.RaisePropertyChanged("Book_Reviews")
        End If
    End Sub

    Private Sub bookPendingRequestVM_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            Me.HasChanges = _bookPendingRequestVM.HasChanges
        ElseIf e.PropertyName = "CanLoad" Then
            Me.CanLoad = _bookPendingRequestVM.CanLoad
        Else
            Me._myPendingRequest = Me._bookPendingRequestVM.Book_Pending_Requests.SourceCollection(0)
            Me.RaisePropertyChanged("My_Previous_Pending_Request")
            If Not IsNothing(Me._myPendingRequest) Then
                Me._requestExpectedDate = Me._myPendingRequest.Expected_Date
                Me.RaisePropertyChanged("RequestExpectedDate")
            End If
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Book As Books_With_Statistics
        Get
            Return Me._bookByBookIDVM.Book
        End Get
    End Property

    Public ReadOnly Property Book_Authors As ObservableCollection(Of Author)
        Get
            Return Me._bookByBookIDVM.Book_Authors
        End Get
    End Property

    Public ReadOnly Property Book_Categories As ObservableCollection(Of Category)
        Get
            Return Me._bookByBookIDVM.Book_Categories
        End Get
    End Property

    Public ReadOnly Property Book_Shelves As ObservableCollection(Of Shelf)
        Get
            Return Me._bookByBookIDVM.Book_Shelves
        End Get
    End Property

    Public ReadOnly Property Book_Reviews As ICollectionView
        Get
            Return Me._bookReviewsVM.Book_Reviews
        End Get
    End Property

    Public ReadOnly Property My_Previous_Pending_Request As Member_Requests
        Get
            Return Me._bookPendingRequestVM.Book_Pending_Requests.SourceCollection(0)
        End Get
    End Property

    Public Property RequestExpectedDate As Date
        Get
            Return _requestExpectedDate
        End Get
        Set(value As Date)
            _requestExpectedDate = value
        End Set
    End Property

    Public Property RequestComment As String
        Get
            Return _requestComment
        End Get
        Set(value As String)
            _requestComment = value
        End Set
    End Property

    Public ReadOnly Property ExpectedStartDate As Date
        Get
            Return Me._requestExpectedDate
        End Get
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   book_id As Integer,
                   member_id As Integer)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        Me._book_id = book_id

        Me._bookByBookIDVM = New BookByBookIDViewModel(pageConductor, libraryDataService, book_id)
        Me._bookReviewsVM = New BookReviewsViewModel(pageConductor, libraryDataService, book_id)
        If member_id <= 0 Then
            Me._bookPendingRequestVM _
                = New BookPendingRequestsViewModel(pageConductor, libraryDataService, book_id, WebContext.Current.User.MemberID)
            Me.LibraryDataService.DomainContext.Count_Unreturned_Transactions_Of_Member(WebContext.Current.User.MemberID, _
                                                                                     AddressOf CountUnreturnedBooksCallBack, Nothing)
        Else
            Me._bookPendingRequestVM _
                = New BookPendingRequestsViewModel(pageConductor, libraryDataService, book_id, member_id)
            Me.LibraryDataService.DomainContext.Count_Unreturned_Transactions_Of_Member(member_id, _
                                                                                     AddressOf CountUnreturnedBooksCallBack, Nothing)
        End If

        'Set Property Changed Event Handler
        AddHandler Me._bookByBookIDVM.PropertyChanged, AddressOf bookViewModel_PropertyChanged
        AddHandler Me._bookReviewsVM.PropertyChanged, AddressOf bookReviewViewModel_PropertyChanged
        AddHandler Me._bookPendingRequestVM.PropertyChanged, AddressOf bookPendingRequestVM_PropertyChanged

        'Initialize Book Collection
        Me._books_Source = New EntityList(Of Book)(Me.LibraryDataService.DomainContext.Books)
        Me._books_Loader = New DomainCollectionViewLoader(Of Book)(
                                        AddressOf Me.LoadBookEntities,
                                        AddressOf Me.OnLoadBookEntitiesCompleted)
        Me._books = New DomainCollectionView(Of Book)(_books_Loader, _books_Source)

        'Load Book
        Me._books.CollectionViewLoader.Load(Nothing)

    End Sub

#End Region

#Region "Commands Properties"

    Public Property CancelPendingRequestBookCommand() As RelayCommand
        Get
            Return _cancelPendingRequestBookCommand
        End Get
        Set(value As RelayCommand)
            _cancelPendingRequestBookCommand = value
        End Set
    End Property

    Public Property OpenRequestBookCommand() As RelayCommand
        Get
            Return _openRequestBookCommand
        End Get
        Set(value As RelayCommand)
            _openRequestBookCommand = value
        End Set
    End Property

    Public Property SaveRequestBookCommand() As RelayCommand
        Get
            Return _saveRequestBookCommand
        End Get
        Set(value As RelayCommand)
            _saveRequestBookCommand = value
        End Set
    End Property

    Public Property SortDescriptorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDescriptorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDescriptorChangeCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        CancelPendingRequestBookCommand = New RelayCommand(AddressOf OnCancelPendingRequestBook)
        OpenRequestBookCommand = New RelayCommand(AddressOf OnOpenRequestBook)
        SaveRequestBookCommand = New RelayCommand(AddressOf OnSaveRequestBook)
        SortDescriptorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDescriptorChange,
                                              Function() CType(Book_Reviews, DomainCollectionView).ItemCount > 0)
    End Sub

    Protected Sub UnregisterCommands()
        SortDescriptorChangeCommand = Nothing
        OpenRequestBookCommand = Nothing
        SaveRequestBookCommand = Nothing
        CancelPendingRequestBookCommand = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If Not IsNothing(Me._bookByBookIDVM) Then
            RemoveHandler Me._bookByBookIDVM.PropertyChanged, AddressOf bookViewModel_PropertyChanged
            Me._bookByBookIDVM.Cleanup()
            Me._bookByBookIDVM = Nothing
        End If
        If Not IsNothing(Me._bookReviewsVM) Then
            RemoveHandler Me._bookReviewsVM.PropertyChanged, AddressOf bookReviewViewModel_PropertyChanged
            Me._bookReviewsVM.Cleanup()
            Me._bookReviewsVM = Nothing
        End If
        If Not IsNothing(Me._bookPendingRequestVM) Then
            RemoveHandler Me._bookPendingRequestVM.PropertyChanged, AddressOf bookPendingRequestVM_PropertyChanged
            Me._bookPendingRequestVM.Cleanup()
            Me._bookPendingRequestVM = Nothing
        End If

        UnregisterCommands()

    End Sub

    Public Sub RemovePropertyChangeHandler()
        If Not IsNothing(Me._bookByBookIDVM) Then
            RemoveHandler Me._bookByBookIDVM.PropertyChanged, AddressOf bookViewModel_PropertyChanged
            Me._bookByBookIDVM.RemovePropertyChangeHandler()
        End If
        If Not IsNothing(Me._bookReviewsVM) Then
            RemoveHandler Me._bookReviewsVM.PropertyChanged, AddressOf bookReviewViewModel_PropertyChanged
            Me._bookReviewsVM.RemovePropertyChangeHandler()
        End If
        If Not IsNothing(Me._bookPendingRequestVM) Then
            RemoveHandler Me._bookPendingRequestVM.PropertyChanged, AddressOf bookPendingRequestVM_PropertyChanged
            Me._bookPendingRequestVM.RemovePropertyChangeHandler()
        End If

        UnregisterCommands()

    End Sub
#End Region

#Region "Private Methods : Handling Commands : Request, Sort"

    Private Sub CountUnreturnedBooksCallBack(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of Integer))
        If result.HasError Or result.Value >= 3 Then
            Me._qualificationForNewComitte = False
        Else
            Me._qualificationForNewComitte = True
        End If
    End Sub

    Private Sub OnOpenRequestBook()
        If Me.LibraryDataService.RequestBookIDCollection.Count >= Me.LibraryDataService.MaxPendingRequest _
            And Not Me.LibraryDataService.RequestBookIDCollection.Contains(_book_id) Then
            Dim myMessageBox = New MyMessageBox("Error", "Confirm", "You can have maximum 3 pending requests.", MessageBoxButton.OK)
            myMessageBox.Show()
        Else
            Dim requestBookWindow = New RequestBookChildWindow()
            requestBookWindow.Show()
        End If
    End Sub

    Private Sub OnCancelPendingRequestBook()
        If Not IsNothing(Me._myPendingRequest) Then
            Dim preComittedDate As String
            If (Me._myPendingRequest.Comitted_Date Is Nothing) Then
                preComittedDate = "01/01/0001"
            Else
                preComittedDate = CType(Me._myPendingRequest.Comitted_Date, DateTime).ToString("MM/dd/yyyy")
            End If
            If Not preComittedDate.Equals("01/01/0001") Then
                Me._editableBook.Book_Quantity_Onhand += 1
                Me._newOnHandQuantity = Me._editableBook.Book_Quantity_Onhand
                Me._isAllocated = False
                Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterRevertBookOnHand, Nothing)
            Else
                AfterRevertBookOnHand()
            End If
        End If
    End Sub

    Private Sub AfterRevertBookOnHand()
        If Not IsNothing(Me._myPendingRequest) Then
            Me.LibraryDataService.DomainContext.Member_Requests.Remove(Me._myPendingRequest)
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveRequestBook, Nothing)
        End If
    End Sub

    Private Sub OnSaveRequestBook()
        Me._newOnHandQuantity = -1
        Me._comittedDate = Nothing
        If Me._myPendingRequest Is Nothing Then
            Me._isNewRequest = True
            Me._requestExpectedDateChanged = True
            _preComittedDate = "01/01/0001"
            If Me._requestExpectedDate <= Me._requestComitteMaxDate And Me._qualificationForNewComitte Then
                'Me._newOnHandQuantity = _editableBook.Book_Quantity_Onhand
                'Me.LibraryDataService.DomainContext.Allocate_Book(Me._editableBook.Book_id, AddressOf Me.AfterAllocatBook, Nothing)
                Me._editableBook.Book_Quantity_Onhand -= 1
                Me._newOnHandQuantity = Me._editableBook.Book_Quantity_Onhand
                Me._isAllocated = True
                Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterAllocatedBook, Nothing)
            Else
                OnSaveRequestBookWithComittedDate()
            End If
        Else
            Me._isNewRequest = False
            If Me._myPendingRequest.Expected_Date = Me._requestExpectedDate Then
                Me._requestExpectedDateChanged = False
            Else
                Me._requestExpectedDateChanged = True
            End If
            If (Me._myPendingRequest.Comitted_Date Is Nothing) Then
                _preComittedDate = "01/01/0001"
            Else
                _preComittedDate = CType(Me._myPendingRequest.Comitted_Date, DateTime).ToString("MM/dd/yyyy")
            End If
            If Me._requestExpectedDate <= Me._requestComitteMaxDate And Me._qualificationForNewComitte Then
                If _preComittedDate.Equals(noDay) Then
                    'Me._newOnHandQuantity = _editableBook.Book_Quantity_Onhand
                    'Me.LibraryDataService.DomainContext.Allocate_Book(Me._editableBook.Book_id, AddressOf Me.AfterAllocatBook, Nothing)
                    _editableBook.Book_Quantity_Onhand -= 1
                    Me._newOnHandQuantity = _editableBook.Book_Quantity_Onhand
                    Me._isAllocated = True
                    Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterAllocatedBook, Nothing)
                Else
                    Me._comittedDate = Me._requestExpectedDate
                    OnSaveRequestBookWithComittedDate()
                End If
            Else
                If _preComittedDate.Equals(noDay) Then
                    OnSaveRequestBookWithComittedDate()
                Else
                    'Me._newOnHandQuantity = _editableBook.Book_Quantity_Onhand
                    'Me.LibraryDataService.DomainContext.Allocate_Book(Me._editableBook.Book_id, AddressOf Me.AfterDeallocatBook, Nothing)
                    _editableBook.Book_Quantity_Onhand += 1
                    Me._newOnHandQuantity = _editableBook.Book_Quantity_Onhand
                    Me._isAllocated = False
                    Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterAllocatedBook, Nothing)
                End If
            End If
        End If
    End Sub

    Private Sub AfterAllocatedBook(result As ServiceSubmitChangesResult)

        If (result.[Error] Is Nothing) Then
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                For Each b As Book In result.ChangeSet.ModifiedEntities
                    If b.Book_id = Me.Book.Book_id Then
                        If Me._newOnHandQuantity = b.Book_Quantity_Onhand Then
                            If Me._isAllocated Then
                                _comittedDate = _requestExpectedDate
                            Else
                                _comittedDate = Nothing
                            End If
                        End If
                        Exit For
                    End If
                Next
            End If
        End If

        OnSaveRequestBookWithComittedDate()

    End Sub

    'Private Sub AfterAllocatBook(result As ServiceModel.DomainServices.Client.InvokeOperation(Of Integer))

    '    Me._isAllocated = False

    '    If (result.[Error] Is Nothing) Then
    '        If result.Value > -1 Then
    '            _editableBook.Book_Quantity_Onhand = result.Value
    '            Me._newOnHandQuantity = _editableBook.Book_Quantity_Onhand
    '            Me._isAllocated = True
    '            _comittedDate = _requestExpectedDate
    '        End If
    '    End If

    '    OnSaveRequestBookWithComittedDate()

    'End Sub

    'Private Sub AfterDeallocatBook(result As ServiceModel.DomainServices.Client.InvokeOperation(Of Integer))

    '    Me._isAllocated = False

    '    If (result.[Error] Is Nothing) Then
    '        If result.Value > -1 Then
    '            _editableBook.Book_Quantity_Onhand = result.Value
    '            Me._newOnHandQuantity = _editableBook.Book_Quantity_Onhand
    '            _comittedDate = Nothing
    '        End If
    '    End If

    '    OnSaveRequestBookWithComittedDate()

    'End Sub

    Private Sub OnSaveRequestBookWithComittedDate()
        Dim comittedDateString As String
        If IsNothing(Me._comittedDate) Then
            comittedDateString = "01/01/0001"
        Else
            comittedDateString = Me._comittedDate.ToString("MM/dd/yyyy")
        End If
        Dim commentHeader As String
        If WebContext.Current.User.Role.Equals("User") Then
            If Me._isNewRequest Then
                commentHeader = WebContext.Current.User.MemberLogin & " : Requested. - " & Today.ToString("MM/dd/yyyy") & vbCrLf
            Else
                If Me._requestExpectedDateChanged Then
                    commentHeader = WebContext.Current.User.MemberLogin & " : Modified the expected date to " _
                                    & _requestExpectedDate.ToString("MM/dd/yyyy") & ". - " & Today.ToString("MM/dd/yyyy") & vbCrLf
                Else
                    commentHeader = WebContext.Current.User.MemberLogin & " : Modified. - " & Today.ToString("MM/dd/yyyy") & vbCrLf
                End If
            End If
        Else
            commentHeader = WebContext.Current.User.MemberLogin & " : commented. - " & Today.ToString("MM/dd/yyyy") & vbCrLf
        End If
        If Not comittedDateString.Equals(noDay) Then
            commentHeader += "    Comitted on " & comittedDateString & vbCrLf
        End If
        If Not String.IsNullOrEmpty(_requestComment) Then
            _requestComment = commentHeader & "    " & _requestComment & vbCrLf
        Else
            _requestComment = commentHeader
        End If
        If IsNothing(Me._myPendingRequest) Then
            Dim newRequest = New Member_Requests With {
                                .MemberID = WebContext.Current.User.MemberID,
                                .Book_id = _book_id,
                                .Expected_Date = _requestExpectedDate,
                                .Comitted_Date = Me._comittedDate,
                                .Comment = _requestComment,
                                .Request_Status = False,
                                .Request_Date = Now,
                                .Modified_Date = Now
                             }

            Me.LibraryDataService.DomainContext.Member_Requests.Add(newRequest)
            Me._myPendingRequest = newRequest
        Else
            If Not IsNothing(_requestComment) Then
                Me._myPendingRequest.Comment = _requestComment & Me._myPendingRequest.Comment
            End If
            Me._myPendingRequest.Expected_Date = _requestExpectedDate
            Me._myPendingRequest.Comitted_Date = Me._comittedDate
            Me._myPendingRequest.Modified_Date = Now
        End If
        Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveRequestBook, Nothing)
    End Sub

    Private Sub AfterSaveRequestBook(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedRequestBookDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim emailMessage As String = String.Empty

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Your request was not completed."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "New request"
                For Each e As Member_Requests In result.ChangeSet.AddedEntities
                    If e.Book Is Nothing Then
                        resultMessage &= "Your request is accepted." & vbCrLf
                        emailMessage = "Your request is accepted."
                    Else
                        resultMessage &= "Your request for " & e.Book.Book_Title & " is accepted." & vbCrLf
                        emailMessage = "Your request for " & e.Book.Book_Title & " is accepted."
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Change request"
                For Each e As Member_Requests In result.ChangeSet.ModifiedEntities
                    If e.Book Is Nothing Then
                        resultMessage &= "Your request is changed." & vbCrLf
                        emailMessage = "Your request is changed."
                    Else
                        resultMessage &= "Your request for " & e.Book.Book_Title & " is changed." & vbCrLf
                        emailMessage = "Your request for " & e.Book.Book_Title & " is changed."
                    End If
                Next
            End If
            If result.ChangeSet.RemovedEntities.Count > 0 Then
                action = "Cancel request"
                For Each e As Member_Requests In result.ChangeSet.RemovedEntities
                    If e.Book Is Nothing Then
                        resultMessage &= "Your request is cancelled." & vbCrLf
                        emailMessage = "Your request is cancelled."
                    Else
                        resultMessage &= "Your request for " & e.Book.Book_Title & " is cancelled." & vbCrLf
                        emailMessage = "Your request for " & Me._myPendingRequest.Book.Book_Title & " is cancelled."
                    End If
                Next
            End If
        End If

        If WebContext.Current.User.Role.Equals("Manager") Then
            dialogMessage = New SavedRequestBookDialogMessage(dialogType, action, "Your new comment is added.")
        Else
            dialogMessage = New SavedRequestBookDialogMessage(dialogType, action, resultMessage)
        End If
        Messenger.[Default].Send(dialogMessage)

        If WebContext.Current.User.Role.Equals("User") Then
            Dim alertMsg As String
            Dim comittedDateString As String
            If IsNothing(_comittedDate) Then
                comittedDateString = "01/01/0001"
            Else
                comittedDateString = _comittedDate.ToString("MM/dd/yyyy")
            End If
            If Not comittedDateString.Equals(noDay) Then
                If emailMessage.Contains("cancelled") Then
                    alertMsg = emailMessage & " - " & _comittedDate.ToString("MM/dd/yyyy") & vbCrLf
                Else
                    alertMsg = emailMessage & vbCrLf & "Comitted Date : " & _comittedDate.ToString("MM/dd/yyyy") & vbCrLf
                End If
            Else
                If emailMessage.Contains("cancelled") Then
                    alertMsg = emailMessage & " - " & _comittedDate.ToString("MM/dd/yyyy") & vbCrLf
                Else
                    alertMsg = emailMessage & " No comitted Date." & vbCrLf
                End If
            End If
            If IsNothing(Me._myPendingRequest.Member) Then
                If String.IsNullOrEmpty(WebContext.Current.User.Email) Then
                    Dim emailDialogMessage = New SendEmailResultMessage("Inform", "Sending email", WebContext.Current.User.FullName & " has no email.")
                    Dim myMessageBox = New MyMessageBox(emailDialogMessage.Type, emailDialogMessage.Content, emailDialogMessage.Caption, emailDialogMessage.Button)
                    myMessageBox.Show()
                Else
                    Me.LibraryDataService.DomainContext.SendMailFromAdmin(WebContext.Current.User.Email, action, alertMsg, _
                                                           AddressOf SendMailCallBack, Nothing)
                End If
            Else
                If String.IsNullOrEmpty(Me._myPendingRequest.Member.Email) Then
                    Dim emailDialogMessage = New SendEmailResultMessage("Inform", "Sending email", Me._myPendingRequest.Member.FullName & " has no email.")
                    Dim myMessageBox = New MyMessageBox(emailDialogMessage.Type, emailDialogMessage.Content, emailDialogMessage.Caption, emailDialogMessage.Button)
                    myMessageBox.Show()
                Else
                    Me.LibraryDataService.DomainContext.SendMailFromAdmin(Me._myPendingRequest.Member.Email, action, alertMsg, _
                                                           AddressOf SendMailCallBack, Nothing)
                End If
            End If
        End If

    End Sub

    Private Sub OnSortDescriptorChange(sortDescriptor As ComboBox)

        If sortDescriptor.SelectedItem.Content.contains("Date") Then
            Book_Reviews.SortDescriptions.Clear()
            Book_Reviews.SortDescriptions.Add(New SortDescription("Review_Date", ListSortDirection.Descending))
        Else
            Book_Reviews.SortDescriptions.Clear()
            Book_Reviews.SortDescriptions.Add(New SortDescription("Rating", ListSortDirection.Descending))
        End If
        Book_Reviews.Refresh()

    End Sub

    Private Sub SendMailCallBack(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of String))
        Dim dialogMessage As SendEmailResultMessage
        If result.Value.Contains("Fail") Then
            dialogMessage = New SendEmailResultMessage("Error", "Sending email", result.Value)
            Dim myMessageBox = New MyMessageBox(dialogMessage.Type, dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button)
            myMessageBox.Show()
        End If
    End Sub

#End Region

#Region "private load operation"

    Private Function LoadBookEntities() As LoadOperation(Of Book)
        Me.CanLoad = False
        Dim query As EntityQuery(Of Book) = Me.LibraryDataService.DomainContext.GetBook_By_Book_IDQuery(Me._book_id)
        Return Me.LibraryDataService.DomainContext.Load(query.SortAndPageBy(Me._books))
    End Function

    Private Sub OnLoadBookEntitiesCompleted(op As LoadOperation(Of Book))
        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Me._books_Source.Source = op.Entities
            If op.TotalEntityCount <> -1 Then
                Me._books.SetTotalItemCount(op.TotalEntityCount)
            End If
            If op.Entities.Count > 0 Then
                Me._editableBook = op.Entities(0) ' get first and default
            End If
        End If
    End Sub

#End Region

End Class
