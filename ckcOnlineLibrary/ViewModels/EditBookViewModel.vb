﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class EditBookViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Public Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"
    'Book Properties Link Collection Views
    Private _book_Authors As DomainCollectionView(Of Author) = Nothing
    Private _book_Categories As DomainCollectionView(Of Category) = Nothing
    Private _book_Shelves As DomainCollectionView(Of Shelf) = Nothing

    'Book Properties Filter
    Private _bookPropertiesFilter As String = String.Empty

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False
    Private _bookChanged As Boolean = False
    Private _bookAuthorsChanged As Boolean = False
    Private _bookCategoriesChanged As Boolean = False
    Private _bookShelvesChanged As Boolean = False

    'Edit Book Status & Original Properties
    Private _editBook As Book
    Private _isNewBookEdited As Boolean = False
    Private _qtyOnMember As Integer
    Private originalBookAuthors As ObservableCollection(Of Author) = Nothing
    Private originalBookCategories As ObservableCollection(Of Category) = Nothing
    Private originalBookShelves As ObservableCollection(Of Shelf) = Nothing

    'Current DataGrid variables
    Private SortingGridByChecked As Boolean = False
    Private _workingDataGrid As DataGrid = Nothing

    'Commands for save & cancel
    Private m_SaveBookChangesCommand As RelayCommand
    Private m_CancelBookChangesCommand As RelayCommand

    'Commands for book properties datagrids
    Private m_SelectEditBookGridRowCommand As RelayCommand(Of DataGrid)
    Private m_SelectEditBookWorkingGridCommand As RelayCommand(Of DataGrid)
    Private m_SelectEditBookGridSortCommand As RelayCommand(Of MouseButtonEventArgs)
    Private m_EditBookGridFilterChangedCommand As RelayCommand(Of TextBox)
#End Region

#Region "Private Method : Property changed event"

    Private Sub editBook_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        Select Case e.PropertyName
            Case "Book_Authors_List"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
            Case "Book_Authors"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookAuthorsChanged = True
            Case "Book_Categories_List"
            Case "Book_Categories"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookCategoriesChanged = True
            Case "Book_Shelves_List"
            Case "Book_Shelves"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookShelvesChanged = True
            Case "Book_Title"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "ImageSource"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Image_File"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Description"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Media"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Media_Set"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Language"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_ISBN"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Published"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Quantity_Avail"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "Book_Quantity_Onhand"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
            Case "IsActive"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveBookChangesCommand.RaiseCanExecuteChanged()
                End If
                _bookChanged = True
        End Select
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Book_Authors As ICollectionView
        Get
            If IsNothing(Me._book_Authors) Then
                Me._book_Authors = LibraryDataService.BookAuthorCollectionView
                InitializeCheckValueOfBookAuthors()
                Me._book_Authors.Refresh()
            End If
            Return Me._book_Authors
        End Get
    End Property
    Public ReadOnly Property Book_Authors_Loader As StaticAutoCompleteViewLoader(Of Author)
        Get
            Return Me.LibraryDataService.BookAuthorCollectionLoader
        End Get
    End Property

    Public ReadOnly Property Book_Categories As ICollectionView
        Get
            If Me._book_Categories Is Nothing Then
                Me._book_Categories = LibraryDataService.BookCategoryCollectionView
                InitializeCheckValueOfBookCategories()
                Me._book_Categories.Refresh()
            End If
            Return Me._book_Categories
        End Get
    End Property
    Public ReadOnly Property Book_Categories_Loader As StaticAutoCompleteViewLoader(Of Category)
        Get
            Return Me.LibraryDataService.BookCategoryCollectionLoader
        End Get
    End Property

    Public ReadOnly Property Book_Shelves As ICollectionView
        Get
            If Me._book_Shelves Is Nothing Then
                Me._book_Shelves = LibraryDataService.BookShelfCollectionView
                InitializeCheckValueOfBookShelves()
                Me._book_Shelves.Refresh()
            End If
            Return Me._book_Shelves
        End Get
    End Property
    Public ReadOnly Property Book_Shelves_Loader As StaticAutoCompleteViewLoader(Of Shelf)
        Get
            Return Me.LibraryDataService.BookShelfCollectionLoader
        End Get
    End Property

    Public Property EditBook() As Book
        Get
            Return _editBook
        End Get
        Set(value As Book)
            _editBook = value
            If Not IsNothing(_editBook) Then
                'Keep Original edit book link properties
                originalBookAuthors = _editBook.Book_Authors
                originalBookCategories = _editBook.Book_Categories
                originalBookShelves = _editBook.Book_Shelves
                'Set IsNewBookEdited Flag
                If _editBook.Book_id > 0 Then
                    _isNewBookEdited = False
                Else
                    _isNewBookEdited = True
                    _editBook.Book_Authors = New ObservableCollection(Of Author)
                    _editBook.Book_Categories = New ObservableCollection(Of Category)
                    _editBook.Book_Shelves = New ObservableCollection(Of Shelf)
                End If
                _qtyOnMember = _editBook.Book_Quantity_Avail - _editBook.Book_Quantity_Onhand
            End If
        End Set
    End Property

    Public Property IsNewBookEdited As Boolean
        Get
            Return _isNewBookEdited
        End Get
        Set(value As Boolean)
            _isNewBookEdited = value
        End Set
    End Property

    Public Property WorkingDataGrid As DataGrid
        Get
            Return _workingDataGrid
        End Get
        Set(value As DataGrid)
            _workingDataGrid = value
        End Set
    End Property

    Public Property BookPropertiesFilter As String
        Get
            Return _bookPropertiesFilter
        End Get
        Set(value As String)
            _bookPropertiesFilter = value
            Me.RaisePropertyChanged("BookPropertiesFilter")
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   editBook As Book)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        Me.EditBook = editBook

        ''''''''''''''''''''''''''''
        'Don't refresh Domain Data Context, we need to use current Book Properties Collection data 
        'Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''

        'Set Property Changed Event Handler
        AddHandler Me._editBook.PropertyChanged, AddressOf editBook_PropertyChanged

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SaveBookChangesCommand() As RelayCommand
        Get
            Return m_SaveBookChangesCommand
        End Get
        Set(value As RelayCommand)
            m_SaveBookChangesCommand = value
        End Set
    End Property

    Public Property CancelBookChangesCommand() As RelayCommand
        Get
            Return m_CancelBookChangesCommand
        End Get
        Set(value As RelayCommand)
            m_CancelBookChangesCommand = value
        End Set
    End Property

    Public Property SelectEditBookGridRowCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_SelectEditBookGridRowCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectEditBookGridRowCommand = value
        End Set
    End Property

    Public Property SelectEditBookWorkingGridCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_SelectEditBookWorkingGridCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectEditBookWorkingGridCommand = value
        End Set
    End Property

    Public Property SelectEditBookGridSortCommand() As RelayCommand(Of MouseButtonEventArgs)
        Get
            Return m_SelectEditBookGridSortCommand
        End Get
        Set(value As RelayCommand(Of MouseButtonEventArgs))
            m_SelectEditBookGridSortCommand = value
        End Set
    End Property

    Public Property EditBookGridFilterChangedCommand() As RelayCommand(Of TextBox)
        Get
            Return m_EditBookGridFilterChangedCommand
        End Get
        Set(value As RelayCommand(Of TextBox))
            m_EditBookGridFilterChangedCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands & Messages"

    Protected Overrides Sub RegisterCommands()
        SaveBookChangesCommand = New RelayCommand(AddressOf OnSaveBookChanges, Function() HasChanges)
        CancelBookChangesCommand = New RelayCommand(AddressOf OnCancelBookChanges)
        SelectEditBookGridRowCommand = New RelayCommand(Of DataGrid)(AddressOf OnSelectGridRow,
                                                            Function(sender) Not IsNothing(sender))
        SelectEditBookWorkingGridCommand = New RelayCommand(Of DataGrid)(AddressOf OnSelectWorkingGrid,
                                                            Function(sender) Not IsNothing(sender))
        SelectEditBookGridSortCommand = New RelayCommand(Of MouseButtonEventArgs)(AddressOf OnSelectGridSort,
                                                                           Function(e) Not IsNothing(e))
        EditBookGridFilterChangedCommand = New RelayCommand(Of TextBox)(AddressOf OnGridFilterChanged)
    End Sub

    Protected Overrides Sub RegisterMessages()

        Messenger.[Default].Register(Of SavedAuthorDialogMessage)(Me, AddressOf OnSavedAuthorDialogMessageReceived)
        Messenger.[Default].Register(Of SavedCategoryDialogMessage)(Me, AddressOf OnSavedCategoryDialogMessageReceived)
        Messenger.[Default].Register(Of SavedShelfDialogMessage)(Me, AddressOf OnSavedShelfDialogMessageReceived)

    End Sub

    Protected Sub UnregisterMessage()

        Messenger.[Default].Unregister(Of SavedAuthorDialogMessage)(Me, AddressOf OnSavedAuthorDialogMessageReceived)
        Messenger.[Default].Unregister(Of SavedCategoryDialogMessage)(Me, AddressOf OnSavedCategoryDialogMessageReceived)
        Messenger.[Default].Unregister(Of SavedShelfDialogMessage)(Me, AddressOf OnSavedShelfDialogMessageReceived)

    End Sub

    Private Sub OnSavedAuthorDialogMessageReceived(msg As SavedAuthorDialogMessage)

        If msg.Content = "Add" Then
            Me.LibraryDataService.LoadAuthors()
            Dim authors As DomainCollectionView(Of Author) = Me.Book_Authors
            If CType(authors.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Author)).AddAuthor(msg.SavedAuthor, msg.SavedAuthor.IsChecked) Then
                If msg.SavedAuthor.IsChecked Then
                    Dim newSelectedAuthor As ObservableCollection(Of Author) = New ObservableCollection(Of Author)
                    For Each a In CType(authors.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Author)).SelectedList
                        newSelectedAuthor.Add(a)
                    Next
                    Me.EditBook.Book_Authors = newSelectedAuthor
                    _bookAuthorsChanged = True
                    UpdateBookLinkProperties()
                End If
            End If
        ElseIf msg.Content = "Update" Then
            Me.LibraryDataService.LoadAuthors()
            Dim authors As DomainCollectionView(Of Author) = Me.Book_Authors
            If CType(authors.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Author)).UpdateAuthor(msg.SavedAuthor, msg.SavedAuthor.IsChecked) Then
                Me.EditBook.Book_Authors = Me.EditBook.Book_Authors
            End If
        End If

    End Sub

    Private Sub OnSavedCategoryDialogMessageReceived(msg As SavedCategoryDialogMessage)

        If msg.Content = "Add" Then
            Me.LibraryDataService.LoadCategories()
            Dim categories As DomainCollectionView(Of Category) = Me.Book_Categories
            If CType(categories.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Category)).AddCategory(msg.SavedCategory, msg.SavedCategory.IsChecked) Then
                If msg.SavedCategory.IsChecked Then
                    Dim newSelectedCategory As ObservableCollection(Of Category) = New ObservableCollection(Of Category)
                    For Each c In CType(categories.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Category)).SelectedList
                        newSelectedCategory.Add(c)
                    Next
                    Me.EditBook.Book_Categories = newSelectedCategory
                    _bookCategoriesChanged = True
                    UpdateBookLinkProperties()
                End If
            End If
        ElseIf msg.Content = "Update" Then
            Me.LibraryDataService.LoadCategories()
            Dim categories As DomainCollectionView(Of Category) = Me.Book_Categories
            If CType(categories.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Category)).UpdateCategory(msg.SavedCategory, msg.SavedCategory.IsChecked) Then
                Me.EditBook.Book_Categories = Me.EditBook.Book_Categories
            End If
        End If

    End Sub

    Private Sub OnSavedShelfDialogMessageReceived(msg As SavedShelfDialogMessage)

        If msg.Content = "Add" Then
            Me.LibraryDataService.LoadShelves()
            Dim shelves As DomainCollectionView(Of Shelf) = Me.Book_Shelves
            If CType(shelves.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Shelf)).AddShelf(msg.SavedShelf, msg.SavedShelf.IsChecked) Then
                If msg.SavedShelf.IsChecked Then
                    Dim newSelectedShelf As ObservableCollection(Of Shelf) = New ObservableCollection(Of Shelf)
                    For Each s In CType(shelves.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Shelf)).SelectedList
                        newSelectedShelf.Add(s)
                    Next
                    Me.EditBook.Book_Shelves = newSelectedShelf
                    _bookShelvesChanged = True
                    UpdateBookLinkProperties()
                End If
            End If
        ElseIf msg.Content = "Update" Then
            Me.LibraryDataService.LoadShelves()
            Dim shelves As DomainCollectionView(Of Shelf) = Me.Book_Shelves
            If CType(shelves.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Shelf)).UpdateShelf(msg.SavedShelf, msg.SavedShelf.IsChecked) Then
                Me.EditBook.Book_Shelves = Me.EditBook.Book_Shelves
            End If
        End If

    End Sub

    Protected Sub UnregisterCommands()
        SaveBookChangesCommand = Nothing
        CancelBookChangesCommand = Nothing
        SelectEditBookGridRowCommand = Nothing
        SelectEditBookWorkingGridCommand = Nothing
        SelectEditBookGridSortCommand = Nothing
        EditBookGridFilterChangedCommand = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If Not IsNothing(Me._editBook) Then
            RemoveHandler Me._editBook.PropertyChanged, AddressOf editBook_PropertyChanged
        End If
        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            Me.LibraryDataService.RefreshBookPropertiesCollectionViews()
        End If

        UnregisterCommands()
        UnregisterMessage()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If Not IsNothing(Me._editBook) Then
            RemoveHandler Me._editBook.PropertyChanged, AddressOf editBook_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessage()

    End Sub
#End Region

#Region "Private Methods : Save & Cancel"

    Private Sub OnSaveBookChanges()
        If Me.EditBook.HasValidationErrors Then
            ErrorWindow.CreateNew(New Exception("Submit operation failed validation"))
            Return
        End If
        If Me.HasChanges Then
            If IsNewBookEdited And EditBook.EntityState = EntityState.Detached Then
                Me.LibraryDataService.DomainContext.Books.Add(EditBook)
            Else
                UpdateBookLinkProperties()
            End If
            EditBook.Book_Quantity_Onhand = EditBook.Book_Quantity_Avail - _qtyOnMember
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveBookChanges, Nothing)
        End If
    End Sub

    Private Sub AfterSaveBookChanges(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedBookDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim savedBook As Book = Nothing

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "Add"
                For Each e As Object In result.ChangeSet.AddedEntities
                    If TypeOf (e) Is Book Then
                        savedBook = e
                        resultMessage &= e.Book_Title & " was added successfully." & vbCrLf
                        UpdateBookLinkProperties()
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Book Then
                        savedBook = e
                        resultMessage &= e.Book_Title & " was updated successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.RemovedEntities.Count > 0 Then
                action = "Delete"
                For Each e As Object In result.ChangeSet.RemovedEntities
                    If TypeOf (e) Is Book Then
                        savedBook = e
                        resultMessage &= e.Book_Title & " was deleted successfully." & vbCrLf
                    End If
                Next
            End If
            If _bookAuthorsChanged And action = "Update" Then
                _bookAuthorsChanged = False
                resultMessage &= "Authors of the book was updated successfully." & vbCrLf
            End If
            If _bookCategoriesChanged And action = "Update" Then
                _bookCategoriesChanged = False
                resultMessage &= "Categories of the book was updated successfully." & vbCrLf
            End If
            If _bookShelvesChanged And action = "Update" Then
                _bookShelvesChanged = False
                resultMessage &= "Shelves of the book was updated successfully." & vbCrLf
            End If
        End If
        If IsNothing(savedBook) Then
            savedBook = EditBook
        End If

        dialogMessage = New SavedBookDialogMessage(dialogType, action, resultMessage, savedBook)
        Messenger.[Default].Send(dialogMessage)

    End Sub

    Private Sub OnCancelBookChanges()
        Me.LibraryDataService.DomainContext.RejectChanges()
        'Keep Original edit book link properties
        _editBook.Book_Authors = originalBookAuthors
        _editBook.Book_Categories = originalBookCategories
        _editBook.Book_Shelves = originalBookShelves
    End Sub

    Private Sub UpdateBookLinkProperties()
        'Update Book Properties of Author, Category, and Shelf
        If _bookAuthorsChanged Then
            Dim author_ids = New List(Of Integer)
            For Each a In EditBook.Book_Authors
                author_ids.Add(a.Author_id)
            Next
            If EditBook.Book_id > 0 Then
                Me.LibraryDataService.UpdateAuthorsOfBook(EditBook.Book_id, author_ids)
            End If
        End If
        If _bookCategoriesChanged Then
            Dim category_ids = New List(Of Integer)
            For Each c In EditBook.Book_Categories
                category_ids.Add(c.Category_id)
            Next
            If EditBook.Book_id > 0 Then
                Me.LibraryDataService.UpdateCategoriesOfBook(EditBook.Book_id, category_ids)
            End If
        End If
        If _bookShelvesChanged Then
            Dim shelf_ids = New List(Of Integer)
            For Each s In EditBook.Book_Shelves
                shelf_ids.Add(s.Shelf_id)
            Next
            If EditBook.Book_id > 0 Then
                Me.LibraryDataService.UpdateShelvesOfBook(EditBook.Book_id, shelf_ids)
            End If
        End If
    End Sub

#End Region

#Region "Private Methods : Initialize Check Value for Books Link Grid"

    Private Sub InitializeCheckValueOfBookAuthors()
        If Not IsNothing(Me._editBook) Then
            If Not Me.LibraryDataService.BookAuthorCollectionLoader Is Nothing Then
                Me.LibraryDataService.BookAuthorCollectionLoader.SelectedList = Me._editBook.Book_Authors
            End If
        End If
    End Sub

    Private Sub InitializeCheckValueOfBookCategories()
        If Not IsNothing(Me._editBook) Then
            If Not Me.LibraryDataService.BookCategoryCollectionLoader Is Nothing Then
                Me.LibraryDataService.BookCategoryCollectionLoader.SelectedList = Me._editBook.Book_Categories
            End If
        End If
    End Sub

    Private Sub InitializeCheckValueOfBookShelves()
        If Not IsNothing(Me._editBook) Then
            If Not Me.LibraryDataService.BookShelfCollectionLoader Is Nothing Then
                Me.LibraryDataService.BookShelfCollectionLoader.SelectedList = Me._editBook.Book_Shelves
            End If
        End If
    End Sub

#End Region

#Region "Private Methods : Handling Event of DataGrid & Filter TextBox"

    'When Datagrid selection mode is "single"
    Private Sub OnSelectGridRow(sender As DataGrid)

        If Not (TypeOf (sender) Is DataGrid) Then
            Return
        End If
        If TypeOf (sender.SelectedItem) Is Author Then
            OnSelectAuthor(sender)
        ElseIf TypeOf (sender.SelectedItem) Is Category Then
            OnSelectCategory(sender)
        ElseIf TypeOf (sender.SelectedItem) Is Shelf Then
            OnSelectShelf(sender)
        Else
            Return
        End If

    End Sub

    Private Sub OnSelectAuthor(dataGrid As DataGrid)

        Dim selectedAuthor As Author = dataGrid.SelectedItem

        If (Not IsNothing(selectedAuthor)) And (Not SortingGridByChecked) Then

            Dim selectedAuthors As ObservableCollection(Of Author) = New ObservableCollection(Of Author)
            If selectedAuthor.IsChecked Then
                selectedAuthor.IsChecked = False
            Else
                selectedAuthor.IsChecked = True
            End If

            Dim orderedSelectedAuthors = From a In dataGrid.ItemsSource
                                         Order By a.Author_Full_Name
                                         Select a

            For Each a In orderedSelectedAuthors
                If a.IsChecked Then
                    selectedAuthors.Add(a)
                End If
            Next

            _editBook.Book_Authors = selectedAuthors

            'Sort by selected
            SortingGridByChecked = True
            Dim collectionView = CType(dataGrid.ItemsSource, DomainCollectionView)
            If collectionView.SortDescriptions.Count > 0 Then
                If collectionView.SortDescriptions(0).PropertyName = "IsChecked" Then
                    collectionView.SortDescriptions.Clear()
                    dataGrid.ItemsSource = orderedSelectedAuthors
                End If
            End If
            Dim selectedIndex = dataGrid.SelectedIndex
            Me.Book_Authors_Loader.SelectedList = selectedAuthors
            SortingGridByChecked = True
            dataGrid.SelectedIndex = selectedIndex
            dataGrid.ScrollIntoView(dataGrid.ItemsSource(selectedIndex), dataGrid.Columns.First())

        End If

        'change selected index to get mouse click event without selection change
        dataGrid.SelectedIndex = -1
        SortingGridByChecked = False

    End Sub

    Private Sub OnSelectCategory(dataGrid As DataGrid)

        Dim selectedCategory As Category = dataGrid.SelectedItem

        If (Not IsNothing(selectedCategory)) And (Not SortingGridByChecked) Then

            Dim selectedCategories As ObservableCollection(Of Category) = New ObservableCollection(Of Category)
            If selectedCategory.IsChecked Then
                selectedCategory.IsChecked = False
            Else
                selectedCategory.IsChecked = True
            End If

            Dim orderedSelectedCategories = From c In dataGrid.ItemsSource
                                            Order By c.Category_Name
                                            Select c

            For Each c In orderedSelectedCategories
                If c.IsChecked Then
                    selectedCategories.Add(c)
                End If
            Next

            _editBook.Book_Categories = selectedCategories

            'Sort by selected
            SortingGridByChecked = True
            Dim collectionView = CType(dataGrid.ItemsSource, DomainCollectionView)
            If collectionView.SortDescriptions.Count > 0 Then
                If collectionView.SortDescriptions(0).PropertyName = "IsChecked" Then
                    collectionView.SortDescriptions.Clear()
                    dataGrid.ItemsSource = orderedSelectedCategories
                End If
            End If
            Dim selectedIndex = dataGrid.SelectedIndex
            Me.Book_Categories_Loader.SelectedList = selectedCategories
            SortingGridByChecked = True
            dataGrid.SelectedIndex = selectedIndex
            dataGrid.ScrollIntoView(dataGrid.ItemsSource(selectedIndex), dataGrid.Columns.First())

        End If

        'change selected index to get mouse click event without selection change
        dataGrid.SelectedIndex = -1
        SortingGridByChecked = False

    End Sub

    Private Sub OnSelectShelf(dataGrid As DataGrid)

        Dim selectedShelf As Shelf = dataGrid.SelectedItem

        If (Not IsNothing(selectedShelf)) And (Not SortingGridByChecked) Then

            Dim selectedShelves As ObservableCollection(Of Shelf) = New ObservableCollection(Of Shelf)
            If selectedShelf.IsChecked Then
                selectedShelf.IsChecked = False
            Else
                selectedShelf.IsChecked = True
            End If

            Dim orderedSelectedShelves = From s In dataGrid.ItemsSource
                                         Order By s.Shelf_Description
                                         Select s

            For Each s In orderedSelectedShelves
                If s.IsChecked Then
                    selectedShelves.Add(s)
                End If
            Next

            _editBook.Book_Shelves = selectedShelves

            'Sort by selected
            SortingGridByChecked = True
            Dim collectionView = CType(dataGrid.ItemsSource, DomainCollectionView)
            If collectionView.SortDescriptions.Count > 0 Then
                If collectionView.SortDescriptions(0).PropertyName = "IsChecked" Then
                    collectionView.SortDescriptions.Clear()
                    dataGrid.ItemsSource = orderedSelectedShelves
                End If
            End If
            Dim selectedIndex = dataGrid.SelectedIndex
            Me.Book_Shelves_Loader.SelectedList = selectedShelves
            SortingGridByChecked = True
            dataGrid.SelectedIndex = selectedIndex
            dataGrid.ScrollIntoView(dataGrid.ItemsSource(selectedIndex), dataGrid.Columns.First())

        End If

        'change selected index to get mouse click event without selection change
        dataGrid.SelectedIndex = -1
        SortingGridByChecked = False

    End Sub

    Public Sub OnSelectWorkingGrid(sender As DataGrid)

        If Not (TypeOf (sender) Is DataGrid) Then
            Return
        End If
        If sender.Name = "BookAuthorDataGrid" Then
            If Not (_workingDataGrid.Name = "BookAuthorDataGrid") Then
                _workingDataGrid.Visibility = Visibility.Collapsed
                _workingDataGrid = sender
                _workingDataGrid.Visibility = Visibility.Visible
                BookPropertiesFilter = Me.Book_Authors_Loader.Filter
            End If
        ElseIf sender.Name = "BookCategoryDataGrid" Then
            If Not (_workingDataGrid.Name = "BookCategoryDataGrid") Then
                _workingDataGrid.Visibility = Visibility.Collapsed
                _workingDataGrid = sender
                _workingDataGrid.Visibility = Visibility.Visible
                BookPropertiesFilter = Me.Book_Categories_Loader.Filter
            End If
        ElseIf sender.Name = "BookShelfDataGrid" Then
            If Not (_workingDataGrid.Name = "BookShelfDataGrid") Then
                _workingDataGrid.Visibility = Visibility.Collapsed
                _workingDataGrid = sender
                _workingDataGrid.Visibility = Visibility.Visible
                BookPropertiesFilter = Me.Book_Shelves_Loader.Filter
            End If
        Else
            Return
        End If

    End Sub

    Private Sub OnSelectGridSort(e As MouseButtonEventArgs)

        If Not (TypeOf (e) Is MouseButtonEventArgs) Then
            Return
        End If
        If IsNothing(_workingDataGrid) Then
            Return
        End If

        'Dim header As DataGridColumnHeader = VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(Me), Me).OfType(Of DataGridColumnHeader)().SingleOrDefault()
        Dim fe As FrameworkElement = DirectCast(e.OriginalSource, FrameworkElement)
        Dim column As DataGridColumn = DirectCast(DataGridColumn.GetColumnContainingElement(fe), DataGridColumn)

        Dim collectionView = CType(_workingDataGrid.ItemsSource, DomainCollectionView)

        If Not (TypeOf (column) Is DataGridTextColumn) Then
            e.Handled = True
            collectionView.SortDescriptions.Clear()
            collectionView.Refresh()
        ElseIf column.Header <> "Authors" And column.Header <> "Categories" And column.Header <> "Shelves" Then
            e.Handled = True
            collectionView.SortDescriptions.Clear()
            collectionView.Refresh()
        End If

    End Sub

    Private Sub OnGridFilterChanged(filter As TextBox)

        If _workingDataGrid.Name = "BookAuthorDataGrid" Then
            Me.Book_Authors_Loader.Filter = filter.Text
            Me.Book_Authors.Refresh()
        ElseIf _workingDataGrid.Name = "BookCategoryDataGrid" Then
            Me.Book_Categories_Loader.Filter = filter.Text
            Me.Book_Categories.Refresh()
        ElseIf _workingDataGrid.Name = "BookShelfDataGrid" Then
            Me.Book_Shelves_Loader.Filter = filter.Text
            Me.Book_Shelves.Refresh()
        End If

    End Sub

#End Region

End Class
