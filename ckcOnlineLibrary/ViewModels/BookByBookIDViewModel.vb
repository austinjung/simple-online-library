﻿Imports Papa.Common
Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports System.Collections.ObjectModel

Public Class BookByBookIDViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'Book
    Private _book_id As Integer = -1
    Private _book As Books_With_Statistics
    Private _book_Authors As ObservableCollection(Of Author)
    Private _book_Categories As ObservableCollection(Of Category)
    Private _book_Shelves As ObservableCollection(Of Shelf)

    'Book Collection views
    Private _books As DomainCollectionView(Of Books_With_Statistics)
    Private _books_Source As EntityList(Of Books_With_Statistics)
    Private _books_Loader As DomainCollectionViewLoader(Of Books_With_Statistics)

    'Queue of Book Properties Collection Load Query
    Private bookPropertiesLoadQueue As Queue(Of EntityQuery) = New Queue(Of EntityQuery)
    Private _bookProperties As DomainCollectionModels = New DomainCollectionModels()

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            If LibraryDataService.DomainContext.Authors.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Authors.HasChanges
                Me.RaisePropertyChanged("BookAuthors")
            ElseIf LibraryDataService.DomainContext.Books_With_Statistics.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Books.HasChanges
                Me.RaisePropertyChanged("Books")
            ElseIf LibraryDataService.DomainContext.Categories.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Categories.HasChanges
                Me.RaisePropertyChanged("BookCategories")
            ElseIf LibraryDataService.DomainContext.Shelfs.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Shelfs.HasChanges
                Me.RaisePropertyChanged("BookShelves")
            End If
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Book As Books_With_Statistics
        Get
            Return _book
        End Get
    End Property

    Public ReadOnly Property Book_Authors As ObservableCollection(Of Author)
        Get
            Return _book_Authors
        End Get
    End Property

    Public ReadOnly Property Book_Categories As ObservableCollection(Of Category)
        Get
            Return _book_Categories
        End Get
    End Property

    Public ReadOnly Property Book_Shelves As ObservableCollection(Of Shelf)
        Get
            Return _book_Shelves
        End Get
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService, book_id As Integer)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService

        Me._book_id = book_id

        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        loadBook()

    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        Me._book_id = -1
        Me._books = Nothing
        Me._bookProperties = New DomainCollectionModels()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

    End Sub
#End Region

#Region "Private Methods : Load Data"

    Private Sub loadBook()

        'Initialize Entity Queries Dictionary with a Default Query for each Entity of Book Properties
        Dim author_source = New EntityList(Of Author)(Me.LibraryDataService.DomainContext.Authors)
        Dim author_loader = New DomainCollectionViewLoader(Of Author)(
                                        AddressOf Me.LoadBookProperties,
                                        AddressOf Me.OnLoadBookPropertiesCompleted)
        Dim author_view = New DomainCollectionView(Of Author)(author_loader, author_source)
        Me._bookProperties(GetType(Author)) = New DomainCollectionModel("Authors", GetType(Author),
                                               author_view, author_source, author_loader)

        Dim category_source = New EntityList(Of Category)(Me.LibraryDataService.DomainContext.Categories)
        Dim category_loader = New DomainCollectionViewLoader(Of Category)(
                                        AddressOf Me.LoadBookProperties,
                                        AddressOf Me.OnLoadBookPropertiesCompleted)
        Dim category_view = New DomainCollectionView(Of Category)(category_loader, category_source)
        Me._bookProperties(GetType(Category)) = New DomainCollectionModel("Categories", GetType(Category),
                                               category_view, category_source, category_loader)

        Dim shelf_source = New EntityList(Of Shelf)(Me.LibraryDataService.DomainContext.Shelfs)
        Dim shelf_loader = New DomainCollectionViewLoader(Of Shelf)(
                                        AddressOf Me.LoadBookProperties,
                                        AddressOf Me.OnLoadBookPropertiesCompleted)
        Dim shelf_view = New DomainCollectionView(Of Shelf)(shelf_loader, shelf_source)
        Me._bookProperties(GetType(Shelf)) = New DomainCollectionModel("Shelves", GetType(Shelf),
                                               shelf_view, shelf_source, shelf_loader)

        'Initialize Book Collection
        Me._books_Source = New EntityList(Of Books_With_Statistics)(Me.LibraryDataService.DomainContext.Books_With_Statistics)
        Me._books_Loader = New DomainCollectionViewLoader(Of Books_With_Statistics)(
                                        AddressOf Me.LoadBookEntities,
                                        AddressOf Me.OnLoadBookEntitiesCompleted)
        Me._books = New DomainCollectionView(Of Books_With_Statistics)(_books_Loader, _books_Source)

        'Load Book
        Me._books.CollectionViewLoader.Load(Nothing)

    End Sub

    Private Sub LoadCategoriesOfBook(ByVal book_id As Integer)
        If Me._bookProperties.ContainsKey(GetType(Category)) Then
            Me._bookProperties(GetType(Category)).Query = Me.LibraryDataService.DomainContext.GetCategories_Of_BookQuery(book_id)
            Me._bookProperties(GetType(Category)).Loader.Load(Nothing)
        End If
    End Sub

    Private Sub LoadAuthorsOfBook(ByVal book_id As Integer)
        If Me._bookProperties.ContainsKey(GetType(Author)) Then
            Me._bookProperties(GetType(Author)).Query = Me.LibraryDataService.DomainContext.GetAuthors_Of_BookQuery(book_id)
            Me._bookProperties(GetType(Author)).Loader.Load(Nothing)
        End If
    End Sub

    Private Sub LoadShelvesOfBook(ByVal book_id As Integer)
        If Me._bookProperties.ContainsKey(GetType(Shelf)) Then
            Me._bookProperties(GetType(Shelf)).Query = Me.LibraryDataService.DomainContext.GetShelves_Of_BookQuery(book_id)
            Me._bookProperties(GetType(Shelf)).Loader.Load(Nothing)
        End If
    End Sub

    Private Function LoadBookEntities() As LoadOperation(Of Books_With_Statistics)
        Me.CanLoad = False
        Dim query As EntityQuery(Of Books_With_Statistics) = Me.LibraryDataService.DomainContext.GetBook_With_Statics_By_Book_IDQuery(Me._book_id)
        Return Me.LibraryDataService.DomainContext.Load(query.SortAndPageBy(Me._books))
    End Function

    Private Sub OnLoadBookEntitiesCompleted(op As LoadOperation(Of Books_With_Statistics))
        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Me._books_Source.Source = op.Entities
            If op.TotalEntityCount <> -1 Then
                Me._books.SetTotalItemCount(op.TotalEntityCount)
            End If
            If op.Entities.Count > 0 Then
                Me._book = op.Entities(0) ' get first and default
                Me.RaisePropertyChanged("BookLoaded")
                Me.LoadAuthorsOfBook(Me._book.Book_id)
                Me.LoadCategoriesOfBook(Me._book.Book_id)
                Me.LoadShelvesOfBook(Me._book.Book_id)
            End If
        End If
    End Sub

    Private Function LoadBookProperties(Of T As Entity)() As LoadOperation(Of T)

        Dim collectionModel As DomainCollectionModel = CType(Me._bookProperties(GetType(T)), DomainCollectionModel)
        Dim entityQuery As EntityQuery(Of T) = collectionModel.Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        Me.CanLoad = False
        Return Me.LibraryDataService.DomainContext.Load(entityQuery.SortAndPageBy(collectionModel.View))

    End Function

    Private Sub OnLoadBookPropertiesCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            ''Austin Jung : Error Correction
            Try
                Dim domainCollectionModel As DomainCollectionModel =
                        CType(Me._bookProperties(GetType(T)), DomainCollectionModel)
                CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
                If op.AllEntities.Count <> -1 Then
                    If op.TotalEntityCount <> -1 Then
                        CType(Me._bookProperties(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                    End If

                    Dim book_id = op.EntityQuery.Parameters("book_id")
                    If book_id = Me._book.Book_id Then
                        If GetType(T) Is GetType(Category) Then
                            Dim bookCategories = New ObservableCollection(Of Category)
                            For Each c As Category In CType(domainCollectionModel.Source, EntityList(Of Category)).Source
                                bookCategories.Add(c)
                            Next
                            Me._book_Categories = bookCategories
                        ElseIf GetType(T) Is GetType(Author) Then
                            Dim bookAuthors = New ObservableCollection(Of Author)
                            For Each a As Author In CType(domainCollectionModel.Source, EntityList(Of Author)).Source
                                bookAuthors.Add(a)
                            Next
                            Me._book_Authors = bookAuthors
                        ElseIf GetType(T) Is GetType(Shelf) Then
                            Dim bookShelves = New ObservableCollection(Of Shelf)
                            For Each s As Shelf In CType(domainCollectionModel.Source, EntityList(Of Shelf)).Source
                                bookShelves.Add(s)
                            Next
                            Me._book_Shelves = bookShelves
                        End If
                        CType(Me._bookProperties(GetType(T)), DomainCollectionModel).View.MoveCurrentToFirst()
                    End If
                End If
            Catch ex As Exception
                'do nothing
            End Try
        Else
            'If query was cancelled, do it again to get all books' properties
            bookPropertiesLoadQueue.Enqueue(op.EntityQuery)
        End If

        'If there are pending query of book properties, do it
        If bookPropertiesLoadQueue.Count > 0 Then
            Dim entityQuery As EntityQuery = bookPropertiesLoadQueue.Dequeue()
            If TypeOf (entityQuery) Is EntityQuery(Of Author) Then
                Me._bookProperties(GetType(Author)).Query = entityQuery
                Me._bookProperties(GetType(Author)).Loader.Load(Nothing)
            ElseIf TypeOf (entityQuery) Is EntityQuery(Of Category) Then
                Me._bookProperties(GetType(Category)).Query = entityQuery
                Me._bookProperties(GetType(Category)).Loader.Load(Nothing)
            ElseIf TypeOf (entityQuery) Is EntityQuery(Of Shelf) Then
                Me._bookProperties(GetType(Shelf)).Query = entityQuery
                Me._bookProperties(GetType(Shelf)).Loader.Load(Nothing)
            End If
        End If

    End Sub

#End Region

End Class
