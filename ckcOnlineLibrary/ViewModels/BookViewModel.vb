﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Linq
Imports Microsoft.Windows.Data.DomainServices
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common

Public Class BookViewModel
    Inherits ViewModel

    Private Const _pageSize As Integer = 10

    Private _pageIndex As Integer

    Protected Property PageConductor() As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = Value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property BookDataService() As IBookDataService
        Get
            Return m_BookDataService
        End Get
        Set(value As IBookDataService)
            m_BookDataService = Value
        End Set
    End Property
    Private m_BookDataService As IBookDataService

    Public Property ClearFilterCommand() As RelayCommand
        Get
            Return m_ClearFilterCommand
        End Get
        Set(value As RelayCommand)
            m_ClearFilterCommand = Value
        End Set
    End Property
    Private m_ClearFilterCommand As RelayCommand

    Public Property EditBookCommand() As RelayCommand
        Get
            Return m_EditBookCommand
        End Get
        Set(value As RelayCommand)
            m_EditBookCommand = Value
        End Set
    End Property
    Private m_EditBookCommand As RelayCommand

    Public Property LoadMoreBooksCommand() As RelayCommand
        Get
            Return m_LoadMoreBooksCommand
        End Get
        Set(value As RelayCommand)
            m_LoadMoreBooksCommand = Value
        End Set
    End Property
    Private m_LoadMoreBooksCommand As RelayCommand
    Public Property SaveBooksCommand() As RelayCommand
        Get
            Return m_SaveBooksCommand
        End Get
        Set(value As RelayCommand)
            m_SaveBooksCommand = Value
        End Set
    End Property
    Private m_SaveBooksCommand As RelayCommand

    Public Property SearchBooksCommand() As RelayCommand
        Get
            Return m_SearchBooksCommand
        End Get
        Set(value As RelayCommand)
            m_SearchBooksCommand = Value
        End Set
    End Property
    Private m_SearchBooksCommand As RelayCommand

    Public Property SelectCategoryCommand() As RelayCommand
        Get
            Return m_SelectCategoryCommand
        End Get
        Set(value As RelayCommand)
            m_SelectCategoryCommand = Value
        End Set
    End Property
    Private m_SelectCategoryCommand As RelayCommand

    Private _books As ICollection(Of Book)
    Public Property Books() As ICollection(Of Book)
        Get
            Return _books
        End Get
        Set(value As ICollection(Of Book))
            _books = value
            RaisePropertyChanged("Books")
        End Set
    End Property

    'Private _booksOfTheDay As IEnumerable(Of BookOfDay)
    'Public Property BooksOfTheDay() As IEnumerable(Of BookOfDay)
    '    Get
    '        Return _booksOfTheDay
    '    End Get
    '    Set(value As IEnumerable(Of BookOfDay))
    '        _booksOfTheDay = value
    '        RaisePropertyChanged("BooksOfTheDay")
    '    End Set
    'End Property

    Private _authors As IEnumerable(Of Author)
    Public Property Authors() As IEnumerable(Of Author)
        Get
            Return _authors
        End Get
        Set(value As IEnumerable(Of Author))
            _authors = value
            RaisePropertyChanged("Authors")
        End Set
    End Property

    Private _categories As IEnumerable(Of Category)
    Public Property Categories() As IEnumerable(Of Category)
        Get
            Return _categories
        End Get
        Set(value As IEnumerable(Of Category))
            _categories = value
            RaisePropertyChanged("Categories")
        End Set
    End Property

    Private _hasChanges As Boolean
    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Set(value As Boolean)
            _hasChanges = value
            RaisePropertyChanged("HasChanges")
        End Set
    End Property

    Private _selectedBook As Book
    Public Property SelectedBook() As Book
        Get
            Return _selectedBook
        End Get
        Set(value As Book)
            _selectedBook = value
            RaisePropertyChanged("SelectedBook")
        End Set
    End Property

    Private _selectedCategory As Category
    Public Property SelectedCategory() As Category
        Get
            Return _selectedCategory
        End Get
        Set(value As Category)
            _selectedCategory = value
            RaisePropertyChanged("SelectedCategory")
        End Set
    End Property

    Private _titleFilter As String
    Public Property TitleFilter() As String
        Get
            Return _titleFilter
        End Get
        Set(value As String)
            _titleFilter = value
            RaisePropertyChanged("TitleFilter")
        End Set
    End Property

    Public Sub New(pageConductor As IPageConductor, bookDataService As IBookDataService)
        Me.PageConductor = pageConductor
        Me.BookDataService = bookDataService
        AddHandler Me.BookDataService.EntityContainer.PropertyChanged, AddressOf BookDataService_PropertyChanged

        RegisterCommands()
        LoadData()
    End Sub

    Private Sub BookDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            HasChanges = BookDataService.EntityContainer.HasChanges
            SaveBooksCommand.RaiseCanExecuteChanged()
        End If
    End Sub

    Protected Overrides Sub RegisterCommands()
        ClearFilterCommand = New RelayCommand(AddressOf OnClearFilter)
        EditBookCommand = New RelayCommand(AddressOf OnEditBook)
        LoadMoreBooksCommand = New RelayCommand(AddressOf OnLoadMoreBooksByCategory)
        SaveBooksCommand = New RelayCommand(AddressOf OnSaveBooks, Function() HasChanges)
        SearchBooksCommand = New RelayCommand(AddressOf OnSearchBooks)
        SelectCategoryCommand = New RelayCommand(AddressOf OnSelectCategory)
    End Sub

    Public Overrides Sub LoadData()
        'LoadBookOfDays()
        LoadAuthors()
        LoadCategories()
        LoadBooksByCategory()
    End Sub

    Private Sub OnClearFilter()
        TitleFilter = String.Empty
        LoadBooksByCategory()
    End Sub

    Private Sub OnEditBook()
        Messenger.Default.Send(New LaunchEditBookMessage() With { _
         .Book = SelectedBook _
        })
    End Sub

    Private Sub OnSaveBooks()
        BookDataService.SubmitChanges(CType(AddressOf SaveBooksCallback, Action(Of ServiceSubmitChangesResult)), Nothing)
        'BookDataService.SubmitChanges(AddressOf SaveBooksCallback, Nothing)
    End Sub

    Private Sub SaveBooksCallback(result As ServiceSubmitChangesResult) 
        Dim msg As String = If((result.Error IsNot Nothing), "Save was unsuccessful", "Save was successful")
        Dim dialogMessage = New SavedBookDialogMessage(msg, "Save")
        Messenger.Default.Send(dialogMessage)
    End Sub

    Private Sub OnSearchBooks()
        If Not String.IsNullOrEmpty(TitleFilter) Then
            LoadBooksByTitle()
        End If
    End Sub

    Private Sub OnSelectCategory()
        If SelectedCategory IsNot Nothing Then
            LoadBooksByCategory()
        End If
    End Sub

    ''Public Sub LoadBookOfDays()
    ''    BooksOfTheDay = Nothing
    ''    BookDataService.LoadBooksOfTheDay(AddressOf LoadBooksOfTheDayCallback, Nothing)
    ''End Sub

    'Private Sub LoadBooksOfTheDayCallback(result As ServiceLoadResult(Of BookOfDay))
    '    ' handle error
    '    If result.[Error] IsNot Nothing Then
    '    ElseIf Not result.Cancelled Then
    '        Me.BooksOfTheDay = result.Entities
    '    End If
    'End Sub

    Public Sub LoadAuthors()
        Authors = Nothing
        BookDataService.LoadAuthors(AddressOf LoadAuthorsCallback, Nothing)
    End Sub

    Private Sub LoadAuthorsCallback(result As ServiceLoadResult(Of Author))
        ' handle error
        If result.Error IsNot Nothing Then
        ElseIf Not result.Cancelled Then
            Me.Authors = result.Entities
            'Me.SelectedCategory = Categories.FirstOrDefault()
            'LoadBooksByCategory()
        End If
    End Sub

    Public Sub LoadCategories()
        Categories = Nothing
        BookDataService.LoadCategories(AddressOf LoadCategoriesCallback, Nothing)
    End Sub

    Private Sub LoadCategoriesCallback(result As ServiceLoadResult(Of Category))
        ' handle error
        If result.Error IsNot Nothing Then
        ElseIf Not result.Cancelled Then
            Me.Categories = result.Entities
            'Me.SelectedCategory = Categories.FirstOrDefault()
            'LoadBooksByCategory()
        End If
    End Sub

    Public Sub LoadBooksByCategory()
        _pageIndex = 0
        Books = Nothing
        If SelectedCategory IsNot Nothing Then
            BookDataService.LoadBooksByCategory(SelectedCategory.Category_id, New QueryBuilder(Of Book)().Take(_pageSize), AddressOf LoadBooksCallback, Nothing)
        Else
            BookDataService.LoadBooksByCategory(1, New QueryBuilder(Of Book)().Take(_pageSize), AddressOf LoadBooksCallback, Nothing)
        End If
    End Sub

    Private Sub OnLoadMoreBooksByCategory()
        If SelectedCategory IsNot Nothing Then
            _pageIndex += 1
            BookDataService.LoadBooksByCategory(SelectedCategory.Category_id, New QueryBuilder(Of Book)().Skip(_pageIndex * _pageSize).Take(_pageSize), AddressOf LoadBooksCallback, Nothing)
        End If
    End Sub
    Public Sub LoadBooksByTitle()
        _pageIndex = 0
        Books = Nothing
        If SelectedCategory IsNot Nothing Then
            BookDataService.LoadBooksByCategory(SelectedCategory.Category_id, New QueryBuilder(Of Book)().Where(Function(b) b.Book_Title.Contains(TitleFilter)), AddressOf LoadBooksCallback, Nothing)
        End If
    End Sub

    Private Sub LoadBooksCallback(result As ServiceLoadResult(Of Book))
        ' handle error
        If result.Error IsNot Nothing Then
        ElseIf Not result.Cancelled Then
            If Books Is Nothing Then
                Books = TryCast(result.Entities, ICollection(Of Book))
            Else
                For Each book In result.Entities
                    Books.Add(book)
                Next
            End If

            SelectedBook = Books.FirstOrDefault()
        End If
    End Sub

End Class
