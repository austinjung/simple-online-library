﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class EditShelfViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'Book Properties to edit
    Private _editShelf As Shelf = Nothing
    Private _shelfBeforeEdit As Shelf = Nothing

    Private _shelves As List(Of Shelf) = Nothing
    Private _shelvesVisibility As Visibility = Visibility.Collapsed

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False
    Private _isNewShelfEdited As Boolean = True
    Private _isChecked As Boolean = False
    Private _isCheckedVisibility As Visibility = Visibility.Collapsed

    'Commands for save & cancel
    Private m_SaveShelfChangesCommand As RelayCommand
    Private m_CancelShelfChangesCommand As RelayCommand
    Private m_SelectedShelfChangedCommand As RelayCommand(Of DataGrid)

#End Region

#Region "Private Method : Property changed event"

    Private Sub editShelf_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        Select Case e.PropertyName
            Case "Shelf_Code"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveShelfChangesCommand.RaiseCanExecuteChanged()
                End If
            Case "Shelf_Description"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveShelfChangesCommand.RaiseCanExecuteChanged()
                End If
            Case "IsActive"
                If Not Me.HasChanges Then
                    Me.HasChanges = True
                    SaveShelfChangesCommand.RaiseCanExecuteChanged()
                End If
        End Select
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"


    Public Property EditShelf() As Shelf
        Get
            Return _editShelf
        End Get
        Set(value As Shelf)
            _editShelf = value
            If IsNothing(value) Then
                IsNewShelfEdited = False
            ElseIf _editShelf.Shelf_id > 0 Then
                IsNewShelfEdited = False
            Else
                IsNewShelfEdited = True
            End If
            SaveShelfBeforeEdit()
            Me.RaisePropertyChanged("EditShelf")
        End Set
    End Property

    Public Property IsNewShelfEdited As Boolean
        Get
            Return _isNewShelfEdited
        End Get
        Set(value As Boolean)
            _isNewShelfEdited = value
            If value Then
                ShelvesVisibility = Visibility.Collapsed
            Else
                ShelvesVisibility = Visibility.Visible
            End If
        End Set
    End Property

    Public Property Shelves As List(Of Shelf)
        Get
            Return _shelves
        End Get
        Set(value As List(Of Shelf))
            _shelves = value
        End Set
    End Property

    Public Property ShelvesVisibility As Visibility
        Get
            Return _shelvesVisibility
        End Get
        Set(value As Visibility)
            _shelvesVisibility = value
            If value = Visibility.Visible Then
                IsCheckedVisibility = Visibility.Collapsed
            Else
                IsCheckedVisibility = Visibility.Visible
            End If
            Me.RaisePropertyChanged("ShelvesVisibility")
        End Set
    End Property

    Public Property IsCheckedVisibility As Visibility
        Get
            Return _isCheckedVisibility
        End Get
        Set(value As Visibility)
            _isCheckedVisibility = value
            Me.RaisePropertyChanged("IsCheckedVisibility")
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   shelves As List(Of Shelf),
                   editShelf As Shelf)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        Me.Shelves = shelves
        Me.EditShelf = editShelf

        ''''''''''''''''''''''''''''
        'Don't refresh Domain Data Context, 
        'Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''

        'Set Property Changed Event Handler

        If Not IsNothing(Me.EditShelf) Then
            AddHandler Me._editShelf.PropertyChanged, AddressOf editShelf_PropertyChanged
        End If

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SaveShelfChangesCommand() As RelayCommand
        Get
            Return m_SaveShelfChangesCommand
        End Get
        Set(value As RelayCommand)
            m_SaveShelfChangesCommand = value
        End Set
    End Property

    Public Property CancelShelfChangesCommand() As RelayCommand
        Get
            Return m_CancelShelfChangesCommand
        End Get
        Set(value As RelayCommand)
            m_CancelShelfChangesCommand = value
        End Set
    End Property

    Public Property SeletedShelfChangedCommand As RelayCommand(Of DataGrid)
        Get
            Return m_SelectedShelfChangedCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_SelectedShelfChangedCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SaveShelfChangesCommand = New RelayCommand(AddressOf OnSaveShelfChanges, Function() HasChanges)
        CancelShelfChangesCommand = New RelayCommand(AddressOf OnCancelShelfChanges)
        SeletedShelfChangedCommand = New RelayCommand(Of DataGrid)(AddressOf OnSelectedShelfChanged)
    End Sub

    Protected Sub UnregisterCommands()
        SaveShelfChangesCommand = Nothing
        CancelShelfChangesCommand = Nothing
        SeletedShelfChangedCommand = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If Not IsNothing(Me._editShelf) Then
            RemoveHandler Me._editShelf.PropertyChanged, AddressOf editShelf_PropertyChanged
        End If

        UnregisterCommands()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If Not IsNothing(Me._editShelf) Then
            RemoveHandler Me._editShelf.PropertyChanged, AddressOf editShelf_PropertyChanged
        End If

        UnregisterCommands()

    End Sub
#End Region

#Region "Private Methods : Save & Cancel"

    Private Sub OnSaveShelfChanges()
        If Me.EditShelf.HasValidationErrors Then
            Return
        End If
        If Me.HasChanges Then
            _isChecked = _editShelf.IsChecked
            If IsNewShelfEdited Then
                Me.LibraryDataService.DomainContext.Shelfs.Add(EditShelf)
            Else
                Dim shelf = (From s In Me.LibraryDataService.DomainContext.Shelfs
                             Where s.Shelf_id = EditShelf.Shelf_id
                             Select s).FirstOrDefault
                shelf.Shelf_Code = EditShelf.Shelf_Code
                shelf.Shelf_Description = EditShelf.Shelf_Description
                shelf.IsActive = EditShelf.IsActive
            End If
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveShelfChanges, Nothing)
        End If
    End Sub

    Private Sub AfterSaveShelfChanges(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedShelfDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim savedShelf As Shelf = Nothing

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "Add"
                For Each e As Object In result.ChangeSet.AddedEntities
                    If TypeOf (e) Is Shelf Then
                        savedShelf = CType(e, Shelf)
                        resultMessage &= savedShelf.Shelf_Code & " was added successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Shelf Then
                        savedShelf = CType(e, Shelf)
                        resultMessage &= savedShelf.Shelf_Code & " was updated successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.RemovedEntities.Count > 0 Then
                action = "Delete"
                For Each e As Object In result.ChangeSet.RemovedEntities
                    If TypeOf (e) Is Shelf Then
                        savedShelf = CType(e, Shelf)
                        resultMessage &= savedShelf.Shelf_Code & " was deleted successfully." & vbCrLf
                    End If
                Next
            End If
            savedShelf.IsChecked = _isChecked
        End If

        dialogMessage = New SavedShelfDialogMessage(dialogType, action, resultMessage, savedShelf)
        Messenger.[Default].Send(dialogMessage)
        Dim myMessageBox = New MyMessageBox(dialogMessage.Type, dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button)
        myMessageBox.Show()

    End Sub

    Private Sub OnCancelShelfChanges()
        RestoreShelfBeforeEdit()
        Me.LibraryDataService.DomainContext.RejectChanges()
    End Sub

    Private Sub OnSelectedShelfChanged(sender As DataGrid)
        If Not (TypeOf (sender) Is DataGrid) Then
            Return
        End If
        If Not IsNothing(Me.EditShelf) Then
            RemoveHandler Me._editShelf.PropertyChanged, AddressOf editShelf_PropertyChanged
        End If
        EditShelf = sender.SelectedItem
        If Not IsNothing(Me.EditShelf) Then
            AddHandler Me._editShelf.PropertyChanged, AddressOf editShelf_PropertyChanged
        End If
    End Sub

    Private Sub SaveShelfBeforeEdit()
        If Not IsNothing(EditShelf) Then
            _shelfBeforeEdit = New Shelf() With {
                                            .Shelf_Code = EditShelf.Shelf_Code,
                                            .Shelf_Description = EditShelf.Shelf_Description,
                                            .IsActive = EditShelf.IsActive,
                                            .IsChecked = EditShelf.IsChecked
                                            }
        Else
            _shelfBeforeEdit = Nothing
        End If
    End Sub

    Private Sub RestoreShelfBeforeEdit()
        If Not IsNothing(_shelfBeforeEdit) Then
            EditShelf.Shelf_Code = _shelfBeforeEdit.Shelf_Code
            EditShelf.Shelf_Description = _shelfBeforeEdit.Shelf_Description
            EditShelf.IsChecked = _shelfBeforeEdit.IsChecked
            EditShelf.IsActive = _shelfBeforeEdit.IsActive
        End If
    End Sub

#End Region

End Class
