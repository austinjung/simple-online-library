﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common

Public Class ManageBooksViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"
    Private ReadOnly _books As DomainCollectionView(Of Book)
    Private ReadOnly _categories As DomainCollectionView(Of Category)
    Private ReadOnly _authors As DomainCollectionView(Of Author)
    Private ReadOnly _shelves As DomainCollectionView(Of Shelf)
    Private _book_Authors As DomainCollectionView(Of Author)
    Private _book_Categories As DomainCollectionView(Of Category)
    Private _book_Shelves As DomainCollectionView(Of Shelf)

    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean
    Private _enableEditSelectedBook As Boolean = False

    Private _editedBook As Book
    Private _isNewBookEdited As Boolean
    Private _selectedBook As Book
    Private _selectedCategories As List(Of Category) = New List(Of Category)
    Private _selectedAuthors As List(Of Author) = New List(Of Author)

    Private m_SelectCategoryCommand As RelayCommand
    Private m_SelectBookCommand As RelayCommand
    Private m_SearchBooksCommand As RelayCommand
    Private m_ClearFilterCommand As RelayCommand
    Private m_EditSelectedBookCommand As RelayCommand
    Private m_CreateNewBookCommand As RelayCommand
    Private m_SaveBookChangesCommand As RelayCommand
    Private m_CancelBookChangesCommand As RelayCommand
#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            HasChanges = LibraryDataService.DomainContext.EntityContainer.HasChanges
            'Me.RaisePropertyChanged("Books")
            '''''''''''''''''''''''''''''''''
            'SaveBooksCommand.RaiseCanExecuteChanged()
        End If
    End Sub

    Private Sub selectedBook_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "HasChanges" Then
            'Me.HasChanges = True
            'Me.RaisePropertyChanged("SelectedBook")
        End If
    End Sub

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Authors As ICollectionView
        Get
            Return Me._authors
        End Get
    End Property

    Public ReadOnly Property Book_Authors As ICollectionView
        Get
            Me._book_Authors = LibraryDataService.BookAuthorCollectionView
            InitializeCheckBookAuthorsConverter(EditedBook)
            Me._book_Authors.Refresh()
            Return Me._book_Authors
        End Get
    End Property

    Public ReadOnly Property Book_Authors_Loader As StaticAutoCompleteViewLoader(Of Author)
        Get
            Return Me.LibraryDataService.BookAuthorCollectionLoader
        End Get
    End Property

    Public ReadOnly Property Books As ICollectionView
        Get
            Return Me._books
        End Get
    End Property

    Public ReadOnly Property Categories As ICollectionView
        Get
            Return Me._categories
        End Get
    End Property

    Public ReadOnly Property Book_Categories As ICollectionView
        Get
            If Me._book_Categories Is Nothing Then
                Me._book_Categories = LibraryDataService.BookCategoryCollectionView
            End If
            Me._book_Categories.Refresh()
            Return Me._book_Categories
        End Get
    End Property

    Public Property EditedBook() As Book
        Get
            Return _editedBook
        End Get
        Set(value As Book)
            _editedBook = value
        End Set
    End Property

    Public Property IsNewBookEdited As Boolean
        Get
            Return _isNewBookEdited
        End Get
        Set(value As Boolean)
            _isNewBookEdited = value
        End Set
    End Property

    Public Property SelectedBook() As Book
        Get
            Return _selectedBook
        End Get
        Set(value As Book)
            _selectedBook = value
        End Set
    End Property

    Public Property SelectedCategories As List(Of Category)
        Get
            Return _selectedCategories
        End Get
        Set(value As List(Of Category))
            _selectedCategories = value
            LoadBooksByCategories()
        End Set
    End Property

#End Region

#Region "Status Properties & Value Converters of Book Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Set(value As Boolean)
            _hasChanges = value
            RaisePropertyChanged("HasChanges")
        End Set
    End Property

    Public Property EnableEditSelectedBook() As Boolean
        Get
            Return _enableEditSelectedBook
        End Get
        Set(value As Boolean)
            _enableEditSelectedBook = value
            RaisePropertyChanged("EnableEditSelectedBook")
        End Set
    End Property

#End Region

#Region "Commands Properties"

    Public Property SelectCategoryCommand() As RelayCommand
        Get
            Return m_SelectCategoryCommand
        End Get
        Set(value As RelayCommand)
            m_SelectCategoryCommand = value
        End Set
    End Property

    Public Property SelectBookCommand() As RelayCommand
        Get
            Return m_SelectBookCommand
        End Get
        Set(value As RelayCommand)
            m_SelectBookCommand = value
        End Set
    End Property

    Public Property SearchBooksCommand() As RelayCommand
        Get
            Return m_SearchBooksCommand
        End Get
        Set(value As RelayCommand)
            m_SearchBooksCommand = value
        End Set
    End Property

    Public Property ClearFilterCommand() As RelayCommand
        Get
            Return m_ClearFilterCommand
        End Get
        Set(value As RelayCommand)
            m_ClearFilterCommand = value
        End Set
    End Property

    Public Property EditSelectedBookCommand() As RelayCommand
        Get
            Return m_EditSelectedBookCommand
        End Get
        Set(value As RelayCommand)
            m_EditSelectedBookCommand = value
        End Set
    End Property

    Public Property CreateNewBookCommand() As RelayCommand
        Get
            Return m_CreateNewBookCommand
        End Get
        Set(value As RelayCommand)
            m_CreateNewBookCommand = value
        End Set
    End Property

    Public Property SaveBookChangesCommand() As RelayCommand
        Get
            Return m_SaveBookChangesCommand
        End Get
        Set(value As RelayCommand)
            m_SaveBookChangesCommand = value
        End Set
    End Property

    Public Property CancelBookChangesCommand() As RelayCommand
        Get
            Return m_CancelBookChangesCommand
        End Get
        Set(value As RelayCommand)
            m_CancelBookChangesCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SelectCategoryCommand = New RelayCommand(AddressOf OnSelectCategory)
        SelectBookCommand = New RelayCommand(AddressOf OnSelectBook)
        EditSelectedBookCommand = New RelayCommand(AddressOf OnEditSelectedBook)
        SaveBookChangesCommand = New RelayCommand(AddressOf OnSaveBookChanges)
        CancelBookChangesCommand = New RelayCommand(AddressOf OnCancelBookChanges)
        CreateNewBookCommand = New RelayCommand(AddressOf OnCreateNewBook)
    End Sub

    Protected Sub UnregisterCommands()
        SelectCategoryCommand = Nothing
        SelectBookCommand = Nothing
        EditSelectedBookCommand = Nothing
        SaveBookChangesCommand = Nothing
        CancelBookChangesCommand = Nothing
        CreateNewBookCommand = Nothing
    End Sub

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context to manage compact : every page will cause a new 
        Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''
        Me._books = libraryDataService.BookCollectionView
        Me._categories = libraryDataService.CategoryCollectionView
        Me._authors = libraryDataService.AuthorCollectionView

        AddHandler libraryDataService.DomainContext.EntityContainer.PropertyChanged, AddressOf libraryDataService_PropertyChanged

        ' Go back to the first page when the sorting changes
        Dim bookNotifyingSortDescriptions As INotifyCollectionChanged = DirectCast(Me.Books.SortDescriptions, INotifyCollectionChanged)
        AddHandler bookNotifyingSortDescriptions.CollectionChanged, New NotifyCollectionChangedEventHandler(AddressOf CType(Me.Books, DomainCollectionView).MoveToFirstPage)

        Using Me.Books.DeferRefresh()
            CType(Me.Books, DomainCollectionView).PageSize = 10
            CType(Me.Books, DomainCollectionView).MoveToFirstPage()
        End Using

        ' Go back to the first page when the sorting changes
        Dim categoryNotifyingSortDescriptions As INotifyCollectionChanged = DirectCast(Me.Books.SortDescriptions, INotifyCollectionChanged)
        AddHandler categoryNotifyingSortDescriptions.CollectionChanged, New NotifyCollectionChangedEventHandler(AddressOf CType(Me.Books, DomainCollectionView).MoveToFirstPage)

        Using Me.Categories.DeferRefresh()
            CType(Me.Categories, DomainCollectionView).PageSize = 10
            CType(Me.Categories, DomainCollectionView).MoveToFirstPage()
        End Using

        Me.LibraryDataService.LoadCategories()
        Categories.MoveCurrentToFirst()
        LoadBooksByCategories()

        Me.LibraryDataService.LoadAuthors()
        Authors.MoveCurrentToFirst()

    End Sub

    Public Sub RefreshBookPropertiesCollectionViews()
        Me.LibraryDataService.RefreshBookPropertiesCollectionViews()
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler LibraryDataService.DomainContext.EntityContainer.PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If
        Dim bookNotifyingSortDescriptions As INotifyCollectionChanged = DirectCast(Me.Books.SortDescriptions, INotifyCollectionChanged)
        If Not IsNothing(bookNotifyingSortDescriptions) Then
            RemoveHandler bookNotifyingSortDescriptions.CollectionChanged, New NotifyCollectionChangedEventHandler(AddressOf CType(Me.Books, DomainCollectionView).MoveToFirstPage)
        End If
        Dim categoryNotifyingSortDescriptions As INotifyCollectionChanged = DirectCast(Me.Books.SortDescriptions, INotifyCollectionChanged)
        If Not IsNothing(bookNotifyingSortDescriptions) Then
            RemoveHandler categoryNotifyingSortDescriptions.CollectionChanged, New NotifyCollectionChangedEventHandler(AddressOf CType(Me.Books, DomainCollectionView).MoveToFirstPage)
        End If
        If Not IsNothing(SelectedBook) Then
            RemoveHandler SelectedBook.PropertyChanged, AddressOf selectedBook_PropertyChanged
        End If

        Me.LibraryDataService.RefreshBookPropertiesCollectionViews()

        UnregisterCommands()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler LibraryDataService.DomainContext.EntityContainer.PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If
        Dim bookNotifyingSortDescriptions As INotifyCollectionChanged = DirectCast(Me.Books.SortDescriptions, INotifyCollectionChanged)
        If Not IsNothing(bookNotifyingSortDescriptions) Then
            RemoveHandler bookNotifyingSortDescriptions.CollectionChanged, New NotifyCollectionChangedEventHandler(AddressOf CType(Me.Books, DomainCollectionView).MoveToFirstPage)
        End If
        Dim categoryNotifyingSortDescriptions As INotifyCollectionChanged = DirectCast(Me.Books.SortDescriptions, INotifyCollectionChanged)
        If Not IsNothing(bookNotifyingSortDescriptions) Then
            RemoveHandler categoryNotifyingSortDescriptions.CollectionChanged, New NotifyCollectionChangedEventHandler(AddressOf CType(Me.Books, DomainCollectionView).MoveToFirstPage)
        End If
        If Not IsNothing(SelectedBook) Then
            RemoveHandler SelectedBook.PropertyChanged, AddressOf selectedBook_PropertyChanged
        End If

        UnregisterCommands()

    End Sub
#End Region

#Region "Private Methods"

    Private Sub LoadBooksByCategories()
        If Not IsNothing(SelectedCategories) Then
            Dim category_ids As List(Of Integer) = New List(Of Integer)
            Dim index As Integer = 0
            For Each category In SelectedCategories
                category_ids.Add(category.Category_id)
            Next
            Me.LibraryDataService.LoadBooksByCategories(category_ids.ToArray())
        End If
    End Sub

    Private Sub OnSelectCategory()
        'This will be done when selectedCategories changed
        'LoadBooksByCategories()
    End Sub

    Private Sub OnEditSelectedBook()
        EditedBook = SelectedBook
        Dim editBook = New EditBookChildWindow()
        editBook.Show()
    End Sub

    Private Sub OnCreateNewBook()
        Dim newBook = New Book With {
                        .IsActive = True,
                        .Book_Quantity_Onhand = 0,
                        .Book_Quantity_Avail = 0,
                        .Book_Media = "Paperback",
                        .Book_Language = "English"
                      }
        InitializeCheckBookAuthorsConverter(newBook)
        EditedBook = newBook
        Dim editBook = New EditBookChildWindow()
        editBook.Show()
    End Sub

    Private Sub OnSelectBook()
        If Not IsNothing(Books.CurrentItem) Then
            Me.SelectedBook = Books.CurrentItem
            AddHandler SelectedBook.PropertyChanged, AddressOf selectedBook_PropertyChanged
            Me.EnableEditSelectedBook = True
            Me.EditSelectedBookCommand.CanExecute(True)
            Me.EditSelectedBookCommand.RaiseCanExecuteChanged()
        Else
            Me.SelectedBook = Nothing
            Me.EnableEditSelectedBook = False
            Me.EditSelectedBookCommand.CanExecute(False)
            Me.EditSelectedBookCommand.RaiseCanExecuteChanged()
        End If

        InitializeCheckBookAuthorsConverter(Books.CurrentItem)

    End Sub

    Public Sub InitializeCheckBookAuthorsConverter(book As Book)

        If Not IsNothing(book) Then
            If Not Me.LibraryDataService.BookAuthorCollectionLoader Is Nothing Then
                Me.LibraryDataService.BookAuthorCollectionLoader.SelectedList = book.Book_Authors
            End If
        End If

    End Sub

    Private Sub OnSaveBookChanges()
        'If EditedBook.BookPropertiesChanged Then
        '    If IsNewBookEdited Then
        '        Me.LibraryDataService.DomainContext.Books.Add(EditedBook)
        '    End If
        '    Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveBookChanges, Nothing)

        '    'Dim author_ids = New List(Of Integer)
        '    'For Each a In EditedBook.Book_Authors
        '    'author_ids.Add(a.Author_id)
        '    'Next
        '    'Me.LibraryDataService.UpdateAuthorsOfBook(EditedBook.Book_id, author_ids)
        'End If
    End Sub

    Private Sub AfterSaveBookChanges(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedBookDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim savedBook As Book = Nothing
        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
        Else
            dialogType = "Success"
        End If
        If result.ChangeSet.AddedEntities.Count > 0 Then
            For Each e As Object In result.ChangeSet.AddedEntities
                If TypeOf (e) Is Book Then
                    savedBook = e
                End If
            Next
            action = "Add"
            resultMessage = ""
        End If
        If (result.[Error] IsNot Nothing) Then
            dialogMessage = New SavedBookDialogMessage("Error", "Save", "Save was unsuccessful.", savedBook)
        Else
            dialogMessage = New SavedBookDialogMessage("Success", "Save", "Save was successful", savedBook)
        End If
        Messenger.[Default].Send(dialogMessage)

    End Sub

    Private Sub OnCancelBookChanges()
        Me.LibraryDataService.DomainContext.RejectChanges()
    End Sub

#End Region

End Class
