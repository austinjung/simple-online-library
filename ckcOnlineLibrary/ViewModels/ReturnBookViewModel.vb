﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class ReturnBookViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'user Transactions
    Private _user_Transactions As DomainCollectionView(Of Member_Transactions) = Nothing

    'selected Transaction
    Private _selected_Transaction As Member_Transactions = Nothing

    'Transaction page size
    Private _transactionsPageSize As Integer = 6

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Filter Transactions 
    Private _filter_Keyword As String = String.Empty
    Private _filter_Targets As ObservableCollection(Of TransactionFilterTarget) = TransactionFilterTargets.GetFilterTargets

    'Current DataGrid variables
    Private SortingGridByChecked As Boolean = False

    'Sort Directions/ Sorter
    Private sortDirection As Integer = ListSortDirection.Ascending
    Private sorter As String = "Sort By Expected"

    'Commands
    Private m_SearchTransactionCommand As RelayCommand(Of ComboBox)
    Private _sortDescriptorChangeCommand As RelayCommand(Of ComboBox)
    Private _sortDirectorChangeCommand As RelayCommand(Of ComboBox)
    Private m_TransactionSelectionChangeCommand As RelayCommand(Of DataGrid)

    Private isFirstTransactionBook As Boolean = False

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "TransactionsLoaded" Then
            isFirstTransactionBook = True
            HasChanges = True
            Me.RaisePropertyChanged("User_Transactions")
        ElseIf e.PropertyName = "TransactionBookLoaded" Then
            If isFirstTransactionBook Then
                _selected_Transaction = Me._user_Transactions.SourceCollection(0)
                If Not _selected_Transaction.Book Is Nothing Then
                    HasChanges = True
                    Me.RaisePropertyChanged("Book")
                    Me.RaisePropertyChanged("Selected_Transaction")
                    isFirstTransactionBook = False
                End If
            End If
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property User_Transactions As ICollectionView
        Get
            Return _user_Transactions
        End Get
    End Property

    Public Property Filter_Keyword As String
        Get
            Return Me._filter_Keyword
        End Get
        Set(value As String)
            Me._filter_Keyword = value
        End Set
    End Property

    Public ReadOnly Property Filter_Targets As ObservableCollection(Of TransactionFilterTarget)
        Get
            Return Me._filter_Targets
        End Get
    End Property

    Public ReadOnly Property Book As Book
        Get
            If IsNothing(_selected_Transaction) Then
                Return Nothing
            Else
                Return _selected_Transaction.Book
            End If
        End Get
    End Property

    Public ReadOnly Property Selected_Transaction As Member_Transactions
        Get
            Return _selected_Transaction
        End Get
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context to manage compact : every page will cause a new 
        Me.LibraryDataService.RefreshLibraryDataService()
        Me.LibraryDataService.MaxUnreturnedTransaction = 0
        ''''''''''''''''''''''''''''
        Me._user_Transactions = libraryDataService.TransactionCollectionView
        Me._user_Transactions.SortDescriptions.Add(New SortDescription("Expected_Return", ListSortDirection.Ascending))


        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf Me.libraryDataService_PropertyChanged
        End If

        Me.LibraryDataService.LoadAllMemberUnreturnedTransactions()

        Using (Me._user_Transactions.DeferRefresh())
            Me._user_Transactions.PageSize = _transactionsPageSize
            Me._user_Transactions.SetTotalItemCount(-1)
            Me._user_Transactions.MoveToFirstPage()
        End Using

    End Sub

#End Region

#Region "Commands Properties"

    Public Property TransactionSelectionChangeCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_TransactionSelectionChangeCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_TransactionSelectionChangeCommand = value
        End Set
    End Property

    Public Property SearchTransactionCommand() As RelayCommand(Of ComboBox)
        Get
            Return m_SearchTransactionCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            m_SearchTransactionCommand = value
        End Set
    End Property

    Public Property SortDescriptorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDescriptorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDescriptorChangeCommand = value
        End Set
    End Property

    Public Property SortDirectorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDirectorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDirectorChangeCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SearchTransactionCommand = New RelayCommand(Of ComboBox)(AddressOf SearchTransaction,
                                                            Function(sender) Not IsNothing(sender))
        SortDescriptorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDescriptorChange,
                                              Function() CType(_user_Transactions, DomainCollectionView).ItemCount > 0)
        SortDirectorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDirectorChange,
                                              Function() CType(_user_Transactions, DomainCollectionView).ItemCount > 0)
        TransactionSelectionChangeCommand = New RelayCommand(Of DataGrid)(AddressOf OnTransactionSelectionChange,
                                              Function() CType(_user_Transactions, DomainCollectionView).ItemCount > 0)
    End Sub

    Protected Sub UnregisterCommands()
        SearchTransactionCommand = Nothing
        SortDescriptorChangeCommand = Nothing
        SortDirectorChangeCommand = Nothing
        TransactionSelectionChangeCommand = Nothing
    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        Me.LibraryDataService.MaxUnreturnedTransaction = 3
        Me.LibraryDataService.RefreshMemberTransactionCollectionViews()

        If TypeOf LibraryDataService Is LibraryDataService Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub
#End Region

#Region "Register Message & Handle"

    Protected Overrides Sub RegisterMessages()
        Messenger.Reset()
        Messenger.[Default].Register(Of ReceiveBookResultMessage)(Me, AddressOf OnSavedReceiveBookDialogMessageReceived)
    End Sub

    Protected Sub UnregisterMessages()
        Messenger.[Default].Unregister(Of ReceiveBookResultMessage)(Me, AddressOf OnSavedReceiveBookDialogMessageReceived)
    End Sub

    Private Sub OnSavedReceiveBookDialogMessageReceived(msg As ReceiveBookResultMessage)

        Dim myMessageBox = New MyMessageBox(msg.Type, msg.Content, msg.Caption, msg.Button)
        myMessageBox.Show()

        Dim currentPosition = Me._user_Transactions.CurrentPosition
        Dim currentPage = Me._user_Transactions.PageIndex
        Dim pageTotal = CType(Me._user_Transactions.SourceCollection, EntityList(Of Member_Transactions)).Count - 1
        If currentPage = 0 And pageTotal = 0 Then
            Dim busyIndicator As BusyIndicator = DirectCast(Application.Current.RootVisual, FrameworkElement)
            Dim mainPage As MainPage = DirectCast(busyIndicator.Content, MainPage)
            mainPage.ContentFrame.Refresh()
        Else
            Using (Me._user_Transactions.DeferRefresh())
                Me._user_Transactions.SetTotalItemCount(-1)
                If currentPosition = 0 And pageTotal = currentPosition And currentPage > 0 Then
                    Me._user_Transactions.MoveToPreviousPage()
                Else
                    Me._user_Transactions.MoveToPage(currentPage)
                End If
            End Using
            'If currentPosition = 0 And pageTotal = currentPosition Then
            '    Me._user_Transactions.MoveCurrentToPosition(pageTotal)
            'Else
            '    Me._user_Transactions.MoveCurrentToPosition(currentPosition)
            'End If
        End If

    End Sub

#End Region

#Region "Private Methods : Handling Commands"

    Private Sub SearchTransaction(searchTargetsCombo As ComboBox)

        'For Each target In Me.Filter_Targets
        For Each target As TransactionFilterTarget In searchTargetsCombo.Items
            If target.Target.Contains("Member") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionMember = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionMember = Nothing
                End If
            ElseIf target.Target.Contains("Title") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionBookTitle = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionBookTitle = Nothing
                End If
            ElseIf target.Target.Contains("Description") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterTransactionBookDescription = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterTransactionBookDescription = Nothing
                End If
            End If
        Next

        Using (Me._user_Transactions.DeferRefresh())
            Me._user_Transactions.SetTotalItemCount(-1)
            Me._user_Transactions.MoveToFirstPage()
        End Using

    End Sub

    Private Sub OnTransactionSelectionChange(grid As DataGrid)

        _selected_Transaction = CType(grid.ItemsSource, DomainCollectionView(Of Member_Transactions)).CurrentItem
        Me.HasChanges = True
        Me.RaisePropertyChanged("Book")
        Me.RaisePropertyChanged("Selected_Transaction")

    End Sub

    Private Sub OnSortDirectorChange(sortDirector As ComboBox)

        If sortDirector.SelectedItem.Content.contains("Asc") Then
            sortDirection = ListSortDirection.Ascending
        Else
            sortDirection = ListSortDirection.Descending
        End If

        SortRequests()

    End Sub

    Private Sub OnSortDescriptorChange(sortDescriptor As ComboBox)

        sorter = sortDescriptor.SelectedItem.Content
        SortRequests()

    End Sub

    Private Sub SortRequests()
        If sorter.Contains("Issued") Then
            _user_Transactions.SortDescriptions.Clear()
            _user_Transactions.SortDescriptions.Add(New SortDescription("Issued_Date", sortDirection))
        ElseIf sorter.Contains("Expected") Then
            _user_Transactions.SortDescriptions.Clear()
            _user_Transactions.SortDescriptions.Add(New SortDescription("Expected_Return", sortDirection))
        End If
        _user_Transactions.Refresh()
    End Sub

#End Region

End Class
