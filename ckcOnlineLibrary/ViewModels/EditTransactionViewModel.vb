﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight
Imports ckcOnlineLibrary.DateTimeExtensions

Public Class EditTransactionViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Public Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    Private _book_Of_Transaction As Book
    Private _memberTransaction As Member_Transactions
    Private _newExpectedReturn As Date
    Private _maxExtendedReturnDueDate As Date
    Private _addedComment As String
    Private _newComment As String

    Private _isChanged As Boolean = False
    Private _expectedReturnDateChanged As Boolean = False

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Command Properties
    Private _saveAddedTransactionCommentCommand As RelayCommand
    Private _cancelAddedTransactionCommentCommand As RelayCommand

    Private _commentOnTransactionWindow As CommentOnTransactionChildWindow = Nothing

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "TransactionBookLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("Book")
        ElseIf e.PropertyName = "HasChanges" Then
            If LibraryDataService.DomainContext.Member_Transactions.HasChanges Then
                HasChanges = LibraryDataService.DomainContext.Member_Requests.HasChanges
                Me.RaisePropertyChanged("Request")
            End If
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Book As Book
        Get
            Return Me._memberTransaction.Book
        End Get
    End Property

    Public Property NewExpectedReturnDate As Date
        Get
            Return Me._newExpectedReturn
        End Get
        Set(value As Date)
            Me._newExpectedReturn = value
            Me._expectedReturnDateChanged = True
            Me.IsChanged = True
        End Set
    End Property

    Public ReadOnly Property DisplayStartDate As Date
        Get
            Return Today.AddDays(1)
        End Get
    End Property

    Public ReadOnly Property MaxExtendedReturnDueDate As Date
        Get
            Return Me._maxExtendedReturnDueDate
        End Get
    End Property

    Public ReadOnly Property ExistingComment As String
        Get
            Return Me._memberTransaction.Comment
        End Get
    End Property

    Public Property AddedComment As String
        Get
            Return Me._addedComment
        End Get
        Set(value As String)
            Me._addedComment = value
            Me.IsChanged = True
        End Set
    End Property

    Public Property IsChanged As Boolean
        Get
            Return Me._isChanged
        End Get
        Set(value As Boolean)
            _isChanged = value
            Me.RaisePropertyChanged("IsChanged")
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   memberTransaction As Member_Transactions)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        Me._memberTransaction = memberTransaction
        Me._newExpectedReturn = Me._memberTransaction.Expected_Return
        Me._maxExtendedReturnDueDate = CType(Me._memberTransaction.Issued_Date, Date).AddDays(2 * 14)
        Me._addedComment = String.Empty
        Me._isChanged = False
        Me._expectedReturnDateChanged = False

        'Set Property Changed Event Handler
        AddHandler CType(Me.LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged

        'Load Book of transaction
        CType(Me.LibraryDataService, LibraryDataService).LoadBookOfTransaction(Me._memberTransaction.Transaction_id)

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SaveAddedTransactionCommentCommand() As RelayCommand
        Get
            Return _saveAddedTransactionCommentCommand
        End Get
        Set(value As RelayCommand)
            _saveAddedTransactionCommentCommand = value
        End Set
    End Property

    Public Property CancelAddedTransactionCommentCommand() As RelayCommand
        Get
            Return _cancelAddedTransactionCommentCommand
        End Get
        Set(value As RelayCommand)
            _cancelAddedTransactionCommentCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SaveAddedTransactionCommentCommand = New RelayCommand(AddressOf OnSaveAddedTransactionCommentCommand)
        CancelAddedTransactionCommentCommand = New RelayCommand(AddressOf OnCancelAddedTransactionCommentCommand)
    End Sub

    Protected Sub UnregisterCommands()
        SaveAddedTransactionCommentCommand = Nothing
        CancelAddedTransactionCommentCommand = Nothing
    End Sub

#End Region

#Region "Register Message & Handle"

    Protected Overrides Sub RegisterMessages()
        Messenger.[Default].Register(Of ReadCommentDialogMessage)(Me, AddressOf OnReadCommentDialogMessageReceived)
    End Sub

    Protected Sub UnregisterMessages()
        Messenger.[Default].Unregister(Of ReadCommentDialogMessage)(Me, AddressOf OnReadCommentDialogMessageReceived)
    End Sub

    Private Sub OnReadCommentDialogMessageReceived(msg As ReadCommentDialogMessage)
        If msg.DialogResult.Equals("OpenAddComment") Then
            _commentOnTransactionWindow = New CommentOnTransactionChildWindow()
            _commentOnTransactionWindow.Show()
        ElseIf msg.DialogResult.Equals("CloseDialog") Then
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterResetCommentViewFlag, Nothing)
        End If
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        RemoveHandler CType(Me.LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged

        UnregisterCommands()
        UnregisterMessages()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        RemoveHandler CType(Me.LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged

        UnregisterCommands()
        UnregisterMessages()

    End Sub
#End Region

#Region "Private Methods : Handling Commands : Request, Sort"

    Private Sub AfterResetCommentViewFlag(result As ServiceSubmitChangesResult)
        If (result.[Error] IsNot Nothing) Then
            Dim myMessageBox = New MyMessageBox("Error", "Mark as read", "Error in marking as read.", MessageBoxButton.OK)
            myMessageBox.Show()
        End If
    End Sub

    Private Sub OnCancelAddedTransactionCommentCommand()
        Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterResetCommentViewFlag, Nothing)
    End Sub

    Private Sub OnSaveAddedTransactionCommentCommand()
        If WebContext.Current.User.Roles.Contains("User") Then
            Me._memberTransaction.UserCommentViewed = False
            Me._memberTransaction.UserCommented = Now
            Me._newComment = WebContext.Current.User.MemberLogin & " : commented." & vbCrLf & "    " & Me._addedComment
            Me._memberTransaction.Comment = Me._newComment & vbCrLf & " - " & Today.ToString("MM/dd/yyyy") & vbCrLf + Me._memberTransaction.Comment
        Else
            Me._memberTransaction.AdminCommentViewed = False
            Me._memberTransaction.AdminCommented = Now
            If Me._expectedReturnDateChanged Then
                Me._newComment = "Admin" & " : changed the expected return date from " & Me._memberTransaction.Expected_Return.ToString("MM/dd/yyyy") _
                                & " to " & _newExpectedReturn.ToString("MM/dd/yyyy") & "." & vbCrLf
                Me._memberTransaction.Expected_Return = Me._newExpectedReturn
            Else
                Me._newComment = "Admin" & " : commented." & vbCrLf
            End If
            If Not String.IsNullOrEmpty(Me._addedComment) Then
                Me._memberTransaction.Comment = Me._newComment & "    " & Me._addedComment & " - " & Today.ToString("MM/dd/yyyy") & vbCrLf + Me._memberTransaction.Comment
            Else
                Me._memberTransaction.Comment = Me._newComment & " - " & Today.ToString("MM/dd/yyyy") & vbCrLf + Me._memberTransaction.Comment
            End If
        End If
        Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveChangedComment, Nothing)
    End Sub

    Private Sub AfterSaveChangedComment(result As ServiceSubmitChangesResult)

        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Your request was not completed."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Change request"
                For Each e In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member_Transactions Then
                        resultMessage &= "Your comment is added." & vbCrLf
                    End If
                Next
            End If
        End If

        Dim myMessageBox = New MyMessageBox(dialogType, action, resultMessage, MessageBoxButton.OK)
        myMessageBox.Show()

    End Sub

#End Region

End Class
