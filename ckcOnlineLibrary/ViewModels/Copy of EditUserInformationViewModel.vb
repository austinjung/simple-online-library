﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight
Imports System.ServiceModel.DomainServices.Client.ApplicationServices

Public Class EditUserInformationViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Public Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'members
    'Private _members As DomainCollectionView(Of Member) = Nothing
    Private _members As List(Of Member) = Nothing

    'selected member
    Private _selected_Member As Member = Nothing

    'View Model Changed Status
    'Private _canLoad As Boolean = True
    'Private _hasChanges As Boolean = False

#End Region

#Region "Private Method : Property changed event"

    'Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
    '    If e.PropertyName = "CurrentMemberInformationLoaded" Then
    '        If Me._members.ItemCount > 0 Then
    '            Me._members.MoveCurrentToFirst()
    '            HasChanges = True
    '            'Me.RaisePropertyChanged("Members")
    '            Me.RaisePropertyChanged("Selected_Member")
    '        End If
    '    End If
    'End Sub

#End Region

#Region "Status Properties"

    'Public Property CanLoad() As Boolean
    '    Get
    '        Return Me._canLoad
    '    End Get
    '    Private Set(value As Boolean)
    '        If Me._canLoad <> value Then
    '            Me._canLoad = value
    '            Me.RaisePropertyChanged("CanLoad")
    '        End If
    '    End Set
    'End Property

    'Public Property HasChanges() As Boolean
    '    Get
    '        Return _hasChanges
    '    End Get
    '    Private Set(value As Boolean)
    '        _hasChanges = value
    '        Me.RaisePropertyChanged("HasChanges")
    '    End Set
    'End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Members As List(Of Member)
        Get
            Return _members
        End Get
    End Property

    Public Property Selected_Member As Member
        Get
            Return _selected_Member
        End Get
        Set(value As Member)
            _selected_Member = value
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

        Me._members = New List(Of Member)
        Me._members.Add(WebContext.Current.User)
        Me._selected_Member = WebContext.Current.User

    End Sub

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context to manage compact : every page will cause a new 
        'Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''
        'Me._members = libraryDataService.CurrentUserInformationCollectionView
        Me._members = New List(Of Member)
        Me._members.Add(WebContext.Current.User)
        Me._selected_Member = WebContext.Current.User

        'If TypeOf libraryDataService Is LibraryDataService Then
        '    AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf Me.libraryDataService_PropertyChanged
        'End If

        'Me.LibraryDataService.LoadCurrentMemberInformation()

        'Using (Me._members.DeferRefresh())
        '    Me._members.SetTotalItemCount(-1)
        '    Me._members.MoveToFirstPage()
        '    If Me._members.ItemCount > 0 Then
        '        Me._members.MoveCurrentToFirst()
        '    End If
        'End Using

    End Sub

#End Region

#Region "Commands Properties"

#End Region

#Region "Register Commands"

#End Region

#Region "ICleanup interface"
    'Public Overrides Sub Cleanup()

    '    'If TypeOf LibraryDataService Is LibraryDataService Then
    '    '    RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
    '    'End If

    'End Sub

    'Public Sub RemovePropertyChangeHandler()

    '    'If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
    '    '    RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
    '    'End If

    'End Sub
#End Region

#Region "Register Message & Handle"

    'Protected Overrides Sub RegisterMessages()
    Private Sub RegisterMessages()
        Messenger.Reset()
        Messenger.[Default].Register(Of SavedMemberDialogMessage)(Me, AddressOf OnSaveMemberResultMessageReceived)
    End Sub

    Private Sub OnSaveMemberResultMessageReceived(msg As SavedMemberDialogMessage)
        If Me._members.Count > 0 Then
            'Me.Selected_Member = Me.Members.CurrentItem
            Me.Selected_Member = Me.Members(0)
        End If
    End Sub

#End Region

#Region "Private Methods : Handling Commands"

    Private Sub SaveEditedMember(msg As ConfirmDialogMessage)

        If msg.Caption.Equals("DialogResultOK") Then
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveEditedMember, Nothing)
        End If
        Messenger.[Default].Unregister(Of ConfirmDialogMessage)(Me, AddressOf SaveEditedMember)

    End Sub

    Public Sub AfterSaveEditedMember(result As ServiceSubmitChangesResult)
        Dim dialogMessage As SavedMemberDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim editedMember As Member = Nothing

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "Add"
                For Each e As Object In result.ChangeSet.AddedEntities
                    If TypeOf (e) Is Member Then
                        editedMember = CType(e, Member)
                        resultMessage = editedMember.FullName & " is added successfully."
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member Then
                        editedMember = CType(e, Member)
                        resultMessage = editedMember.FullName & " is updated successfully."
                    End If
                Next
            End If
        End If

        If Not String.IsNullOrEmpty(resultMessage) Then
            dialogMessage = New SavedMemberDialogMessage(dialogType, action, resultMessage, editedMember)
            Messenger.[Default].Send(dialogMessage)
            Dim MyMessageBox = New MyMessageBox(dialogType, action, resultMessage, MessageBoxButton.OK)
            MyMessageBox.Show()
        End If

    End Sub

#End Region

End Class
