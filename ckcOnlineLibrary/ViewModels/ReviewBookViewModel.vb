﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class ReviewBookViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    Private _reviewTransaction As Transactions_Detail
    Private _hasChanges As Boolean = False

    'Commands for save & cancel
    Private m_SaveReviewBookCommand As RelayCommand
    Private m_CancelReviewBookCommand As RelayCommand

#End Region

#Region "Private Method : Property changed event"

    Private Sub ReviewBook_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        Select Case e.PropertyName
            Case "HasChanges"
                Me.HasChanges = True
                SaveReviewBookCommand.RaiseCanExecuteChanged()
        End Select
    End Sub

#End Region

#Region "Status Properties"

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public Property ReviewTransaction As Transactions_Detail
        Get
            Return _reviewTransaction
        End Get
        Set(value As Transactions_Detail)
            _reviewTransaction = value
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor,
                   libraryDataService As ILibraryDataService,
                   selectedTransaction As Transactions_Detail)

        'set Service Providers
        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        Me.ReviewTransaction = selectedTransaction

        ''''''''''''''''''''''''''''
        'Don't refresh Domain Data Context, we need to use current Book Properties Collection data 
        'Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''

        'Set Property Changed Event Handler
        AddHandler Me._reviewTransaction.PropertyChanged, AddressOf ReviewBook_PropertyChanged

    End Sub

#End Region

#Region "Commands Properties"

    Public Property SaveReviewBookCommand() As RelayCommand
        Get
            Return m_SaveReviewBookCommand
        End Get
        Set(value As RelayCommand)
            m_SaveReviewBookCommand = value
        End Set
    End Property

    Public Property CancelReviewBookCommand() As RelayCommand
        Get
            Return m_CancelReviewBookCommand
        End Get
        Set(value As RelayCommand)
            m_CancelReviewBookCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SaveReviewBookCommand = New RelayCommand(AddressOf OnSaveReviewBook, Function() HasChanges)
        CancelReviewBookCommand = New RelayCommand(AddressOf OnCancelReviewBook)
    End Sub

    Protected Sub UnregisterCommands()
        SaveReviewBookCommand = Nothing
        CancelReviewBookCommand = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()

        If Not IsNothing(Me._reviewTransaction) Then
            RemoveHandler Me._reviewTransaction.PropertyChanged, AddressOf ReviewBook_PropertyChanged
        End If

        UnregisterCommands()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If Not IsNothing(Me._reviewTransaction) Then
            RemoveHandler Me._reviewTransaction.PropertyChanged, AddressOf ReviewBook_PropertyChanged
        End If

        UnregisterCommands()

    End Sub
#End Region

#Region "Private Methods : Save & Cancel"

    Private Sub OnSaveReviewBook()
        If Me.HasChanges Then
            Dim newReview = New Member_Reviews With {
                                .MemberID = Me.ReviewTransaction.MemberID,
                                .Book_id = Me.ReviewTransaction.Book_id,
                                .Rating = Me.ReviewTransaction.UserInputRating,
                                .Review = Me.ReviewTransaction.UserInputComment,
                                .Review_Date = Now
                             }
            Me.LibraryDataService.DomainContext.Member_Reviews.Add(newReview)

            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveReviewBook, Nothing)
        End If
    End Sub

    Private Sub AfterSaveReviewBook(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedReviewDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
            _reviewTransaction.UserInputComment = String.Empty
            _reviewTransaction.UserInputRating = 0.0
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "Add"
                For Each e As Object In result.ChangeSet.AddedEntities
                    If TypeOf (e) Is Member_Reviews Then
                        resultMessage &= "Thanks for your review. Your review was added successfully." & vbCrLf
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member_Reviews Then
                        resultMessage &= "Thanks for your review. Your review was updated successfully." & vbCrLf
                    End If
                Next
            End If
        End If

        dialogMessage = New SavedReviewDialogMessage(dialogType, action, resultMessage)
        Messenger.[Default].Send(dialogMessage)

    End Sub

    Private Sub OnCancelReviewBook()

        Me.ReviewTransaction.UserInputComment = Nothing
        Me.ReviewTransaction.UserInputRating = Nothing

    End Sub

#End Region

End Class
