﻿Imports Papa.Common

Public Class AuthorWithCheckedFlag
    Inherits NotifiableObject

    Private _author As Author
    Public Property Author As Author
        Get
            Return _author
        End Get
        Set(value As Author)
            _author = value
            RaisePropertyChanged("Author_Full_Name")
        End Set
    End Property

    Private _isChecked As Boolean
    Public Property IsChecked As Boolean
        Get
            Return _isChecked
        End Get
        Set(value As Boolean)
            _isChecked = value
            RaisePropertyChanged("IsChecked")
        End Set
    End Property

    Public ReadOnly Property Author_Full_Name As String
        Get
            Return _author.Author_First_Name
        End Get
    End Property

End Class
