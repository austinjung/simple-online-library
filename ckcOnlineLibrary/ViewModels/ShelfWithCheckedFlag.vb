﻿Imports Papa.Common

Public Class ShelfWithCheckedFlag
    Inherits NotifiableObject

    Private _shelf As Shelf
    Public Property Shelf As Shelf
        Get
            Return _shelf
        End Get
        Set(value As Shelf)
            _shelf = value
            RaisePropertyChanged("Shelf_Code")
        End Set
    End Property

    Private _isChecked As Boolean
    Public Property IsChecked As Boolean
        Get
            Return _isChecked
        End Get
        Set(value As Boolean)
            _isChecked = value
            RaisePropertyChanged("IsChecked")
        End Set
    End Property

    Public ReadOnly Property Shelf_Code As String
        Get
            Return _shelf.Shelf_Code
        End Get
    End Property

End Class
