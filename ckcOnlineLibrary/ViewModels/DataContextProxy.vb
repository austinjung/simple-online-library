﻿Imports System.Windows.Data

Public Class DataContextProxy
    Inherits FrameworkElement

    Public Sub New()
        AddHandler Me.Loaded, AddressOf DataContextProxyLoaded
    End Sub

    Private Sub DataContextProxyLoaded(sender As Object, e As RoutedEventArgs)
        Dim binding As New Binding()
        If Not [String].IsNullOrEmpty(BindingPropertyName) Then
            binding.Path = New PropertyPath(BindingPropertyName)
        End If
        binding.Source = Me.DataContext
        binding.Mode = BindingMode
        Me.SetBinding(DataContextProxy.DataSourceProperty, binding)
    End Sub

    Public Property DataSource() As [Object]
        Get
            Return DirectCast(GetValue(DataSourceProperty), [Object])
        End Get
        Set(value As [Object])
            SetValue(DataSourceProperty, value)
        End Set
    End Property

    Public Shared ReadOnly DataSourceProperty As DependencyProperty = DependencyProperty.Register("DataSource", GetType([Object]), GetType(DataContextProxy), Nothing)

    Public Property BindingPropertyName() As String
        Get
            Return m_BindingPropertyName
        End Get
        Set(value As String)
            m_BindingPropertyName = value
        End Set
    End Property
    Private m_BindingPropertyName As String

    Public Property BindingMode() As BindingMode
        Get
            Return m_BindingMode
        End Get
        Set(value As BindingMode)
            m_BindingMode = value
        End Set
    End Property
    Private m_BindingMode As BindingMode

End Class