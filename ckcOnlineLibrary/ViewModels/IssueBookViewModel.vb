﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class IssueBookViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Protected Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'user requests
    Private _user_Pending_Requests As DomainCollectionView(Of Member_Requests) = Nothing

    'selected request
    Private _selected_Request As Member_Requests = Nothing

    'requests page size
    Private _requestsPageSize As Integer = 6

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Filter Books 
    Private _filter_Keyword As String = String.Empty
    Private _filter_Targets As ObservableCollection(Of RequestFilterTarget) = RequestFilterTargets.GetFilterTargets

    'Current DataGrid variables
    Private SortingGridByChecked As Boolean = False

    'Sort Directions/ Sorter
    Private sortDirection As Integer = ListSortDirection.Ascending
    Private sorter As String = "Sort By Expected"

    'Commands
    Private m_SearchRequestCommand As RelayCommand(Of ComboBox)
    Private _sortDescriptorChangeCommand As RelayCommand(Of ComboBox)
    Private _sortDirectorChangeCommand As RelayCommand(Of ComboBox)
    Private m_RequestSelectionChangeCommand As RelayCommand(Of DataGrid)

    Private isFirstRequestBook As Boolean = False

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "RequestsLoaded" Then
            isFirstRequestBook = True
            HasChanges = True
            Me.RaisePropertyChanged("User_Pending_Requests")
        ElseIf e.PropertyName = "RequestBookLoaded" Then
            If isFirstRequestBook Then
                _selected_Request = Me._user_Pending_Requests.SourceCollection(0)
                If Not _selected_Request.Book Is Nothing Then
                    HasChanges = True
                    Me.RaisePropertyChanged("Book")
                    Me.RaisePropertyChanged("Selected_Request")
                    isFirstRequestBook = False
                End If
            End If
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property User_Pending_Requests As ICollectionView
        Get
            Return _user_Pending_Requests
        End Get
    End Property

    Public Property Filter_Keyword As String
        Get
            Return Me._filter_Keyword
        End Get
        Set(value As String)
            Me._filter_Keyword = value
        End Set
    End Property

    Public ReadOnly Property Filter_Targets As ObservableCollection(Of RequestFilterTarget)
        Get
            Return Me._filter_Targets
        End Get
    End Property

    Public ReadOnly Property Book As Book
        Get
            If IsNothing(_selected_Request) Then
                Return Nothing
            Else
                Return _selected_Request.Book
            End If
        End Get
    End Property

    Public ReadOnly Property Selected_Request As Member_Requests
        Get
            Return _selected_Request
        End Get
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context to manage compact : every page will cause a new 
        Me.LibraryDataService.RefreshLibraryDataService()
        Me.LibraryDataService.MaxPendingRequest = 0
        ''''''''''''''''''''''''''''
        Me._user_Pending_Requests = libraryDataService.RequestCollectionView
        Me._user_Pending_Requests.SortDescriptions.Add(New SortDescription("Expected_Date", ListSortDirection.Ascending))


        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf Me.libraryDataService_PropertyChanged
        End If

        Me.LibraryDataService.LoadAllPendingRequests()

        Using (Me._user_Pending_Requests.DeferRefresh())
            Me._user_Pending_Requests.PageSize = _requestsPageSize
            Me._user_Pending_Requests.SetTotalItemCount(-1)
            Me._user_Pending_Requests.MoveToFirstPage()
        End Using

    End Sub

#End Region

#Region "Commands Properties"

    Public Property RequestSelectionChangeCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_RequestSelectionChangeCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_RequestSelectionChangeCommand = value
        End Set
    End Property

    Public Property SearchRequestCommand() As RelayCommand(Of ComboBox)
        Get
            Return m_SearchRequestCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            m_SearchRequestCommand = value
        End Set
    End Property

    Public Property SortDescriptorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDescriptorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDescriptorChangeCommand = value
        End Set
    End Property

    Public Property SortDirectorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDirectorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDirectorChangeCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SearchRequestCommand = New RelayCommand(Of ComboBox)(AddressOf SearchRequest,
                                                            Function(sender) Not IsNothing(sender))
        SortDescriptorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDescriptorChange,
                                              Function() CType(_user_Pending_Requests, DomainCollectionView).ItemCount > 0)
        SortDirectorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDirectorChange,
                                              Function() CType(_user_Pending_Requests, DomainCollectionView).ItemCount > 0)
        RequestSelectionChangeCommand = New RelayCommand(Of DataGrid)(AddressOf OnRequestSelectionChange,
                                              Function() CType(_user_Pending_Requests, DomainCollectionView).ItemCount > 0)
    End Sub

    Protected Sub UnregisterCommands()
        SearchRequestCommand = Nothing
        SortDescriptorChangeCommand = Nothing
        SortDirectorChangeCommand = Nothing
        RequestSelectionChangeCommand = Nothing
    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        Me.LibraryDataService.MaxPendingRequest = 3
        Me.LibraryDataService.RefreshMemberTransactionCollectionViews()

        If TypeOf LibraryDataService Is LibraryDataService Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMassages()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMassages()

    End Sub
#End Region

#Region "Register Message & Handle"

    Protected Overrides Sub RegisterMessages()
        Messenger.Reset()
        Messenger.[Default].Register(Of IssueBookResultMessage)(Me, AddressOf OnSavedIssueBookDialogMessageReceived)
        Messenger.[Default].Register(Of ReadCommentDialogMessage)(Me, AddressOf OnReadCommentDialogMessageReceived)
    End Sub

    Private Sub UnregisterMassages()
        Messenger.[Default].Unregister(Of IssueBookResultMessage)(Me, AddressOf OnSavedIssueBookDialogMessageReceived)
        Messenger.[Default].Unregister(Of ReadCommentDialogMessage)(Me, AddressOf OnReadCommentDialogMessageReceived)
    End Sub

    Private Sub OnReadCommentDialogMessageReceived(msg As ReadCommentDialogMessage)
        If msg.DialogResult.Equals("OpenAddComment") Then
            Dim editBookRequestWindow = New RequestBookChildWindow()
            editBookRequestWindow.Title = "Modify member's request"
            editBookRequestWindow.Show()
        End If
    End Sub

    Private Sub OnSavedIssueBookDialogMessageReceived(msg As IssueBookResultMessage)

        Dim myMessageBox = New MyMessageBox(msg.Type, msg.Content, msg.Caption, msg.Button)
        myMessageBox.Show()

        Dim currentPosition = Me._user_Pending_Requests.CurrentPosition
        Dim currentPage = Me._user_Pending_Requests.PageIndex
        Dim pageTotal = CType(Me._user_Pending_Requests.SourceCollection, EntityList(Of Member_Requests)).Count - 1 'total items in current page
        If currentPage = 0 And pageTotal = 0 Then
            Dim busyIndicator As BusyIndicator = DirectCast(Application.Current.RootVisual, FrameworkElement)
            Dim mainPage As MainPage = DirectCast(busyIndicator.Content, MainPage)
            mainPage.ContentFrame.Refresh()
        Else
            Using (Me._user_Pending_Requests.DeferRefresh())
                Me._user_Pending_Requests.SetTotalItemCount(-1)
                If currentPosition = 0 And pageTotal = currentPosition And currentPage > 0 Then
                    Me._user_Pending_Requests.MoveToPreviousPage()
                Else
                    Me._user_Pending_Requests.MoveToPage(currentPage)
                End If
            End Using
            'If currentPosition = 0 And pageTotal = currentPosition Then
            '    Me._user_Pending_Requests.MoveCurrentToPosition(pageTotal)
            'Else
            '    Me._user_Pending_Requests.MoveCurrentToPosition(currentPosition)
            'End If
        End If

    End Sub

#End Region

#Region "Private Methods : Handling Commands"

    Private Sub SearchRequest(searchTargetsCombo As ComboBox)

        'For Each target In Me.Filter_Targets
        For Each target As RequestFilterTarget In searchTargetsCombo.Items
            If target.Target.Contains("Member") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterRequestMember = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterRequestMember = Nothing
                End If
            ElseIf target.Target.Contains("Title") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterRequestBookTitle = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterRequestBookTitle = Nothing
                End If
            ElseIf target.Target.Contains("Description") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterRequestBookDescription = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterRequestBookDescription = Nothing
                End If
            End If
        Next

        Using (Me._user_Pending_Requests.DeferRefresh())
            Me._user_Pending_Requests.SetTotalItemCount(-1)
            Me._user_Pending_Requests.MoveToFirstPage()
        End Using

    End Sub

    Private Sub OnRequestSelectionChange(grid As DataGrid)

        _selected_Request = CType(grid.ItemsSource, DomainCollectionView(Of Member_Requests)).CurrentItem
        Me.HasChanges = True
        Me.RaisePropertyChanged("Book")
        Me.RaisePropertyChanged("Selected_Request")

    End Sub

    Private Sub OnSortDirectorChange(sortDirector As ComboBox)

        If sortDirector.SelectedItem.Content.contains("Asc") Then
            sortDirection = ListSortDirection.Ascending
        Else
            sortDirection = ListSortDirection.Descending
        End If

        SortRequests()

    End Sub

    Private Sub OnSortDescriptorChange(sortDescriptor As ComboBox)

        sorter = sortDescriptor.SelectedItem.Content
        SortRequests()

    End Sub

    Private Sub SortRequests()
        If sorter.Contains("Comitted") Then
            _user_Pending_Requests.SortDescriptions.Clear()
            _user_Pending_Requests.SortDescriptions.Add(New SortDescription("Comitted_Date", sortDirection))
        ElseIf sorter.Contains("Expected") Then
            _user_Pending_Requests.SortDescriptions.Clear()
            _user_Pending_Requests.SortDescriptions.Add(New SortDescription("Expected_Date", sortDirection))
        End If
        _user_Pending_Requests.Refresh()
    End Sub

#End Region

End Class
