﻿Imports Papa.Common
Imports Microsoft.Windows.Data.DomainServices

Public Class ViewModelLocator

    Private _sp As ServiceProviderBase
    Private m_lastViewModel As ViewModel = Nothing
    Public ReadOnly Property LastViewModel As ViewModel
        Get
            Return m_lastViewModel
        End Get
    End Property
    Private Sub CleanLastViewModel(ByVal newVM As ViewModel)
        If Not IsNothing(m_lastViewModel) And Not IsNothing(newVM) Then
            Dim type1 As String = m_lastViewModel.GetType().ToString()
            Dim type2 As String = newVM.GetType().ToString()

            If (type1.Equals(type2)) Then
                Return
            End If

            If ((type1.Equals("ckcOnlineLibrary.ManageBooksViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.SearchBookForManagerViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.SearchBooksViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.IssueBookViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.ReturnBookViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.ManageMemberViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.UserReviewBookViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.UserDashboardViewModel")) _
                    Or (type1.Equals("ckcOnlineLibrary.ManagerDashboardViewModel"))) Then

                If ((type2.Equals("ckcOnlineLibrary.ManageBooksViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.SearchBookForManagerViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.SearchBooksViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.IssueBookViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.ReturnBookViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.ManageMemberViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.UserReviewBookViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.UserDashboardViewModel")) _
                        Or (type2.Equals("ckcOnlineLibrary.ManagerDashboardViewModel"))) Then

                    m_lastViewModel.Cleanup()

                End If

            End If

        End If
    End Sub
    Private Sub RegisterNewViewModel(ByRef vm As ViewModel)
        m_lastViewModel = vm
    End Sub

    Public Sub New()

        _sp = ServiceProviderBase.Instance

    End Sub

    Public Sub RefreshService()

        '_sp.LibraryDataService.RefreshLibraryService()
        _sp = ServiceProviderBase.CreateInstance

    End Sub

    Public ReadOnly Property DomainContext As ckcLibraryDomainContext
        Get
            Return _sp.LibraryDataService.DomainContext
        End Get
    End Property

    Public ReadOnly Property LibraryDataService As ILibraryDataService
        Get
            Return _sp.LibraryDataService
        End Get
    End Property

    Private m_SuggestionStatusVM As SuggestionStatusViewModel
    Public ReadOnly Property SuggestionStatusVM As SuggestionStatusViewModel
        Get
            m_SuggestionStatusVM = New SuggestionStatusViewModel(_sp.PageConductor, _sp.LibraryDataService)
            Return m_SuggestionStatusVM
        End Get
    End Property
    Public ReadOnly Property ExistingSuggestionStatusVM As SuggestionStatusViewModel
        Get
            Return m_SuggestionStatusVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 VM for all places that use it. Just an option
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private m_EditUserInformationVM As EditUserInformationViewModel
    Public ReadOnly Property EditUserInformationVM As EditUserInformationViewModel
        Get
            If IsNothing(m_EditUserInformationVM) Then
                m_EditUserInformationVM = New EditUserInformationViewModel(_sp.PageConductor, _sp.LibraryDataService)
            ElseIf (WebContext.Current.User.IsAuthenticated) And IsNothing(m_EditUserInformationVM.Members(0)) Then
                m_EditUserInformationVM = New EditUserInformationViewModel(_sp.PageConductor, _sp.LibraryDataService)
            End If
            Return m_EditUserInformationVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 VM for all places that use it. Just an option
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Private m_ManageBooksVM As ManageBooksViewModel
    Public ReadOnly Property ManageBooksVMForChildWindow As ManageBooksViewModel
        Get
            CleanLastViewModel(New ManageBooksViewModel())
            If m_ManageBooksVM Is Nothing Then
                m_ManageBooksVM = New ManageBooksViewModel(_sp.PageConductor, _sp.LibraryDataService)
            End If
            RegisterNewViewModel(m_ManageBooksVM)
            Return m_ManageBooksVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property ManageBooksVM As ManageBooksViewModel
        Get
            CleanLastViewModel(New ManageBooksViewModel())
            m_ManageBooksVM = New ManageBooksViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(m_ManageBooksVM)
            Return m_ManageBooksVM
        End Get
    End Property

    Private m_SearchBookForUserVM As SearchBookViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property SearchBookForUserVM As SearchBookViewModel
        Get
            CleanLastViewModel(New SearchBookViewModel())
            m_SearchBookForUserVM = New SearchBookViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(m_SearchBookForUserVM)
            Return m_SearchBookForUserVM
        End Get
    End Property

    Private m_SearchBookForManagerVM As SearchBookForManagerViewModel
    Public ReadOnly Property SearchBookForManagerVM As SearchBookForManagerViewModel
        Get
            CleanLastViewModel(New SearchBookForManagerViewModel())
            m_SearchBookForManagerVM = New SearchBookForManagerViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(m_SearchBookForManagerVM)
            Return m_SearchBookForManagerVM
        End Get
    End Property

    Private m_EditBook As Book
    Public Property EditBook As Book
        Get
            Return m_EditBook
        End Get
        Set(value As Book)
            m_EditBook = value
        End Set
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View, but using collection source data of existing ManageBooksViewModel
    Private m_EditBookVM As EditBookViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property EditBookVM As EditBookViewModel
        Get
            m_EditBookVM = New EditBookViewModel(
                                _sp.PageConductor,
                                _sp.LibraryDataService,
                                m_EditBook
                            )
            Return m_EditBookVM
        End Get
    End Property

    Private m_EditAuthor As Author
    Public Property EditAuthor As Author
        Get
            Return m_EditAuthor
        End Get
        Set(value As Author)
            m_EditAuthor = value
            If Not IsNothing(value) Then
                Authors = Nothing
            End If
        End Set
    End Property
    Private m_Authors As List(Of Author)
    Public Property Authors As List(Of Author)
        Get
            Return m_Authors
        End Get
        Set(value As List(Of Author))
            m_Authors = value
            If Not IsNothing(value) Then
                EditAuthor = Nothing
            End If
        End Set
    End Property
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View, but using collection source data of existing ManageBooksViewModel
    Private m_EditAuthorVM As EditAuthorViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property EditAuthorVM As EditAuthorViewModel
        Get
            m_EditAuthorVM = New EditAuthorViewModel(
                                    _sp.PageConductor,
                                    _sp.LibraryDataService,
                                    m_Authors,
                                    m_EditAuthor
                                 )
            Return m_EditAuthorVM
        End Get
    End Property

    Private m_EditCategory As Category
    Public Property EditCategory As Category
        Get
            Return m_EditCategory
        End Get
        Set(value As Category)
            m_EditCategory = value
            If Not IsNothing(value) Then
                Categories = Nothing
            End If
        End Set
    End Property
    Private m_Categories As List(Of Category)
    Public Property Categories As List(Of Category)
        Get
            Return m_Categories
        End Get
        Set(value As List(Of Category))
            m_Categories = value
            If Not IsNothing(value) Then
                EditCategory = Nothing
            End If
        End Set
    End Property
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View, but using collection source data of existing ManageBooksViewModel
    Private m_EditCategoryVM As EditCategoryViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property EditCategoryVM As EditCategoryViewModel
        Get
            m_EditCategoryVM = New EditCategoryViewModel(
                                    _sp.PageConductor,
                                    _sp.LibraryDataService,
                                    m_Categories,
                                    m_EditCategory
                                 )
            Return m_EditCategoryVM
        End Get
    End Property

    Private m_EditShelf As Shelf
    Public Property EditShelf As Shelf
        Get
            Return m_EditShelf
        End Get
        Set(value As Shelf)
            m_EditShelf = value
            If Not IsNothing(value) Then
                Shelves = Nothing
            End If
        End Set
    End Property
    Private m_Shelves As List(Of Shelf)
    Public Property Shelves As List(Of Shelf)
        Get
            Return m_Shelves
        End Get
        Set(value As List(Of Shelf))
            m_Shelves = value
            If Not IsNothing(value) Then
                EditShelf = Nothing
            End If
        End Set
    End Property
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View, but using collection source data of existing ManageBooksViewModel
    Private m_EditShelfVM As EditShelfViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property EditShelfVM As EditShelfViewModel
        Get
            m_EditShelfVM = New EditShelfViewModel(
                                    _sp.PageConductor,
                                    _sp.LibraryDataService,
                                    m_Shelves,
                                    m_EditShelf
                                 )
            Return m_EditShelfVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    Private _manageMemberVM As ManageMemberViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property ManageMemberVM As ManageMemberViewModel
        Get
            CleanLastViewModel(New ManageMemberViewModel())
            _manageMemberVM = New ManageMemberViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(_manageMemberVM)
            Return _manageMemberVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    Private _managerDashBoardVM As ManagerDashboardViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property ManagerDashBoardVM As ManagerDashboardViewModel
        Get
            CleanLastViewModel(New ManagerDashboardViewModel())
            _managerDashBoardVM = New ManagerDashboardViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(_managerDashBoardVM)
            Return _managerDashBoardVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    Private _userDashBoardVM As UserDashboardViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property UserDashBoardVM As UserDashboardViewModel
        Get
            CleanLastViewModel(New UserDashboardViewModel())
            _userDashBoardVM = New UserDashboardViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(_userDashBoardVM)
            Return _userDashBoardVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    Private _userReviewBookVM As UserReviewBookViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property UserReviewBookVM As UserReviewBookViewModel
        Get
            CleanLastViewModel(New UserReviewBookViewModel())
            _userReviewBookVM = New UserReviewBookViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(_userReviewBookVM)
            Return _userReviewBookVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    Private _issueBookVM As IssueBookViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property IssueBookVM As IssueBookViewModel
        Get
            CleanLastViewModel(New IssueBookViewModel())
            _issueBookVM = New IssueBookViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(_issueBookVM)
            Return _issueBookVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    Private _returnBookVM As ReturnBookViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public ReadOnly Property ReturnBookVM As ReturnBookViewModel
        Get
            CleanLastViewModel(New ReturnBookViewModel())
            _returnBookVM = New ReturnBookViewModel(_sp.PageConductor, _sp.LibraryDataService)
            RegisterNewViewModel(_returnBookVM)
            Return _returnBookVM
        End Get
    End Property

    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 1 new instance per View 
    Private _bookDetailID As Integer
    Private _memberID As Integer
    Private _bookDetailHistoryVM As BookDetailHistoryViewModel
    ''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public WriteOnly Property BookDetailID As Integer
        Set(value As Integer)
            _bookDetailID = value
            _bookDetailHistoryVM = Nothing
            _memberID = -1
        End Set
    End Property
    Public WriteOnly Property MemberID As Integer
        Set(value As Integer)
            _memberID = value
        End Set
    End Property

    Public ReadOnly Property BookDetailHistoryVM As BookDetailHistoryViewModel
        Get
            CleanLastViewModel(New BookDetailHistoryViewModel())
            If IsNothing(_bookDetailHistoryVM) Then
                _bookDetailHistoryVM = New BookDetailHistoryViewModel(_sp.PageConductor, _sp.LibraryDataService, _bookDetailID, _memberID)
            End If
            RegisterNewViewModel(_bookDetailHistoryVM)
            Return _bookDetailHistoryVM
        End Get
    End Property

    Private _reviewTransaction As Transactions_Detail
    Public Property ReviewTransaction As Transactions_Detail
        Get
            Return _reviewTransaction
        End Get
        Set(value As Transactions_Detail)
            _reviewTransaction = value
        End Set
    End Property
    Public ReadOnly Property ReviewBookVM As ReviewBookViewModel
        Get
            Return New ReviewBookViewModel(_sp.PageConductor, _sp.LibraryDataService, _reviewTransaction)
        End Get
    End Property

    Private _readCommentTransaction As Member_Transactions
    Public Property ReadCommentTransaction As Member_Transactions
        Get
            Return _readCommentTransaction
        End Get
        Set(value As Member_Transactions)
            _readCommentTransaction = value
            _editTransactionVM = New EditTransactionViewModel(_sp.PageConductor, _sp.LibraryDataService, _readCommentTransaction)
        End Set
    End Property
    Private _editTransactionVM As EditTransactionViewModel
    Public ReadOnly Property EditTransactionVM As EditTransactionViewModel
        Get
            Return _editTransactionVM
        End Get
    End Property

End Class