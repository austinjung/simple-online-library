﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common


Public Class BookDomainCollectionViewModel
    Inherits ViewModel

    Private ReadOnly _context As ckcLibraryDomainContext = New ckcLibraryDomainContext()

    Private ReadOnly _book_View As DomainCollectionView(Of Book)
    Private ReadOnly _book_Loader As DomainCollectionViewLoader(Of Book)
    Private ReadOnly _book_Source As EntityList(Of Book)

    Private ReadOnly _category_View As DomainCollectionView(Of Category)
    Private ReadOnly _category_Loader As DomainCollectionViewLoader(Of Category)
    Private ReadOnly _category_Source As EntityList(Of Category)

    Private ReadOnly _author_View As DomainCollectionView(Of Author)
    Private ReadOnly _author_Loader As DomainCollectionViewLoader(Of Author)
    Private ReadOnly _author_Source As EntityList(Of Author)

    Private ReadOnly _searchCommand As ICommand

    Private _canLoad As Boolean = True
    Private _searchText As String = String.Empty

#Region "Properties"
    Public ReadOnly Property CategoryCollectionView As ICollectionView
        Get
            Return Me._category_View
        End Get
    End Property

    Public ReadOnly Property AuthorCollectionView As ICollectionView
        Get
            Return Me._author_View
        End Get
    End Property

    Public ReadOnly Property BookCollectionView As ICollectionView
        Get
            Return Me._book_View
        End Get
    End Property

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property SearchText() As String
        Get
            Return Me._searchText
        End Get
        Set(value As String)
            If Me._searchText <> value Then
                Me._searchText = value
                Me.RaisePropertyChanged("SearchText")
            End If
        End Set
    End Property

    Public ReadOnly Property SearchCommand() As ICommand
        Get
            Return Me._searchCommand
        End Get
    End Property
#End Region

    Public Sub New()
        Me._book_Source = New EntityList(Of Book)(Me._context.Books)
        Me._book_Loader = New DomainCollectionViewLoader(Of Book)(
                                        AddressOf Me.LoadBookEntities,
                                        AddressOf Me.OnLoadBookEntitiesCompleted)
        Me._book_View = New DomainCollectionView(Of Book)(Me._book_Loader, Me._book_Source)

        ' Region : Swap out the loader for design-time scenarios
        If DesignerProperties.IsInDesignTool Then
            Dim bookDesignTimeLoader As BookDomainCollectionViewDesignTimeLoader = New BookDomainCollectionViewDesignTimeLoader()
            Me._book_View = New DomainCollectionView(Of Book)(bookDesignTimeLoader, bookDesignTimeLoader.Source)
        End If
        ' EndRegion

        Me._searchCommand = New RelayCommand(AddressOf Me.OnSearch)

        ' Go back to the first page when the sorting changes
        Dim notifyingSortDescriptions As INotifyCollectionChanged = DirectCast(Me.BookCollectionView.SortDescriptions, INotifyCollectionChanged)
        AddHandler notifyingSortDescriptions.CollectionChanged, New NotifyCollectionChangedEventHandler(AddressOf Me._book_View.MoveToFirstPage)

        Using Me.BookCollectionView.DeferRefresh()
            Me._book_View.PageSize = 10
            Me._book_View.MoveToFirstPage()
        End Using

    End Sub

    Private Function LoadBookEntities() As LoadOperation(Of Book)
        Me.CanLoad = False
        Dim query As EntityQuery(Of Book)
        If Not String.IsNullOrWhiteSpace(Me.SearchText) Then
            query = Me._context.GetActiveBooks_By_TitleQuery(Me.SearchText)
        Else
            query = Me._context.GetActiveBooksQuery()
        End If
        Return Me._context.Load(query.SortAndPageBy(Me._book_View))
    End Function

    Private Sub OnLoadBookEntitiesCompleted(op As LoadOperation(Of Book))
        Me.CanLoad = True

        If op.HasError Then
            ' TODO: handle errors
            op.MarkErrorAsHandled()
        ElseIf Not op.IsCanceled Then
            Me._book_Source.Source = op.Entities

            If op.TotalEntityCount <> -1 Then
                Me._book_View.SetTotalItemCount(op.TotalEntityCount)
            End If
        End If
    End Sub

    Private Sub OnSearch()
        ' This makes sure we refresh even if we're already on the first page
        Using Me._book_View.DeferRefresh()
            ' This will lead us to re-query for the total count
            Me._book_View.SetTotalItemCount(-1)
            Me._book_View.MoveToFirstPage()
        End Using
    End Sub

End Class
