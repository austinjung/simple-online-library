﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.ServiceModel.DomainServices.Client
Imports GalaSoft.MvvmLight.Command
Imports GalaSoft.MvvmLight.Messaging
Imports Ria.Common
Imports Papa.Common
Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports GalaSoft.MvvmLight

Public Class ManageMemberViewModel
    Inherits ViewModel

#Region "Data & Page Services"
    Protected Property PageConductor As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property
    Private m_PageConductor As IPageConductor

    Public Property LibraryDataService As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property
    Private m_LibraryDataService As ILibraryDataService
#End Region

#Region "Private Variables"

    'members
    Private _members As DomainCollectionView(Of Member) = Nothing

    'selected member
    Private _selected_Member As Member = Nothing
    Private _isEdited As Boolean = False

    'member page size
    Private _memberPageSize As Integer = 7

    'View Model Changed Status
    Private _canLoad As Boolean = True
    Private _hasChanges As Boolean = False

    'Filter Transactions 
    Private _filter_Keyword As String = String.Empty
    Private _filter_Targets As ObservableCollection(Of MemberFilterTarget) = MemberFilterTargets.GetFilterTargets

    'Current DataGrid variables
    Private SortingGridByChecked As Boolean = False

    'Sort Directions/ Sorter
    Private sortDirection As Integer = ListSortDirection.Ascending
    Private sorter As String = "Sort By Name"

    'Commands
    Private m_SearchMemberCommand As RelayCommand(Of ComboBox)
    Private _sortDescriptorChangeCommand As RelayCommand(Of ComboBox)
    Private _sortDirectorChangeCommand As RelayCommand(Of ComboBox)
    Private m_MemberSelectionChangeCommand As RelayCommand(Of DataGrid)

#End Region

#Region "Private Method : Property changed event"

    Private Sub libraryDataService_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "MembersLoaded" Then
            HasChanges = True
            Me.RaisePropertyChanged("Members")
        End If
    End Sub

#End Region

#Region "Status Properties"

    Public Property CanLoad() As Boolean
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

    Public Property HasChanges() As Boolean
        Get
            Return _hasChanges
        End Get
        Private Set(value As Boolean)
            _hasChanges = value
            Me.RaisePropertyChanged("HasChanges")
        End Set
    End Property

#End Region

#Region "View Models Public Properties"

    Public ReadOnly Property Members As ICollectionView
        Get
            Return _members
        End Get
    End Property

    Public Property Filter_Keyword As String
        Get
            Return Me._filter_Keyword
        End Get
        Set(value As String)
            Me._filter_Keyword = value
        End Set
    End Property

    Public ReadOnly Property Filter_Targets As ObservableCollection(Of MemberFilterTarget)
        Get
            Return Me._filter_Targets
        End Get
    End Property

    Public Property Selected_Member As Member
        Get
            Return _selected_Member
        End Get
        Set(value As Member)
            _selected_Member = value
        End Set
    End Property

    Public Property IsEdited As Boolean
        Get
            Return _isEdited
        End Get
        Set(value As Boolean)
            _isEdited = value
        End Set
    End Property

#End Region

#Region "Constructors & Public Methods"

    Public Sub New()

    End Sub

    Public Sub New(pageConductor As IPageConductor, libraryDataService As ILibraryDataService)

        Me.PageConductor = pageConductor
        Me.LibraryDataService = libraryDataService
        ''''''''''''''''''''''''''''
        'Refresh Domain Data Context to manage compact : every page will cause a new 
        'Me.LibraryDataService.RefreshLibraryDataService()
        ''''''''''''''''''''''''''''
        Me._members = libraryDataService.MemberCollectionView
        Me._members.SortDescriptions.Add(New SortDescription("FullName", ListSortDirection.Ascending))
        Me.LibraryDataService.FilterMemberRole = "Manager,User"
        Me.LibraryDataService.FilterActiveMember = True

        If TypeOf libraryDataService Is LibraryDataService Then
            AddHandler CType(libraryDataService, LibraryDataService).PropertyChanged, AddressOf Me.libraryDataService_PropertyChanged
        End If

        Me.LibraryDataService.LoadMembers()

        Using (Me._members.DeferRefresh())
            Me._members.PageSize = _memberPageSize
            Me._members.SetTotalItemCount(-1)
            Me._members.MoveToFirstPage()
        End Using

    End Sub

#End Region

#Region "Commands Properties"

    Public Property MemberSelectionChangeCommand() As RelayCommand(Of DataGrid)
        Get
            Return m_MemberSelectionChangeCommand
        End Get
        Set(value As RelayCommand(Of DataGrid))
            m_MemberSelectionChangeCommand = value
        End Set
    End Property

    Public Property SearchMemberCommand() As RelayCommand(Of ComboBox)
        Get
            Return m_SearchMemberCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            m_SearchMemberCommand = value
        End Set
    End Property

    Public Property SortDescriptorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDescriptorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDescriptorChangeCommand = value
        End Set
    End Property

    Public Property SortDirectorChangeCommand As RelayCommand(Of ComboBox)
        Get
            Return _sortDirectorChangeCommand
        End Get
        Set(value As RelayCommand(Of ComboBox))
            _sortDirectorChangeCommand = value
        End Set
    End Property

#End Region

#Region "Register Commands"

    Protected Overrides Sub RegisterCommands()
        SearchMemberCommand = New RelayCommand(Of ComboBox)(AddressOf SearchMember,
                                                            Function(sender) Not IsNothing(sender))
        SortDescriptorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDescriptorChange,
                                              Function() CType(_members, DomainCollectionView).ItemCount > 0)
        SortDirectorChangeCommand = New RelayCommand(Of ComboBox)(AddressOf OnSortDirectorChange,
                                              Function() CType(_members, DomainCollectionView).ItemCount > 0)
        MemberSelectionChangeCommand = New RelayCommand(Of DataGrid)(AddressOf OnMemberSelectionChange,
                                              Function() CType(_members, DomainCollectionView).ItemCount > 0)
    End Sub

    Protected Sub UnregisterCommands()
        SearchMemberCommand = Nothing
        SortDescriptorChangeCommand = Nothing
        SortDirectorChangeCommand = Nothing
        MemberSelectionChangeCommand = Nothing
    End Sub

#End Region

#Region "ICleanup interface"
    Public Overrides Sub Cleanup()

        If TypeOf LibraryDataService Is LibraryDataService Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub

    Public Sub RemovePropertyChangeHandler()

        If TypeOf LibraryDataService Is LibraryDataService And Not IsNothing(LibraryDataService) Then
            RemoveHandler CType(LibraryDataService, LibraryDataService).PropertyChanged, AddressOf libraryDataService_PropertyChanged
        End If

        UnregisterCommands()
        UnregisterMessages()

    End Sub
#End Region

#Region "Register Message & Handle"

    Protected Overrides Sub RegisterMessages()
        Messenger.Reset()
        Messenger.[Default].Register(Of SavedMemberDialogMessage)(Me, AddressOf OnSaveMemberResultMessageReceived)
    End Sub

    Private Sub UnregisterMessages()
        Messenger.[Default].Unregister(Of SavedMemberDialogMessage)(Me, AddressOf OnSaveMemberResultMessageReceived)
    End Sub

    Private Sub OnSaveMemberResultMessageReceived(msg As SavedMemberDialogMessage)

        Dim currentPosition = Me._members.CurrentPosition
        Dim currentPage = Me._members.PageIndex
        Dim pageTotal = CType(Me._members.SourceCollection, EntityList(Of Member)).Count - 1
        If msg.Type.Equals("Error") Then
        ElseIf IsNothing(msg.SavedMember) Then
        ElseIf msg.Content = "Add" Then
            Using (Me._members.DeferRefresh())
                Me._members.SetTotalItemCount(-1)
                Me._members.MoveToFirstPage()
            End Using
        ElseIf Me.LibraryDataService.FilterActiveMember And msg.SavedMember.IsActive = False Then
            Using (Me._members.DeferRefresh())
                Me._members.SetTotalItemCount(-1)
                If currentPosition = 0 And pageTotal = currentPosition Then
                    Me._members.MoveToPreviousPage()
                Else
                    Me._members.MoveToPage(currentPage)
                End If
            End Using
            If currentPosition = 0 And pageTotal = currentPosition Then
                Me._members.MoveCurrentToPosition(pageTotal)
            Else
                Me._members.MoveCurrentToPosition(currentPosition)
            End If
        Else
            Me._members.Refresh()
        End If

        Me.Selected_Member = Me.Members.CurrentItem

    End Sub

#End Region

#Region "Private Methods : Handling Commands"

    Private Sub SearchMember(searchTargetsCombo As ComboBox)

        'For Each target In Me.Filter_Targets
        Dim includeManager As Boolean = True
        Dim includeUser As Boolean = True
        Dim includeDeactive As Boolean = False

        For Each target As MemberFilterTarget In searchTargetsCombo.Items
            If target.Target.Contains("Full Name") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterMemberName = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterMemberName = Nothing
                End If
            ElseIf target.Target.Contains("Login") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterMemberLogin = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterMemberLogin = Nothing
                End If
            ElseIf target.Target.Contains("Email") Then
                If target.IsChecked Then
                    Me.LibraryDataService.FilterMemberEmail = Me._filter_Keyword.Trim()
                Else
                    Me.LibraryDataService.FilterMemberEmail = Nothing
                End If
            ElseIf target.Target.Contains("Manager") Then
                If target.IsChecked Then
                    includeManager = True
                Else
                    includeManager = False
                End If
            ElseIf target.Target.Contains("User") Then
                If target.IsChecked Then
                    includeUser = True
                Else
                    includeUser = False
                End If
            ElseIf target.Target.Contains("Active Only") Then
                If target.IsChecked Then
                    includeDeactive = False
                Else
                    includeDeactive = True
                End If
            End If
        Next

        If includeDeactive Then
            Me.LibraryDataService.FilterActiveMember = False
        Else
            Me.LibraryDataService.FilterActiveMember = True
        End If
        If includeManager Then
            Me.LibraryDataService.FilterMemberRole = "Manager,"
        Else
            Me.LibraryDataService.FilterMemberRole = String.Empty
        End If
        If includeUser Then
            Me.LibraryDataService.FilterMemberRole &= "User"
        End If

        Using (Me._members.DeferRefresh())
            Me._members.SetTotalItemCount(-1)
            Me._members.MoveToFirstPage()
        End Using

    End Sub

    Private Sub OnMemberSelectionChange(grid As DataGrid)

        If Not IsNothing(_selected_Member) And _isEdited Then
            _isEdited = False
            Messenger.[Default].Register(Of ConfirmDialogMessage)(Me, AddressOf SaveEditedMember)
            Dim alertMsg As String = _selected_Member.FullName & "'s information is changed." & vbCrLf & "Do you want to save?"
            Dim MyMessageBox = New MyMessageBox("Inform", "Information Changed", alertMsg, MessageBoxButton.OKCancel)
            MyMessageBox.Show()
        End If

        _selected_Member = CType(grid.ItemsSource, DomainCollectionView(Of Member)).CurrentItem
        Me.HasChanges = True
        Me.RaisePropertyChanged("Selected_Member")

    End Sub

    Private Sub SaveEditedMember(msg As ConfirmDialogMessage)

        If msg.Caption.Equals("DialogResultOK") Then
            Me.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveEditedMember, Nothing)
        End If
        Messenger.[Default].Unregister(Of ConfirmDialogMessage)(Me, AddressOf SaveEditedMember)

    End Sub

    Public Sub AfterSaveEditedMember(result As ServiceSubmitChangesResult)
        Dim dialogMessage As SavedMemberDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim editedMember As Member = Nothing

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.AddedEntities.Count > 0 Then
                action = "Add"
                For Each e As Object In result.ChangeSet.AddedEntities
                    If TypeOf (e) Is Member Then
                        editedMember = CType(e, Member)
                        resultMessage = editedMember.FullName & " is added successfully."
                    End If
                Next
            End If
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member Then
                        editedMember = CType(e, Member)
                        resultMessage = editedMember.FullName & " is updated successfully."
                        If editedMember.MemberID = WebContext.Current.User.MemberID Then
                            Dim busyIndicator As BusyIndicator = DirectCast(Application.Current.RootVisual, FrameworkElement)
                            Dim mainPage As MainPage = DirectCast(busyIndicator.Content, MainPage)
                            Dim loginStatus = CType(mainPage.loginContainer.Child, LoginUI.LoginStatus)
                            loginStatus.UpdateWelcomeText(editedMember.FullName)
                        End If
                    End If
                Next
            End If
        End If

        If Not String.IsNullOrEmpty(resultMessage) Then
            dialogMessage = New SavedMemberDialogMessage(dialogType, action, resultMessage, editedMember)
            Messenger.[Default].Send(dialogMessage)
            Dim MyMessageBox = New MyMessageBox(dialogType, action, resultMessage, MessageBoxButton.OK)
            MyMessageBox.Show()
        End If

    End Sub

    Private Sub OnSortDirectorChange(sortDirector As ComboBox)

        If sortDirector.SelectedItem.Content.contains("Asc") Then
            sortDirection = ListSortDirection.Ascending
        Else
            sortDirection = ListSortDirection.Descending
        End If

        SortRequests()

    End Sub

    Private Sub OnSortDescriptorChange(sortDescriptor As ComboBox)

        sorter = sortDescriptor.SelectedItem.Content
        SortRequests()

    End Sub

    Private Sub SortRequests()
        If sorter.Contains("Name") Then
            _members.SortDescriptions.Clear()
            _members.SortDescriptions.Add(New SortDescription("FullName", sortDirection))
        ElseIf sorter.Contains("Login") Then
            _members.SortDescriptions.Clear()
            _members.SortDescriptions.Add(New SortDescription("MemberLogin", sortDirection))
        ElseIf sorter.Contains("Active") Then
            _members.SortDescriptions.Clear()
            _members.SortDescriptions.Add(New SortDescription("MemberActive", sortDirection))
        ElseIf sorter.Contains("Since") Then
            _members.SortDescriptions.Clear()
            _members.SortDescriptions.Add(New SortDescription("MemberSince", sortDirection))
        ElseIf sorter.Contains("Deactivated") Then
            _members.SortDescriptions.Clear()
            _members.SortDescriptions.Add(New SortDescription("MemberDeactivatedFrom", sortDirection))
        End If
        _members.Refresh()
    End Sub

#End Region

End Class
