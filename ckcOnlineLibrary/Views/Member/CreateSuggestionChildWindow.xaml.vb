﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common
Imports System.ServiceModel.DomainServices.Client.ApplicationServices
Imports System.Windows.Browser

Partial Public Class CreateSuggestionChildWindow
    Inherits ChildWindow

    Private newSuggestion As NewBookRequest = Nothing
    Private linkUri As String = String.Empty
    Private popupOption As HtmlPopupWindowOptions
    Private domainContext As ckcLibraryDomainContext
    Private libraryDataService As LibraryDataService

    Public Sub New()

        InitializeComponent()

        libraryDataService = CType(Me.DataContext, LibraryDataService)
        domainContext = libraryDataService.DomainContext

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

    End Sub

    Private Sub OKButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles OKButton.Click

        newSuggestion = New NewBookRequest With {
            .MemberID = WebContext.Current.User.MemberID, _
            .RequestContent = Me.RequestTextBox.Text.Trim, _
            .BookTitle = Me.BookTitle.Text.Trim, _
            .ISBN = Me.ISBN.Text.Trim, _
            .LinkURL = Me.LinkURL.Text.Trim, _
            .RequestDate = Now(), _
            .RequestViewed = False, _
            .SuggestionCategoryID = 1, _
            .ReviewSequence = 1, _
            .ReviewViewed = False, _
            .RequestClosed = False
            }
        domainContext.NewBookRequests.Add(newSuggestion)
        libraryDataService.SubmitChanges(AddressOf AfterSaveNewSuggestion, Nothing)
        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub

    Private Sub AfterSaveNewSuggestion(result As ServiceSubmitChangesResult)

        If (result.[Error] IsNot Nothing) Then
            Dim myMessageBox = New MyMessageBox("Error", "Save", "Your suggestion was not saved." & vbCrLf & result.Error.Message, MessageBoxButton.OK)
            myMessageBox.Show()
        Else
            Dim myMessageBox = New MyMessageBox("Success", "Save", "Your suggestion was saved successfully.", MessageBoxButton.OK)
            myMessageBox.Show()
        End If

    End Sub

    Private Sub LinkURL_TextChanged(sender As System.Object, e As System.Windows.Controls.TextChangedEventArgs) Handles LinkURL.TextChanged
        Me.OKButton.IsEnabled = True
    End Sub

    Private Sub BookTitle_TextChanged(sender As System.Object, e As System.Windows.Controls.TextChangedEventArgs) Handles BookTitle.TextChanged
        Me.OKButton.IsEnabled = True
    End Sub

    Private Sub ISBN_TextChanged(sender As System.Object, e As System.Windows.Controls.TextChangedEventArgs) Handles ISBN.TextChanged
        Me.OKButton.IsEnabled = True
    End Sub

End Class
