﻿Imports System.Windows.Data
Imports System.Windows.Controls.DataGrid
Imports GalaSoft.MvvmLight.Messaging
Imports System.Collections.ObjectModel
Imports Microsoft.Windows.Data.DomainServices

Partial Public Class ReviewBooksPage
    Inherits Page

    Private _comboLabelItem As KeywordFilterTarget = New KeywordFilterTarget("Add/Remove Target(s)")
    Private _FilterTargetListWithoutLabel As ObservableCollection(Of KeywordFilterTarget)
    Private _FilterTargetListWithLabel As ObservableCollection(Of KeywordFilterTarget)

    Public Sub New()
        InitializeComponent()

        _FilterTargetListWithoutLabel = KeywordFilterTargets.GetFilterTargets()
        _FilterTargetListWithLabel = KeywordFilterTargets.GetFilterTargets()
        _FilterTargetListWithLabel.Insert(0, _comboLabelItem)

        RegisterMessage()
    End Sub

    'Executes when the user navigates to this page.
    Protected Overrides Sub OnNavigatedTo(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    'Executes when the user navigates from this page.
    Protected Overrides Sub OnNavigatedFrom(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Unloaded

        CType(Me.DataContext, UserReviewBookViewModel).RemovePropertyChangeHandler()

    End Sub

    Private Sub RegisterMessage()
        'Messenger.Reset()
        'Messenger.Default.Register(Of LaunchEditBookMessage)(Me, AddressOf OnLaunchEditBook)
    End Sub

    Private Sub FilterTargetList_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles FilterTargetList.DropDownClosed
        DoSomethingOnClose()
    End Sub

    Private Sub FilterTargetList_DropDownOpened(sender As System.Object, e As System.EventArgs) Handles FilterTargetList.DropDownOpened
        CType(sender, ComboBox).SelectedIndex = -1
        CType(sender, ComboBox).ItemsSource = _FilterTargetListWithoutLabel
    End Sub

    Private Sub Page_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        DoSomethingOnClose()
    End Sub

    Private Sub DoSomethingOnClose()
        If Not IsNothing(Me.FilterTargetList.SelectedItem) Then

            CType(Me.FilterTargetList.SelectedItem, KeywordFilterTarget).IsChecked = Not CType(Me.FilterTargetList.SelectedItem, KeywordFilterTarget).IsChecked

            For Each target In Me._FilterTargetListWithLabel
                If target.Target = CType(Me.FilterTargetList.SelectedItem, KeywordFilterTarget).Target Then
                    target.IsChecked = CType(Me.FilterTargetList.SelectedItem, KeywordFilterTarget).IsChecked
                    Exit For
                End If
            Next

        End If

        Me.FilterTargetList.ItemsSource = _FilterTargetListWithLabel
        Me.FilterTargetList.SelectedIndex = 0
    End Sub

    'Private Sub BooksDataGrid_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles BooksDataGrid.MouseRightButtonDown
    '    If TypeOf (sender) Is DataGrid Then
    '        Dim dataGrid = CType(sender, DataGrid)
    '        Dim originalSource = e.OriginalSource
    '        Dim gridItem As Book
    '        If TypeOf (originalSource) Is FrameworkElement Then
    '            Try
    '                gridItem = CType(originalSource, FrameworkElement).DataContext
    '                CType(dataGrid.ItemsSource, DomainCollectionView(Of Book)).MoveCurrentTo(gridItem)
    '            Catch ex As Exception
    '                'do nothing
    '                Return
    '            End Try
    '        End If
    '    End If
    'End Sub
End Class


