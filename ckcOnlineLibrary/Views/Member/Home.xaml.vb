﻿Imports System.Windows.Controls
Imports System.Windows.Navigation
Imports ckcOnlineLibrary.LoginUI
Imports Papa.Common
Imports System.ComponentModel
Imports GalaSoft.MvvmLight.Messaging
Imports Microsoft.Windows.Data.DomainServices
Imports SilverlightHtmlHost
Imports System.Windows.Browser

''' <summary>
''' Home page for the application.
''' </summary>
Partial Public Class Home
    Inherits Page

    Private newTransactions As MyTransactions = Nothing

    Private notificationUri As String = String.Empty
    Private isNotified As Boolean = False
    Private popupOption As HtmlPopupWindowOptions = New HtmlPopupWindowOptions With {
                                                            .Left = 0,
                                                            .Top = 0,
                                                            .Width = 800,
                                                            .Height = 600,
                                                            .Menubar = False,
                                                            .Toolbar = False}
    Private notificationWindow As HtmlHostChildWindow

    ''' <summary>
    ''' Creates a new <see cref="Home"/> instance.
    ''' </summary>
    Public Sub New()

        InitializeComponent()
        Me.Title = ApplicationStrings.HomePageTitle
        AddHandler CType(Me.DataContext, UserDashboardViewModel).PropertyChanged, AddressOf UserDashboard_PropertyChanged

        If Not WebContext.Notified Then
            Dim vmLocator As ViewModelLocator = Nothing
            For Each resource In App.Current.Resources.MergedDictionaries
                vmLocator = resource.Item("VMLocator")
                If Not IsNothing(vmLocator) Then
                    Exit For
                End If
            Next
            Dim domainContext As ckcLibraryDomainContext = vmLocator.DomainContext
            Dim notificationResult = domainContext.GetNotificationUrl(AddressOf ShowNotification, Nothing)
        End If

    End Sub

    ''' <summary>
    ''' Executes when the user navigates to this page.
    ''' </summary>
    Protected Overloads Overrides Sub OnNavigatedTo(ByVal e As NavigationEventArgs)

        'Dim eventId As String
        'Try
        '    eventId = NavigationContext.QueryString("EventID")
        'Catch ex As Exception
        'End Try

        'If Not WebContext.Current.User.IsAuthenticated Then
        '    Dim loginWindow As LoginRegistrationWindow = New LoginRegistrationWindow()
        '    loginWindow.Show()
        'End If

    End Sub

#Region "Private Method : Property changed event"

    Private Sub UserDashboard_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "TopReviewsLoaded" Then
            Me.FilterAccordion.SelectionMode = AccordionSelectionMode.One
            Me.FilterAccordion.SelectedItem = Me.TopReviews
            Me.TopReviews.UpdateLayout()
        ElseIf e.PropertyName = "User_Pending_Requests" Then
            'If IsNothing(newTransactions) And Me.myPendingRequests.ActualHeight > 750 Then
            If IsNothing(newTransactions) _
                And CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount > 6 Then
                'CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 4
                CType(Me.DataContext, UserDashboardViewModel).LoadCurrentMemberTransactions()
                newTransactions = New MyTransactions()
                Me.MyContentStackPanel.Children.Remove(myTransactions)
                Me.MyAccordionPanel.Children.Add(newTransactions)
                Me.MyAccordionPanel.Width = 489
                Me.myTransactions = Nothing
                Me.MyAccordionPanel.UpdateLayout()
                Me.MyContentStackPanel.UpdateLayout()
            ElseIf IsNothing(myTransactions) _
                And CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount <= 6 Then
                If CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount > 3 Then
                    'CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 4
                    CType(Me.DataContext, UserDashboardViewModel).LoadCurrentMemberTransactions()
                Else
                    'CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 6
                    CType(Me.DataContext, UserDashboardViewModel).LoadCurrentMemberTransactions()
                End If
                myTransactions = New MyTransactions()
                Me.MyAccordionPanel.Children.Remove(newTransactions)
                Me.MyContentStackPanel.Children.Add(myTransactions)
                Me.newTransactions = Nothing
                Me.MyAccordionPanel.UpdateLayout()
                Me.MyContentStackPanel.UpdateLayout()
            Else
                If CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount > 3 Then
                    'CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 4
                    CType(Me.DataContext, UserDashboardViewModel).LoadCurrentMemberTransactions()
                Else
                    'CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 6
                    CType(Me.DataContext, UserDashboardViewModel).LoadCurrentMemberTransactions()
                End If
            End If
        ElseIf e.PropertyName = "Extend_User_Transactions" Then
            Dim busyIndicator As BusyIndicator = DirectCast(Application.Current.RootVisual, FrameworkElement)
            Dim mainPage As MainPage = DirectCast(busyIndicator.Content, MainPage)
            'mainPage.ContentFrame.Navigate(New Uri("/Views/Member/Home.xaml", UriKind.Relative))
            mainPage.ContentFrame.Refresh()
        End If
    End Sub

#End Region

    Private Sub Page_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Unloaded

        CType(Me.DataContext, UserDashboardViewModel).RemovePropertyChangeHandler()
        RemoveHandler CType(Me.DataContext, UserDashboardViewModel).PropertyChanged, AddressOf UserDashboard_PropertyChanged

    End Sub

    Private Sub ShowNotification(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of String))
        If Not String.IsNullOrEmpty(result.Value) Then

            WebContext.Notified = True

            '''''''''''''''''''''''''''''''''''''''''''''''''
            'Using iFrame in Silverlight
            notificationWindow = New HtmlHostChildWindow(result.Value & "?#toolbar=1")
            notificationWindow.Title = "Notification"
            notificationWindow.Show()
            '''''''''''''''''''''''''''''''''''''''''''''''''

            Dim source = Application.Current.Host.Source
            Dim myUrl As String = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/" & result.Value & "?#toolbar=1"

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Using HtmlPage.PopupWindow to launch new web browser
            'If HtmlPage.IsPopupWindowAllowed Then
            '    HtmlPage.PopupWindow(New Uri(myUrl), "new", popupOption)
            '    isNotified = True
            'Else
            '    notificationUri = myUrl
            'End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Using Javascript in default.html/default.aspx
            'HtmlPage.Window.Invoke("DialogWindow", myUrl)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        End If
    End Sub

End Class