﻿Imports System.Windows.Controls
Imports System.Windows.Navigation
Imports Nemiro.FileManager
Imports System.IO
Imports System.Windows.Media.Imaging

''' <summary>
''' <see cref="Page"/> class to present information about the current application.
''' </summary>
Partial Public Class About
    Inherits Page

    Private myPleaseWait As PleaseWait = Nothing ' progressbar
    Private bookImagesUrl As String

    ''' <summary>
    ''' Creates a new instance of the <see cref="About"/> class.
    ''' </summary>
    Public Sub New()
        InitializeComponent()

        Me.Title = ApplicationStrings.AboutPageTitle

        Thumbnails.Items.Clear()
        Dim source = Application.Current.Host.Source
        Dim myUrl As String = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/FileManagerGateway.ashx"
        bookImagesUrl = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/Upload/Book_Images/"
        fileList1.Url = myUrl 'set url

        ' handlers for progress
        AddHandler fileList1.Process, AddressOf fileList1_Process
        AddHandler fileList1.Complete, AddressOf fileList1_Complete

    End Sub

    ''' <summary>
    ''' Executes when the user navigates to this page.
    ''' </summary>
    Protected Overloads Overrides Sub OnNavigatedTo(ByVal e As NavigationEventArgs)
    End Sub

    Private Sub Page_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        'Me.Thumbnails.ItemsSource = Photograph.GetPhotographs().OrderBy(Function(p) p.Name)
    End Sub

    Private Sub fileList1_Process(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      If myPleaseWait IsNot Nothing AndAlso myPleaseWait.Visibility = System.Windows.Visibility.Visible Then
                                          Return
                                      End If
                                      ' show porgress
                                      myPleaseWait = New PleaseWait()
                                      myPleaseWait.Show()

                                  End Sub)
    End Sub

    Private Sub fileList1_Complete(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      'close progress
                                      If myPleaseWait IsNot Nothing Then
                                          myPleaseWait.Close()
                                          myPleaseWait = Nothing
                                      End If
                                      For Each itm As FileItem In fileList1.Items
                                          If itm.FileName <> "..." Then
                                              Dim img = New Image()
                                              img.Height = 100
                                              img.Source = New BitmapImage(New Uri(bookImagesUrl & itm.FileName.ToString(), UriKind.Absolute))
                                              Thumbnails.Items.Add(img)
                                          End If
                                      Next
                                  End Sub)
    End Sub

End Class