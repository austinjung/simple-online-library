﻿Public NotInheritable Class ViewTokens

    Private Sub New()
    End Sub

    Public Const Root As String = "Views"

    ' All View names go here
    Public Const ErrorOverlay As String = "ErrorWindow"
    Public Const Checkout As String = "CheckoutView"
    Public Const Home As String = "BookView"
    Public Const About As String = "About"

End Class