﻿Imports System.Windows.Controls
Imports System.Windows.Navigation
Imports ckcOnlineLibrary.LoginUI
Imports Papa.Common
Imports System.ComponentModel
Imports GalaSoft.MvvmLight.Messaging
Imports Microsoft.Windows.Data.DomainServices

''' <summary>
''' Home page for the application.
''' </summary>
Partial Public Class Home
    Inherits Page

    Private newTransactions As MyTransactions = Nothing

    ''' <summary>
    ''' Creates a new <see cref="Home"/> instance.
    ''' </summary>
    Public Sub New()

        InitializeComponent()
        Me.Title = ApplicationStrings.HomePageTitle
        AddHandler CType(Me.DataContext, UserDashboardViewModel).PropertyChanged, AddressOf UserDashboard_PropertyChanged

    End Sub

    ''' <summary>
    ''' Executes when the user navigates to this page.
    ''' </summary>
    Protected Overloads Overrides Sub OnNavigatedTo(ByVal e As NavigationEventArgs)

        'Dim eventId As String
        'Try
        '    eventId = NavigationContext.QueryString("EventID")
        'Catch ex As Exception
        'End Try

        'If Not WebContext.Current.User.IsAuthenticated Then
        '    Dim loginWindow As LoginRegistrationWindow = New LoginRegistrationWindow()
        '    loginWindow.Show()
        'End If

    End Sub

#Region "Private Method : Property changed event"

    Private Sub UserDashboard_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "TopReviewsLoaded" Then
            Me.FilterAccordion.SelectedIndex = Me.FilterAccordion.Items.Count() - 1
            Me.FilterAccordion.SelectedItem.UpdateLayout()
        ElseIf e.PropertyName = "User_Pending_Requests" Then
            'If IsNothing(newTransactions) And Me.myPendingRequests.ActualHeight > 750 Then
            If IsNothing(newTransactions) _
                And CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount > 6 Then
                CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 4
                newTransactions = New MyTransactions()
                Me.MyContentStackPanel.Children.Remove(myTransactions)
                Me.MyAccordionPanel.Children.Add(newTransactions)
                Me.MyAccordionPanel.Width = 489
                Me.myTransactions = Nothing
                Me.MyAccordionPanel.UpdateLayout()
                Me.MyContentStackPanel.UpdateLayout()
            ElseIf IsNothing(myTransactions) _
                And CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount <= 6 Then
                If CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount > 3 Then
                    CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 4
                Else
                    CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 6
                End If
                myTransactions = New MyTransactions()
                Me.MyAccordionPanel.Children.Remove(newTransactions)
                Me.MyContentStackPanel.Children.Add(myTransactions)
                Me.newTransactions = Nothing
                Me.MyAccordionPanel.UpdateLayout()
                Me.MyContentStackPanel.UpdateLayout()
            Else
                If CType(CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests, DomainCollectionView).TotalItemCount > 3 Then
                    CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 4
                Else
                    CType(Me.DataContext, UserDashboardViewModel).User_Transaction_Take_Size = 6
                End If
            End If
            Me.myPendingRequests.Refresh()
            Me.FilterAccordion.SelectedItem.UpdateLayout()
        ElseIf e.PropertyName = "User_Transactions" Then
            If Not IsNothing(Me.myTransactions) Then
                Me.myTransactions.Refresh()
            Else
                Me.newTransactions.Refresh()
            End If
            Me.FilterAccordion.SelectedItem.UpdateLayout()
        End If
    End Sub

#End Region

End Class