﻿Imports System.Globalization
Imports System.ServiceModel.DomainServices.Client.ApplicationServices
Imports System.Windows
Imports System.Windows.Controls

Namespace LoginUI
    ''' <summary>
    ''' <see cref="UserControl"/> class that shows the current login status and allows login and logout.
    ''' </summary>
    Partial Public Class LoginStatus
        Inherits UserControl
        Private WithEvents authService As AuthenticationService = WebContext.Current.Authentication

        ''' <summary>
        ''' Creates a new <see cref="LoginStatus"/> instance.
        ''' </summary>
        Public Sub New()
            Me.InitializeComponent()

            Me.welcomeText.SetBinding(TextBlock.TextProperty, WebContext.Current.CreateOneWayBinding("User.FullName", New StringFormatValueConverter(ApplicationStrings.WelcomeMessage)))
            Me.UpdateLoginState()
        End Sub

        Private Sub LoginButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim loginWindow As New LoginRegistrationWindow()
            loginWindow.Show()
        End Sub

        Private Sub SuggestionButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            If WebContext.Current.User.Roles.Contains("User") Then
                Dim viewMySuggestionStatusWindow = New ViewMySuggestionStatusChildWindow(Me.NewBookRequestsButton.Content)
                viewMySuggestionStatusWindow.Show()
            Else
                Dim viewMySuggestionStatusWindow = New ViewMySuggestionStatusChildWindow(Me.NewBookRequestsButton.Content)
                viewMySuggestionStatusWindow.Show()
            End If
        End Sub

        Private Sub EditUserInformationButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim editUserInformationWindow = New EditUserInformationChildWindow()
            editUserInformationWindow.Show()
        End Sub

        Private Sub LogoutButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Me.authService.Logout(AddressOf Me.HandleLogoutOperationErrors, Nothing)
            'If user logout, redirect to About page
            If TypeOf CType(Application.Current.RootVisual, BusyIndicator).Content Is MainPage Then
                Dim mainPage As MainPage =
                    DirectCast(CType(Application.Current.RootVisual, BusyIndicator).Content, MainPage)
                mainPage.ContentFrame.Navigate(New Uri("/About", UriKind.Relative))
            End If
        End Sub

        Private Sub HandleLogoutOperationErrors(ByVal logoutOperation As LogoutOperation)
            If logoutOperation.HasError Then
                ErrorWindow.CreateNew(logoutOperation.Error)
                logoutOperation.MarkErrorAsHandled()
            End If
        End Sub

        Private Sub Authentication_LoggedIn(ByVal sender As Object, ByVal e As AuthenticationEventArgs) Handles authService.LoggedIn
            Me.UpdateLoginState()
        End Sub

        Private Sub Authentication_LoggedOut(ByVal sender As Object, ByVal e As AuthenticationEventArgs) Handles authService.LoggedOut
            Me.UpdateLoginState()
        End Sub

        Public Sub UpdateLoginState()
            If IsNothing(Me.DataContext) Then
                Dim vmLocator As ViewModelLocator = Nothing
                For Each resource In App.Current.Resources.MergedDictionaries
                    vmLocator = resource.Item("VMLocator")
                    If Not IsNothing(vmLocator) Then
                        Exit For
                    End If
                Next

                If Not IsNothing(vmLocator) Then
                    Me.DataContext = vmLocator.SuggestionStatusVM
                End If
            End If
            If Not IsNothing(Me.DataContext) Then
                CType(Me.DataContext, SuggestionStatusViewModel).Refresh()
            End If
            If WebContext.Current.User.IsAuthenticated Then
                Me.welcomeText.Text = "Welcome " & WebContext.Current.User.FullName
                VisualStateManager.GoToState(Me, If((TypeOf (WebContext.Current.Authentication) Is WindowsAuthentication), "windowsAuth", "loggedIn"), True)
                If WebContext.Current.User.Roles.Contains("User") Then
                    Me.NewBookRequestsButton.Content = "My Suggestion Status"
                Else
                    Me.NewBookRequestsButton.Content = "Pending Suggestions"
                End If
            Else
                VisualStateManager.GoToState(Me, "loggedOut", True)
            End If
        End Sub

        Public Sub UpdateWelcomeText(ByVal modifiedName As String)
            Me.welcomeText.Text = "Welcome " & modifiedName
        End Sub

    End Class
End Namespace