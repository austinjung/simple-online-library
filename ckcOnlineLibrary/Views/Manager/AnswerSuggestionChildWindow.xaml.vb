﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common
Imports System.ServiceModel.DomainServices.Client.ApplicationServices
Imports System.Windows.Browser

Partial Public Class AnswerSuggestionChildWindow
    Inherits ChildWindow

    Private selectedRequestTree As TopicTree
    Private selectedTreeViewItem As TreeViewItem
    Private selectedSuggestion As NewBookRequest = Nothing
    Private linkUri As String = String.Empty
    Private popupOption As HtmlPopupWindowOptions

    Public Sub New()

        InitializeComponent()

        CType(Me.DataContext, SuggestionStatusViewModel).ReviewTextBox = Me.ReviewTextBox

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

    End Sub

    Private Sub OKButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles OKButton.Click

        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False

    End Sub

    Private Sub Answers_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles Answers.MouseRightButtonDown
        If TypeOf (sender) Is DataGrid Then
            Dim dataGrid = CType(sender, DataGrid)
            Dim originalSource = e.OriginalSource
            Dim gridItem As AnswerTemplate
            If TypeOf (originalSource) Is FrameworkElement Then
                Try
                    gridItem = CType(originalSource, FrameworkElement).DataContext
                    CType(dataGrid.ItemsSource, DomainCollectionView(Of AnswerTemplate)).MoveCurrentTo(gridItem)
                Catch ex As Exception
                    e.Handled = True
                    Return
                End Try
            End If
        End If
    End Sub

    Private Sub CheckBox1_Checked(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles CheckBox1.Checked
        CType(Me.DataContext, SuggestionStatusViewModel).IsAnswered = True
    End Sub

    Private Sub CheckBox1_Unchecked(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles CheckBox1.Unchecked
        If Me.ReviewTextBox.Text.Equals(CType(Me.DataContext, SuggestionStatusViewModel).OriginalReview) Then
            CType(Me.DataContext, SuggestionStatusViewModel).IsAnswered = False
        End If
    End Sub

    Private Sub ReviewTextBox_TextChanged(sender As System.Object, e As System.Windows.Controls.TextChangedEventArgs) Handles ReviewTextBox.TextChanged
        CType(Me.DataContext, SuggestionStatusViewModel).IsAnswered = True
    End Sub
End Class
