﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class EditAuthorChildWindow
    Inherits ChildWindow

    Public Sub New()

        InitializeComponent()

    End Sub

    Public Sub New(ByVal newAuthor As Author)

        InitializeComponent()
        CType(Me.DataContext, EditAuthorViewModel).EditAuthor = newAuthor

    End Sub

    Private Sub SaveButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles SaveButton.Click

        If CType(Me.DataContext, EditAuthorViewModel).EditAuthor.HasValidationErrors() Then
            Return
        End If
        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        CType(Me.DataContext, EditAuthorViewModel).Cleanup()

    End Sub

    Private Sub ChildWindow_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded

        If Not IsNothing(CType(Me.DataContext, EditAuthorViewModel).EditAuthor) Then
            Dim author = CType(DataContext, EditAuthorViewModel).EditAuthor
            If (Not String.IsNullOrEmpty(author.Author_First_Name)) And (Not String.IsNullOrEmpty(author.Author_Last_Name)) Then
                CType(Me.DataContext, EditAuthorViewModel).HasChanges = True
            End If
        End If

    End Sub
End Class
