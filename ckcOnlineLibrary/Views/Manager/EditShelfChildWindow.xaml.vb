﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class EditShelfChildWindow
    Inherits ChildWindow

    Public Sub New()

        InitializeComponent()

    End Sub

    Public Sub New(ByVal newShelf As Shelf)

        InitializeComponent()
        CType(Me.DataContext, EditShelfViewModel).EditShelf = newShelf

    End Sub

    Private Sub SaveButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles SaveButton.Click

        If CType(Me.DataContext, EditShelfViewModel).EditShelf.HasValidationErrors() Then
            Return
        End If
        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Unloaded

        CType(Me.DataContext, EditShelfViewModel).Cleanup()

    End Sub

    Private Sub ChildWindow_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded

        If Not IsNothing(CType(Me.DataContext, EditShelfViewModel).EditShelf) Then
            If Not String.IsNullOrEmpty(CType(Me.DataContext, EditShelfViewModel).EditShelf.Shelf_Code) Then
                CType(Me.DataContext, EditShelfViewModel).HasChanges = True
            End If
        End If

    End Sub
End Class
