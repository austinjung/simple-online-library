﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class EditBookChildWindow
    Inherits ChildWindow

    Public Sub New()

        InitializeComponent()
        RegisterMessages()
        CType(Me.DataContext, EditBookViewModel).WorkingDataGrid = Me.BookAuthorDataGrid

    End Sub

    Public Sub New(ByVal newBook As Book)

        InitializeComponent()
        RegisterMessages()
        CType(Me.DataContext, EditBookViewModel).WorkingDataGrid = Me.BookAuthorDataGrid
        CType(Me.DataContext, EditBookViewModel).EditBook = newBook

    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click

        'do OnSavedBookDialogMessageReceived

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False

    End Sub

    Private Sub btnSelectBookImage_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles btnSelectBookImage.Click

        Dim fileManagerDialog As FileManagerDialog = New FileManagerDialog
        fileManagerDialog.Show()

    End Sub

    Private Sub btnUploadBookImages_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles btnUploadBookImages.Click

        Dim FileUploadView = New SingleFileUploadWindow()
        FileUploadView.Show()

    End Sub

    Private Sub Book_Image_FileTextBox_TextChanged(sender As System.Object, e As System.Windows.Controls.TextChangedEventArgs) Handles Book_Image_FileTextBox.TextChanged

        SyncBookImage()

    End Sub

    Private Sub SyncBookImage()

        Dim source = Application.Current.Host.Source
        Dim bookImagesUrl As String = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/Upload/Book_Images/"
        Me.imgSmall.Source = New BitmapImage(New Uri(bookImagesUrl & Me.Book_Image_FileTextBox.Text, UriKind.Absolute))
        Dim editBookVM As EditBookViewModel = CType(Me.DataContext, EditBookViewModel)
        editBookVM.EditBook.ImageSource = Me.Book_Image_FileTextBox.Text

    End Sub

#Region "Register Message & Handle"

    Protected Sub RegisterMessages()

        Messenger.[Default].Register(Of FileManagerDialogMessage)(Me, AddressOf OnFileManagerDialogMessageReceived)
        Messenger.[Default].Register(Of SavedBookDialogMessage)(Me, AddressOf OnSavedBookDialogMessageReceived)

    End Sub

    Protected Sub UnregisterMessages()

        Messenger.[Default].Unregister(Of FileManagerDialogMessage)(Me, AddressOf OnFileManagerDialogMessageReceived)
        Messenger.[Default].Unregister(Of SavedBookDialogMessage)(Me, AddressOf OnSavedBookDialogMessageReceived)

    End Sub

    Private Sub OnFileManagerDialogMessageReceived(msg As FileManagerDialogMessage)

        If msg.DialogResult Then
            Me.Book_Image_FileTextBox.Text = msg.SelectedFileName
            SyncBookImage()
        End If

    End Sub

    Private Sub OnSavedBookDialogMessageReceived(msg As SavedBookDialogMessage)

        If Not msg.SavedBook.HasValidationErrors Then
            If msg.Type.Contains("Success") Then
                Dim myMessageBox = New MyMessageBox(msg.Type, msg.Content, msg.Caption, msg.Button)
                myMessageBox.Show()
                Me.DialogResult = True
            End If
        End If

    End Sub

#End Region

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Unloaded

        UnregisterMessages()
        CType(Me.DataContext, EditBookViewModel).Cleanup()

    End Sub

    Private Sub Book_Categories_ListTextBox_GotFocus(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles Book_Categories_ListTextBox.GotFocus

        'Me.addNewBookProperty.Header = "New Category"
        Dim editBookVM As EditBookViewModel = CType(CType(sender, TextBox).DataContext, EditBookViewModel)
        editBookVM.OnSelectWorkingGrid(Me.BookCategoryDataGrid)

    End Sub

    Private Sub Book_Authors_ListTextBox_GotFocus(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles Book_Authors_ListTextBox.GotFocus

        'Me.addNewBookProperty.Header = "New Author"
        Dim editBookVM As EditBookViewModel = CType(CType(sender, TextBox).DataContext, EditBookViewModel)
        editBookVM.OnSelectWorkingGrid(Me.BookAuthorDataGrid)

    End Sub

    Private Sub Book_Shelves_ListTextBox_GotFocus(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles Book_Shelves_ListTextBox.GotFocus

        'Me.addNewBookProperty.Header = "New Shelf"
        Dim editBookVM As EditBookViewModel = CType(CType(sender, TextBox).DataContext, EditBookViewModel)
        editBookVM.OnSelectWorkingGrid(Me.BookShelfDataGrid)

    End Sub

End Class
