﻿Imports System.Windows.Data
Imports System.Windows.Controls.DataGrid
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class ManageBooksPage
    Inherits Page

    Private _FilterGrid_Max_Width As GridLength
    Private _FilterGrid_Min_Width As GridLength

    Public Sub New()
        InitializeComponent()
        'Me.Title = ApplicationStrings.HomePageTitle
        'Me.editSelectedBookButton.SetBinding(Button.IsEnabledProperty, _
        '                                     New Binding("User.IsAuthenticated") { _
        '                                            Source = Application.Current.Resources("WebContext") _
        '                                     })
        RegisterMessage()
    End Sub

    'Executes when the user navigates to this page.
    Protected Overrides Sub OnNavigatedTo(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    'Executes when the user navigates to this page.
    Protected Overrides Sub OnNavigatedFrom(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    Private Sub RegisterMessage()
        Messenger.Reset()
        Messenger.[Default].Register(Of LaunchEditBookMessage)(Me, AddressOf OnLaunchEditBook)
        Messenger.[Default].Register(Of SavedBookDialogMessage)(Me, AddressOf OnSaveBookDialogMessageReceived)
    End Sub

    Private Sub OnLaunchEditBook()
        'Throw New NotImplementedException
    End Sub

    Private Sub OnSaveBookDialogMessageReceived(msg As SavedBookDialogMessage)
        Dim myMessageBox = New MyMessageBox(msg.Type, msg.Content, msg.Caption, msg.Button)
        myMessageBox.Show()
    End Sub

    Private Sub CategoryDataGrid_SelectionChanged(sender As System.Object, e As System.Windows.Controls.SelectionChangedEventArgs)

        Dim selectedCategories As List(Of Category) = New List(Of Category)

        If CType(sender, DataGrid).SelectedItems.Count > 0 Then
            For i As Integer = 0 To CType(sender, DataGrid).SelectedItems.Count - 1
                Dim row = CType(sender, DataGrid).SelectedItems(i)
                selectedCategories.Add(row)
            Next
        End If

        CType(Me.DataContext, ManageBooksViewModel).SelectedCategories = selectedCategories

    End Sub

    Private Sub Toggle_Filter_Scroll_View(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs)

        _FilterGrid_Min_Width = New GridLength(Me.FilterGrid.MinWidth)
        _FilterGrid_Max_Width = New GridLength(Me.FilterScrollViewer.ScrollableWidth + Me.FilterGrid.ActualWidth)

        If Me.GridSlideButton.Margin.Left = 0 Then
            Me.FilterGrid.Width = _FilterGrid_Min_Width
            Me.GridSlideButton.Margin = New Thickness(-14.0, 0.0, 0.0, 0.0)
            Me.GridSlideButtonRect.Rect = New Rect(14, 0, 20, 40)
        Else
            Me.FilterGrid.Width = _FilterGrid_Max_Width
            Me.GridSlideButton.Margin = New Thickness(0.0, 0.0, 0.0, 0.0)
            Me.GridSlideButtonRect.Rect = New Rect(0, 0, 14, 40)
        End If
    End Sub

End Class


