﻿Imports System.Windows.Data
Imports System.Windows.Controls.DataGrid
Imports GalaSoft.MvvmLight.Messaging
Imports System.Collections.ObjectModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.ServiceModel.DomainServices.Client

Partial Public Class ManageMembersPage
    Inherits Page

    Private _comboLabelItem As MemberFilterTarget = New MemberFilterTarget("Add/Remove Target(s)")
    Private _FilterTargetListWithoutLabel As ObservableCollection(Of MemberFilterTarget)
    Private _FilterTargetListWithLabel As ObservableCollection(Of MemberFilterTarget)

    Public Sub New()
        InitializeComponent()

        _FilterTargetListWithoutLabel = MemberFilterTargets.GetFilterTargets()
        _FilterTargetListWithLabel = MemberFilterTargets.GetFilterTargets()
        _FilterTargetListWithLabel.Insert(0, _comboLabelItem)

    End Sub

    'Executes when the user navigates to this page.
    Protected Overrides Sub OnNavigatedTo(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    'Executes when the user navigates from this page.
    Protected Overrides Sub OnNavigatedFrom(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        CType(Me.DataContext, ManageMemberViewModel).RemovePropertyChangeHandler()

    End Sub

    Private Sub FilterTargetList_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles FilterTargetList.DropDownClosed
        DoSomethingOnClose()
    End Sub

    Private Sub FilterTargetList_DropDownOpened(sender As System.Object, e As System.EventArgs) Handles FilterTargetList.DropDownOpened
        CType(sender, ComboBox).SelectedIndex = -1
        CType(sender, ComboBox).ItemsSource = _FilterTargetListWithoutLabel
    End Sub

    Private Sub Page_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        DoSomethingOnClose()
    End Sub

    Private Sub DoSomethingOnClose()
        If Not IsNothing(Me.FilterTargetList.SelectedItem) Then

            CType(Me.FilterTargetList.SelectedItem, MemberFilterTarget).IsChecked = Not CType(Me.FilterTargetList.SelectedItem, MemberFilterTarget).IsChecked

            For Each target In Me._FilterTargetListWithLabel
                If target.Target = CType(Me.FilterTargetList.SelectedItem, MemberFilterTarget).Target Then
                    target.IsChecked = CType(Me.FilterTargetList.SelectedItem, MemberFilterTarget).IsChecked
                    Exit For
                End If
            Next

        End If

        Me.FilterTargetList.ItemsSource = _FilterTargetListWithLabel
        Me.FilterTargetList.SelectedIndex = 0
    End Sub

    Private Sub dataPager_PageIndexChanged(sender As System.Object, e As System.EventArgs) Handles dataPager.PageIndexChanged
        Me.MemberDataForm.CancelEdit()
    End Sub

    Private Sub MemberDataForm_EditEnded(sender As System.Object, e As System.Windows.Controls.DataFormEditEndedEventArgs) Handles MemberDataForm.EditEnded

        Dim dataContext As ManageMemberViewModel = CType(Me.DataContext, ManageMemberViewModel)
        If e.EditAction = DataFormEditAction.Commit Then
            Dim editedMember As Member = Me.MemberDataForm.CurrentItem
            If editedMember.EntityState = EntityState.New And String.IsNullOrEmpty(editedMember.Name) Then
                editedMember.Name = editedMember.MemberLogin
            ElseIf editedMember.EntityState = EntityState.Modified _
                    And Not editedMember.Name.Equals(editedMember.MemberLogin) Then
                Dim alertMsg As String = "Login name cannot be changed."
                Dim MyMessageBox = New MyMessageBox("Inform", "Information Changed", alertMsg, MessageBoxButton.OK)
                MyMessageBox.Show()
            End If
            dataContext.LibraryDataService.SubmitChanges(AddressOf dataContext.AfterSaveEditedMember, Nothing)
        End If
        dataContext.IsEdited = False

    End Sub

    Private Sub MemberDataForm_CurrentItemChanged(sender As System.Object, e As System.EventArgs) Handles MemberDataForm.CurrentItemChanged

        Dim editedMember As Member = Me.MemberDataForm.CurrentItem
        If Not IsNothing(editedMember) Then
            If editedMember.EntityState = EntityState.New And String.IsNullOrEmpty(editedMember.Role) Then
                editedMember.Role = "User"
                editedMember.MemberActive = True
                editedMember.Country = "Canada"
                editedMember.Province = "BC"
                editedMember.AllowEmails = True
                editedMember.MemberSince = Date.Today()
            End If
        End If

    End Sub

End Class


