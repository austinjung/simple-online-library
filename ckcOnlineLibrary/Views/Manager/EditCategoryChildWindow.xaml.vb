﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class EditCategoryChildWindow
    Inherits ChildWindow

    Public Sub New()

        InitializeComponent()

    End Sub

    Public Sub New(ByVal newCategory As Category)

        InitializeComponent()
        CType(Me.DataContext, EditCategoryViewModel).EditCategory = newCategory

    End Sub

    Private Sub SaveButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles SaveButton.Click

        If CType(Me.DataContext, EditCategoryViewModel).EditCategory.HasValidationErrors() Then
            Return
        End If
        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Unloaded

        CType(Me.DataContext, EditCategoryViewModel).Cleanup()

    End Sub

    Private Sub ChildWindow_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded

        If Not IsNothing(CType(Me.DataContext, EditCategoryViewModel).EditCategory) Then
            If Not String.IsNullOrEmpty(CType(Me.DataContext, EditCategoryViewModel).EditCategory.Category_Name) Then
                CType(Me.DataContext, EditCategoryViewModel).HasChanges = True
            End If
        End If

    End Sub

    Private Sub CategoryNameTextBox_KeyDown(sender As System.Object, e As System.Windows.Input.KeyEventArgs) Handles CategoryNameTextBox.KeyDown
        CType(Me.DataContext, EditCategoryViewModel).HasChanges = True
    End Sub
End Class
