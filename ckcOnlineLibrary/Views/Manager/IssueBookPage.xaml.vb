﻿Imports System.Windows.Data
Imports System.Windows.Controls.DataGrid
Imports GalaSoft.MvvmLight.Messaging
Imports System.Collections.ObjectModel
Imports Microsoft.Windows.Data.DomainServices

Partial Public Class IssueBookPage
    Inherits Page

    Private _comboLabelItem As RequestFilterTarget = New RequestFilterTarget("Add/Remove Target(s)")
    Private _FilterTargetListWithoutLabel As ObservableCollection(Of RequestFilterTarget)
    Private _FilterTargetListWithLabel As ObservableCollection(Of RequestFilterTarget)

    Public Sub New()
        InitializeComponent()

        _FilterTargetListWithoutLabel = RequestFilterTargets.GetFilterTargets()
        _FilterTargetListWithLabel = RequestFilterTargets.GetFilterTargets()
        _FilterTargetListWithLabel.Insert(0, _comboLabelItem)

        AddHandler CType(Me.DataContext, IssueBookViewModel).PropertyChanged, AddressOf IssueBook_PropertyChanged

        RegisterMessage()
    End Sub

    'Executes when the user navigates to this page.
    Protected Overrides Sub OnNavigatedTo(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    'Executes when the user navigates from this page.
    Protected Overrides Sub OnNavigatedFrom(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    Private Sub RegisterMessage()
        'Messenger.Reset()
        'Messenger.Default.Register(Of LaunchEditBookMessage)(Me, AddressOf OnLaunchEditBook)
    End Sub

    Private Sub FilterTargetList_DropDownClosed(sender As System.Object, e As System.EventArgs) Handles FilterTargetList.DropDownClosed
        DoSomethingOnClose()
    End Sub

    Private Sub FilterTargetList_DropDownOpened(sender As System.Object, e As System.EventArgs) Handles FilterTargetList.DropDownOpened
        CType(sender, ComboBox).SelectedIndex = -1
        CType(sender, ComboBox).ItemsSource = _FilterTargetListWithoutLabel
    End Sub

    Private Sub Page_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        DoSomethingOnClose()
    End Sub

    Private Sub DoSomethingOnClose()
        If Not IsNothing(Me.FilterTargetList.SelectedItem) Then

            CType(Me.FilterTargetList.SelectedItem, RequestFilterTarget).IsChecked = Not CType(Me.FilterTargetList.SelectedItem, RequestFilterTarget).IsChecked

            For Each target In Me._FilterTargetListWithLabel
                If target.Target = CType(Me.FilterTargetList.SelectedItem, RequestFilterTarget).Target Then
                    target.IsChecked = CType(Me.FilterTargetList.SelectedItem, RequestFilterTarget).IsChecked
                    Exit For
                End If
            Next

        End If

        Me.FilterTargetList.ItemsSource = _FilterTargetListWithLabel
        Me.FilterTargetList.SelectedIndex = 0
    End Sub

    'Private Sub BooksDataGrid_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles BooksDataGrid.MouseRightButtonDown
    '    If TypeOf (sender) Is DataGrid Then
    '        Dim dataGrid = CType(sender, DataGrid)
    '        Dim originalSource = e.OriginalSource
    '        Dim gridItem As Book
    '        If TypeOf (originalSource) Is FrameworkElement Then
    '            Try
    '                gridItem = CType(originalSource, FrameworkElement).DataContext
    '                CType(dataGrid.ItemsSource, DomainCollectionView(Of Book)).MoveCurrentTo(gridItem)
    '            Catch ex As Exception
    '                'do nothing
    '                Return
    '            End Try
    '        End If
    '    End If
    'End Sub

    Private Sub IssueBook_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If e.PropertyName = "Book" Then
            If CType(Me.DataContext, IssueBookViewModel).Book Is Nothing Then
                Me.RequestDetailPanel.Visibility = Windows.Visibility.Collapsed
            Else
                Me.RequestDetailPanel.Visibility = Windows.Visibility.Visible
            End If
            Me.RequestDetailPanel.UpdateLayout()
        End If
    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        CType(Me.DataContext, IssueBookViewModel).RemovePropertyChangeHandler()
        RemoveHandler CType(Me.DataContext, IssueBookViewModel).PropertyChanged, AddressOf IssueBook_PropertyChanged

    End Sub

End Class


