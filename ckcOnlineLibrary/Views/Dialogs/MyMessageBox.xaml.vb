﻿Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class MyMessageBox
    Inherits ChildWindow

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(type As String, caption As String, Content As String, button As MessageBoxButton)
        InitializeComponent()
        SetHeader(type)
        Me.LabelText.Text = caption
        Me.MessageTextBox.Text = Content
        SetButton(button)
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Dim dialogMessage As ConfirmDialogMessage
        dialogMessage = New ConfirmDialogMessage(Me.IntroductoryText.Text, Me.LabelText.Text, "DialogResultOK")
        Messenger.[Default].Send(dialogMessage)
        Me.DialogResult = True
    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Dim dialogMessage As ConfirmDialogMessage
        dialogMessage = New ConfirmDialogMessage(Me.IntroductoryText.Text, Me.LabelText.Text, "DialogResultCancel")
        Messenger.[Default].Send(dialogMessage)
        Me.DialogResult = False
    End Sub

    Private Sub SetHeader(type As String)
        Select Case type
            Case "Error"
                Me.IconImage.Source = New BitmapImage(
                    New Uri("/ckcOnlineLibrary;component/Assets/Dialogs/dialog_error.png", UriKind.RelativeOrAbsolute))
                Me.IntroductoryText.Text = "Error"
                Me.MessageTextBox.TextWrapping = TextWrapping.Wrap
                Me.MessageTextBox.Height = 90
            Case "Warning"
                Me.IconImage.Source = New BitmapImage(
                    New Uri("/ckcOnlineLibrary;component/Assets/Dialogs/dialog_warning.png", UriKind.RelativeOrAbsolute))
                Me.IntroductoryText.Text = "Warning"
                Me.MessageTextBox.TextWrapping = TextWrapping.Wrap
                Me.MessageTextBox.Height = 90
            Case "Success"
                Me.IconImage.Source = New BitmapImage(
                    New Uri("/ckcOnlineLibrary;component/Assets/Dialogs/dialog_success.png", UriKind.RelativeOrAbsolute))
                Me.IntroductoryText.Text = "Success"
                Me.MessageTextBox.TextWrapping = TextWrapping.Wrap
                Me.MessageTextBox.Height = 90
            Case "View Comment"
                Me.IconImage.Source = New BitmapImage(
                    New Uri("/ckcOnlineLibrary;component/Assets/Dialogs/dialog_success.png", UriKind.RelativeOrAbsolute))
                Me.IntroductoryText.Text = "View Comment"
                Me.MessageTextBox.TextWrapping = TextWrapping.NoWrap
            Case Else
                Me.IconImage.Source = New BitmapImage(
                    New Uri("/ckcOnlineLibrary;component/Assets/Dialogs/dialog_info.png", UriKind.RelativeOrAbsolute))
                Me.IntroductoryText.Text = "Information"
                Me.MessageTextBox.TextWrapping = TextWrapping.Wrap
                Me.MessageTextBox.Height = 90
        End Select

        Me.LayoutRoot.Children.Remove(HeaderStackPanel)
        Me.Title = HeaderStackPanel

    End Sub

    Private Sub SetButton(button As MessageBoxButton)
        If button = MessageBoxButton.OK Then
            Me.CancelButton.Visibility = Windows.Visibility.Collapsed
        Else
            Me.CancelButton.Visibility = Windows.Visibility.Visible
        End If
    End Sub

End Class
