﻿Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class ReadCommentMessageBox
    Inherits ChildWindow

    Private commentContainerType As Type
    Private commentContainerID As Integer

    Public Sub New(comment As String, commentButtonVisibility As Visibility, containerType As Type, containerID As Integer)

        InitializeComponent()

        Me.CommentButton.Visibility = commentButtonVisibility
        Me.commentContainerType = containerType
        Me.commentContainerID = containerID
        Me.MessageTextBox.Text = comment
    End Sub

    Private Sub CommentButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Dim dialogMessage As ReadCommentDialogMessage
        dialogMessage = New ReadCommentDialogMessage("OpenAddComment")
        Messenger.[Default].Send(dialogMessage)
        Me.DialogResult = True
    End Sub

    Private Sub CloseButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Dim dialogMessage As ReadCommentDialogMessage
        dialogMessage = New ReadCommentDialogMessage("CloseDialog")
        Messenger.[Default].Send(dialogMessage)
        Me.DialogResult = False
    End Sub

End Class
