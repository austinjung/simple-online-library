﻿Imports System
Imports System.Diagnostics
Imports System.Windows
Imports System.Windows.Controls
Imports System.Net
Imports Microsoft.VisualBasic.ControlChars
Imports ckcOnlineLibrary.LoginUI
Imports System.Windows.Media.Imaging

''' <summary>
''' Controls when a stack trace should be displayed on the
''' Error Window
''' 
''' Defaults to <see cref="StackTracePolicy.OnlyWhenDebuggingOrRunningLocally"/>
''' </summary>
Public Enum StackTracePolicy
    ''' <summary>
    ''' Stack trace is showed only when running with a debugger attached
    ''' or when running the app on the local machine. Use this to get
    ''' additional debug information you don't want the end users to see
    ''' </summary>
    OnlyWhenDebuggingOrRunningLocally

    ''' <summary>
    ''' Always show the stack trace, even if debugging
    ''' </summary>
    Always

    ''' <summary>
    ''' Never show the stack trace, even when debugging
    ''' </summary>
    Never
End Enum

''' <summary>
''' <see cref="ChildWindow"/> class that presents errors to the user.
''' </summary>
Partial Public Class ErrorWindow
    Inherits ChildWindow

    ''' <summary>
    ''' Creates a new <see cref="ErrorWindow"/> instance.
    ''' </summary>
    ''' <param name="message">The error message to display.</param>
    ''' <param name="errorDetails">Extra information about the error.</param>
    Protected Sub New(ByVal message As String, ByVal errorDetails As String)
        InitializeComponent()

        ErrorTextBox.Text = errorDetails

        If errorDetails.Contains("error") Then
            Me.IconImage.Source = New BitmapImage(
                New Uri("/ckcOnlineLibrary;component/Assets/Dialogs/dialog_error.png", UriKind.RelativeOrAbsolute))
            Me.IntroductoryText.Text = "Error"
        Else
            Me.IconImage.Source = New BitmapImage(
                New Uri("/ckcOnlineLibrary;component/Assets/Dialogs/dialog_warning.png", UriKind.RelativeOrAbsolute))
            Me.IntroductoryText.Text = "Warning"
        End If

        Me.LayoutRoot.Children.Remove(HeaderStackPanel)
        Me.Title = HeaderStackPanel

    End Sub

#Region "Factory Shortcut Methods"
    ''' <summary>
    ''' Creates a new Error Window given an error message.
    ''' Current stack trace will be displayed if app is running under
    ''' debug or on the local machine
    ''' </summary>
    ''' <param name="message">The message to display.</param>
    Public Shared Sub CreateNew(ByVal message As String)
        'Austin Jung Start
        'CreateNew(message, StackTracePolicy.OnlyWhenDebuggingOrRunningLocally)
        CreateNew(message, StackTracePolicy.Never)
        'Austin Jung End
    End Sub

    ''' <summary>
    ''' Creates a new Error Window given an exception.
    ''' Current stack trace will be displayed if app is running under
    ''' debug or on the local machine
    ''' 
    ''' The exception is converted onto a message using
    ''' <see cref="ConvertExceptionToMessage"/>
    ''' </summary>
    ''' <param name="exception">The exception to display.</param>
    Public Shared Sub CreateNew(ByVal exception As Exception)
        'Austin Jung Start
        'CreateNew(exception, StackTracePolicy.OnlyWhenDebuggingOrRunningLocally)
        CreateNew(exception, StackTracePolicy.Never)
        'Austin Jung End
    End Sub

    ''' <summary>
    ''' Creates a new Error Window given an exception. The exception is converted onto a message using
    ''' <see cref="ConvertExceptionToMessage"/>
    ''' </summary>
    ''' <param name="exception">The exception to display.</param>
    ''' <param name="policy">When to display the stack trace, see <see cref="StackTracePolicy"/>.</param>
    Public Shared Sub CreateNew(ByVal exception As Exception, ByVal policy As StackTracePolicy)
        If exception Is Nothing Then
            Throw New ArgumentNullException("exception")
        End If

        'if current url = "/About" and error is denied, then don;t show erroe windows
        Dim convertedException = ConvertExceptionToMessage(exception)
        If String.IsNullOrEmpty(convertedException) Then
            Return
        End If

        Dim fullStackTrace As String = exception.StackTrace

        ' Account for nested exceptions. Since this is for debugging purposes we don't care
        ' to use ConvertExceptionToMessage or AppendStackTraceToMessage which are concerned
        ' with providing something nicer to the user
        Dim innerException As Exception = exception.InnerException

        While innerException IsNot Nothing
            fullStackTrace += (CrLf & "Caused by: " & exception.Message & CrLf & CrLf) + exception.StackTrace
            innerException = innerException.InnerException
        End While

        CreateNew(convertedException, fullStackTrace, policy)
    End Sub

    ''' <summary>
    ''' Creates a new Error Window given an error message.
    ''' </summary>
    ''' <param name="message">The message to display.</param>
    ''' <param name="policy">When to display the stack trace, see <see cref="StackTracePolicy"/>.</param>
    Public Shared Sub CreateNew(ByVal message As String, ByVal policy As StackTracePolicy)
        CreateNew(message, New StackTrace().ToString(), policy)
    End Sub
#End Region

#Region "Factory Methods"
    ''' <summary>
    ''' All other factory methods will result in a call to this one
    ''' </summary>
    ''' 
    ''' <param name="message">Which message to display.</param>
    ''' <param name="stackTrace">The associated stack trace.</param>
    ''' <param name="policy">In which situations the stack trace should be appended to the message.</param>
    Private Shared Sub CreateNew(ByVal message As String, ByVal stackTrace As String, ByVal policy As StackTracePolicy)
        Dim errorDetails As String = String.Empty

        If policy = StackTracePolicy.Always OrElse policy = StackTracePolicy.OnlyWhenDebuggingOrRunningLocally AndAlso IsRunningUnderDebugOrLocalhost Then
            errorDetails = If(stackTrace Is Nothing, String.Empty, stackTrace)
            ' Austin Jung Start
        Else
            errorDetails = message
            message = "Attention"
            'Austin Jung End
        End If

        Dim window As New ErrorWindow(message, errorDetails)
        window.Show()
    End Sub
#End Region

#Region "Factory Helpers"
    ''' <summary>
    ''' Returns whether running under a dev environment, i.e., with a debugger attached or
    ''' with the server hosted on localhost
    ''' </summary>
    Private Shared ReadOnly Property IsRunningUnderDebugOrLocalhost() As Boolean
        Get
            If Debugger.IsAttached Then
                Return True
            Else
                Dim hostUrl As String = Application.Current.Host.Source.Host
                Return hostUrl.Contains("::1") OrElse hostUrl.Contains("localhost") OrElse hostUrl.Contains("127.0.0.1")
            End If
        End Get
    End Property

    Public Shared AskingLogin As Boolean = False
    Public Shared Showing As Boolean = False

    ''' <summary>
    ''' Creates a user friendly message given an Exception. Currently this simply
    ''' takes the Exception.Message value, optionally but you might want to change this to treat
    ''' some specific Exception classes differently
    ''' </summary>
    Private Shared Function ConvertExceptionToMessage(ByVal e As Exception) As String
        If Not AskingLogin And (e.Message.Contains("denied") Or e.Message.Contains("Load operation failed")) Then
            If Not WebContext.Current.User.IsAuthenticated Then
                Dim currentUri = Application.Current.Host.NavigationState
                If currentUri <> "/About" Then
                    If TypeOf CType(Application.Current.RootVisual, BusyIndicator).Content Is MainPage Then
                        Dim mainPage As MainPage =
                            DirectCast(CType(Application.Current.RootVisual, BusyIndicator).Content, MainPage)
                        mainPage.ContentFrame.Navigate((New Uri("/About", UriKind.Relative)))
                    End If
                    AskingLogin = True
                    Showing = True
                    Dim loginWindow As LoginRegistrationWindow = New LoginRegistrationWindow()
                    loginWindow.Show()
                    Return "You need to login to access this page." & vbCrLf & "Please login."
                Else
                    Return String.Empty
                End If
            Else
                WebContext.Current.Authentication.Logout(AddressOf HandleLogoutOperationErrors, Nothing)
                If TypeOf CType(Application.Current.RootVisual, BusyIndicator).Content Is MainPage Then
                    Dim mainPage As MainPage =
                        DirectCast(CType(Application.Current.RootVisual, BusyIndicator).Content, MainPage)
                    mainPage.ContentFrame.Navigate((New Uri("/About", UriKind.Relative)))
                End If
                Showing = True
                Return "Your previous session is expired or you need higher privilege to access this page." & vbCrLf & "Please login again with higher privilege."
            End If
        ElseIf Not AskingLogin And e.Message.Contains("Submit operation failed validation") Then
            Return "Please correct input data you provided."
        Else
            Showing = True
        End If
        Return e.Message
    End Function
#End Region

    Private Shared Sub HandleLogoutOperationErrors(ByVal logoutOperation As ServiceModel.DomainServices.Client.ApplicationServices.LogoutOperation)
        If logoutOperation.HasError Then
            ErrorWindow.CreateNew(logoutOperation.Error)
            logoutOperation.MarkErrorAsHandled()
        End If
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Me.DialogResult = True
        AskingLogin = False
        Showing = False
    End Sub
End Class