﻿Imports System.Windows.Media.Imaging
Imports Nemiro.FileManager
Imports System.IO
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class FileManagerDialog
    Inherits ChildWindow

    Private myPleaseWait As PleaseWait = New PleaseWait ' progressbar
    Private bookImagesUrl As String
    Private previousImage As Image

    Public Sub New()
        InitializeComponent()

        Thumbnails.Items.Clear()
        Me.selectedFileImage.Source = Nothing
        Dim source = Application.Current.Host.Source
        Dim myUrl As String = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/FileManagerGateway.ashx"
        bookImagesUrl = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/Upload/Book_Images/"
        fileList1.Url = myUrl 'set url

        ' handlers for progress
        AddHandler fileList1.Process, AddressOf fileList1_Process
        AddHandler fileList1.Complete, AddressOf fileList1_Complete

    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Me.DialogResult = True

        Dim dialogMessage = New FileManagerDialogMessage(Me.SelectedFileName.Text)
        Messenger.[Default].Send(dialogMessage)

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Me.DialogResult = False

        Dim dialogMessage = New FileManagerDialogMessage()
        Messenger.[Default].Send(dialogMessage)

    End Sub

    Private Sub Thumbnails_SelectionChanged(sender As System.Object, e As System.Windows.Controls.SelectionChangedEventArgs) Handles Thumbnails.SelectionChanged

        Dim image As Image = CType(CType(sender, ListBox).SelectedItem, Image)
        Me.selectedFileImage.Source = image.Source

        'Dim uri = CType(image.Source, BitmapImage).UriSource.LocalPath
        'Me.SelectedFileName.Text = uri.Replace("/Upload/Book_Images/", "")
        Me.SelectedFileName.Text = image.Name

        image.Opacity = 0.5
        If Not IsNothing(previousImage) Then
            previousImage.Opacity = 1.0
        End If

        previousImage = image

    End Sub

    Private Sub fileList1_Process(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      If myPleaseWait IsNot Nothing AndAlso myPleaseWait.Visibility = System.Windows.Visibility.Visible Then
                                          Return
                                      End If
                                      ' show porgress
                                      myPleaseWait = New PleaseWait()
                                      myPleaseWait.Show()

                                  End Sub)
    End Sub

    Private Sub fileList1_Complete(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      'close progress
                                      If myPleaseWait IsNot Nothing Then
                                          myPleaseWait.Close()
                                          myPleaseWait = Nothing
                                      End If
                                      For Each itm As FileItem In fileList1.Items
                                          If itm.FileName <> "..." Then
                                              Dim img = New Image()
                                              img.Height = 100
                                              img.Name = itm.FileName.ToString()
                                              img.Source = New BitmapImage(New Uri(bookImagesUrl & itm.FileName.ToString(), UriKind.Absolute))
                                              Thumbnails.Items.Add(img)
                                          End If
                                      Next
                                  End Sub)
    End Sub

End Class
