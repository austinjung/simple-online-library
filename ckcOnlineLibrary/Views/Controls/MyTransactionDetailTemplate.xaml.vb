﻿Partial Public Class MyTransactionDetailTemplate
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.LayoutRoot.Width
        End Get
        Set(value As Integer)
            Me.LayoutRoot.Width = value
            Me.txbTitle.Width = value - Me.imgSmall.Width - Me.miscLabelGrid1.Width - Me.miscLabelGrid2.Width - Me.reviewLabelGrid.Width - 10
            Me.scrollDescription.Width = Me.txbTitle.Width
            Me.txbDescription.Width = Me.scrollDescription.Width - 20
            Me.scrollDescription1.Width = Me.reviewLabelGrid.Width
            Me.txbReview.Width = Me.reviewLabelGrid.Width - 20
        End Set
    End Property

    Private Sub txbTitle_SizeChanged(sender As System.Object, e As System.Windows.SizeChangedEventArgs) Handles txbTitle.SizeChanged
        If Me.txbTitle.ActualHeight > 10 And Me.txbTitle.ActualHeight < 150 Then
            Me.scrollDescription1.Height = Me.txbTitle.ActualHeight + 50
            CType(Me.Parent, DataGridCell).MaxHeight = Me.txbTitle.ActualHeight + 74
        End If
    End Sub
End Class
