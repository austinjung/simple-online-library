﻿Imports Microsoft.Windows.Data.DomainServices

Partial Public Class MyPendingRequests
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth As Integer
        Get
            Return Me.MyPendingRequestDataGrid.Width
        End Get
        Set(value As Integer)
            Me.MyPendingRequestDataGrid.Width = value
        End Set
    End Property

    Public Sub Refresh()
        Dim collectionView = CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests
        If collectionView.IsEmpty Then
            Me.noPendingRequestText.Visibility = Windows.Visibility.Visible
        Else
            Me.noPendingRequestText.Visibility = Windows.Visibility.Collapsed
        End If
        Me.UpdateLayout()
    End Sub

    Private Sub MyPendingRequestDataGrid_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles MyPendingRequestDataGrid.MouseRightButtonDown
        If TypeOf (sender) Is DataGrid Then
            Dim dataGrid = CType(sender, DataGrid)
            Dim originalSource = e.OriginalSource
            Dim gridItem As Member_Requests
            If TypeOf (originalSource) Is FrameworkElement Then
                Try
                    gridItem = CType(originalSource, FrameworkElement).DataContext
                    CType(dataGrid.ItemsSource, DomainCollectionView(Of Member_Requests)).MoveCurrentTo(gridItem)
                Catch ex As Exception
                    e.Handled = True
                    Return
                End Try
            End If
        End If
    End Sub

End Class
