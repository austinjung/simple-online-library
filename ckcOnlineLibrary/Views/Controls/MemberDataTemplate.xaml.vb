﻿Partial Public Class MemberDataTemplate
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.LayoutRoot.Width
        End Get
        Set(value As Integer)
            Me.LayoutRoot.Width = value
            Me.dateGrid.Width = value
        End Set
    End Property

End Class
