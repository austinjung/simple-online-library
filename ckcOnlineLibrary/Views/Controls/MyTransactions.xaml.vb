﻿Imports Microsoft.Windows.Data.DomainServices

Partial Public Class MyTransactions
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth As Integer
        Get
            Return Me.TopRequestDataGrid.Width
        End Get
        Set(value As Integer)
            Me.TopRequestDataGrid.Width = value
        End Set
    End Property

    Public Sub Refresh()
        Dim collectionView = CType(Me.DataContext, UserDashboardViewModel).User_Pending_Requests
        If collectionView.IsEmpty Then
            Me.noTransactionHistoryText.Visibility = Windows.Visibility.Visible
        Else
            Me.noTransactionHistoryText.Visibility = Windows.Visibility.Collapsed
        End If
        Me.UpdateLayout()
    End Sub

    Private Sub TopRequestDataGrid_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles TopRequestDataGrid.MouseRightButtonDown
        If TypeOf (sender) Is DataGrid Then
            Dim dataGrid = CType(sender, DataGrid)
            Dim originalSource = e.OriginalSource
            Dim gridItem As Member_Transactions
            If TypeOf (originalSource) Is FrameworkElement Then
                Try
                    gridItem = CType(originalSource, FrameworkElement).DataContext
                    CType(dataGrid.ItemsSource, DomainCollectionView(Of Member_Transactions)).MoveCurrentTo(gridItem)
                Catch ex As Exception
                    e.Handled = True
                    Return
                End Try
            End If
        End If
    End Sub

End Class
