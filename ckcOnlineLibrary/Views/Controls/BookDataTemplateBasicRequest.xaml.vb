﻿Partial Public Class BookDataTemplateBasicRequest
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.LayoutRoot.Width
        End Get
        Set(value As Integer)
            Me.LayoutRoot.Width = value
            Me.gridTitle.Width = value - Me.imgSmall.Width - 10
            Me.scrollDescription.Width = Me.gridTitle.Width
            Me.txbDescription.Width = Me.scrollDescription.Width - 20
        End Set
    End Property

    Private Sub txbTitle_SizeChanged(sender As System.Object, e As System.Windows.SizeChangedEventArgs) Handles txbTitle.SizeChanged
        If Me.txbTitle.ActualHeight < 85 Then
            Me.scrollDescription.MaxHeight = 85 - Me.txbTitle.ActualHeight
        End If
    End Sub
End Class
