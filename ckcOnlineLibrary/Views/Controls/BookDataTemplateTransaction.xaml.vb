﻿Partial Public Class BookDataTemplateTransaction
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.LayoutRoot.Width
        End Get
        Set(value As Integer)
            Me.LayoutRoot.Width = value
            Me.txbTitle.Width = value - Me.imgSmall.Width - 10
            Me.dateGrid.Width = Me.txbTitle.Width
            Me.scrollDescription.Width = Me.txbTitle.Width
            Me.txbAdminComment.Width = Me.scrollDescription.Width - 20
            Me.txbUserComment.Width = Me.scrollDescription.Width - 20
        End Set
    End Property

    Private Sub txbTitle_SizeChanged(sender As System.Object, e As System.Windows.SizeChangedEventArgs) Handles txbTitle.SizeChanged
        If (Me.txbTitle.ActualHeight + Me.dateGrid.ActualHeight) < 85 Then
            Me.scrollDescription.MaxHeight = 85 - Me.txbTitle.ActualHeight - Me.dateGrid.ActualHeight
        End If
    End Sub

    Private Sub dateGrid_SizeChanged(sender As System.Object, e As System.Windows.SizeChangedEventArgs) Handles dateGrid.SizeChanged
        If (Me.txbTitle.ActualHeight + Me.dateGrid.ActualHeight) < 85 Then
            Me.scrollDescription.MaxHeight = 85 - Me.txbTitle.ActualHeight - Me.dateGrid.ActualHeight
        End If
    End Sub
End Class
