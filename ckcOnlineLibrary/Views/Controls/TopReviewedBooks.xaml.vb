﻿Imports Microsoft.Windows.Data.DomainServices

Partial Public Class TopReviewedBooks
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth As Integer
        Get
            Return Me.TopReviewDataGrid.Width
        End Get
        Set(value As Integer)
            Me.TopReviewDataGrid.Width = value
        End Set
    End Property

    Private Sub TopReviewDataGrid_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles TopReviewDataGrid.MouseRightButtonDown
        If TypeOf (sender) Is DataGrid Then
            Dim dataGrid = CType(sender, DataGrid)
            Dim originalSource = e.OriginalSource
            Dim gridItem As Books_By_Reviews
            If TypeOf (originalSource) Is FrameworkElement Then
                Try
                    gridItem = CType(originalSource, FrameworkElement).DataContext
                    CType(dataGrid.ItemsSource, DomainCollectionView(Of Books_By_Reviews)).MoveCurrentTo(gridItem)
                Catch
                    'do nothing
                    Return
                End Try
            End If
        End If
    End Sub
End Class
