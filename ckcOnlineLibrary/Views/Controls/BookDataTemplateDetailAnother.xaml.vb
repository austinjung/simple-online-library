﻿Partial Public Class BookDataTemplateDetailAnother
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.LayoutRoot.Width
        End Get
        Set(value As Integer)
            Me.LayoutRoot.Width = value
            Me.txbTitle.Width = value - Me.imgSmall.Width - Me.miscLabelGrid1.Width - Me.miscLabelGrid2.Width - 10
            Me.scrollDescription.Width = Me.txbTitle.Width
            Me.txbDescription.Width = Me.scrollDescription.Width - 20
        End Set
    End Property

End Class
