﻿Imports Microsoft.Windows.Data.DomainServices

Partial Public Class MembersDataGrid
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth As Integer
        Get
            Return Me.MembersDataGrid.Width
        End Get
        Set(value As Integer)
            Me.MembersDataGrid.Width = value
        End Set
    End Property

    Public Sub Refresh()
        Dim collectionView = CType(Me.DataContext, ManageMemberViewModel).Members
        If collectionView.IsEmpty Then
            Me.noMemberText.Visibility = Windows.Visibility.Visible
        Else
            Me.noMemberText.Visibility = Windows.Visibility.Collapsed
        End If
        Me.UpdateLayout()
    End Sub

    Private Sub MembersDataGrid_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles MembersDataGrid.MouseRightButtonDown
        If TypeOf (sender) Is DataGrid Then
            Dim dataGrid = CType(sender, DataGrid)
            Dim originalSource = e.OriginalSource
            Dim gridItem As Member
            If TypeOf (originalSource) Is FrameworkElement Then
                Try
                    gridItem = CType(originalSource, FrameworkElement).DataContext
                    CType(dataGrid.ItemsSource, DomainCollectionView(Of Member)).MoveCurrentTo(gridItem)
                Catch ex As Exception
                    e.Handled = True
                    Return
                End Try
            End If
        End If
    End Sub

End Class
