﻿Partial Public Class BookReviewDataTemplate
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.LayoutRoot.Width
        End Get
        Set(value As Integer)
            Me.LayoutRoot.Width = value
            Me.scrollDescription.Width = value - 10
            Me.txbDescription.Width = Me.scrollDescription.Width - 20
        End Set
    End Property

End Class
