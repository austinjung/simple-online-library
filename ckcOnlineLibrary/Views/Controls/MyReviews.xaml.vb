﻿Partial Public Class MyReviews
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth As Integer
        Get
            Return Me.TopRequestDataGrid.Width
        End Get
        Set(value As Integer)
            Me.TopRequestDataGrid.Width = value
        End Set
    End Property

End Class
