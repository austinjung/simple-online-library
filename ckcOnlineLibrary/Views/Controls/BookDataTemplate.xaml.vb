﻿Partial Public Class BookDataTemplate
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.TopBorder.Width
        End Get
        Set(value As Integer)
            Me.TopBorder.Width = value
            Me.txbTitle.Width = Me.TopBorder.Width - Me.imgSmall.Width - 10 - Me.TopBorder.BorderThickness.Left - Me.TopBorder.BorderThickness.Right
            Me.scrollDescription.Width = Me.txbTitle.Width
            Me.txbDescription.Width = Me.scrollDescription.Width - 20
        End Set
    End Property

    Public Property SetBackgroundColor() As Brush
        Get
            Return Me.LayoutRoot.Background
        End Get
        Set(value As Brush)
            Me.LayoutRoot.Background = value
        End Set
    End Property

    Public Property SetBorder() As Thickness
        Get
            Return Me.TopBorder.BorderThickness
        End Get
        Set(value As Thickness)
            Me.TopBorder.BorderThickness = value
            Me.txbTitle.Width = Me.TopBorder.Width - Me.imgSmall.Width - 10 - Me.TopBorder.BorderThickness.Left - Me.TopBorder.BorderThickness.Right
            Me.txbDescription.Width = Me.txbTitle.Width
        End Set
    End Property

End Class
