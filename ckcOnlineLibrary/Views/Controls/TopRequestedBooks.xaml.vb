﻿Imports Microsoft.Windows.Data.DomainServices

Partial Public Class TopRequestedBooks
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth As Integer
        Get
            Return Me.TopRequestDataGrid.Width
        End Get
        Set(value As Integer)
            Me.TopRequestDataGrid.Width = value
        End Set
    End Property

    Private Sub TopRequestDataGrid_MouseRightButtonDown(sender As System.Object, e As System.Windows.Input.MouseButtonEventArgs) Handles TopRequestDataGrid.MouseRightButtonDown
        If TypeOf (sender) Is DataGrid Then
            Dim dataGrid = CType(sender, DataGrid)
            Dim originalSource = e.OriginalSource
            Dim gridItem As Books_By_Requests
            If TypeOf (originalSource) Is FrameworkElement Then
                Try
                    gridItem = CType(originalSource, FrameworkElement).DataContext
                    CType(dataGrid.ItemsSource, DomainCollectionView(Of Books_By_Requests)).MoveCurrentTo(gridItem)
                Catch ex As Exception
                    'do nothing
                    Return
                End Try
            End If
        End If
    End Sub
End Class
