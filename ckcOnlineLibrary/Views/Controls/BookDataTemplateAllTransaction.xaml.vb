﻿Partial Public Class BookDataTemplateAllTransaction
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Property ControlWidth() As Integer
        Get
            Return Me.LayoutRoot.Width
        End Get
        Set(value As Integer)
            Me.LayoutRoot.Width = value
            Me.txbTitle.Width = value - Me.imgSmall.Width
            Me.dateGrid.Width = Me.txbTitle.Width
        End Set
    End Property

End Class
