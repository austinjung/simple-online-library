﻿Imports System.Windows.Controls
Imports System.Windows.Navigation
Imports Nemiro.FileManager
Imports System.IO
Imports System.Windows.Media.Imaging
Imports System.Windows.Browser
Imports SilverlightHtmlHost

''' <summary>
''' <see cref="Page"/> class to present information about the current application.
''' </summary>
Partial Public Class About
    Inherits Page

    Private myPleaseWait As PleaseWait = New PleaseWait ' progressbar
    Private bookImagesUrl As String
    Private notificationUri As String = String.Empty
    Private isNotified As Boolean = False
    Private popupOption As HtmlPopupWindowOptions = New HtmlPopupWindowOptions With {
                                                            .Left = 0,
                                                            .Top = 0,
                                                            .Width = 800,
                                                            .Height = 600,
                                                            .Menubar = False,
                                                            .Toolbar = False}
    Private notificationWindow As HtmlHostChildWindow

    ''' <summary>
    ''' Creates a new instance of the <see cref="About"/> class.
    ''' </summary>
    Public Sub New()
        InitializeComponent()

        Me.Title = ApplicationStrings.AboutPageTitle

        'If Not WebContext.Notified And Not WebContext.Current.User.IsAuthenticated Then

        '    WebContext.Notified = True

        '    Dim vmLocator As ViewModelLocator = Nothing
        '    For Each resource In App.Current.Resources.MergedDictionaries
        '        vmLocator = resource.Item("VMLocator")
        '        If Not IsNothing(vmLocator) Then
        '            Exit For
        '        End If
        '    Next
        '    Dim domainContext As ckcLibraryDomainContext = vmLocator.DomainContext
        '    Dim notificationResult = domainContext.GetNotificationUrl(AddressOf ShowNotification, Nothing)
        'End If

        Thumbnails.Items.Clear()
        Dim source = Application.Current.Host.Source
        Dim myUrl As String = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/FileManagerGateway.ashx"
        bookImagesUrl = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/Upload/Book_Images/"

        If WebContext.Current.User.IsAuthenticated Then
            If WebContext.Current.User.Roles.Contains("Manager") Then
                fileList2.TopLevelPath = "/Help Files For Administrator/"
                fileList2.Path = "/Help Files For Administrator/"
            End If
            fileList2.Url = myUrl 'set url
            Me.Thumbnails.Visibility = Windows.Visibility.Collapsed
        Else
            fileList1.Url = myUrl 'set url
            ' handlers for progress
            AddHandler fileList1.Process, AddressOf fileList1_Process
            AddHandler fileList1.Complete, AddressOf fileList1_Complete
        End If


    End Sub

    Private Sub ShowNotification(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of String))
        If Not String.IsNullOrEmpty(result.Value) Then

            WebContext.Notified = True

            '''''''''''''''''''''''''''''''''''''''''''''''''
            'Using iFrame in Silverlight
            notificationWindow = New HtmlHostChildWindow(result.Value & "?#toolbar=1")
            notificationWindow.Title = "Notification"
            notificationWindow.Show()
            '''''''''''''''''''''''''''''''''''''''''''''''''

            Dim source = Application.Current.Host.Source
            Dim myUrl As String = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/" & result.Value & "?#toolbar=1"

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Using HtmlPage.PopupWindow to launch new web browser
            'If HtmlPage.IsPopupWindowAllowed Then
            '    HtmlPage.PopupWindow(New Uri(myUrl), "new", popupOption)
            '    isNotified = True
            'Else
            '    notificationUri = myUrl
            'End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Using Javascript in default.html/default.aspx
            'HtmlPage.Window.Invoke("DialogWindow", myUrl)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        End If
    End Sub

    ''' <summary>
    ''' Executes when the user navigates to this page.
    ''' </summary>
    Protected Overloads Overrides Sub OnNavigatedTo(ByVal e As NavigationEventArgs)

    End Sub

    Private Sub Page_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        'Me.Thumbnails.ItemsSource = Photograph.GetPhotographs().OrderBy(Function(p) p.Name)
    End Sub

    Private Sub fileList1_Process(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      If myPleaseWait IsNot Nothing AndAlso myPleaseWait.Visibility = System.Windows.Visibility.Visible Then
                                          Return
                                      End If
                                      ' show porgress
                                      myPleaseWait = New PleaseWait()
                                      myPleaseWait.Show()

                                  End Sub)
    End Sub

    Private Sub fileList1_Complete(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      'close progress
                                      If myPleaseWait IsNot Nothing Then
                                          myPleaseWait.Close()
                                          myPleaseWait = Nothing
                                      End If
                                      Dim imageCounter As Integer = 0
                                      For Each itm As FileItem In fileList1.Items
                                          If itm.FileName <> "..." Then
                                              Dim img = New Image()
                                              img.Height = 100
                                              img.Name = itm.FileName.ToString()
                                              img.Source = New BitmapImage(New Uri(bookImagesUrl & itm.FileName.ToString(), UriKind.Absolute))
                                              Thumbnails.Items.Add(img)
                                              imageCounter += 1
                                          End If
                                          If imageCounter > 40 Then
                                              Return
                                          End If
                                      Next
                                  End Sub)
    End Sub

End Class