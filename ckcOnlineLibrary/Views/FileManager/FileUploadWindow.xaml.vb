﻿Imports System.Windows.Browser

Partial Public Class FileUploadWindow
    Inherits ChildWindow

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        Me.DialogResult = True
    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub

    Private Sub uploadControl_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles uploadControl.Loaded
        Dim uri As New Uri(HtmlPage.Document.DocumentUri, "FileUpload.ashx")
        uploadControl.UploadUrl = uri
        uploadControl.UploadChunkSize = 25000000
    End Sub

End Class
