﻿Imports System.Windows.Controls
Imports System.Windows.Navigation
Imports Nemiro.FileManager
Imports System.IO

''' <summary>
''' <see cref="Page"/> class to present information about the current application.
''' </summary>
Partial Public Class FileManager
    Inherits Page

    Private myPleaseWait As PleaseWait = Nothing ' progressbar

    Public Sub New()
        InitializeComponent()

        Dim source = Application.Current.Host.Source
        Dim myUrl As String = source.Scheme.ToString() & "://" & source.Host.ToString() & ":" & source.Port.ToString & "/FileManagerGateway.ashx"
        fileList1.Url = myUrl 'set url

        ' handlers for progress
        AddHandler fileList1.Process, AddressOf fileList1_Process
        AddHandler fileList1.Complete, AddressOf fileList1_Complete

    End Sub


    ''' <summary>
    ''' Executes when the user navigates to this page.
    ''' </summary>
    Protected Overloads Overrides Sub OnNavigatedTo(ByVal e As NavigationEventArgs)
    End Sub

    Private Sub Page_Loaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
    End Sub

    ''' <summary>
    ''' Create New Directory button click handler
    ''' </summary>
    Private Sub btnCreateDir_Click(sender As Object, e As RoutedEventArgs) Handles btnCreateDir.Click

        ' init the Prompt Window
        Dim myPrompt As New Prompt("Input", "Please, input directory name:")

        ' add closed handlers to the Prompt Window
        AddHandler myPrompt.Closed, Sub(s, args)
                                        If Not myPrompt.DialogResult.Value Then
                                            Return
                                        End If
                                        ' name is empty
                                        ' name is not empty, send request for create a new directory
                                        fileList1.CreateDirectory(myPrompt.InputValue)

                                    End Sub

        ' show the Prompt Window
        myPrompt.Show()
    End Sub

    ''' <summary>
    ''' Selected files button click handler
    ''' </summary>
    Private Sub btnUploadFile_Click(sender As Object, e As RoutedEventArgs) Handles btnUploadFile.Click

        ' show open file dialog
        Dim op As New OpenFileDialog() With { _
             .Filter = "Images|*.jpg;*.jpeg;*.gif;*.png|Documents|*.txt;*.rtf;*.doc;*.docx;*.xls;*.xlsx;*.pdf|Audio files|*.wav;*.mp3;*.wma|Video files|*.avi;*.mpg;*.mov;*.wmv;*.mp4;*.mpeg|All files|*.*", _
             .Multiselect = True _
        }
        If op.ShowDialog() <> True Then
            Return
        End If
        ' dialog is closed
        ' add files to upload list
        For Each f As FileInfo In op.Files
            fileList1.AddUploadItem(f)
        Next

        ' upload files
        fileList1.Upload()
    End Sub

    ''' <summary>
    ''' book images upload button click handler
    ''' </summary>
    Private Sub btnUploadBookImages_Click(sender As Object, e As RoutedEventArgs) Handles btnUploadBookImages.Click
        ' show open dialog
        Dim uploadChildWindow = New FileUploadWindow()
        uploadChildWindow.Show()
    End Sub

    Private Sub fileList1_Process(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      If myPleaseWait IsNot Nothing AndAlso myPleaseWait.Visibility = System.Windows.Visibility.Visible Then
                                          Return
                                      End If
                                      ' show porgress
                                      myPleaseWait = New PleaseWait()
                                      myPleaseWait.Show()

                                  End Sub)
    End Sub

    Private Sub fileList1_Complete(sender As Object, e As EventArgs)
        Me.Dispatcher.BeginInvoke(Sub()
                                      'close progress
                                      If myPleaseWait IsNot Nothing Then
                                          myPleaseWait.Close()
                                          myPleaseWait = Nothing
                                      End If
                                      ' set new path from fileList
                                      tbPath.Text = fileList1.Path

                                  End Sub)
    End Sub

End Class