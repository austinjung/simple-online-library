﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Browser

Partial Public Class PDFViewerChildWindow
    Inherits ChildWindow

    Private doc As HtmlDocument
    Private pdfDiv As HtmlElement

    Public Sub New()

        InitializeComponent()

    End Sub

    Private Sub ChildWindow_Closing(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        If IsNothing(doc) Then
            doc = HtmlPage.Document
        End If
        If IsNothing(pdfDiv) Then
            pdfDiv = doc.GetElementById("PDFdiv")
        End If
        If Not IsNothing(pdfDiv) Then
            pdfDiv.Parent.RemoveChild(pdfDiv)
        End If

    End Sub

    Private Sub ChildWindow_GotFocus(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles MyBase.GotFocus

        If IsNothing(doc) Then
            doc = HtmlPage.Document
        End If
        If IsNothing(pdfDiv) Then
            pdfDiv = doc.GetElementById("PDFdiv")
        End If
        If Not IsNothing(pdfDiv) Then
            pdfDiv.SetStyleAttribute("z-index", "0")
        End If

    End Sub
End Class
