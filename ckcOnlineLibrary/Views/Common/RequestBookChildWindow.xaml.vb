﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging

Partial Public Class RequestBookChildWindow
    Inherits ChildWindow

    Private myViewModel As BookDetailHistoryViewModel

    Public Sub New()

        InitializeComponent()

        myViewModel = CType(Me.DataContext, BookDetailHistoryViewModel)
        If (Not IsNothing(myViewModel.My_Previous_Pending_Request)) Then
            If WebContext.Current.User.Roles.Contains("User") Then
                If (Not IsNothing(myViewModel.My_Previous_Pending_Request.AdminCommented)) _
                    And (myViewModel.My_Previous_Pending_Request.AdminCommentViewed = False) Then
                    myViewModel.My_Previous_Pending_Request.AdminCommentViewed = True
                    myViewModel.IsCommentViewed = True
                End If
            Else
                If (Not IsNothing(myViewModel.My_Previous_Pending_Request.UserCommented)) _
                    And (myViewModel.My_Previous_Pending_Request.UserCommentViewed = False) Then
                    myViewModel.My_Previous_Pending_Request.UserCommentViewed = True
                    myViewModel.IsCommentViewed = True
                End If
            End If
        End If
        AddHandler CType(Me.DataContext, BookDetailHistoryViewModel).PropertyChanged, AddressOf BookDetail_PropertyChanged

    End Sub

    Private Sub SaveButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles SaveButton.Click

        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False
        If myViewModel.IsCommentViewed Then
            myViewModel.LibraryDataService.SubmitChanges(AddressOf AfterSaveRequestCommentViewFlag, Nothing)
        Else
        End If

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        CType(Me.DataContext, BookDetailHistoryViewModel).RemovePropertyChangeHandler()
        RemoveHandler CType(Me.DataContext, BookDetailHistoryViewModel).PropertyChanged, AddressOf BookDetail_PropertyChanged

    End Sub

    Private Sub BookDetail_PropertyChanged(sender As Object, e As PropertyChangedEventArgs)
        If e.PropertyName = "RequestExpectedDate" Then
            If WebContext.Current.User.Roles.Contains("User") Then
                If (Not IsNothing(myViewModel.My_Previous_Pending_Request.AdminCommented)) _
                    And (myViewModel.My_Previous_Pending_Request.AdminCommentViewed = False) Then
                    myViewModel.My_Previous_Pending_Request.AdminCommentViewed = True
                    myViewModel.IsCommentViewed = True
                End If
            Else
                If (Not IsNothing(myViewModel.My_Previous_Pending_Request.UserCommented)) _
                    And (myViewModel.My_Previous_Pending_Request.UserCommentViewed = False) Then
                    myViewModel.My_Previous_Pending_Request.UserCommentViewed = True
                    myViewModel.IsCommentViewed = True
                End If
            End If
        End If
    End Sub

    Private Sub AfterSaveRequestCommentViewFlag(result As ServiceSubmitChangesResult)

        Dim dialogMessage As SavedRequestBookDialogMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim emailMessage As String = String.Empty

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Error in change comment view flag."

            dialogMessage = New SavedRequestBookDialogMessage(dialogType, action, resultMessage)
            Messenger.[Default].Send(dialogMessage)
        End If

    End Sub

End Class
