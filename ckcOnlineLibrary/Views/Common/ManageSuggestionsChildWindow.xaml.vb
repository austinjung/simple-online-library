﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common
Imports System.ServiceModel.DomainServices.Client.ApplicationServices

Partial Public Class ManageSuggestionsChildWindow
    Inherits ChildWindow

    Public Sub New()

        InitializeComponent()

    End Sub

    Public Sub New(ByVal newAuthor As Author)

        InitializeComponent()
        CType(Me.DataContext, EditAuthorViewModel).EditAuthor = newAuthor

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        Me.UserInformationDataForm.AutoEdit = False
        CType(Me.DataContext, EditUserInformationViewModel).RemovePropertyChangeHandler()

    End Sub

    Private Sub UserInformationDataForm_EditEnded(sender As System.Object, e As System.Windows.Controls.DataFormEditEndedEventArgs) Handles UserInformationDataForm.EditEnded

        Dim dataContext As EditUserInformationViewModel = CType(Me.DataContext, EditUserInformationViewModel)
        If e.EditAction = DataFormEditAction.Commit Then
            Dim editedMember As Member = Me.UserInformationDataForm.CurrentItem
            If Not IsNothing(editedMember) Then
                If editedMember.EntityState = EntityState.Modified _
                        And Not editedMember.Name.Equals(editedMember.MemberLogin) Then
                    Dim alertMsg As String = "Login name cannot be changed."
                    Dim MyMessageBox = New MyMessageBox("Inform", "Information Changed", alertMsg, MessageBoxButton.OK)
                    MyMessageBox.Show()
                    CType(sender, DataForm).CancelEdit()
                    dataContext.LibraryDataService.RejectChanges(AddressOf Cancel_Edit, Nothing)
                Else
                    dataContext.LibraryDataService.SubmitChanges(AddressOf dataContext.AfterSaveEditedMember, Nothing)
                    If TypeOf CType(Application.Current.RootVisual, BusyIndicator).Content Is MainPage Then
                        Dim mainPage As MainPage =
                            DirectCast(CType(Application.Current.RootVisual, BusyIndicator).Content, MainPage)
                        Dim loginStatus = CType(mainPage.loginContainer.Child, LoginUI.LoginStatus)
                        loginStatus.welcomeText.Text = "Welcome " & editedMember.FullName
                        loginStatus.UpdateLayout()
                    End If
                    CType(sender, DataForm).CancelEdit()
                    Me.Close()
                End If
            Else
            End If
        Else
            CType(sender, DataForm).CancelEdit()
            Me.Close()
        End If

    End Sub

    Private Sub Cancel_Edit(result As ServiceSubmitChangesResult)

        Me.Close()

    End Sub
End Class
