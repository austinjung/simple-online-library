﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices
Imports System.Windows.Media.Imaging
Imports GalaSoft.MvvmLight.Messaging
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common
Imports System.ServiceModel.DomainServices.Client.ApplicationServices
Imports System.Windows.Browser

Partial Public Class ViewMySuggestionStatusChildWindow
    Inherits ChildWindow

    Private selectedRequestTree As TopicTree
    Private selectedTreeViewItem As TreeViewItem
    Private selectedSuggestion As NewBookRequest = Nothing
    Private linkUri As String = String.Empty
    Private popupOption As HtmlPopupWindowOptions

    Public Sub New()

        InitializeComponent()
        SetPopupOption()

    End Sub

    Public Sub New(ByVal title As String)

        InitializeComponent()
        Me.Title = title
        SetPopupOption()
        RegisterMessage()

    End Sub

    Private Sub SetPopupOption()

        ' open file in new window
        Dim doc As HtmlDocument = HtmlPage.Document
        Dim _isNavigator As Boolean = HtmlPage.BrowserInformation.Name.Contains("Netscape")
        Dim _screenWidth As Integer = CType(doc.Body.GetProperty("clientWidth"), Integer)
        Dim _screenHeight As Integer = CType(doc.Body.GetProperty("clientHeight"), Integer)
        Dim _screenTop As Integer = 0
        Dim _screenLeft As Integer = 0
        If _isNavigator Then
            _screenTop = (_screenHeight - 600) \ 2
            If _screenTop < 0 Then
                _screenTop = 50
            End If
            _screenLeft = CType(HtmlPage.Window.GetProperty("screenX"), Integer)
        Else
            _screenLeft = CType(HtmlPage.Window.GetProperty("screenLeft"), Integer)
            _screenTop = CType(HtmlPage.Window.GetProperty("screenTop"), Integer)
        End If
        Dim _myCenteredLeft As Integer = (_screenWidth - 800) \ 2 + _screenLeft
        If _myCenteredLeft < 0 Then
            _myCenteredLeft = 0
        End If
        Dim _htmlControlTop As Integer = _screenTop
        popupOption = New HtmlPopupWindowOptions() With { _
                                        .Left = _myCenteredLeft, _
                                        .Top = _htmlControlTop, _
                                        .Width = 800, _
                                        .Height = 600, _
                                        .Menubar = True, _
                                        .Toolbar = False, _
                                        .Directories = False, _
                                        .Status = False, _
                                        .Scrollbars = True, _
                                        .Resizeable = True, _
                                        .Location = True _
                            }

    End Sub

#Region "Register Messages"

    Protected Sub RegisterMessage()
        Messenger.[Default].Register(Of SavedAnswerSuggestionDialogMessage)(Me, AddressOf OnSavedAnswerSuggestionDialogMessageReceived)
    End Sub

    Protected Sub UnregisterMessage()
        Messenger.[Default].Unregister(Of SavedAnswerSuggestionDialogMessage)(Me, AddressOf OnSavedAnswerSuggestionDialogMessageReceived)
    End Sub

#End Region

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        CType(Me.DataContext, SuggestionStatusViewModel).ReLoad()
        CType(Me.DataContext, SuggestionStatusViewModel).RemovePropertyChangeHandler()
        UnregisterMessage()

    End Sub

    Private Sub OKButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles OKButton.Click

    End Sub

    Private Sub CancelButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False

    End Sub

    Private Sub ExpandAll(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles ExpandButton.Click
        For i As Integer = 0 To myTreeView.Items.Count - 1
            Dim childItem As TreeViewItem = _
                TryCast(myTreeView.ItemContainerGenerator.ContainerFromItem(myTreeView.Items(i)),  _
                    TreeViewItem)
            If childItem IsNot Nothing Then
                ExpandAllTreeViewItems(childItem)
            End If
        Next
    End Sub

    ' Declare a delegate for the method to expand all children. 
    Public Delegate Sub ExpandChildren(ByVal currentTreeViewItem As TreeViewItem)

    '   Declare a delegate for the method to expand all children. You'll do this
    ' asynchronously to ensure the items are instatiated before they are expanded. 
    Private Sub ExpandAllTreeViewItems(ByVal currentTreeViewItem As TreeViewItem)
        If Not IsNothing(currentTreeViewItem) Then
            If Not currentTreeViewItem.IsExpanded Then

                currentTreeViewItem.IsExpanded = True
                ' Call this method asynchronously to ensure the items are 
                ' instantiated before expansion. 
                currentTreeViewItem.Dispatcher.BeginInvoke( _
                    New ExpandChildren(AddressOf ExpandAllTreeViewItems), _
                    New Object() {currentTreeViewItem})
            Else

                For i As Integer = 0 To currentTreeViewItem.Items.Count - 1
                    Dim child As TreeViewItem = _
                        DirectCast(currentTreeViewItem.ItemContainerGenerator.ContainerFromIndex(i),  _
                        TreeViewItem)
                    ExpandAllTreeViewItems(child)
                Next
            End If
        End If
    End Sub

    Private Sub myTreeView_SelectedItemChanged(ByVal sender As Object, ByVal e As RoutedPropertyChangedEventArgs(Of Object)) _
                                                                Handles myTreeView.SelectedItemChanged
        Dim dateString As String

        ' Set the data context of the text block to the selected value. 
        If Not IsNothing(Me.myTreeView.SelectedValue) Then
            selectedRequestTree = Me.myTreeView.SelectedValue

            Dim childItem = TryCast(myTreeView.ItemContainerGenerator.ContainerFromItem(Me.myTreeView.SelectedValue), TreeViewItem)
            If Not IsNothing(childItem) Then
                childItem.IsExpanded = True
            End If

            If Not IsNothing(selectedRequestTree) Then
                selectedSuggestion = (From r In CType(Me.DataContext, SuggestionStatusViewModel).Suggestions
                                      Where r.NewBookRequestID = selectedRequestTree.NewBookRequestID
                                      Select r).FirstOrDefault

            End If

            If IsNothing(selectedSuggestion) Then
                Me.OKButton.IsEnabled = False
                Me.OpenLinkButton.IsEnabled = False
            Else
                CType(Me.DataContext, SuggestionStatusViewModel).SelectedSuggection = selectedSuggestion

                If WebContext.Current.User.Roles.Contains("User") Then
                    If IsNothing(selectedSuggestion.LastReviewDate) Then
                        dateString = "01/01/0001"
                    Else
                        dateString = CType(selectedSuggestion.LastReviewDate, Date).ToString("MM/dd/yyyy")
                    End If

                    If (selectedSuggestion.ReviewViewed = False) And (Not dateString.Equals("01/01/0001")) Then
                        selectedSuggestion.ReviewViewed = True
                        CType(Me.DataContext, SuggestionStatusViewModel).LibraryDataService.SubmitChanges(AddressOf AfterSaveChanges, Nothing)
                    End If
                Else
                    If selectedSuggestion.RequestViewed = False Then
                        selectedSuggestion.RequestViewed = True
                        If String.IsNullOrEmpty(selectedSuggestion.Reviews) Then
                            selectedSuggestion.LastReviewDate = Now()
                            selectedSuggestion.Reviews = "We're processing your suggestion."
                            selectedSuggestion.ReviewViewed = False
                            selectedSuggestion.ReviewSequence += 1
                        End If
                        CType(Me.DataContext, SuggestionStatusViewModel).LibraryDataService.SubmitChanges(AddressOf AfterSaveChanges, Nothing)
                    End If
                End If

                Me.OKButton.IsEnabled = True
                If Not String.IsNullOrEmpty(selectedSuggestion.LinkURL) Then
                    linkUri = selectedSuggestion.LinkURL
                    Me.OpenLinkButton.IsEnabled = True
                ElseIf Not String.IsNullOrEmpty(selectedSuggestion.ISBN) Then
                    linkUri = "http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Dstripbooks&field-keywords=" & selectedSuggestion.ISBN
                    Me.OpenLinkButton.IsEnabled = True
                ElseIf Not String.IsNullOrEmpty(selectedSuggestion.BookTitle) Then
                    linkUri = "http://www.amazon.com/s/ref=nb_sb_ss_i_1_6?url=search-alias%3Dstripbooks&field-keywords=" _
                            & HttpUtility.HtmlEncode(selectedSuggestion.BookTitle.Trim)
                    Me.OpenLinkButton.IsEnabled = True
                Else
                    Me.OpenLinkButton.IsEnabled = False
                End If
            End If
        End If

    End Sub

    Private Sub OpenLinkButton_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles OpenLinkButton.Click

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Using HtmlPage.PopupWindow to launch new web browser
        If HtmlPage.IsPopupWindowAllowed Then
            HtmlPage.PopupWindow(New Uri(linkUri), "new", popupOption)
        End If

    End Sub

    Private Sub AfterSaveChanges(result As ServiceSubmitChangesResult)

        Dim changedTree As TopicTree = Nothing
        If (result.[Error] IsNot Nothing) Then
            Dim myMessageBox = New MyMessageBox("Error", "Save", "Save was unsuccessful.", MessageBoxButton.OK)
            myMessageBox.Show()
        Else
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is NewBookRequest Then
                        changedTree = CType(Me.DataContext, SuggestionStatusViewModel).UpdateSuggestionTree(e)
                        Exit For
                    End If
                Next
            End If

            selectedTreeViewItem = TryCast(myTreeView.ItemContainerGenerator.ContainerFromItem(myTreeView.SelectedItem), TreeViewItem)
            Dim selectedChild = TryCast(myTreeView.SelectedItem, TopicTree)

            If selectedTreeViewItem IsNot Nothing Then

                selectedTreeViewItem.DataContext = changedTree
                selectedTreeViewItem.Header = changedTree
                selectedTreeViewItem.UpdateLayout()

                For i As Integer = 0 To selectedTreeViewItem.Items.Count - 1
                    Dim child As TreeViewItem = _
                        TryCast(selectedTreeViewItem.ItemContainerGenerator.ContainerFromIndex(i),  _
                        TreeViewItem)
                    If Not IsNothing(child) Then
                        child.DataContext = changedTree.ChildTopics(i)
                        child.Header = changedTree.ChildTopics(i)
                        child.UpdateLayout()
                    End If
                Next

            ElseIf Not IsNothing(selectedChild) Then
                Dim childItem As TreeViewItem = Nothing
                For i As Integer = 0 To myTreeView.Items.Count - 1
                    childItem = TryCast(myTreeView.ItemContainerGenerator.ContainerFromItem(myTreeView.Items(i)), TreeViewItem)
                    If childItem IsNot Nothing Then
                        If CType(childItem.DataContext, TopicTree).NewBookRequestID = selectedChild.NewBookRequestID Then

                            childItem.DataContext = changedTree
                            childItem.Header = changedTree
                            childItem.UpdateLayout()

                            For j As Integer = 0 To childItem.Items.Count - 1
                                Dim child As TreeViewItem = _
                                    TryCast(childItem.ItemContainerGenerator.ContainerFromIndex(j),  _
                                    TreeViewItem)
                                If Not IsNothing(child) Then
                                    child.DataContext = changedTree.ChildTopics(j)
                                    child.Header = changedTree.ChildTopics(j)
                                    child.UpdateLayout()
                                End If
                            Next

                            Exit For
                        End If
                    End If
                Next
            End If

        End If

    End Sub

    Private Sub OnSavedAnswerSuggestionDialogMessageReceived(msg As SavedAnswerSuggestionDialogMessage)

        Dim changedTree As TopicTree = Nothing
        changedTree = CType(Me.DataContext, SuggestionStatusViewModel).UpdateSuggestionTree(msg.SavedNewBookRequest)

        selectedTreeViewItem = TryCast(myTreeView.ItemContainerGenerator.ContainerFromItem(myTreeView.SelectedItem), TreeViewItem)
        Dim selectedChild = TryCast(myTreeView.SelectedItem, TopicTree)

        If selectedTreeViewItem IsNot Nothing Then

            selectedTreeViewItem.DataContext = changedTree
            selectedTreeViewItem.Header = changedTree
            selectedTreeViewItem.UpdateLayout()

            For i As Integer = 0 To selectedTreeViewItem.Items.Count - 1
                Dim child As TreeViewItem = _
                    TryCast(selectedTreeViewItem.ItemContainerGenerator.ContainerFromIndex(i),  _
                    TreeViewItem)
                If Not IsNothing(child) Then
                    child.DataContext = changedTree.ChildTopics(i)
                    child.Header = changedTree.ChildTopics(i)
                    child.UpdateLayout()
                End If
            Next

        ElseIf Not IsNothing(selectedChild) Then
            Dim childItem As TreeViewItem = Nothing
            For i As Integer = 0 To myTreeView.Items.Count - 1
                childItem = TryCast(myTreeView.ItemContainerGenerator.ContainerFromItem(myTreeView.Items(i)), TreeViewItem)
                If childItem IsNot Nothing Then
                    If CType(childItem.DataContext, TopicTree).NewBookRequestID = selectedChild.NewBookRequestID Then

                        childItem.DataContext = changedTree
                        childItem.Header = changedTree
                        childItem.UpdateLayout()

                        For j As Integer = 0 To childItem.Items.Count - 1
                            Dim child As TreeViewItem = _
                                TryCast(childItem.ItemContainerGenerator.ContainerFromIndex(j),  _
                                TreeViewItem)
                            If Not IsNothing(child) Then
                                child.DataContext = changedTree.ChildTopics(j)
                                child.Header = changedTree.ChildTopics(j)
                                child.UpdateLayout()
                            End If
                        Next

                        Exit For
                    End If
                End If
            Next
        End If
    End Sub

End Class
