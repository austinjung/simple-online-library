﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices

Partial Public Class ViewBookDetailChildWindow
    Inherits ChildWindow

    Public Sub New()

        InitializeComponent()

    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click

        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click

        CType(Me.DataContext, BookDetailHistoryViewModel).Cleanup()
        Me.DialogResult = False

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        'don't cleanup viewmodel because OKButton_click case, request chilewindow will use same viewmodel

    End Sub

End Class
