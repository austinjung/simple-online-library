﻿Imports System.Collections.ObjectModel
Imports System.Windows.Controls.Primitives
Imports System.Windows.Data
Imports System.ComponentModel
Imports Microsoft.Windows.Data.DomainServices

Partial Public Class MakeReviewChildWindow
    Inherits ChildWindow

    Public Sub New()

        InitializeComponent()

    End Sub

    Private Sub SaveButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles SaveButton.Click

        Me.DialogResult = True

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click

        Me.DialogResult = False

    End Sub

    Private Sub LayoutRoot_Unloaded(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles LayoutRoot.Unloaded

        CType(Me.DataContext, ReviewBookViewModel).Cleanup()

    End Sub

End Class
