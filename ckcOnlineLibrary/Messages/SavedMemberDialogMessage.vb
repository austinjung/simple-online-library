﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedMemberDialogMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Private _savedMember As Member
    Public Property SavedMember As Member
        Get
            Return _savedMember
        End Get
        Set(value As Member)
            _savedMember = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String, member As Member)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type
        Me.SavedMember = member

    End Sub

End Class
