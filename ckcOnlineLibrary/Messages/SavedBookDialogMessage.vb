﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedBookDialogMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Private _savedBook As Book
    Public Property SavedBook As Book
        Get
            Return _savedBook
        End Get
        Set(value As Book)
            _savedBook = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String, ByVal savedBook As Book)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type
        Me.SavedBook = savedBook

    End Sub

End Class
