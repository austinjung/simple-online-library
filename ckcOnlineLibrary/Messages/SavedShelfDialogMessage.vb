﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedShelfDialogMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Private _savedShelf As Shelf
    Public Property SavedShelf As Shelf
        Get
            Return _savedShelf
        End Get
        Set(value As Shelf)
            _savedShelf = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String, shelf As Shelf)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type
        Me.SavedShelf = shelf

    End Sub

End Class
