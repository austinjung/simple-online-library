﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedOverDueTransactionMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Private _savedTransaction As Member_Transactions
    Public Property SavedTransaction As Member_Transactions
        Get
            Return _savedTransaction
        End Get
        Set(value As Member_Transactions)
            _savedTransaction = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String, transaction As Member_Transactions)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type
        Me.SavedTransaction = transaction

    End Sub

End Class
