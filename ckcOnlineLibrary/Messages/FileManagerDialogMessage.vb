﻿Imports GalaSoft.MvvmLight.Messaging

Friend Class FileManagerDialogMessage
    Inherits MessageBase

    Public Sub New()

    End Sub

    Public Sub New(ByVal fileName)
        SelectedFileName = fileName
        If String.IsNullOrEmpty(fileName) Then
            DialogResult = False
        Else
            DialogResult = True
        End If
    End Sub

    Private _selectedFileName As String = String.Empty
    Public Property SelectedFileName As String
        Get
            Return _selectedFileName
        End Get
        Set(value As String)
            _selectedFileName = value
        End Set
    End Property

    Private _dialogResult As Boolean = False
    Public Property DialogResult As Boolean
        Get
            Return _dialogResult
        End Get
        Set(value As Boolean)
            _dialogResult = value
        End Set
    End Property

End Class
