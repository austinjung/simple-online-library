﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedAuthorDialogMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Private _savedAuthor As Author
    Public Property SavedAuthor As Author
        Get
            Return _savedAuthor
        End Get
        Set(value As Author)
            _savedAuthor = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String, author As Author)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type
        Me.SavedAuthor = author

    End Sub

End Class
