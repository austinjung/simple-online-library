﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedCheckoutDialogMessage
    Inherits DialogMessage

    Public Sub New(ByVal content As String, ByVal title As String)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title

    End Sub

End Class
