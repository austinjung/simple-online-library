﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedAnswerSuggestionDialogMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Private _savedNewBookRequest As NewBookRequest
    Public Property SavedNewBookRequest As NewBookRequest
        Get
            Return _savedNewBookRequest
        End Get
        Set(value As NewBookRequest)
            _savedNewBookRequest = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String, savedNewBookRequest As NewBookRequest)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type
        Me.SavedNewBookRequest = savedNewBookRequest

    End Sub

End Class
