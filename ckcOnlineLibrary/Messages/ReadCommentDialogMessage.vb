﻿Imports GalaSoft.MvvmLight.Messaging

Public Class ReadCommentDialogMessage

    Private _dialogResult As String
    Public Property DialogResult As String
        Get
            Return _dialogResult
        End Get
        Set(value As String)
            _dialogResult = value
        End Set
    End Property

    Public Sub New(ByVal result As String)

        Me.DialogResult = result

    End Sub

End Class
