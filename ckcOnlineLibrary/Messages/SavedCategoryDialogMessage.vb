﻿Imports System.Windows
Imports GalaSoft.MvvmLight.Messaging

Friend Class SavedCategoryDialogMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Private _savedCategory As Category
    Public Property SavedCategory As Category
        Get
            Return _savedCategory
        End Get
        Set(value As Category)
            _savedCategory = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String, category As Category)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type
        Me.SavedCategory = category

    End Sub

End Class
