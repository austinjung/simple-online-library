﻿Imports GalaSoft.MvvmLight.Messaging

Friend Class ReceiveBookResultMessage
    Inherits DialogMessage

    Private _type As String
    Public Property Type As String
        Get
            Return _type
        End Get
        Set(value As String)
            _type = value
        End Set
    End Property

    Public Sub New(ByVal type As String, ByVal content As String, ByVal title As String)
        MyBase.New(content, Nothing)

        Button = MessageBoxButton.OK
        Caption = title
        Me.Type = type

    End Sub

End Class
