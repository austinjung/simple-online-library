﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations
Imports FirstLook.ServiceModel.DomainServices.Client.Security

Public Class SiteMapAuthorizationRule
    Inherits AuthorizationRule

    Public Overrides Function GetAuthorizationAttributes(target As Object) _
                    As System.Collections.Generic.IEnumerable(Of System.ComponentModel.DataAnnotations.AuthorizationAttribute)

        Dim attributes = New List(Of AuthorizationAttribute)
        Dim targetUri As Uri = TryCast(target, Uri)
        If targetUri IsNot Nothing Then
            If WebContext.AuthorizationMap.ContainsKey(targetUri) Then
                attributes.AddRange(WebContext.AuthorizationMap(targetUri))
            End If
        End If

        Return attributes

    End Function
End Class
