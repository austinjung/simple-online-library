﻿Imports System
Imports System.ComponentModel.DataAnnotations
Imports System.Collections.Generic

Public Class SiteMapNode

    Private ReadOnly _authorizationAttributes As IEnumerable(Of AuthorizationAttribute)
    Private ReadOnly _description As String
    Private ReadOnly _mappedUri As Uri
    Private ReadOnly _mappingUri As Uri
    Private ReadOnly _targetName As String
    Private ReadOnly _title As String
    Private ReadOnly _uri As Uri

    Friend Sub New(ByVal uri As Uri, _
                   ByVal targetName As String, _
                   Optional ByVal title As String = Nothing, _
                   Optional ByVal authorizationAttributes As IEnumerable(Of AuthorizationAttribute) = Nothing, _
                   Optional ByVal description As String = Nothing, _
                   Optional ByVal mappingUri As Uri = Nothing, _
                   Optional ByVal mappedUri As Uri = Nothing _
                   )

        If uri Is Nothing Then
            Throw New ArgumentNullException("uri")
        End If
        If String.IsNullOrEmpty(targetName) Then
            Throw New ArgumentNullException("targetName")
        End If
        Me._uri = uri
        Me._targetName = targetName

        If String.IsNullOrEmpty(title) Then
            Me._title = uri.ToString()
        Else
            Me._title = title
        End If

        If authorizationAttributes Is Nothing Then
            authorizationAttributes = New List(Of AuthorizationAttribute)
        End If
        Me._authorizationAttributes = authorizationAttributes

        If String.IsNullOrEmpty(description) Then
            Me._description = Me._title
        Else
            Me._description = description
        End If

        Me._mappingUri = mappingUri
        If mappedUri Is Nothing Then
            Me._mappedUri = Me._uri
        Else
            Me._mappedUri = mappedUri
        End If

    End Sub

    Sub New()
        ' TODO: Complete member initialization 
    End Sub

    Public ReadOnly Property AuthorizationAttributes
        Get
            Return Me._authorizationAttributes
        End Get
    End Property

    Public ReadOnly Property Description
        Get
            Return Me._description
        End Get
    End Property

    Public ReadOnly Property MappedUri
        Get
            Return Me._mappedUri
        End Get
    End Property

    Public ReadOnly Property MappingUri
        Get
            Return Me._mappingUri
        End Get
    End Property

    Public ReadOnly Property TargetName
        Get
            Return Me._targetName
        End Get
    End Property

    Public ReadOnly Property Title
        Get
            Return Me._title
        End Get
    End Property

    Public ReadOnly Property Uri
        Get
            Return Me._uri
        End Get
    End Property

End Class
