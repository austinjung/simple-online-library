﻿Imports System
Imports System.ComponentModel.DataAnnotations
Imports System.Collections.Generic
Imports System.ServiceModel.DomainServices.Server

Partial Public Class WebContext

    ' There are many ways of representing a site map. For brevity, we'll do it inline.
    Private Shared ReadOnly Map As IEnumerable(Of SiteMapNode)

    Shared Sub New()
        WebContext.Map _
            = New SiteMapNode() {
                                New SiteMapNode(
                                                New Uri("/Views/Member/Home.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Home",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("User")
                                                                              },
                                                Nothing,
                                                New Uri("/Home", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Member/SearchBooksForUserPage.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Search Books",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("User")
                                                                              },
                                                Nothing,
                                                New Uri("/SearchBooks", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Member/ReviewBooksPage.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Review Books",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("User")
                                                                              },
                                                Nothing,
                                                New Uri("/ReviewBooks", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Manager/ManagerHome.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Home",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/ManagerHome", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Manager/IssueBookPage.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Issued",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/Manager/IssueBookPage", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Manager/ReturnBookPage.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Returns",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/Manager/ReturnBookPage", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Manager/ManageBooksPage.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Books",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/Manager/ManageBooksPage", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Manager/ManageMembersPage.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Members",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/Manager/ManageMembersPage", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/FileManager/FileManager.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Files",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/FileManager/FileManager", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/About.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "About",
                                                Nothing,
                                                Nothing,
                                                New Uri("/About", UriKind.Relative),
                                                Nothing
                                                )
                                }

        Dim authorizationMap = New Dictionary(Of Uri, IEnumerable(Of AuthorizationAttribute))
        For Each node As SiteMapNode In WebContext.Map
            authorizationMap(node.Uri) = node.AuthorizationAttributes
        Next

        WebContext.AuthorizationMap = authorizationMap
    End Sub

    Public Shared ReadOnly Property SiteMap As IEnumerable(Of SiteMapNode)
        Get
            Return WebContext.Map
        End Get
    End Property

    Public Shared Property AuthorizationMap As Dictionary(Of Uri, IEnumerable(Of AuthorizationAttribute))

    Private Shared _Notified As Boolean = False
    Public Shared Property Notified As Boolean
        Get
            Return _Notified
        End Get
        Set(value As Boolean)
            _Notified = value
        End Set
    End Property

End Class
