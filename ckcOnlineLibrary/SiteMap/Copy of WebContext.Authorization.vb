﻿Imports System
Imports System.ComponentModel.DataAnnotations
Imports System.Collections.Generic
Imports System.ServiceModel.DomainServices.Server

Partial Public Class WebContext

    ' There are many ways of representing a site map. For brevity, we'll do it inline.
    Private Shared ReadOnly Map As IEnumerable(Of SiteMapNode)

    Shared Sub New()
        WebContext.Map _
            = New SiteMapNode() {
                                New SiteMapNode(
                                                New Uri("/Views/Home.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Home",
                                                Nothing,
                                                Nothing,
                                                New Uri("/Home", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Manager/ManageBooks.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Manage Books",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/Manager/ManageBooks", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Manager/ManageBooksDCVM.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Manage Books with Domain Collection View Model",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/Manager/ManageBooksDCVM", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/Member/MemberMainPage.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "Member's Home",
                                                New AuthorizationAttribute() {
                                                                                New RequiresRoleAttribute("User"),
                                                                                New RequiresRoleAttribute("Manager")
                                                                              },
                                                Nothing,
                                                New Uri("/Member/MemberHome", UriKind.Relative),
                                                Nothing
                                                ),
                                New SiteMapNode(
                                                New Uri("/Views/About.xaml", UriKind.Relative),
                                                "ContentFrame",
                                                "About",
                                                Nothing,
                                                Nothing,
                                                New Uri("/About", UriKind.Relative),
                                                Nothing
                                                )
                                }

        Dim authorizationMap = New Dictionary(Of Uri, IEnumerable(Of AuthorizationAttribute))
        For Each node As SiteMapNode In WebContext.Map
            authorizationMap(node.Uri) = node.AuthorizationAttributes
        Next

        WebContext.AuthorizationMap = authorizationMap
    End Sub

    Public Shared ReadOnly Property SiteMap As IEnumerable(Of SiteMapNode)
        Get
            Return WebContext.Map
        End Get
    End Property

    Public Shared Property AuthorizationMap As Dictionary(Of Uri, IEnumerable(Of AuthorizationAttribute))

End Class
