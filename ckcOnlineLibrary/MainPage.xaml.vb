﻿Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Navigation
' For Silverlight Business Application Default Login Template
Imports ckcOnlineLibrary.LoginUI

' For Site-Map Navigation : Start
Imports System.Windows.Shapes
Imports System.ComponentModel.Composition
Imports FirstLook.ServiceModel.DomainServices.Client
Imports FirstLook.ServiceModel.DomainServices.Client.Security
Imports System.ServiceModel.DomainServices.Client.ApplicationServices
' For Site-Map Navigation : End

''' <summary>
''' <see cref="UserControl"/> class providing the main UI for the application.
''' </summary>
<Export(GetType(UserControl))>
Partial Public Class MainPage
    Inherits UserControl

    ' Application's Authentication Service
    Private ReadOnly _AuthService As AuthenticationService = WebContext.Current.Authentication

    ''' <summary>
    ''' Creates a new <see cref="MainPage"/> instance.
    ''' </summary>
    Public Sub New()

        InitializeComponent()
        Me.loginContainer.Child = New LoginStatus()

        ' Austin Jung : Add Event Handler for Login User Changed
        AddHandler _AuthService.LoggedIn, New EventHandler(Of AuthenticationEventArgs)(AddressOf AuthService_LoginChanged)
        AddHandler _AuthService.LoggedOut, New EventHandler(Of AuthenticationEventArgs)(AddressOf AuthService_LoginChanged)

        ' Austin Jung : For Site-Map Navigation : Start
        Authorization.SetNavigationMode(Me.ContentFrame, AuthorizationNavigationMode.Redirect)
        AuthorizationRuleManager.AddAuthorizationRule(GetType(Uri), New SiteMapAuthorizationRule())
        Me.ProcessSiteMap()
        ' For Site-Map Navigation : End

    End Sub

    ''' <summary>
    ''' After the Frame navigates, ensure the HyperlinkButton representing the current page is selected
    ''' </summary>
    Private Sub ContentFrame_Navigated(ByVal sender As Object, ByVal e As NavigationEventArgs)
        For Each child As UIElement In LinksStackPanel.Children
            Dim hb As HyperlinkButton = TryCast(child, HyperlinkButton)
            If hb IsNot Nothing AndAlso hb.NavigateUri IsNot Nothing Then
                If hb.NavigateUri.ToString().Equals(e.Uri.ToString()) Then
                    VisualStateManager.GoToState(hb, "ActiveLink", True)
                Else
                    VisualStateManager.GoToState(hb, "InactiveLink", True)
                End If
            End If
        Next
    End Sub

    ''' <summary>
    ''' If an error occurs during navigation, show an error window
    ''' </summary>
    Private Sub ContentFrame_NavigationFailed(ByVal sender As Object, ByVal e As NavigationFailedEventArgs)
        e.Handled = True
        If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
            'ErrorWindow.CreateNew(e.Exception)
            'If navigation failed, redirect to About page
            Me.ContentFrame.Navigate(New Uri("/About", UriKind.Relative))
        End If
    End Sub

    ' Austin Jung : For Site-Map Navigation : Start
    Private Sub ProcessSiteMap()

        Dim addDivider As Boolean = False
        Dim mapper As UriMapper = New UriMapper()

        For Each node As SiteMapNode In WebContext.SiteMap

            If addDivider Then
                Dim divider As UIElement = New Rectangle() With {.Style = TryCast(Application.Current.Resources("DividerStyle"), Style)}
                Me.LinksStackPanel.Children.Add(divider)
                Authorization.Authorize(divider, node.AuthorizationAttributes)
            End If

            Dim uri As Uri
            If node.MappingUri Is Nothing Then
                uri = node.Uri
            Else
                uri = node.MappingUri
            End If
            Dim link As HyperlinkButton = New HyperlinkButton() With
                                          {
                                            .Content = node.Title,
                                            .NavigateUri = uri,
                                            .Style = CType(Application.Current.Resources("LinkStyle"), Style),
                                            .TargetName = node.TargetName
                                          }
            Me.LinksStackPanel.Children.Add(link)

            Authorization.Authorize(link, node.AuthorizationAttributes)

            If node.MappingUri IsNot Nothing Then
                mapper.UriMappings.Add(New UriMapping() With {.Uri = node.MappingUri, .MappedUri = node.MappedUri})
            End If

            addDivider = True

        Next

        If mapper.UriMappings.Count > 0 Then
            Me.ContentFrame.UriMapper = mapper
        End If

        ' For Site-Map Navigation : End
    End Sub

    ' Austin Jung : Handle the event of login user changed
    Private Sub AuthService_LoginChanged(sender As Object, e As AuthenticationEventArgs)

        WebContext.Notified = False

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not WebContext.Current.User.IsAuthenticated Then
            If Not IsNothing(vmLocator) Then
                vmLocator.RefreshService()
            End If
            'If user logout, redirect to About page
            If Me.ContentFrame.Source.Equals("/About") Then
                Me.ContentFrame.Refresh()
            Else
                Me.ContentFrame.Navigate(New Uri("/About", UriKind.Relative))
            End If
        Else
            ' If user login, redirect to their home page
            If WebContext.Current.User.Roles.Contains("Manager") Then
                If Not IsNothing(vmLocator) Then
                    vmLocator.ManagerDashBoardVM.User_Transaction_Take_Size = 6
                End If
                Me.ContentFrame.Navigate(New Uri("/ManagerHome", UriKind.Relative))
            Else
                If Not IsNothing(vmLocator) Then
                    vmLocator.UserDashBoardVM.User_Transaction_Take_Size = 6
                End If
                Me.ContentFrame.Navigate(New Uri("/Home", UriKind.Relative))
            End If
            'refresh the current page 
            'ContentFrame.Refresh()
        End If
    End Sub


End Class