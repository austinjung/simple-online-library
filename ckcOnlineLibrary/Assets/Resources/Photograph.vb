﻿Imports System.Collections.Generic
Imports System.Diagnostics.CodeAnalysis
Imports System.Windows.Controls

''' <summary>
''' Photograph business object used in examples.
''' </summary>
Partial Public NotInheritable Class Photograph
    ''' <summary>
    ''' Gets the name of the Photograph.
    ''' </summary>
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Private Set(value As String)
            m_Name = Value
        End Set
    End Property
    Private m_Name As String

    ''' <summary>
    ''' Gets an Image control containing the Photograph.
    ''' </summary>
    Public Property Image() As Image
        Get
            Return m_Image
        End Get
        Private Set(value As Image)
            m_Image = Value
        End Set
    End Property
    Private m_Image As Image

    ''' <summary>
    ''' Initializes a new instance of the Photograph class.
    ''' </summary>
    ''' <param name="resourceName">
    ''' Name of the resource defining the photograph.
    ''' </param>
    Friend Sub New(resourceName As String)
        Name = resourceName
        Image = SharedResources.GetImage(resourceName)
    End Sub

    ''' <summary>
    ''' Overrides the string to return the name.
    ''' </summary>
    ''' <returns>Returns the photograph name.</returns>
    Public Overrides Function ToString() As String
        Return Name
    End Function

    ''' <summary>
    ''' Get all of the photographs defined in the assembly as embedded
    ''' resources.
    ''' </summary>
    ''' <returns>
    ''' All of the photographs defined in the assembly as embedded
    ''' resources.
    ''' </returns>
    <SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification:="Doing more work than a property should")> _
    Public Shared Function GetPhotographs() As IEnumerable(Of Photograph)
        Dim photographs As New List(Of Photograph)
        For Each resourceName As String In SharedResources.GetImageNames()
            photographs.Add(New Photograph(resourceName))
        Next
        Return photographs.ToArray
    End Function
End Class