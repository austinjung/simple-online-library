﻿Imports System.Collections.Generic
Imports System.Diagnostics.CodeAnalysis
Imports System.Linq
Imports System.Reflection
Imports System.Windows
Imports System.Windows.Controls
Imports System.Windows.Markup
Imports System.Windows.Media.Imaging
Imports System.Xml
Imports System.IO
Imports ckcOnlineLibrary.Web

Public NotInheritable Class SharedResources

    ''' <summary>
    ''' Prefix of images loaded as resources.
    ''' </summary>
    Private Const ResourceImagePrefix As String = "ckcOnlineLibrary.Assets.Images."

    ''' <summary>
    ''' Prefix of icons loaded as resources.
    ''' </summary>
    Private Const ResourceIconPrefix As String = "ckcOnlineLibrary.Assets.Icons."

    ''' <summary>
    ''' Book Images directory
    ''' </summary>
    Private Const BookImageDirectory As String = "/Upload/Book_Images"

    ''' <summary>
    ''' Get an embedded resource image from the assembly.
    ''' </summary>
    ''' <param name="name">Name of the image resource.</param>
    ''' <returns>
    ''' Desired embedded resource image from the assembly.
    ''' </returns>
    Public Shared Function GetImage(name As String) As Image
        Return CreateImage(ResourceImagePrefix, name)
    End Function

    ''' <summary>
    ''' Get an embedded resource icon from the assembly.
    ''' </summary>
    ''' <param name="name">Name of the icon resource.</param>
    ''' <returns>
    ''' Desired embedded resource icon from the assembly.
    ''' </returns>
    Public Shared Function GetIcon(name As String) As Image
        Return CreateImage(ResourceIconPrefix, name)
    End Function

    ''' <summary>
    ''' A cached dictionary of the bitmap images.
    ''' </summary>
    Private Shared cachedBitmapImages As IDictionary(Of String, BitmapImage) = New Dictionary(Of String, BitmapImage)()

    ''' <summary>
    ''' Get an embedded resource image from the assembly.
    ''' </summary>
    ''' <param name="prefix">The prefix of the full name of the resource.</param>
    ''' <param name="name">Name of the image resource.</param>
    ''' <returns>
    ''' Desired embedded resource image from the assembly.
    ''' </returns>
    Public Shared Function CreateImage(prefix As String, name As String) As Image
        Dim image As New Image() With { _
                                      .Tag = name _
                                      }

        Dim source As BitmapImage = Nothing
        Dim resourceName As String = prefix & name
        If Not cachedBitmapImages.TryGetValue(resourceName, source) Then
            Dim assembly As Assembly = GetType(SharedResources).Assembly

            Using resource As Stream = assembly.GetManifestResourceStream(resourceName)
                If resource IsNot Nothing Then
                    source = New BitmapImage()
                    source.SetSource(resource)
                End If
            End Using
            cachedBitmapImages(resourceName) = source
        End If
        image.Source = source
        Return image
    End Function

    ''' <summary>
    ''' Get all of the names of embedded resources images in the assembly.
    ''' </summary>
    ''' <returns>
    ''' All of the names of embedded resources images in the assembly.
    ''' </returns>
    <SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification:="Does more work than a property should.")> _
    Public Shared Function GetImageNames() As IEnumerable(Of String)
        Return GetResourceNames(ResourceImagePrefix)
    End Function

    ''' <summary>
    ''' Get all of the names of embedded resources icons in the assembly.
    ''' </summary>
    ''' <returns>
    ''' All of the names of embedded resources icons in the assembly.
    ''' </returns>
    <SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification:="Does more work than a property should.")> _
    Public Shared Function GetIconNames() As IEnumerable(Of String)
        Return GetResourceNames(ResourceIconPrefix)
    End Function

    ''' <summary>
    ''' Get all of the images in the assembly.
    ''' </summary>
    ''' <returns>All of the images in the assembly.</returns>
    <SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification:="Does more work than a property should")> _
    Public Shared Function GetImages() As IEnumerable(Of Image)
        Dim images As New List(Of Image)
        For Each name As String In GetImageNames()
            images.Add(GetImage(name))
        Next
        Return images.ToArray()
    End Function

    ''' <summary>
    ''' Get all of the icons in the assembly.
    ''' </summary>
    ''' <returns>All of the icons in the assembly.</returns>
    <SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification:="Does more work than a property should")> _
    Public Shared Function GetIcons() As IEnumerable(Of Image)
        Dim images As New List(Of Image)
        For Each name As String In GetIconNames()
            images.Add(GetImage(name))
        Next
        Return images.ToArray()
    End Function

    ''' <summary>
    ''' Get all the names of embedded resources in the assembly with the 
    ''' provided prefix value.
    ''' </summary>
    ''' <param name="prefix">The prefix for the full resource name.</param>
    ''' <returns>Returns an enumerable of all the resource names that match.</returns>
    Private Shared Function GetResourceNames(prefix As String) As IEnumerable(Of String)
        Dim names As New List(Of String)
        Dim assembly As Assembly = GetType(SharedResources).Assembly
        For Each name As String In assembly.GetManifestResourceNames()
            ' Ignore resources that don't share the images prefix
            If Not name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase) Then
                Continue For
            End If

            ' Trim the prefix off of the name
            names.Add(name.Substring(prefix.Length, name.Length - prefix.Length))
        Next
        Return names.ToArray()
    End Function

    Private Shared Function GetBookImagesNames(path As String) As IEnumerable(Of String)
        Dim names As New List(Of String)
        Dim assembly As Assembly = GetType(SharedResources).Assembly

        'Dim ServiceClient = New Ser


        Return names.ToArray()
    End Function

End Class