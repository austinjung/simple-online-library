﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.Collections.ObjectModel
Imports System.ComponentModel

Public Class EditCategoryCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        Dim categories As List(Of Category) = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is StaticAutoCompleteViewLoader(Of Category) Then
                categories = CType(parameter, StaticAutoCompleteViewLoader(Of Category)).Source
                Dim copiedCategories As New List(Of Category)
                For Each category As Category In categories
                    Dim copiedCategory = New Category With {
                                        .Category_id = category.Category_id,
                                        .Category_Name = category.Category_Name,
                                        .IsActive = category.IsActive,
                                        .IsChecked = category.IsChecked
                                        }
                    copiedCategories.Add(copiedCategory)
                Next
                vmLocator.Categories = copiedCategories
            ElseIf IsNothing(parameter) Then
                Dim categoryName As String = String.Empty
                If Not String.IsNullOrEmpty(vmLocator.EditBookVM.Book_Categories_Loader.OriginalFilter) Then
                    categoryName = vmLocator.EditBookVM.Book_Categories_Loader.OriginalFilter.Trim
                End If
                Dim category = New Category() With {
                               .Category_Name = categoryName,
                               .IsActive = True,
                               .IsChecked = True
                               }
                vmLocator.EditCategory = category
            End If
            If Not IsNothing(vmLocator.EditCategory) Or Not IsNothing(vmLocator.Categories) Then
                Dim editCategoryDialog As EditCategoryChildWindow = New EditCategoryChildWindow()
                If IsNothing(vmLocator.Categories) Then
                    editCategoryDialog.Title = "New Category"
                End If
                editCategoryDialog.Show()
            End If
        End If
        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class