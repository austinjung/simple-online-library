﻿Imports System.Windows.Controls.Theming
Imports Microsoft.Windows.Data.DomainServices

Public Class EditUserInformationCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim editUserInformationWindow As New EditUserInformationChildWindow()
        editUserInformationWindow.Show()

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class