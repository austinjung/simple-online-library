﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.Collections.ObjectModel
Imports System.ComponentModel

Public Class EditShelfCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        Dim shelves As List(Of Shelf) = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is StaticAutoCompleteViewLoader(Of Shelf) Then
                shelves = CType(parameter, StaticAutoCompleteViewLoader(Of Shelf)).Source
                Dim copiedShelves As New List(Of Shelf)
                For Each shelf As Shelf In shelves
                    Dim copiedShelf = New Shelf With {
                                        .Shelf_id = shelf.Shelf_id,
                                        .Shelf_Code = shelf.Shelf_Code,
                                        .Shelf_Description = shelf.Shelf_Description,
                                        .IsActive = shelf.IsActive,
                                        .IsChecked = shelf.IsChecked
                                        }
                    copiedShelves.Add(copiedShelf)
                Next
                vmLocator.Shelves = copiedShelves
            ElseIf IsNothing(parameter) Then
                Dim selfCodeName As String = String.Empty
                If Not String.IsNullOrEmpty(vmLocator.EditBookVM.Book_Shelves_Loader.OriginalFilter) Then
                    selfCodeName = vmLocator.EditBookVM.Book_Shelves_Loader.OriginalFilter.Trim
                End If
                Dim shelf = New Shelf() With {
                               .Shelf_Code = selfCodeName,
                               .Shelf_Description = "",
                               .IsActive = True,
                               .IsChecked = True
                               }
                vmLocator.EditShelf = shelf
            End If
            If Not IsNothing(vmLocator.EditShelf) Or Not IsNothing(vmLocator.Shelves) Then
                Dim editShelfDialog As EditShelfChildWindow = New EditShelfChildWindow()
                If IsNothing(vmLocator.Shelves) Then
                    editShelfDialog.Title = "New Shelf"
                End If
                editShelfDialog.Show()
            End If
        End If
        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class