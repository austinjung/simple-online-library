﻿Imports System.Windows.Controls.Theming
Imports Microsoft.Windows.Data.DomainServices

Public Class NavigateToReviewCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        If TypeOf (parameter) Is UserDashboardViewModel Then
            Dim count = CType(parameter, UserDashboardViewModel).UnreviewdReturnedBookInPastTwoMonths
            If count < 1 Then
                Return False
            Else
                Return True
            End If
        Else
            Return False
        End If
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim busyIndicator As BusyIndicator = DirectCast(Application.Current.RootVisual, FrameworkElement)
        Dim mainPage As MainPage = DirectCast(busyIndicator.Content, MainPage)
        mainPage.ContentFrame.Navigate(New Uri("/ReviewBooks", UriKind.Relative))

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class