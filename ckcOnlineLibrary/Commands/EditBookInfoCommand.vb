﻿Imports System.Windows.Controls.Theming
Imports Microsoft.Windows.Data.DomainServices

Public Class EditBookInfoCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If IsNothing(parameter) Then
                vmLocator.EditBook = New Book With {
                    .Book_id = 0,
                    .Book_Media = "Paperback",
                    .Book_Language = "English",
                    .Book_Quantity_Avail = 0,
                    .Book_Quantity_Onhand = 0,
                    .IsActive = True
                    }
            ElseIf TypeOf (parameter) Is DomainCollectionView(Of Book) Then
                vmLocator.EditBook = CType(parameter, DomainCollectionView(Of Book)).CurrentItem
            End If
            Dim editBookInfoView = New EditBookChildWindow()
            editBookInfoView.Show()
        End If
        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class