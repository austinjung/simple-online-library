﻿Imports System.Windows.Controls.Theming
Imports Microsoft.Windows.Data.DomainServices

Public Class ViewBookDetailCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is DomainCollectionView(Of Book) Then
                vmLocator.BookDetailID = CType(CType(parameter, DomainCollectionView(Of Book)).CurrentItem, Book).Book_id
            ElseIf TypeOf (parameter) Is DomainCollectionView(Of Books_By_Requests) Then
                vmLocator.BookDetailID = CType(CType(parameter, DomainCollectionView(Of Books_By_Requests)).CurrentItem, Books_By_Requests).Book_id
            ElseIf TypeOf (parameter) Is DomainCollectionView(Of Books_By_Reviews) Then
                vmLocator.BookDetailID = CType(CType(parameter, DomainCollectionView(Of Books_By_Reviews)).CurrentItem, Books_By_Reviews).Book_id
            ElseIf TypeOf (parameter) Is DomainCollectionView(Of Member_Requests) Then
                vmLocator.BookDetailID = CType(CType(parameter, DomainCollectionView(Of Member_Requests)).CurrentItem, Member_Requests).Book_id
            ElseIf TypeOf (parameter) Is DomainCollectionView(Of Member_Reviews) Then
                vmLocator.BookDetailID = CType(CType(parameter, DomainCollectionView(Of Member_Reviews)).CurrentItem, Member_Reviews).Book_id
            ElseIf TypeOf (parameter) Is DomainCollectionView(Of Transactions_Detail) Then
                vmLocator.BookDetailID = CType(CType(parameter, DomainCollectionView(Of Transactions_Detail)).CurrentItem, Transactions_Detail).Book_id
            End If
            Dim bookDetailView = New ViewBookDetailChildWindow()
            bookDetailView.Show()
        End If
        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class