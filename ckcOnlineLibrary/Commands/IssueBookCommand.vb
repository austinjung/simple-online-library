﻿Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging


Public Class IssueBookCommand
    Implements ICommand

    Private issueRequest As Member_Requests
    Private alreadyIssued As Boolean
    Private pendingRequests As DomainCollectionView(Of Member_Requests)
    Private vmLocator As ViewModelLocator = Nothing
    Private domainContext As ckcLibraryDomainContext
    Private myMessageBox As MyMessageBox
    Private comittedDateString As String

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        alreadyIssued = False
        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is DomainCollectionView(Of Member_Requests) Then
                pendingRequests = CType(parameter, DomainCollectionView(Of Member_Requests))
                issueRequest = pendingRequests.CurrentItem
                domainContext = vmLocator.DomainContext
                If IsNothing(issueRequest.Comitted_Date) Then
                    comittedDateString = "01/01/0001"
                Else
                    comittedDateString = CType(issueRequest.Comitted_Date, Date).ToString("MM/dd/yyyy")
                End If
                If issueRequest.Comment Is Nothing Then
                    issueRequest.Comment = String.Empty
                End If
                If issueRequest.Request_Status Then
                    alreadyIssued = True
                Else
                    Messenger.[Default].Register(Of ConfirmDialogMessage)(Me, AddressOf ProcessIssueBook)
                    If (issueRequest.Comitted_Date > Date.Today) Then
                        Dim alertMsg As String = "[" & issueRequest.Book.Book_Title & "] is scheduled to be issued later."
                        myMessageBox = New MyMessageBox("Warning", "Issue early", alertMsg, MessageBoxButton.OKCancel)
                    ElseIf (issueRequest.Comitted_Date < Date.Today) Then
                        Dim alertMsg As String = "[" & issueRequest.Book.Book_Title & "] is scheduled to be issued earlier."
                        myMessageBox = New MyMessageBox("Warning", "Issue late", alertMsg, MessageBoxButton.OKCancel)
                    Else
                        Dim alertMsg As String = "[" & issueRequest.Book.Book_Title & "] will be issued to " & issueRequest.Member.FullName & "."
                        myMessageBox = New MyMessageBox("Inform", "Issue book", alertMsg, MessageBoxButton.OKCancel)
                    End If
                    myMessageBox.Show()
                End If
            End If
        End If

        If alreadyIssued Then
            Dim dialogMessage As IssueBookResultMessage
            Dim alertMsg As String = "[" & issueRequest.Book.Book_Title & "] was issued already."
            dialogMessage = New IssueBookResultMessage("Inform", "Issued already", alertMsg)
            Messenger.[Default].Send(dialogMessage)
        End If

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

    Private Sub ProcessIssueBook(msg As ConfirmDialogMessage)

        If msg.Caption.Equals("DialogResultOK") Then
            If comittedDateString.Equals("01/01/0001") Then
                vmLocator.LibraryDataService.DomainContext.Allocate_Book(Me.issueRequest.Book, AddressOf Me.AfterAllocateBook, Nothing)
                issueRequest.Comitted_Date = Date.Today
            End If
            issueRequest.Comment = "Issued at " & Date.Now.ToString("MM/dd/yyyy") & " " & Date.Now.ToShortTimeString() & vbCrLf _
                                    & issueRequest.Comment
            issueRequest.Request_Status = True
            issueRequest.Modified_Date = DateTime.Now
            vmLocator.LibraryDataService.SubmitChanges(AddressOf Me.AfterIssueBook, Nothing)

            domainContext.ProcessIssueBook(issueRequest.Member.Email, issueRequest.Book.Book_Title, _
                                              issueRequest.Request_id, AddressOf ProcessIssueBookCallBack, Nothing)
        End If
        Messenger.[Default].Unregister(Of ConfirmDialogMessage)(Me, AddressOf ProcessIssueBook)

    End Sub

    Private Sub ProcessIssueBookCallBack(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of String))
        If result.HasError Or result.Value.Contains("Fail") Then
            Dim dialogMessage As IssueBookResultMessage
            dialogMessage = New IssueBookResultMessage("Error", "Process receive error.", result.Value)
            Messenger.[Default].Send(dialogMessage)
        End If
    End Sub

    Private Sub AfterIssueBook(result As ServiceSubmitChangesResult)
        Dim dialogMessage As IssueBookResultMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim modifiedIssuedRequest As Member_Requests

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member_Requests Then
                        modifiedIssuedRequest = CType(e, Member_Requests)
                        If issueRequest.Request_id = modifiedIssuedRequest.Request_id Then
                            resultMessage = "[" & issueRequest.Book.Book_Title & "] is issued to " & issueRequest.Member.FullName & " on " & Date.Now.ToString("MM/dd/yyyy")
                        End If
                    End If
                Next
            End If
        End If

        dialogMessage = New IssueBookResultMessage(dialogType, action, resultMessage)
        Messenger.[Default].Send(dialogMessage)

    End Sub

    Private Sub AfterAllocateBook(result As ServiceModel.DomainServices.Client.InvokeOperation(Of Book))

        If (result.[Error] Is Nothing) Then
            If Not IsNothing(result.Value) Then
                issueRequest.Book = result.Value
            End If
        End If

    End Sub

End Class