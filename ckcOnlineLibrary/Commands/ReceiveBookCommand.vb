﻿Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging


Public Class ReceiveBookCommand
    Implements ICommand

    Private receiveTransaction As Member_Transactions
    Private alreadyReceived As Boolean
    Private memberTransactions As DomainCollectionView(Of Member_Transactions)

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        alreadyReceived = False
        Dim returnedDatestring As String = String.Empty
        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is DomainCollectionView(Of Member_Transactions) Then
                memberTransactions = CType(parameter, DomainCollectionView(Of Member_Transactions))
                Dim domainContext As ckcLibraryDomainContext = vmLocator.DomainContext
                receiveTransaction = memberTransactions.CurrentItem
                If IsNothing(receiveTransaction.Returned_Date) Then
                    returnedDatestring = "01/01/0001"
                Else
                    returnedDatestring = CType(receiveTransaction.Returned_Date, Date).ToString("MM/dd/yyyy")
                End If
                If returnedDatestring.Equals("01/01/0001") Then
                    receiveTransaction.Returned_Date = Date.Now
                    If receiveTransaction.Comment Is Nothing Then
                        receiveTransaction.Comment = String.Empty
                    End If
                    If receiveTransaction.Comment.Contains(DateTime.Now.ToString("MM/dd/yyyy")) _
                        And receiveTransaction.Comment.Contains("Thanks for your return.") Then
                        alreadyReceived = True
                    Else
                        receiveTransaction.Comment = "Thanks for your return. - " & Date.Now.ToString("MM/dd/yyyy") & vbCrLf _
                                                     & receiveTransaction.Comment
                        vmLocator.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveReceive, Nothing)

                        domainContext.ProcessReceiveBook(receiveTransaction.Member.Email, receiveTransaction.Book.Book_Title, _
                                                          receiveTransaction.Book_id, AddressOf ProcessReceiveBookCallBack, Nothing)
                    End If
                Else
                    alreadyReceived = True
                End If
            End If
        End If

        If alreadyReceived Then
            Dim dialogMessage As ReceiveBookResultMessage
            Dim alertMsg As String = receiveTransaction.Member.FullName & " returned " & receiveTransaction.Book.Book_Title & " on " & returnedDatestring & vbCrLf
            dialogMessage = New ReceiveBookResultMessage("Inform", "Received already", alertMsg)
            Messenger.[Default].Send(dialogMessage)
        End If

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

    Private Sub ProcessReceiveBookCallBack(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of String))
        If result.HasError Or result.Value.Contains("Fail") Then
            Dim dialogMessage As ReceiveBookResultMessage
            dialogMessage = New ReceiveBookResultMessage("Error", "Process receive error.", result.Value)
            Messenger.[Default].Send(dialogMessage)
        End If
    End Sub

    Private Sub AfterSaveReceive(result As ServiceSubmitChangesResult)
        Dim dialogMessage As ReceiveBookResultMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim modifiedReceivedTransaction As Member_Transactions

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member_Transactions Then
                        modifiedReceivedTransaction = CType(e, Member_Transactions)
                        If receiveTransaction.Transaction_id = modifiedReceivedTransaction.Transaction_id Then
                            resultMessage = receiveTransaction.Member.FullName & " returned " & receiveTransaction.Book.Book_Title & " on " & Date.Now.ToString("MM/dd/yyyy")
                        End If
                    End If
                Next
            End If
        End If

        dialogMessage = New ReceiveBookResultMessage(dialogType, action, resultMessage)
        Messenger.[Default].Send(dialogMessage)
    End Sub

End Class