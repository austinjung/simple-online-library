﻿Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging
Imports System.Text.RegularExpressions


Public Class ReadCommentOnTransactionCommand
    Implements ICommand

    Private memberTransaction As Member_Transactions

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        If TypeOf (parameter) Is Member_Transactions Then
            memberTransaction = CType(parameter, Member_Transactions)
            If WebContext.Current.User.Roles.Contains("User") Then
                If memberTransaction.AdminCommentViewed Then
                    Return False
                ElseIf IsNothing(memberTransaction.AdminCommented) Then
                    Return False
                Else
                    Return True
                End If
            Else
                If memberTransaction.UserCommentViewed Then
                    Return False
                ElseIf IsNothing(memberTransaction.UserCommented) Then
                    Return False
                Else
                    Return True
                End If
            End If
        Else
            Return False
        End If
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is Member_Transactions Then
                memberTransaction = CType(parameter, Member_Transactions)
                Dim quote As Char = ChrW(34)
                Dim regexString As String = _
                    "[a-zA-Z0-9" & quote.ToString & "_:;'< >,?/`~{}()+=@#$%^&*\.\\\!\[\]\-\w\n\r\t]+"
                Dim regex As Regex
                Dim match As Match
                If WebContext.Current.User.Roles.Contains("User") Then
                    regex = New Regex("Admin" & regexString & CType(memberTransaction.AdminCommented, Date).ToString("MM/dd/yyyy") & "\r\n")
                    match = regex.Match(memberTransaction.Comment)
                    memberTransaction.AdminCommentViewed = True
                Else
                    regex = New Regex(memberTransaction.Member.MemberLogin & regexString & CType(memberTransaction.UserCommented, Date).ToString("MM/dd/yyyy") & "\r\n")
                    match = regex.Match(memberTransaction.Comment)
                    memberTransaction.UserCommentViewed = True
                End If
                vmLocator.ReadCommentTransaction = memberTransaction
                Dim readCommentMessageBox As ReadCommentMessageBox
                If match.Success Then
                    readCommentMessageBox = New ReadCommentMessageBox(match.Value, Visibility.Visible, _
                                                                      GetType(Member_Transactions), memberTransaction.Transaction_id)
                Else
                    readCommentMessageBox = New ReadCommentMessageBox(memberTransaction.Comment, Visibility.Visible, _
                                                                      GetType(Member_Transactions), memberTransaction.Transaction_id)
                End If
                readCommentMessageBox.Show()
            End If
        End If

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class