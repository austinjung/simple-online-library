﻿Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging


Public Class SendOverDueNoticeCommand
    Implements ICommand

    Private overDueTransaction As Member_Transactions
    Dim isOverDue As Boolean = False
    Dim isAlreadyReturned As Boolean = False
    Dim notOverDueMsg As String

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        If TypeOf (parameter) Is Member_Transactions Then
            overDueTransaction = CType(parameter, Member_Transactions)
            Return CheckOverDue()
        Else
            overDueTransaction = Nothing
            isOverDue = False
            Return False
        End If

    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is Member_Transactions Then
                Dim domainContext As ckcLibraryDomainContext = vmLocator.DomainContext
                CheckOverDue()
                If isOverDue Then
                    Dim alertMsg As String = "Your returning [" & overDueTransaction.Book.Book_Title & "] is overdue." _
                                 & vbCrLf & "    Please return as soon as possible. - " & Today.ToString("MM/dd/yyyy") & vbCrLf
                    overDueTransaction.Comment = "Admin : " & alertMsg & overDueTransaction.Comment
                    overDueTransaction.AdminCommented = Now()
                    overDueTransaction.AdminCommentViewed = False
                    vmLocator.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveOverDueComment, Nothing)

                    If String.IsNullOrEmpty(Me.overDueTransaction.Member.Email) Then
                        Dim emailDialogMessage = New SendEmailResultMessage("Inform", "Sending email", Me.overDueTransaction.Member.FullName & " has no email.")
                        Dim myMessageBox = New MyMessageBox(emailDialogMessage.Type, emailDialogMessage.Content, emailDialogMessage.Caption, emailDialogMessage.Button)
                        myMessageBox.Show()
                    Else
                        domainContext.SendMailFromAdmin(overDueTransaction.Member.Email, "Overdue Book Alert", alertMsg, _
                                                                "[" & overDueTransaction.Book.Book_Title & "] is overdue.", _
                                                                AddressOf SendMailCallBack, Nothing)
                    End If
                End If
            End If
        End If

        If isOverDue = False Then
            Dim dialogMessage As SendEmailResultMessage
            dialogMessage = New SendEmailResultMessage("Inform", "Sending overdue email", notOverDueMsg)
            Dim myMessageBox = New MyMessageBox(dialogMessage.Type, dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button)
            myMessageBox.Show()
        End If

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

    Private Function CheckOverDue() As Boolean
        Dim returnedDatestring As String
        If IsNothing(overDueTransaction.Returned_Date) Then
            returnedDatestring = "01/01/0001"
        Else
            returnedDatestring = CType(overDueTransaction.Returned_Date, Date).ToString("MM/dd/yyyy")
        End If
        If returnedDatestring.Equals("01/01/0001") Then
            isAlreadyReturned = False

            If overDueTransaction.Expected_Return < Today() Then
                Dim alertMsg As String = " is overdue." _
                                 & vbCrLf & "    Please return as soon as possible. - " & Today.ToString("MM/dd/yyyy") & vbCrLf
                If overDueTransaction.Comment.Contains(alertMsg) Then
                    notOverDueMsg = "Today's overdue alert email was sent already!"
                    isOverDue = False
                Else
                    isOverDue = True
                End If
            Else
                notOverDueMsg = "This transaction is not overdue yet!"
                isOverDue = False
            End If
        Else
            notOverDueMsg = "This book is returned already!"
            isAlreadyReturned = True
            isOverDue = False
        End If

        Return isOverDue
    End Function

    Private Sub SendMailCallBack(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of String))
        Dim dialogMessage As SendEmailResultMessage
        If result.Value.Contains("Fail") Then
            dialogMessage = New SendEmailResultMessage("Error", "Sending email", result.Value)
        Else
            dialogMessage = New SendEmailResultMessage("Success", "Sending email", result.Value)
        End If
        Dim myMessageBox = New MyMessageBox(dialogMessage.Type, dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button)
        myMessageBox.Show()
    End Sub

    Private Sub AfterSaveOverDueComment(result As ServiceSubmitChangesResult)
        Dim dialogMessage As SavedOverDueTransactionMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim modifiedOverDueTransaction As Member_Transactions

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member_Transactions Then
                        modifiedOverDueTransaction = CType(e, Member_Transactions)
                        If overDueTransaction.Transaction_id = modifiedOverDueTransaction.Transaction_id Then
                            resultMessage &= "Alert to return " & CType(e, Member_Transactions).Book.Book_Title & " was added successfully." & vbCrLf
                        End If
                    End If
                Next
            End If
        End If

        dialogMessage = New SavedOverDueTransactionMessage(dialogType, action, resultMessage, overDueTransaction)
        'Messenger.[Default].Send(dialogMessage)
        Dim myMessageBox = New MyMessageBox(dialogMessage.Type, dialogMessage.Content, dialogMessage.Caption, dialogMessage.Button)
        myMessageBox.Show()
    End Sub

End Class