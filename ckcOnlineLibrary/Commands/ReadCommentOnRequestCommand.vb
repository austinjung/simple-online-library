﻿Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging
Imports System.Text.RegularExpressions


Public Class ReadCommentOnRequestCommand
    Implements ICommand

    Private memberRequest As Member_Requests

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        If TypeOf (parameter) Is Member_Requests Then
            memberRequest = CType(parameter, Member_Requests)
            If WebContext.Current.User.Roles.Contains("User") Then
                If memberRequest.AdminCommentViewed Then
                    Return False
                ElseIf IsNothing(memberRequest.AdminCommented) Then
                    Return False
                Else
                    Return True
                End If
            Else
                If memberRequest.UserCommentViewed Then
                    Return False
                ElseIf IsNothing(memberRequest.UserCommented) Then
                    Return False
                Else
                    Return True
                End If
            End If
        Else
            Return False
        End If
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is Member_Requests Then
                memberRequest = CType(parameter, Member_Requests)
                Dim quote As Char = ChrW(34)
                Dim regexString As String = _
                    "[a-zA-Z0-9" & quote.ToString & "_:;'< >,?/`~{}()+=@#$%^&*\.\\\!\[\]\-\w\n\r\t]+"
                Dim regex As Regex
                Dim match As Match
                If WebContext.Current.User.Roles.Contains("User") Then
                    regex = New Regex("Admin" & regexString & CType(memberRequest.AdminCommented, Date).ToString("MM/dd/yyyy") & "\r\n")
                    match = regex.Match(memberRequest.Comment)
                    memberRequest.AdminCommentViewed = True
                Else
                    regex = New Regex(memberRequest.Member.MemberLogin & regexString & CType(memberRequest.UserCommented, Date).ToString("MM/dd/yyyy") & "\r\n")
                    match = regex.Match(memberRequest.Comment)
                    memberRequest.UserCommentViewed = True
                End If
                vmLocator.LibraryDataService.SubmitChanges(AddressOf Me.AfterResetCommentViewFlag, Nothing)

                Dim readCommentMessageBox As ReadCommentMessageBox
                vmLocator.BookDetailID = memberRequest.Book_id
                vmLocator.MemberID = memberRequest.MemberID
                If match.Success Then
                    readCommentMessageBox = New ReadCommentMessageBox(match.Value, Visibility.Visible, _
                                                                      GetType(Member_Requests), memberRequest.Request_id)
                Else
                    readCommentMessageBox = New ReadCommentMessageBox(memberRequest.Comment, Visibility.Visible, _
                                                                      GetType(Member_Requests), memberRequest.Request_id)
                End If
                readCommentMessageBox.Show()
            End If
        End If

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

    Private Sub AfterResetCommentViewFlag(result As ServiceSubmitChangesResult)

        If (result.[Error] IsNot Nothing) Then
            Dim myMessageBox = New MyMessageBox("Error", "Mark as read", "Error in marking as read.", MessageBoxButton.OK)
            myMessageBox.Show()
        End If

    End Sub

End Class