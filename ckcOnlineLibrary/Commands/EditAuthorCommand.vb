﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.Collections.ObjectModel
Imports System.ComponentModel

Public Class EditAuthorCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        Dim authors As List(Of Author) = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is StaticAutoCompleteViewLoader(Of Author) Then
                authors = CType(parameter, StaticAutoCompleteViewLoader(Of Author)).Source
                Dim copiedAuthors As New List(Of Author)
                For Each author As Author In authors
                    Dim copiedAuthor = New Author With {
                                        .Author_id = author.Author_id,
                                        .Author_First_Name = author.Author_First_Name,
                                        .Author_Last_Name = author.Author_Last_Name,
                                        .IsActive = author.IsActive,
                                        .IsChecked = author.IsChecked
                                        }
                    copiedAuthors.Add(copiedAuthor)
                Next
                vmLocator.Authors = copiedAuthors
            ElseIf IsNothing(parameter) Then
                Dim filter As String = String.Empty
                If Not String.IsNullOrEmpty(vmLocator.EditBookVM.Book_Authors_Loader.OriginalFilter) Then
                    filter = vmLocator.EditBookVM.Book_Authors_Loader.OriginalFilter.Trim()
                End If
                Dim firstName As String = String.Empty
                Dim lastName As String = String.Empty
                If Not String.IsNullOrEmpty(filter) Then
                    Dim seperators = (" ")
                    Dim nameSegments = filter.Split(" ")
                    If nameSegments.Length < 2 Then
                        firstName = filter
                    Else
                        lastName = nameSegments(nameSegments.Length - 1)
                        firstName = filter.Remove(filter.Length - lastName.Length - 1, lastName.Length).Trim
                    End If
                End If
                Dim author = New Author() With {
                            .Author_First_Name = firstName,
                            .Author_Last_Name = lastName,
                            .IsActive = True,
                            .IsChecked = True
                            }
                vmLocator.EditAuthor = author
            End If
            If Not IsNothing(vmLocator.EditAuthor) Or Not IsNothing(vmLocator.Authors) Then
                Dim editAuthorDialog As EditAuthorChildWindow = New EditAuthorChildWindow()
                If IsNothing(vmLocator.Authors) Then
                    editAuthorDialog.Title = "New Author"
                End If
                editAuthorDialog.Show()
            End If
        End If
        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class