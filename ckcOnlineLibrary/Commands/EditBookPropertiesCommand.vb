﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.Collections.ObjectModel

Public Class EditBookPropertiesCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        If TypeOf (parameter) Is EditBookViewModel Then
            Dim editVM = CType(parameter, EditBookViewModel)
            Dim dataGrid As DataGrid = editVM.WorkingDataGrid
            If dataGrid.Name.Contains("Author") Then
                Dim authors As DomainCollectionView(Of Author) = editVM.Book_Authors
                Dim author = New Author() With {
                    .Author_First_Name = "Austin",
                    .Author_Last_Name = "Jung",
                    .IsActive = True
                    }
                If CType(authors.CollectionViewLoader, StaticAutoCompleteViewLoader(Of Author)).AddAuthor(author, True) Then
                    editVM.EditBook.Book_Authors.Add(author)
                    Dim orderedSelectedAuthors = From a In editVM.EditBook.Book_Authors
                                                 Order By a.Author_Full_Name
                                                 Select a

                    Dim newSelectedAuthor As ObservableCollection(Of Author) = New ObservableCollection(Of Author)
                    For Each a In orderedSelectedAuthors
                        newSelectedAuthor.Add(a)
                    Next
                    editVM.EditBook.Book_Authors = newSelectedAuthor
                End If
            End If
        End If
        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class