﻿Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging
Imports System.Text.RegularExpressions


Public Class MakeCommentOnTransactionCommand
    Implements ICommand

    Private memberTransaction As Member_Transactions

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute

        Return True

    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is Member_Transactions Then
                memberTransaction = CType(parameter, Member_Transactions)
                If WebContext.Current.User.Roles.Contains("User") Then
                    memberTransaction.AdminCommentViewed = True
                Else
                    memberTransaction.UserCommentViewed = True
                End If
                vmLocator.LibraryDataService.SubmitChanges(AddressOf Me.AfterResetCommentViewFlag, Nothing)
                vmLocator.ReadCommentTransaction = memberTransaction
                Dim commentChildWindow As CommentOnTransactionChildWindow = New CommentOnTransactionChildWindow()
                commentChildWindow.Show()
            End If
        End If

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

    Private Sub AfterResetCommentViewFlag(result As ServiceSubmitChangesResult)

        If (result.[Error] IsNot Nothing) Then
            Dim myMessageBox = New MyMessageBox("Error", "Mark as read", "Error in marking as read.", MessageBoxButton.OK)
            myMessageBox.Show()
        End If

    End Sub

End Class