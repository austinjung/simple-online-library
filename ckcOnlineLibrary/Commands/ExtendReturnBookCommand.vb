﻿Imports Microsoft.Windows.Data.DomainServices
Imports Ria.Common
Imports GalaSoft.MvvmLight.Messaging


Public Class ExtendReturnBookCommand
    Implements ICommand

    Private memberTransactions As DomainCollectionView(Of Member_Transactions)
    Private extendTransaction As Member_Transactions
    Private alreadyReceived As Boolean = False
    Private extendAvailable As Boolean = False
    Private returnedDatestring As String
    Private issuedDatestring As String
    Private expectedReturnedDatestring As String

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        If TypeOf (parameter) Is Member_Transactions Then
            Dim transaction = CType(parameter, Member_Transactions)
            If IsNothing(extendTransaction) Then
                VerifyExtendAvailable(parameter)
            ElseIf ((Not extendTransaction.Transaction_id.Equals(transaction.Transaction_id)) _
                 Or (Not extendTransaction.Returned_Date.Equals(transaction.Returned_Date)) _
                 Or (Not extendTransaction.Issued_Date.Equals(transaction.Issued_Date)) _
                 Or (Not extendTransaction.Expected_Return.Equals(transaction.Expected_Return))) Then
                VerifyExtendAvailable(parameter)
            End If
            Return extendAvailable
        Else
            Return False
        End If
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        If Not (TypeOf (parameter) Is Member_Transactions) Then
            Return
        End If

        Dim transaction = CType(parameter, Member_Transactions)
        If IsNothing(extendTransaction) Then
            VerifyExtendAvailable(parameter)
        ElseIf ((Not extendTransaction.Transaction_id.Equals(transaction.Transaction_id)) _
             Or (Not extendTransaction.Returned_Date.Equals(transaction.Returned_Date)) _
             Or (Not extendTransaction.Issued_Date.Equals(transaction.Issued_Date)) _
             Or (Not extendTransaction.Expected_Return.Equals(transaction.Expected_Return))) Then
            VerifyExtendAvailable(parameter)
        End If

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            Dim newExtendedReturnDueDate As Date = CType(extendTransaction.Expected_Return, Date).AddDays(14)
            extendTransaction.Expected_Return = newExtendedReturnDueDate
            If extendTransaction.Comment Is Nothing Then
                extendTransaction.Comment = String.Empty
            End If

            Dim newComment = WebContext.Current.User.MemberLogin & " : extended the due date to " & newExtendedReturnDueDate.ToString("MM/dd/yyyy") & vbCrLf
            extendTransaction.Comment = newComment & " - " & Today.ToString("MM/dd/yyyy") & vbCrLf + extendTransaction.Comment
            extendTransaction.UserCommented = Now
            extendTransaction.UserCommentViewed = False
            vmLocator.LibraryDataService.SubmitChanges(AddressOf Me.AfterSaveExtendReturn, Nothing)
            If Not String.IsNullOrEmpty(extendTransaction.Member.Email) Then
                vmLocator.LibraryDataService.DomainContext.ProcessExtendReturnBook(extendTransaction.Member.Email, extendTransaction.Book.Book_Title, _
                                                      newExtendedReturnDueDate.ToString("MM/dd/yyyy"), AddressOf ProcessExtendReturnBookCallBack, Nothing)
            End If
        End If

        Dim dialogMessage As ExtendReturnBookResultMessage
        If alreadyReceived Then
            Dim alertMsg As String = extendTransaction.Member.FullName & " returned [" & extendTransaction.Book.Book_Title & "] on " & returnedDatestring & vbCrLf
            dialogMessage = New ExtendReturnBookResultMessage("Inform", "Received already", alertMsg)
            Messenger.[Default].Send(dialogMessage)
        ElseIf Not extendAvailable Then
            Dim alertMsg As String = "You can't extend the return date of [" & extendTransaction.Book.Book_Title & "]"
            dialogMessage = New ExtendReturnBookResultMessage("Inform", "Can't extend", alertMsg)
            Messenger.[Default].Send(dialogMessage)
        End If

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

    Private Sub VerifyExtendAvailable(parameter As Object)
        extendTransaction = CType(parameter, Member_Transactions)
        If IsNothing(extendTransaction.Returned_Date) Then
            returnedDatestring = "01/01/0001"
        Else
            returnedDatestring = CType(extendTransaction.Returned_Date, Date).ToString("MM/dd/yyyy")
        End If
        If IsNothing(extendTransaction.Issued_Date) Then
            issuedDatestring = "01/01/0001"
        Else
            issuedDatestring = CType(extendTransaction.Issued_Date, Date).ToString("MM/dd/yyyy")
        End If
        If IsNothing(extendTransaction.Expected_Return) Then
            expectedReturnedDatestring = "01/01/0001"
        Else
            expectedReturnedDatestring = CType(extendTransaction.Expected_Return, Date).ToString("MM/dd/yyyy")
        End If
        If returnedDatestring.Equals("01/01/0001") Then
            If issuedDatestring.Equals("01/01/0001") Or expectedReturnedDatestring.Equals("01/01/0001") Then
                extendAvailable = False
            Else
                Dim maxExtendedReturnDueDate As Date = CType(extendTransaction.Issued_Date, Date).AddDays(2 * 14)
                Dim newExtendedReturnDueDate As Date = CType(extendTransaction.Expected_Return, Date).AddDays(14)
                If newExtendedReturnDueDate > maxExtendedReturnDueDate Then
                    extendAvailable = False
                Else
                    extendAvailable = True
                End If
            End If
        Else
            alreadyReceived = True
            extendAvailable = False
        End If
    End Sub

    Private Sub ProcessExtendReturnBookCallBack(ByVal result As ServiceModel.DomainServices.Client.InvokeOperation(Of String))
        If result.HasError Or result.Value.Contains("Fail") Then
            Dim dialogMessage As ExtendReturnBookResultMessage
            dialogMessage = New ExtendReturnBookResultMessage("Error", "Process receive error.", result.Value)
            Messenger.[Default].Send(dialogMessage)
        End If
    End Sub

    Private Sub AfterSaveExtendReturn(result As ServiceSubmitChangesResult)
        Dim dialogMessage As ExtendReturnBookResultMessage
        Dim dialogType As String
        Dim action As String
        Dim resultMessage As String
        Dim modifiedExtendTransaction As Member_Transactions

        If (result.[Error] IsNot Nothing) Then
            dialogType = "Error"
            action = "Save"
            resultMessage = "Save was unsuccessful."
        Else
            dialogType = "Success"
            resultMessage = ""
            action = "Update"
            If result.ChangeSet.ModifiedEntities.Count > 0 Then
                action = "Update"
                For Each e As Object In result.ChangeSet.ModifiedEntities
                    If TypeOf (e) Is Member_Transactions Then
                        modifiedExtendTransaction = CType(e, Member_Transactions)
                        If extendTransaction.Transaction_id = modifiedExtendTransaction.Transaction_id Then
                            resultMessage = "This book's new return due date is " & CType(modifiedExtendTransaction.Expected_Return, Date).ToString("MM/dd/yyyy")
                        End If
                    End If
                Next
            End If
        End If

        dialogMessage = New ExtendReturnBookResultMessage(dialogType, action, resultMessage)
        Messenger.[Default].Send(dialogMessage)
    End Sub

End Class