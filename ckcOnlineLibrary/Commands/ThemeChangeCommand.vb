﻿Imports System.Windows.Controls.Theming

Public Class ThemeChangeCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute
        'Dim themeContainer As Theme =
        '    DirectCast(DirectCast(Application.Current.RootVisual, FrameworkElement).FindName("ThemeContainer"), Theme)
        Dim busyIndicator As BusyIndicator = DirectCast(Application.Current.RootVisual, FrameworkElement)
        Dim mainPage As MainPage = DirectCast(busyIndicator.Content, MainPage)
        Dim themeContainer As Theme =
            DirectCast(mainPage.FindName("ThemeContainer"), Theme)
        Dim themeName As String = TryCast(parameter, String)

        If themeName Is Nothing Then
            themeContainer.ThemeUri = Nothing
        Else
            themeContainer.ThemeUri =
                New Uri(String.Format("/ckcOnlineLibrary;component/Assets/Themes/{0}.xaml", themeName), UriKind.Relative)
        End If

        Theme.SetApplicationThemeUri(App.Current, themeContainer.ThemeUri)

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class