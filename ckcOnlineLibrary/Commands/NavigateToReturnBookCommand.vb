﻿Imports System.Windows.Controls.Theming
Imports Microsoft.Windows.Data.DomainServices

Public Class NavigateToReturnBookCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        Return True
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim busyIndicator As BusyIndicator = DirectCast(Application.Current.RootVisual, FrameworkElement)
        Dim mainPage As MainPage = DirectCast(busyIndicator.Content, MainPage)
        mainPage.ContentFrame.Navigate(New Uri("/Manager/ReturnBookPage", UriKind.Relative))

        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class