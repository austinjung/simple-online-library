﻿Imports System.Windows.Controls.Theming
Imports Microsoft.Windows.Data.DomainServices

Public Class MakeReviewCommand
    Implements ICommand

#Region "ICommand Members"

    Public Function CanExecute(parameter As Object) As Boolean Implements System.Windows.Input.ICommand.CanExecute
        If TypeOf (parameter) Is Transactions_Detail Then
            Dim datePastTwoMonth As Date = Date.Today.AddMonths(-2)
            If CType(parameter, Transactions_Detail).Returned_Date > datePastTwoMonth Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Event CanExecuteChanged As EventHandler Implements System.Windows.Input.ICommand.CanExecuteChanged

    Public Sub Execute(parameter As Object) Implements System.Windows.Input.ICommand.Execute

        Dim vmLocator As ViewModelLocator = Nothing
        For Each resource In App.Current.Resources.MergedDictionaries
            vmLocator = resource.Item("VMLocator")
            If Not IsNothing(vmLocator) Then
                Exit For
            End If
        Next

        If Not IsNothing(vmLocator) Then
            If TypeOf (parameter) Is Transactions_Detail Then
                vmLocator.ReviewTransaction = CType(parameter, Transactions_Detail)
                Dim makeReviewView = New MakeReviewChildWindow()
                makeReviewView.Show()
            End If
        End If
        RaiseEvent CanExecuteChanged(Me, New EventArgs())
    End Sub

#End Region

End Class