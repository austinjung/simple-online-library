﻿Imports System
Imports System.ServiceModel.DomainServices.Client
Imports Microsoft.Windows.Data.DomainServices
Imports Papa.Common
Imports Ria.Common

Public Interface IBookDataService

    ReadOnly Property EntityContainer As EntityContainer

    Sub SubmitChanges(callback As Action(Of ServiceSubmitChangesResult), ByVal state As Object)

    Sub LoadBooksByCategory(ByVal categoryID As Integer, query As QueryBuilder(Of Book),
                            callback As Action(Of ServiceLoadResult), ByVal state As Object)
    Sub LoadBooksOfTheDay(callback As Action(Of ServiceLoadResult), ByVal state As Object)
    Sub LoadAuthors(callback As Action(Of ServiceLoadResult), ByVal state As Object)
    Sub LoadCategories(callback As Action(Of ServiceLoadResult), ByVal state As Object)

    Sub LoadCheckouts(callback As Action(Of ServiceLoadResult), ByVal state As Object)

End Interface
