﻿Imports GalaSoft.MvvmLight.Messaging
Imports Papa.Common

Public Class PageConductor
    Implements IPageConductor

    Protected ReadOnly StateQueue As New StateQueue()
    Private m_RootFrame As Frame
    Protected Property RootFrame() As Frame
        Get
            Return m_RootFrame
        End Get
        Set(value As Frame)
            m_RootFrame = value
        End Set
    End Property

    Public Sub New()
        Messenger.Default.Register(Of FrameMessage)(Me, AddressOf OnReceiveFrameMessage)
    End Sub

    Private Sub OnReceiveFrameMessage(msg As FrameMessage)
        RootFrame = msg.RootFrame
    End Sub

    Public Sub DisplayError(origin As String, e As System.Exception) Implements IPageConductor.DisplayError
        DisplayError(origin, e, String.Empty)
    End Sub

    Public Sub DisplayError(origin As String, e As System.Exception, details As String) Implements IPageConductor.DisplayError
        Dim description As String = String.Format("Error occured in {0}. {1} {2}", origin, details, e.Message)
        Dim notifiableError = New NotifiableError() With { _
                                                          .Description = description, _
                                                          .Title = "Error Occurred" _
                                                         }
        PushState(ViewTokens.ErrorOverlay, notifiableError)
        Messenger.Default.Send(New ErrorMessage() With { _
                                                         .NotifiableError = notifiableError _
                                                        })
    End Sub

    Public Sub GoBack() Implements IPageConductor.GoBack
        RootFrame.GoBack()
    End Sub

    Public Sub GoToCheckoutView(viewToken As String, stateKey As String, book As Book) Implements IPageConductor.GoToCheckoutView
        PushState(stateKey, book)
        Go(FormatViewPath(viewToken, stateKey))
    End Sub

    Public Sub GoToView(viewToken As String) Implements IPageConductor.GoToView
        Go(FormatViewPath(viewToken))
    End Sub

    Public Function PeekState(Of T As Class)(key As String) As T Implements IPageConductor.PeekState
        Return StateQueue.PeekState(Of T)(key)
    End Function

    Public Function PopState(Of T As Class)(key As String) As T Implements IPageConductor.PopState
        Return StateQueue.PopState(Of T)(key)
    End Function

    Public Sub PushState(key As String, value As Object) Implements IPageConductor.PushState
        StateQueue.PushState(key, value)
    End Sub

    Private Sub Go(path As String)
        RootFrame.Navigate(New Uri(path, UriKind.Relative))
    End Sub

    Private Function FormatViewPath(viewToken As String) As String
        Return String.Format("/{0}/{1}.xaml", ViewTokens.Root, viewToken)
    End Function

    Private Function FormatViewPath(viewToken As String, stateKey As String) As String
        Return String.Format("/{0}/{1}.xaml?{2}={3}", ViewTokens.Root, viewToken, Core.StateKeyParameter, stateKey)
    End Function

End Class
