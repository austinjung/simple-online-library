﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common

Public Class StaticAutoCompleteViewLoader(Of TEntity As Entity)
    Inherits CollectionViewLoader

    Private _authorEntityList As List(Of Author)
    Private _categoryEntityList As List(Of Category)
    Private _shelfEntityList As List(Of Shelf)
    Private _type As Type

    Private _authorSelectedList As List(Of Author)
    Private _categorySelectedList As List(Of Category)
    Private _shelfSelectedList As List(Of Shelf)
    Private _source As IEnumerable(Of TEntity)
    Private _collectionViewSource As EntityList(Of TEntity)
    Private _collectionViewListSource As List(Of TEntity)
    Private _originalFilter As String
    Public ReadOnly Property OriginalFilter As String
        Get
            Return _originalFilter
        End Get
    End Property
    Private _filter As String
    Public Property Filter As String
        Get
            Return _filter
        End Get
        Set(value As String)
            _originalFilter = value
            _filter = value.ToLower()
            DoQuery()
            SortBySelected()
            _collectionViewSource.Source = _source
        End Set
    End Property

    Public ReadOnly Property Source As IEnumerable(Of TEntity)
        Get
            Return _source
        End Get
    End Property

    Public ReadOnly Property CollectionViewSource As EntityList(Of TEntity)
        Get
            Return _collectionViewSource
        End Get
    End Property

    Public Property SelectedList As IEnumerable(Of TEntity)
        Get
            If _type Is GetType(Author) Then
                Return _authorSelectedList
            ElseIf _type Is GetType(Category) Then
                Return _categorySelectedList
            ElseIf _type Is GetType(Shelf) Then
                Return _shelfSelectedList
            End If
            Return Nothing
        End Get
        Set(value As IEnumerable(Of TEntity))
            cloneSelectedList(value)
            initializeIsChecked(False, _source)
            'initializeIsChecked(True, _selectedList)
            DoQuery()
            SortBySelected()
            _collectionViewSource.Source = _source
        End Set
    End Property

    Public Sub New(ByRef collectionViewSource As EntityList(Of TEntity), entities As IQueryable(Of TEntity))
        _type = GetType(TEntity)
        cloneEntity(entities)
        _collectionViewSource = collectionViewSource
        DoQuery()
        initializeIsChecked(False, _source)
    End Sub

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Function AddAuthor(author As Author, isSelected As Boolean) As Boolean
        If _type Is GetType(Author) Then
            Dim existAuthor = From a In _authorEntityList
                              Where a.Author_First_Name = author.Author_First_Name _
                                And a.Author_Last_Name = author.Author_Last_Name _
                              Select a
            If existAuthor.Count > 0 Then
                Return False
            End If

            If IsNothing(_authorEntityList) Then
                _authorEntityList = New List(Of Author)
            End If
            Dim clonedAuthor = New Author With {
                                .Author_id = author.Author_id,
                                .Author_First_Name = author.Author_First_Name,
                                .Author_Last_Name = author.Author_Last_Name,
                                .IsActive = author.IsActive,
                                .IsChecked = author.IsChecked
                                }
            _authorEntityList.Add(clonedAuthor)
            Dim tmp = From a In _authorEntityList
                      Order By a.Author_Full_Name
                      Select a

            _authorEntityList = tmp.ToList()
            If isSelected Then
                If IsNothing(_authorSelectedList) Then
                    _authorSelectedList = New List(Of Author)
                End If
                Dim clonedAuthor1 = New Author With {
                                    .Author_id = author.Author_id,
                                    .Author_First_Name = author.Author_First_Name,
                                    .Author_Last_Name = author.Author_Last_Name,
                                    .IsActive = author.IsActive,
                                    .IsChecked = True
                                    }
                _authorSelectedList.Add(clonedAuthor1)
                tmp = From a In _authorSelectedList
                      Order By a.Author_Full_Name
                      Select a

                _authorSelectedList = tmp.ToList()
            End If
            Me.SelectedList = _authorSelectedList
            Return True
        End If
        Return False
    End Function

    Public Function UpdateAuthor(author As Author, isSelected As Boolean) As Boolean
        If _type Is GetType(Author) Then
            Dim existAuthor = (From a In _authorEntityList
                               Where a.Author_id = author.Author_id
                               Select a).FirstOrDefault
            If IsNothing(existAuthor) Then
                Return False
            End If

            If IsNothing(_authorEntityList) Then
                _authorEntityList = New List(Of Author)
            End If

            existAuthor.Author_First_Name = author.Author_First_Name
            existAuthor.Author_Last_Name = author.Author_Last_Name
            existAuthor.Author_Career = author.Author_Career
            existAuthor.IsChecked = author.IsChecked
            existAuthor.IsActive = author.IsActive

            If Not author.IsActive Then
                _authorEntityList.Remove(existAuthor)
            End If

            Dim tmp = From a In _authorEntityList
                      Order By a.Author_Full_Name
                      Select a

            _authorEntityList = tmp.ToList()
            If isSelected Then
                If IsNothing(_authorSelectedList) Then
                    _authorSelectedList = New List(Of Author)
                End If
                Dim existAuthorInSelected = (From a In _authorSelectedList
                                             Where a.Author_id = author.Author_id
                                             Select a).FirstOrDefault
                If IsNothing(existAuthorInSelected) Then
                    Dim clonedAuthor1 = New Author With {
                                        .Author_id = author.Author_id,
                                        .Author_First_Name = author.Author_First_Name,
                                        .Author_Last_Name = author.Author_Last_Name,
                                        .IsActive = author.IsActive,
                                        .IsChecked = True
                                        }
                    _authorSelectedList.Add(clonedAuthor1)
                Else
                    existAuthorInSelected.Author_First_Name = author.Author_First_Name
                    existAuthorInSelected.Author_Last_Name = author.Author_Last_Name
                    existAuthorInSelected.Author_Career = author.Author_Career
                    existAuthorInSelected.IsChecked = True
                    existAuthorInSelected.IsActive = author.IsActive
                End If
                tmp = From a In _authorSelectedList
                      Order By a.Author_Full_Name
                      Select a

                _authorSelectedList = tmp.ToList()
                Me.SelectedList = _authorSelectedList
                Return True
            Else
                Me.SelectedList = _authorSelectedList
                Return False
            End If
        End If
            Return False
    End Function

    Public Function AddCategory(category As Category, isSelected As Boolean) As Boolean
        If _type Is GetType(Category) Then
            Dim existCategory = From c In _categoryEntityList
                                Where c.Category_Name = category.Category_Name
                                Select c
            If existCategory.Count > 0 Then
                Return False
            End If

            If IsNothing(_categoryEntityList) Then
                _categoryEntityList = New List(Of Category)
            End If
            Dim clonedCategory = New Category With {
                                .Category_id = category.Category_id,
                                .Category_Name = category.Category_Name,
                                .IsActive = category.IsActive,
                                .IsChecked = category.IsChecked
                                }
            _categoryEntityList.Add(clonedCategory)
            Dim tmp = From c In _categoryEntityList
                      Order By c.Category_Name
                      Select c

            _categoryEntityList = tmp.ToList()
            If isSelected Then
                If IsNothing(_categorySelectedList) Then
                    _categorySelectedList = New List(Of Category)
                End If
                Dim clonedCategory1 = New Category With {
                                     .Category_id = category.Category_id,
                                     .Category_Name = category.Category_Name,
                                     .IsActive = category.IsActive,
                                     .IsChecked = True
                                     }
                _categorySelectedList.Add(clonedCategory1)
                tmp = From c In _categorySelectedList
                      Order By c.Category_Name
                      Select c

                _categorySelectedList = tmp.ToList()
            End If
            Me.SelectedList = _categorySelectedList
            Return True
        End If
        Return False
    End Function

    Public Function UpdateCategory(category As Category, isSelected As Boolean) As Boolean
        If _type Is GetType(Category) Then
            Dim existCategory = (From c In _categoryEntityList
                                 Where c.Category_id = category.Category_id
                                 Select c).FirstOrDefault
            If IsNothing(existCategory) Then
                Return False
            End If

            If IsNothing(_categoryEntityList) Then
                _categoryEntityList = New List(Of Category)
            End If

            existCategory.Category_Name = category.Category_Name
            existCategory.IsChecked = category.IsChecked
            existCategory.IsActive = category.IsActive

            If Not category.IsActive Then
                _categoryEntityList.Remove(existCategory)
            End If

            Dim tmp = From c In _categoryEntityList
                      Order By c.Category_Name
                      Select c

            _categoryEntityList = tmp.ToList()
            If isSelected Then
                If IsNothing(_categorySelectedList) Then
                    _categorySelectedList = New List(Of Category)
                End If
                Dim existCategoryInSelected = (From c In _categorySelectedList
                                               Where c.Category_id = category.Category_id
                                               Select c).FirstOrDefault
                If IsNothing(existCategoryInSelected) Then
                    Dim clonedCategory1 = New Category With {
                                          .Category_id = category.Category_id,
                                          .Category_Name = category.Category_Name,
                                          .IsActive = category.IsActive,
                                          .IsChecked = True
                                          }
                    _categorySelectedList.Add(clonedCategory1)
                Else
                    existCategoryInSelected.Category_Name = category.Category_Name
                    existCategoryInSelected.IsChecked = True
                    existCategoryInSelected.IsActive = category.IsActive
                End If
                tmp = From c In _categorySelectedList
                      Order By c.Category_Name
                      Select c

                _categorySelectedList = tmp.ToList()
                Me.SelectedList = _categorySelectedList
                Return True
            Else
                Me.SelectedList = _categorySelectedList
                Return False
            End If
        End If
        Return False
    End Function

    Public Function AddShelf(shelf As Shelf, isSelected As Boolean) As Boolean
        If _type Is GetType(Shelf) Then
            Dim existShelf = From s In _shelfEntityList
                             Where s.Shelf_Code = shelf.Shelf_Code
                             Select s
            If existShelf.Count > 0 Then
                Return False
            End If

            If IsNothing(_shelfEntityList) Then
                _shelfEntityList = New List(Of Shelf)
            End If
            Dim clonedShelf = New Shelf With {
                                .Shelf_id = shelf.Shelf_id,
                                .Shelf_Code = shelf.Shelf_Code,
                                .Shelf_Description = shelf.Shelf_Description,
                                .IsActive = shelf.IsActive,
                                .IsChecked = shelf.IsChecked
                                }
            _shelfEntityList.Add(clonedShelf)
            Dim tmp = From s In _shelfEntityList
                      Order By s.Shelf_Code
                      Select s

            _shelfEntityList = tmp.ToList()
            If isSelected Then
                If IsNothing(_shelfSelectedList) Then
                    _shelfSelectedList = New List(Of Shelf)
                End If
                Dim clonedShelf1 = New Shelf With {
                                     .Shelf_id = shelf.Shelf_id,
                                     .Shelf_Code = shelf.Shelf_Code,
                                     .Shelf_Description = shelf.Shelf_Description,
                                     .IsActive = shelf.IsActive,
                                     .IsChecked = True
                                     }
                _shelfSelectedList.Add(clonedShelf1)
                tmp = From s In _shelfSelectedList
                      Order By s.Shelf_Code
                      Select s

                _shelfSelectedList = tmp.ToList()
            End If
            Me.SelectedList = _shelfSelectedList
            Return True
        End If
        Return False
    End Function

    Public Function UpdateShelf(shelf As Shelf, isSelected As Boolean) As Boolean
        If _type Is GetType(Shelf) Then
            Dim existShelf = (From s In _shelfEntityList
                              Where s.Shelf_id = shelf.Shelf_id
                              Select s).FirstOrDefault
            If IsNothing(existShelf) Then
                Return False
            End If

            If IsNothing(_shelfEntityList) Then
                _shelfEntityList = New List(Of Shelf)
            End If

            existShelf.Shelf_Code = shelf.Shelf_Code
            existShelf.Shelf_Description = shelf.Shelf_Description
            existShelf.IsChecked = shelf.IsChecked
            existShelf.IsActive = shelf.IsActive

            If Not shelf.IsActive Then
                _shelfEntityList.Remove(existShelf)
            End If

            Dim tmp = From s In _shelfEntityList
                      Order By s.Shelf_Code
                      Select s

            _shelfEntityList = tmp.ToList()
            If isSelected Then
                If IsNothing(_shelfSelectedList) Then
                    _shelfSelectedList = New List(Of Shelf)
                End If
                Dim existShelfInSelected = (From s In _shelfSelectedList
                                            Where s.Shelf_id = shelf.Shelf_id
                                            Select s).FirstOrDefault
                If IsNothing(existShelfInSelected) Then
                    Dim clonedShelf1 = New Shelf With {
                                          .Shelf_id = shelf.Shelf_id,
                                          .Shelf_Code = shelf.Shelf_Code,
                                          .Shelf_Description = shelf.Shelf_Description,
                                          .IsActive = shelf.IsActive,
                                          .IsChecked = True
                                          }
                    _shelfSelectedList.Add(clonedShelf1)
                Else
                    existShelfInSelected.Shelf_Code = shelf.Shelf_Code
                    existShelfInSelected.Shelf_Description = shelf.Shelf_Description
                    existShelfInSelected.IsChecked = True
                    existShelfInSelected.IsActive = shelf.IsActive
                End If
                tmp = From s In _shelfSelectedList
                      Order By s.Shelf_Code
                      Select s

                _shelfSelectedList = tmp.ToList()
                Me.SelectedList = _shelfSelectedList
                Return True
            Else
                Me.SelectedList = _shelfSelectedList
                Return False
            End If
        End If
        Return False
    End Function

    Public Overrides Sub Load(userState As Object)
        DoQuery()
        SortBySelected()
        _collectionViewSource.Source = _source
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

    Public Sub DoQuery()
        If _type Is GetType(Author) Then
            DoQueryAuthor(Me._authorEntityList)
        ElseIf _type Is GetType(Category) Then
            DoQueryCategory(Me._categoryEntityList)
        ElseIf _type Is GetType(Shelf) Then
            DoQueryShelf(Me._shelfEntityList)
        End If
    End Sub

    Private Sub DoQueryAuthor(target As List(Of Author))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From a In target
                         Where a.Author_First_Name.ToLower.Contains(_filter) _
                         OrElse a.Author_Last_Name.ToLower.Contains(_filter) _
                         Order By a.Author_Full_Name _
                         Select a
            _source = result
        Else
            Dim result = From a In target
                         Order By a.Author_Full_Name _
                         Select a
            _source = result
        End If
    End Sub

    Private Sub DoQueryCategory(target As List(Of Category))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From c In target
                         Where c.Category_Name.ToLower.Contains(_filter) _
                         Order By c.Category_Name _
                         Select c
            _source = result
        Else
            Dim result = From c In target
                         Order By c.Category_Name _
                         Select c
            _source = result
        End If
    End Sub

    Private Sub DoQueryShelf(target As List(Of Shelf))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From s In target
                         Where s.Shelf_Code.ToLower.Contains(_filter) _
                         Order By s.Shelf_Code _
                         Select s
            _source = result
        Else
            Dim result = From s In target
                         Order By s.Shelf_Code _
                         Select s
            _source = result
        End If
    End Sub

    Private Sub SortBySelected()
        If _type Is GetType(Author) Then
            SortAuthorBySelected(_authorSelectedList, _source)
        ElseIf _type Is GetType(Category) Then
            SortCategoryBySelected(_categorySelectedList, _source)
        ElseIf _type Is GetType(Shelf) Then
            SortShelfBySelected(_shelfSelectedList, _source)
        End If
    End Sub

    Private Sub SortAuthorBySelected(ByVal selectedList As IEnumerable(Of Author), ByVal source As IEnumerable(Of Author))
        Dim tmpSource = source.ToList()
        If IsNothing(selectedList) Then
            _source = tmpSource
            Return
        End If
        Dim tgSource = New List(Of Author)
        Dim _selectedListIds = New List(Of Integer)
        For Each e As Author In selectedList
            tgSource.Add(e)
            _selectedListIds.Add(e.Author_id)
        Next
        For Each e As Author In tmpSource
            If Not _selectedListIds.Contains(e.Author_id) Then
                tgSource.Add(e)
                _selectedListIds.Add(e.Author_id)
            End If
        Next
        _source = tgSource
    End Sub

    Private Sub SortCategoryBySelected(ByVal selectedList As IEnumerable(Of Category), ByVal source As IEnumerable(Of Category))
        Dim tmpSource = source.ToList()
        If IsNothing(selectedList) Then
            _source = tmpSource
            Return
        End If
        Dim tgSource = New List(Of Category)
        Dim _selectedListIds = New List(Of Integer)
        For Each e As Category In selectedList
            tgSource.Add(e)
            _selectedListIds.Add(e.Category_id)
        Next
        For Each e As Category In tmpSource
            If Not _selectedListIds.Contains(e.Category_id) Then
                tgSource.Add(e)
                _selectedListIds.Add(e.Category_id)
            End If
        Next
        _source = tgSource
    End Sub

    Private Sub SortShelfBySelected(ByVal selectedList As IEnumerable(Of Shelf), ByVal source As IEnumerable(Of Shelf))
        Dim tmpSource = source.ToList()
        If IsNothing(selectedList) Then
            _source = tmpSource
            Return
        End If
        Dim tgSource = New List(Of Shelf)
        Dim _selectedListIds = New List(Of Integer)
        For Each e As Shelf In selectedList
            tgSource.Add(e)
            _selectedListIds.Add(e.Shelf_id)
        Next
        For Each e As Shelf In tmpSource
            If Not _selectedListIds.Contains(e.Shelf_id) Then
                tgSource.Add(e)
                _selectedListIds.Add(e.Shelf_id)
            End If
        Next
        _source = tgSource
    End Sub

    Private Sub initializeIsChecked(value As Boolean, list As IEnumerable(Of TEntity))
        If _type Is GetType(Author) Then
            initializeIsCheckedAuthor(value, list)
        ElseIf _type Is GetType(Category) Then
            initializeIsCheckedCategory(value, list)
        ElseIf _type Is GetType(Shelf) Then
            initializeIsCheckedShelf(value, list)
        End If
    End Sub

    Private Sub initializeIsCheckedAuthor(value As Boolean, list As IEnumerable(Of Author))
        If Not IsNothing(list) Then
            For Each author In list
                author.IsChecked = value
            Next
        End If
    End Sub

    Private Sub initializeIsCheckedCategory(value As Boolean, list As IEnumerable(Of Category))
        If Not IsNothing(list) Then
            For Each category In list
                category.IsChecked = value
            Next
        End If
    End Sub

    Private Sub initializeIsCheckedShelf(value As Boolean, list As IEnumerable(Of Shelf))
        If Not IsNothing(list) Then
            For Each shelf In list
                shelf.IsChecked = value
            Next
        End If
    End Sub

    Private Sub cloneEntity(entityList As IQueryable(Of TEntity))
        If _type Is GetType(Author) Then
            cloneEntityAuthor(entityList)
        ElseIf _type Is GetType(Category) Then
            cloneEntityCategory(entityList)
        ElseIf _type Is GetType(Shelf) Then
            cloneEntityShelf(entityList)
        End If
    End Sub

    Private Sub cloneEntityAuthor(entityList As IQueryable(Of Author))
        If Not IsNothing(entityList) Then
            _authorEntityList = New List(Of Author)
            For Each a In entityList
                Dim author = New Author With {
                    .Author_id = a.Author_id,
                    .Author_First_Name = a.Author_First_Name,
                    .Author_Last_Name = a.Author_Last_Name,
                    .IsActive = a.IsActive
                    }
                _authorEntityList.Add(author)
            Next
        End If
    End Sub

    Private Sub cloneEntityCategory(entityList As IQueryable(Of Category))
        If Not IsNothing(entityList) Then
            _categoryEntityList = New List(Of Category)
            For Each c In entityList
                Dim category = New Category With {
                    .Category_id = c.Category_id,
                    .Category_Name = c.Category_Name,
                    .IsActive = c.IsActive
                    }
                _categoryEntityList.Add(category)
            Next
        End If
    End Sub

    Private Sub cloneEntityShelf(entityList As IQueryable(Of Shelf))
        If Not IsNothing(entityList) Then
            _shelfEntityList = New List(Of Shelf)
            For Each s In entityList
                Dim shelf = New Shelf With {
                    .Shelf_id = s.Shelf_id,
                    .Shelf_Code = s.Shelf_Code,
                    .IsActive = s.IsActive
                    }
                _shelfEntityList.Add(shelf)
            Next
        End If
    End Sub

    Private Sub cloneSelectedList(selectedList As IEnumerable(Of TEntity))
        If _type Is GetType(Author) Then
            cloneSelectedAuthor(selectedList)
        ElseIf _type Is GetType(Category) Then
            cloneSelectedCategory(selectedList)
        ElseIf _type Is GetType(Shelf) Then
            cloneSelectedShelf(selectedList)
        End If
    End Sub

    Private Sub cloneSelectedAuthor(entityList As IEnumerable(Of Author))
        If Not IsNothing(entityList) Then
            _authorSelectedList = New List(Of Author)
            For Each a In entityList
                Dim author = New Author With {
                    .Author_id = a.Author_id,
                    .IsActive = a.IsActive,
                    .IsChecked = True,
                    .Author_First_Name = a.Author_First_Name,
                    .Author_Last_Name = a.Author_Last_Name
                    }
                _authorSelectedList.Add(author)
            Next
        End If
    End Sub

    Private Sub cloneSelectedCategory(entityList As IEnumerable(Of Category))
        If Not IsNothing(entityList) Then
            _categorySelectedList = New List(Of Category)
            For Each c In entityList
                Dim category = New Category With {
                    .Category_id = c.Category_id,
                    .IsActive = c.IsActive,
                    .IsChecked = True,
                    .Category_Name = c.Category_Name
                    }
                _categorySelectedList.Add(category)
            Next
        End If
    End Sub

    Private Sub cloneSelectedShelf(entityList As IEnumerable(Of Shelf))
        If Not IsNothing(entityList) Then
            _shelfSelectedList = New List(Of Shelf)
            For Each s In entityList
                Dim shelf = New Shelf With {
                    .Shelf_id = s.Shelf_id,
                    .IsActive = s.IsActive,
                    .IsChecked = True,
                    .Shelf_Code = s.Shelf_Code
                    }
                _shelfSelectedList.Add(shelf)
            Next
        End If
    End Sub

End Class

