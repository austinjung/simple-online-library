﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ServiceModel.DomainServices.Client

Public Class DomainCollectionModels
    Inherits Dictionary(Of Type, DomainCollectionModel)

End Class

'Public Class DomainCollectionModel(Of TEntity As Entity)
Public Class DomainCollectionModel

#Region "Private Variables"

    'Private _view As DomainCollectionView(Of TEntity)
    'Private _source As EntityList(Of TEntity)
    'Private _loader As DomainCollectionViewLoader(Of TEntity)
    Private _view As DomainCollectionView
    Private _source As IEnumerable(Of Object)
    Private _loader As DomainCollectionViewLoader
    Private _query As EntityQuery
    Private _entityType As Type
    Private _key As String

#End Region

#Region "Properties"

    Public Property View As DomainCollectionView
        Get
            Return _view
        End Get
        Set(value As DomainCollectionView)
            _view = value
        End Set
    End Property
    Public Property Source As IEnumerable(Of Object)
        Get
            Return _source
        End Get
        Set(value As IEnumerable(Of Object))
            _source = value
        End Set
    End Property
    Public Property Loader As DomainCollectionViewLoader
        Get
            Return _loader
        End Get
        Set(value As DomainCollectionViewLoader)
            _loader = value
        End Set
    End Property
    Public Property Query As EntityQuery
        Get
            Return _query
        End Get
        Set(value As EntityQuery)
            _query = value
        End Set
    End Property
    Public Property EntityType As Type
        Get
            Return _entityType
        End Get
        Set(value As Type)
            _entityType = value
        End Set
    End Property
    Public Property Key As String
        Get
            Return _key
        End Get
        Set(value As String)
            _key = value
        End Set
    End Property

#End Region

#Region "Constructor & Public Methods"

    Public Sub New(key As String, entityType As Type,
                   view As DomainCollectionView,
                   source As IEnumerable(Of Object),
                   loader As DomainCollectionViewLoader)

        _key = key
        _entityType = entityType
        _query = Nothing ' initial query is nothing
        _view = view
        _source = source
        _loader = loader

    End Sub

#End Region

End Class
