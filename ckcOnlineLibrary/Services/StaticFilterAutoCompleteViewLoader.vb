﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common

Public Class StaticFilterAutoCompleteViewLoader(Of TEntity As Entity)
    Inherits CollectionViewLoader

    Private _target As IQueryable(Of TEntity)
    Private _type As Type

    Private _isSelectedAll As Boolean
    Private selectAllAuthor As Author
    Private selectAllCategory As Category

    Private _selectedList As IEnumerable(Of TEntity)
    Private _source As IEnumerable(Of TEntity)
    Private _collectionViewSource As EntityList(Of TEntity)
    Private _filter As String
    Public Property Filter As String
        Get
            Return _filter
        End Get
        Set(value As String)
            _filter = value.ToLower()
            DoQuery()
            SortBySelected()
            _collectionViewSource.Source = _source
        End Set
    End Property

    Public ReadOnly Property Source As IEnumerable(Of TEntity)
        Get
            Return _source
        End Get
    End Property

    Public ReadOnly Property CollectionViewSource As EntityList(Of TEntity)
        Get
            Return _collectionViewSource
        End Get
    End Property

    Public Property SelectedList As IEnumerable(Of TEntity)
        Get
            Return _selectedList
        End Get
        Set(value As IEnumerable(Of TEntity))
            If CheckIsSelectedAll(value) Then
                _selectedList = value
                If value.Count = 1 Then
                    _selectedList = _source
                End If
                initializeIsChecked(True, _selectedList)
            Else
                _selectedList = value
                initializeIsChecked(False, _source)
                initializeIsChecked(True, _selectedList)
            End If
            DoQuery()
            SortBySelected()
            _collectionViewSource.Source = _source
        End Set
    End Property

    Public Sub New(ByRef collectionViewSource As EntityList(Of TEntity), entities As IQueryable(Of TEntity))
        _target = entities
        _collectionViewSource = collectionViewSource
        _type = GetType(TEntity)
        selectAllAuthor = New Author With {.Author_First_Name = "- Select All"}
        selectAllCategory = New Category With {.Category_Name = "- Select All"}
        DoQuery()
        initializeIsChecked(False, _source)
    End Sub

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Sub AddAuthor(author As Author)
        Dim _authorSource As List(Of Author) = CType(_source, List(Of Author))
        If _type Is GetType(Author) Then
            Dim existAuthor = From a In _authorSource
                              Where a.Author_First_Name = author.Author_First_Name _
                                And a.Author_Last_Name = author.Author_Last_Name _
                              Select a
            If existAuthor.Count > 0 Then
                Return
            End If

            CType(_selectedList, List(Of Author)).Add(author)
            _selectedList = CType(_selectedList, List(Of Author)).OrderBy(Function(a) a.Author_Full_Name)
            SortBySelected()
            _collectionViewSource.Source = _source
        End If

    End Sub

    Public Sub AddCategory(category As Category)
        Dim _categorySource As List(Of Category) = CType(_source, List(Of Category))
        If _type Is GetType(Category) Then
            Dim existCategory = From c In _categorySource
                                Where c.Category_Name = category.Category_Name
                                Select c
            If existCategory.Count > 0 Then
                Return
            End If

            CType(_selectedList, List(Of Category)).Add(category)
            _selectedList = CType(_selectedList, List(Of Category)).OrderBy(Function(c) c.Category_Name)
            SortBySelected()
            _collectionViewSource.Source = _source
        End If

    End Sub

    Public Overrides Sub Load(userState As Object)
        DoQuery()
        SortBySelected()
        _collectionViewSource.Source = _source
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

    Public Sub DoQuery()
        If _type Is GetType(Author) Then
            DoQueryAuthor(_target)
        ElseIf _type Is GetType(Category) Then
            DoQueryCategory(_target)
        End If
    End Sub

    Private Sub DoQueryAuthor(target As IQueryable(Of Author))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From a In target
                         Where a.Author_First_Name.ToLower.Contains(_filter) _
                         OrElse a.Author_Last_Name.ToLower.Contains(_filter) _
                         Order By a.Author_Full_Name _
                         Select a
            _source = result
        Else
            Dim result = From a In target
                         Order By a.Author_Full_Name _
                         Select a
            _source = result
        End If
        If IsNothing(selectAllAuthor) Then
            selectAllAuthor = New Author With {.Author_First_Name = "- Select All"}
        End If
        Dim all_Authors = New List(Of Author)
        all_Authors.Add(selectAllAuthor)
        all_Authors.AddRange(_source)
        _source = all_Authors
    End Sub

    Private Sub DoQueryCategory(target As IQueryable(Of Category))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From c In target
                         Where c.Category_Name.ToLower.Contains(_filter) _
                         Order By c.Category_Name _
                         Select c
            _source = result
        Else
            Dim result = From c In target
                         Order By c.Category_Name _
                         Select c
            _source = result
        End If
        If IsNothing(selectAllCategory) Then
            selectAllCategory = New Category With {.Category_Name = "- Select All"}
        End If
        Dim all_Categories = New List(Of Category)
        all_Categories.Add(selectAllCategory)
        all_Categories.AddRange(_source)
        _source = all_Categories
    End Sub

    Private Sub SortBySelected()
        If _type Is GetType(Author) Then
            SortBySelectedAuthor(_selectedList, _source)
        ElseIf _type Is GetType(Category) Then
            SortBySelectedCategory(_selectedList, _source)
        End If
    End Sub

    Private Sub SortBySelectedAuthor(selectedList As IEnumerable(Of Author), source As IEnumerable(Of Author))

        If Not IsNothing(selectedList) Then
            Dim tmpSource = source.ToList()

            Dim tmp = copyAuthorList(selectedList)
            Dim tgSource = New List(Of Author)
            If tmp.Count < 1 Then
                tgSource.Add(selectAllAuthor)
            ElseIf tmp(0).Author_First_Name <> "- Select All" Then
                tgSource.Add(selectAllAuthor)
            End If
            tgSource.AddRange(copyAuthorList(selectedList))
            Dim addedCount = 0
            For Each e In tmpSource
                If e.Author_First_Name <> "- Select All" And Not selectedList.Contains(e) Then
                    tgSource.Add(e)
                    addedCount += 1
                End If
            Next
            If (tgSource.Count = addedCount + 1) And (tgSource(0).IsChecked = True) Then
                initializeIsChecked(True, tgSource)
            ElseIf addedCount = 0 And tgSource.Count > 0 Then
                tgSource(0).IsChecked = True
            ElseIf tgSource.Count > 0 Then
                tgSource(0).IsChecked = False
            End If
            _source = tgSource
        End If
    End Sub

    Private Sub SortBySelectedCategory(selectedList As IEnumerable(Of Category), source As IEnumerable(Of Category))

        If Not IsNothing(selectedList) Then
            Dim tmpSource = source.ToList()

            Dim tmp = copyCategoryList(selectedList)
            Dim tgSource = New List(Of Category)
            If tmp.Count < 1 Then
                tgSource.Add(selectAllCategory)
            ElseIf tmp(0).Category_Name <> "- Select All" Then
                tgSource.Add(selectAllCategory)
            End If
            tgSource.AddRange(copyCategoryList(selectedList))
            Dim addedCount = 0
            For Each e In tmpSource
                If e.Category_Name <> "- Select All" And Not selectedList.Contains(e) Then
                    tgSource.Add(e)
                    addedCount += 1
                End If
            Next
            If (tgSource.Count = addedCount + 1) And (tgSource(0).IsChecked = True) Then
                initializeIsChecked(True, tgSource)
            ElseIf addedCount = 0 And tgSource.Count > 0 Then
                tgSource(0).IsChecked = True
            ElseIf tgSource.Count > 0 Then
                tgSource(0).IsChecked = False
            End If
            _source = tgSource
        End If
    End Sub

    Private Function copyAuthorList(selectedList As IEnumerable(Of Author)) As List(Of Author)
        Dim copiedList = New List(Of Author)
        For Each e In selectedList
            copiedList.Add(e)
        Next
        Return copiedList
    End Function

    Private Function copyCategoryList(selectedList As IEnumerable(Of Category)) As List(Of Category)
        Dim copiedList = New List(Of Category)
        For Each e In selectedList
            copiedList.Add(e)
        Next
        Return copiedList
    End Function

    Private Sub initializeIsChecked(value As Boolean, list As IEnumerable(Of TEntity))
        If _type Is GetType(Author) Then
            initializeIsCheckedAuthor(value, list)
        ElseIf _type Is GetType(Category) Then
            initializeIsCheckedCategory(value, list)
        End If
    End Sub

    Private Sub initializeIsCheckedAuthor(value As Boolean, list As IEnumerable(Of Author))
        If Not IsNothing(list) Then
            For Each author In list
                author.IsChecked = value
            Next
        End If
    End Sub

    Private Sub initializeIsCheckedCategory(value As Boolean, list As IEnumerable(Of Category))
        If Not IsNothing(list) Then
            For Each category In list
                category.IsChecked = value
            Next
        End If
    End Sub

    Private Function CheckIsSelectedAll(selectedList As IEnumerable(Of TEntity)) As Boolean
        If _type Is GetType(Author) Then
            Return CheckIsSelectedAllAuthors(selectedList)
        ElseIf _type Is GetType(Category) Then
            Return CheckIsSelectedAllCategories(selectedList)
        End If
        Return False
    End Function

    Private Function CheckIsSelectedAllAuthors(selectedList As IEnumerable(Of Author)) As Boolean
        If selectedList.Count < 1 Then
            Return False
        ElseIf selectedList(0).Author_First_Name = "- Select All" Then
            selectAllAuthor = selectedList(0)
            Return True
        Else
            Return False
        End If
    End Function

    Private Function CheckIsSelectedAllCategories(selectedList As IEnumerable(Of Category)) As Boolean
        If selectedList.Count < 1 Then
            Return False
        ElseIf selectedList(0).Category_Name = "- Select All" Then
            selectAllCategory = selectedList(0)
            Return True
        Else
            Return False
        End If
    End Function

End Class

