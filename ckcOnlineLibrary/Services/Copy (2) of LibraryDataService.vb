﻿Imports System
Imports System.Collections.Generic
Imports System.ServiceModel.DomainServices.Client
Imports Microsoft.Windows.Data.DomainServices
Imports Papa.Common
Imports Ria.Common
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.Collections.ObjectModel

Public Class LibraryDataService
    Inherits ViewModel
    Implements ILibraryDataService

#Region "Private Variables"

    'Domain Context
    Private ReadOnly _domainContext As ckcLibraryDomainContext = New ckcLibraryDomainContext()

    'Domain Collections
    Private ReadOnly _domainCollections As DomainCollectionModels = New DomainCollectionModels()
    Private ReadOnly _bookProperties As DomainCollectionModels = New DomainCollectionModels()
    Private ReadOnly _reviewProperties As DomainCollectionModels = New DomainCollectionModels()
    Private ReadOnly _requestProperties As DomainCollectionModels = New DomainCollectionModels()
    Private ReadOnly _transactionProperties As DomainCollectionModels = New DomainCollectionModels()

    'Collection Models
    Private _authors As DomainCollectionModel
    Private _books As DomainCollectionModel
    Private _categories As DomainCollectionModel
    Private _members As DomainCollectionModel
    Private _requests As DomainCollectionModel
    Private _reviews As DomainCollectionModel
    Private _transactions As DomainCollectionModel
    Private _shelves As DomainCollectionModel

    'Collection Models for Selected Book Properties
    Private _book_Authors As DomainCollectionModel
    Private _book_Shelves As DomainCollectionModel
    Private _book_Categories As DomainCollectionModel

    'Collection Views for Selected Book's Authors, Category, and Shelves
    Private _book_Authors_View As DomainCollectionView(Of Author)
    Private _book_Authors_Loader As StaticAutoCompleteViewLoader(Of Author)
    Private _book_Authors_Source As EntityList(Of Author)

    Private _book_Categories_View As DomainCollectionView(Of Category)
    Private _book_Categories_Loader As StaticAutoCompleteViewLoader(Of Category)
    Private _book_Categories_Source As EntityList(Of Category)

    Private _book_Shelves_View As DomainCollectionView(Of Shelf)
    Private _book_Shelves_Loader As StaticAutoCompleteViewLoader(Of Shelf)
    Private _book_Shelves_Source As EntityList(Of Shelf)

    'Collection Views for Book's Authors, Category to filter book search
    Private _book_Authors_Filter_View As DomainCollectionView(Of Author)
    Private _book_Authors_Filter_Loader As StaticFilterAutoCompleteViewLoader(Of Author)
    Private _book_Authors_Filter_Source As EntityList(Of Author)

    Private _book_Categories_Filter_View As DomainCollectionView(Of Category)
    Private _book_Categories_Filter_Loader As StaticFilterAutoCompleteViewLoader(Of Category)
    Private _book_Categories_Filter_Source As EntityList(Of Category)

    'Conllection Views for popular requests & top reviews
    Private _top_Requests As DomainCollectionView(Of Books_By_Requests)
    Private _top_Requests_source As EntityList(Of Books_By_Requests)
    Private _top_Requests_loader As DomainCollectionViewLoader(Of Books_By_Requests)

    Private _top_Reviews As DomainCollectionView(Of Books_By_Reviews)
    Private _top_Reviews_source As EntityList(Of Books_By_Reviews)
    Private _top_Reviews_loader As DomainCollectionViewLoader(Of Books_By_Reviews)

    'Load Control Status for Basic Domain Collection Views
    Private ReadOnly _pendingLoads As IDictionary(Of Type, LoadOperation) = New Dictionary(Of Type, LoadOperation)()
    Private _canLoad As Boolean = True
    'Queue of Book Properties Collection Load Query
    Private bookPropertiesLoadQueue As Queue(Of EntityQuery) = New Queue(Of EntityQuery)
    Private requestPropertiesLoadQueue As Queue(Of EntityQuery) = New Queue(Of EntityQuery)
    Private reviewPropertiesLoadQueue As Queue(Of EntityQuery) = New Queue(Of EntityQuery)
    Private transactionPropertiesLoadQueue As Queue(Of EntityQuery) = New Queue(Of EntityQuery)

    'Filters on Load Operations
    Private _filterBookTitle As String
    Private _filterBookDescription As String
    Private _filterBookMedia As String
    Private _filterBookLanguage As String
    Private _filterBookISBN As String
    Private _filterBookPublished As Date '= New Date(2011, 1, 2)
    Private _filterActiveBook As Boolean = True
    Private _filterBookByAuthors As List(Of Author)
    Private _filterBookByCategories As List(Of Category)

    Private _filterAuthorName As String
    Private _filterAuthorCareer As String
    Private _filterActiveAuthor As Boolean = True

    Private _filterCategoryName As String
    Private _filterActiveCategory As Boolean = True

    Private _filterShelf As String
    Private _filterActiveShelf As Boolean = True

    Private _filterMemberLogin As String
    Private _filterMemberName As String
    Private _filterMemberPIN As String
    Private _filterMemberRole As String
    Private _filterMemberEmail As String
    Private _filterMemberPhone As String
    Private _filterMemberAddress As String
    Private _filterActiveMember As Boolean = True

    Private _filterRequestExpectedDate As Date = Nothing
    Private _filterReview As String
    Private _filterTransactionExpectedReturn As Date

    'Select Top
    Private _selectTopRequests As Integer = 6 ' Default Select Top Value = 6
    Private _selectTopReviews As Integer = 6 ' Default Select Top Value = 6
    Private _selectTopTransactions As Integer = 6 ' Default Select Top Value = 6

#End Region

#Region "Properties : Domain Context & Collection Views & Loaders"

    Public ReadOnly Property DomainContext As ckcLibraryDomainContext Implements ILibraryDataService.DomainContext
        Get
            Return Me._domainContext
        End Get
    End Property

    Public Property AuthorCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.AuthorCollectionView
        Get
            If Me._authors Is Nothing Then
                Dim source = New EntityList(Of Author)(Me._domainContext.Authors)
                Dim loader = New DomainCollectionViewLoader(Of Author)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Author)(loader, source)
                Me._authors = New DomainCollectionModel("Authors", GetType(Author),
                                                        view, source, loader)
                Me._domainCollections(GetType(Author)) = Me._authors
            End If
            Return Me._authors.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._authors = Nothing
        End Set
    End Property

    Public Property BookAuthorCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookAuthorCollectionView
        Get
            If IsNothing(Me._book_Authors_View) Then
                Me._book_Authors_Source = New EntityList(Of Author)(Me._domainContext.Authors)
                Me._book_Authors_Loader = New StaticAutoCompleteViewLoader(Of Author)(
                                            Me._book_Authors_Source,
                                            Me.AuthorCollectionView.SourceCollection.AsQueryable
                                          )
                Me._book_Authors_View = New DomainCollectionView(Of Author)(Me._book_Authors_Loader, Me._book_Authors_Source)
            End If
            Return Me._book_Authors_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._book_Authors_View = Nothing
            Me._book_Authors_Loader = Nothing
        End Set
    End Property
    Public ReadOnly Property BookAuthorCollectionLoader As StaticAutoCompleteViewLoader(Of Author) Implements ILibraryDataService.BookAuthorCollectionLoader
        Get
            Return Me._book_Authors_Loader
        End Get
    End Property

    Public Property BookAuthorFilterCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookAuthorFilterCollectionView
        Get
            If IsNothing(Me._book_Authors_Filter_View) Then
                Me._book_Authors_Filter_Source = New EntityList(Of Author)(Me._domainContext.Authors)
                Me._book_Authors_Filter_Loader = New StaticFilterAutoCompleteViewLoader(Of Author)(
                                                     Me._book_Authors_Filter_Source,
                                                     Me.AuthorCollectionView.SourceCollection.AsQueryable
                                                 )
                Me._book_Authors_Filter_View = New DomainCollectionView(Of Author)(Me._book_Authors_Filter_Loader, Me._book_Authors_Filter_Source)
            End If
            Return Me._book_Authors_Filter_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._book_Authors_Filter_View = Nothing
            Me._book_Authors_Filter_Loader = Nothing
        End Set
    End Property
    Public ReadOnly Property BookAuthorFilterCollectionLoader As StaticFilterAutoCompleteViewLoader(Of Author) Implements ILibraryDataService.BookAuthorFilterCollectionLoader
        Get
            Return Me._book_Authors_Filter_Loader
        End Get
    End Property

    Public Property BookCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookCollectionView
        Get
            If Me._books Is Nothing Then
                Dim source = New EntityList(Of Book)(Me._domainContext.Books)
                Dim loader = New DomainCollectionViewLoader(Of Book)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Book)(loader, source)
                Me._books = New DomainCollectionModel("Books", GetType(Book),
                                                      view, source, loader)
                Me._domainCollections(GetType(Book)) = Me._books
            End If
            Return Me._books.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._books = Nothing
        End Set
    End Property

    Public Property CategoryCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.CategoryCollectionView
        Get
            If Me._categories Is Nothing Then
                Dim source = New EntityList(Of Category)(Me._domainContext.Categories)
                Dim loader = New DomainCollectionViewLoader(Of Category)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Category)(loader, source)
                Me._categories = New DomainCollectionModel("Categories", GetType(Category),
                                                       view, source, loader)
                Me._domainCollections(GetType(Category)) = Me._categories
            End If
            Return Me._categories.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._categories = Nothing
        End Set
    End Property

    Public Property BookCategoryCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookCategoryCollectionView
        Get
            If Me._book_Categories_View Is Nothing Then
                Me._book_Categories_Source = New EntityList(Of Category)(Me._domainContext.Categories)
                Me._book_Categories_Loader = New StaticAutoCompleteViewLoader(Of Category)(
                                               Me._book_Categories_Source,
                                               Me.CategoryCollectionView.SourceCollection.AsQueryable
                                             )
                Me._book_Categories_View = New DomainCollectionView(Of Category)(Me._book_Categories_Loader, Me._book_Categories_Source)
            End If
            Return Me._book_Categories_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._book_Categories_View = Nothing
            Me._book_Categories_Loader = Nothing
        End Set
    End Property
    Public ReadOnly Property BookCategoryCollectionLoader As StaticAutoCompleteViewLoader(Of Category) Implements ILibraryDataService.BookCategoryCollectionLoader
        Get
            Return Me._book_Categories_Loader
        End Get
    End Property

    Public Property BookCategoryFilterCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookCategoryFilterCollectionView
        Get
            If Me._book_Categories_Filter_View Is Nothing Then
                Me._book_Categories_Filter_Source = New EntityList(Of Category)(Me._domainContext.Categories)
                Me._book_Categories_Filter_Loader = New StaticFilterAutoCompleteViewLoader(Of Category)(
                                                      Me._book_Categories_Filter_Source,
                                                      Me.CategoryCollectionView.SourceCollection.AsQueryable
                                                    )
                Me._book_Categories_Filter_View = New DomainCollectionView(Of Category)(Me._book_Categories_Filter_Loader, Me._book_Categories_Filter_Source)
            End If
            Return Me._book_Categories_Filter_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._book_Categories_Filter_View = Nothing
            Me._book_Categories_Filter_Loader = Nothing
        End Set
    End Property
    Public ReadOnly Property BookCategoryFilterCollectionLoader As StaticFilterAutoCompleteViewLoader(Of Category) Implements ILibraryDataService.BookCategoryFilterCollectionLoader
        Get
            Return Me._book_Categories_Filter_Loader
        End Get
    End Property

    Public Property MemberCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.MemberCollectionView
        Get
            If Me._members Is Nothing Then
                Dim source = New EntityList(Of Member)(Me._domainContext.Members)
                Dim loader = New DomainCollectionViewLoader(Of Member)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Member)(loader, source)
                Me._members = New DomainCollectionModel("Members", GetType(Member),
                                                       view, source, loader)
                Me._domainCollections(GetType(Member)) = Me._members
            End If
            Return Me._members.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._members = Nothing
        End Set
    End Property

    Public Property RequestCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.RequestCollectionView
        Get
            If Me._requests Is Nothing Then
                Dim source = New EntityList(Of Member_Requests)(Me._domainContext.Member_Requests)
                Dim loader = New DomainCollectionViewLoader(Of Member_Requests)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Member_Requests)(loader, source)
                Me._requests = New DomainCollectionModel("Requests", GetType(Member_Requests),
                                                       view, source, loader)
                Me._domainCollections(GetType(Member_Requests)) = Me._requests
            End If
            Return Me._requests.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._requests = Nothing
        End Set
    End Property

    Public Property ReviewCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.ReviewCollectionView
        Get
            If Me._reviews Is Nothing Then
                Dim source = New EntityList(Of Member_Reviews)(Me._domainContext.Member_Reviews)
                Dim loader = New DomainCollectionViewLoader(Of Member_Reviews)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Member_Reviews)(loader, source)
                Me._reviews = New DomainCollectionModel("Reviews", GetType(Member_Reviews),
                                                       view, source, loader)
                Me._domainCollections(GetType(Member_Reviews)) = Me._reviews
            End If
            Return Me._reviews.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._reviews = Nothing
        End Set
    End Property

    Public Property ShelfCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.ShelfCollectionView
        Get
            If Me._shelves Is Nothing Then
                Dim source = New EntityList(Of Shelf)(Me._domainContext.Shelfs)
                Dim loader = New DomainCollectionViewLoader(Of Shelf)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Shelf)(loader, source)
                Me._shelves = New DomainCollectionModel("Shelves", GetType(Shelf),
                                                       view, source, loader)
                Me._domainCollections(GetType(Shelf)) = Me._shelves
            End If
            Return Me._shelves.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._shelves = Nothing
        End Set
    End Property

    Public Property BookShelfCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookShelfCollectionView
        Get
            If Me._book_Shelves_View Is Nothing Then
                Me._book_Shelves_Source = New EntityList(Of Shelf)(Me._domainContext.Shelfs)
                Me._book_Shelves_Loader = New StaticAutoCompleteViewLoader(Of Shelf)(
                                            Me._book_Shelves_Source,
                                            Me.ShelfCollectionView.SourceCollection.AsQueryable
                                          )
                Me._book_Shelves_View = New DomainCollectionView(Of Shelf)(Me._book_Shelves_Loader, Me._book_Shelves_Source)
            End If
            Return (Me._book_Shelves_View)
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._book_Shelves_View = Nothing
            Me._book_Shelves_Loader = Nothing
        End Set
    End Property
    Public ReadOnly Property BookShelfCollectionLoader As StaticAutoCompleteViewLoader(Of Shelf) Implements ILibraryDataService.BookShelfCollectionLoader
        Get
            Return Me._book_Shelves_Loader
        End Get
    End Property

    Public Property TransactionCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.TransactionCollectionView
        Get
            If Me._transactions Is Nothing Then
                Dim source = New EntityList(Of Member_Transactions)(Me._domainContext.Member_Transactions)
                Dim loader = New DomainCollectionViewLoader(Of Member_Transactions)(
                                                AddressOf Me.LoadEntities,
                                                AddressOf Me.OnLoadEntitiesCompleted)
                Dim view = New DomainCollectionView(Of Member_Transactions)(loader, source)
                Me._transactions = New DomainCollectionModel("Transactions", GetType(Member_Transactions),
                                                       view, source, loader)
                Me._domainCollections(GetType(Member_Transactions)) = Me._transactions
            End If
            Return Me._transactions.View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._transactions = Nothing
        End Set
    End Property

    Public Property TopRequestsCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.TopRequestsCollectionView
        Get
            If Me._top_Requests Is Nothing Then
                _top_Requests_source = New EntityList(Of Books_By_Requests)(Me._domainContext.Books_By_Requests)
                _top_Requests_loader = New DomainCollectionViewLoader(Of Books_By_Requests)(
                                                AddressOf Me.LoadTopRequests,
                                                AddressOf Me.OnLoadTopRequestsCompleted)
                _top_Requests = New DomainCollectionView(Of Books_By_Requests)(_top_Requests_loader, _top_Requests_source)
            End If
            Return Me._top_Requests
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._top_Requests = Nothing
        End Set
    End Property

    Public Property TopReviewsCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.TopReviewsCollectionView
        Get
            If Me._top_Reviews Is Nothing Then
                _top_Reviews_source = New EntityList(Of Books_By_Reviews)(Me._domainContext.Books_By_Reviews)
                _top_Reviews_loader = New DomainCollectionViewLoader(Of Books_By_Reviews)(
                                                AddressOf Me.LoadTopReviews,
                                                AddressOf Me.OnLoadTopReviewsCompleted)
                _top_Reviews = New DomainCollectionView(Of Books_By_Reviews)(_top_Reviews_loader, _top_Reviews_source)
            End If
            Return Me._top_Reviews
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            If Not IsNothing(value) Then
                Throw New NotImplementedException("CollectionView should be reset with Null/Nothing.")
            End If
            Me._top_Reviews = Nothing
        End Set
    End Property

#End Region

#Region "Properties : Data Service Control Status"

    Public Property CanLoad() As Boolean Implements ILibraryDataService.CanLoad
        Get
            Return Me._canLoad
        End Get
        Private Set(value As Boolean)
            If Me._canLoad <> value Then
                Me._canLoad = value
                Me.RaisePropertyChanged("CanLoad")
            End If
        End Set
    End Property

#End Region

#Region "Properties : Filters on Load Operations"

    Public Property FilterAuthorCareer As String Implements ILibraryDataService.FilterAuthorCareer
        Get
            Return Me._filterAuthorCareer
        End Get
        Set(value As String)
            Me._filterAuthorCareer = value
        End Set
    End Property

    Public Property FilterAuthorName As String Implements ILibraryDataService.FilterAuthorName
        Get
            Return Me._filterAuthorName
        End Get
        Set(value As String)
            Me._filterAuthorName = value
        End Set
    End Property

    Public Property FilterActiveAuthor As Boolean Implements ILibraryDataService.FilterActiveAuthor
        Get
            Return Me._filterActiveAuthor
        End Get
        Set(value As Boolean)
            Me._filterActiveAuthor = value
        End Set
    End Property

    Public Property FilterBookDescription As String Implements ILibraryDataService.FilterBookDescription
        Get
            Return Me._filterBookDescription
        End Get
        Set(value As String)
            Me._filterBookDescription = value
        End Set
    End Property

    Public Property FilterBookISBN As String Implements ILibraryDataService.FilterBookISBN
        Get
            Return Me._filterBookISBN
        End Get
        Set(value As String)
            Me._filterBookISBN = value
        End Set
    End Property

    Public Property FilterBookLanguage As String Implements ILibraryDataService.FilterBookLanguage
        Get
            Return Me._filterBookLanguage
        End Get
        Set(value As String)
            Me._filterBookLanguage = value
        End Set
    End Property

    Public Property FilterBookMedia As String Implements ILibraryDataService.FilterBookMedia
        Get
            Return Me._filterBookMedia
        End Get
        Set(value As String)
            Me._filterBookMedia = value
        End Set
    End Property

    Public Property FilterBookPublished As Date Implements ILibraryDataService.FilterBookPublished
        Get
            Return Me._filterBookPublished
        End Get
        Set(value As Date)
            Me._filterBookPublished = value
        End Set
    End Property

    Public Property FilterBookTitle As String Implements ILibraryDataService.FilterBookTitle
        Get
            Return Me._filterBookTitle
        End Get
        Set(value As String)
            Me._filterBookTitle = value
        End Set
    End Property

    Public Property FilterActiveBook As Boolean Implements ILibraryDataService.FilterActiveBook
        Get
            Return Me._filterActiveBook
        End Get
        Set(value As Boolean)
            Me._filterActiveBook = value
        End Set
    End Property

    Public Property FilterBookByAuthors As List(Of Author) Implements ILibraryDataService.FilterBookByAuthors
        Get
            Return Me._filterBookByAuthors
        End Get
        Set(value As List(Of Author))
            Me._filterBookByAuthors = value
        End Set
    End Property

    Public Property FilterBookByCategories As List(Of Category) Implements ILibraryDataService.FilterBookByCategories
        Get
            Return Me._filterBookByCategories
        End Get
        Set(value As List(Of Category))
            Me._filterBookByCategories = value
        End Set
    End Property

    Public Property FilterCategoryName As String Implements ILibraryDataService.FilterCategoryName
        Get
            Return Me._filterCategoryName
        End Get
        Set(value As String)
            Me._filterCategoryName = value
        End Set
    End Property

    Public Property FilterActiveCategory As Boolean Implements ILibraryDataService.FilterActiveCategory
        Get
            Return Me._filterActiveCategory
        End Get
        Set(value As Boolean)
            Me._filterActiveCategory = value
        End Set
    End Property

    Public Property FilterMemberAddress As String Implements ILibraryDataService.FilterMemberAddress
        Get
            Return Me._filterMemberAddress
        End Get
        Set(value As String)
            Me._filterMemberAddress = value
        End Set
    End Property

    Public Property FilterMemberEmail As String Implements ILibraryDataService.FilterMemberEmail
        Get
            Return Me._filterMemberEmail
        End Get
        Set(value As String)
            Me._filterMemberEmail = value
        End Set
    End Property

    Public Property FilterMemberLogin As String Implements ILibraryDataService.FilterMemberLogin
        Get
            Return Me._filterMemberLogin
        End Get
        Set(value As String)
            Me._filterMemberLogin = value
        End Set
    End Property

    Public Property FilterMemberName As String Implements ILibraryDataService.FilterMemberName
        Get
            Return Me._filterMemberName
        End Get
        Set(value As String)
            Me._filterMemberName = value
        End Set
    End Property

    Public Property FilterMemberPhone As String Implements ILibraryDataService.FilterMemberPhone
        Get
            Return Me._filterMemberPhone
        End Get
        Set(value As String)
            Me._filterMemberPhone = value
        End Set
    End Property

    Public Property FilterMemberPIN As String Implements ILibraryDataService.FilterMemberPIN
        Get
            Return Me._filterMemberPIN
        End Get
        Set(value As String)
            Me._filterMemberPIN = value
        End Set
    End Property

    Public Property FilterMemberRole As String Implements ILibraryDataService.FilterMemberRole
        Get
            Return Me._filterMemberRole
        End Get
        Set(value As String)
            Me._filterMemberRole = value
        End Set
    End Property

    Public Property FilterActiveMember As Boolean Implements ILibraryDataService.FilterActiveMember
        Get
            Return Me._filterActiveMember
        End Get
        Set(value As Boolean)
            Me._filterActiveMember = value
        End Set
    End Property

    Public Property FilterRequestExpectedDate As Date Implements ILibraryDataService.FilterRequestExpectedDate
        Get
            Return Me._filterRequestExpectedDate
        End Get
        Set(value As Date)
            Me._filterRequestExpectedDate = value
        End Set
    End Property

    Public Property FilterReview As String Implements ILibraryDataService.FilterReview
        Get
            Return Me._filterReview
        End Get
        Set(value As String)
            Me._filterReview = value
        End Set
    End Property

    Public Property FilterShelf As String Implements ILibraryDataService.FilterShelf
        Get
            Return Me._filterShelf
        End Get
        Set(value As String)
            Me._filterShelf = value
        End Set
    End Property

    Public Property FilterActiveShelf As Boolean Implements ILibraryDataService.FilterActiveShelf
        Get
            Return Me._filterActiveShelf
        End Get
        Set(value As Boolean)
            Me._filterActiveShelf = value
        End Set
    End Property

    Public Property FilterTransactionExpectedReturn As Date Implements ILibraryDataService.FilterTransactionExpectedReturn
        Get
            Return Me._filterTransactionExpectedReturn
        End Get
        Set(value As Date)
            Me._filterTransactionExpectedReturn = value
        End Set
    End Property

#End Region

#Region "Properties : Select Top"

    Public Property SelectTopRequests As Integer Implements ILibraryDataService.SelectTopRequests
        Get
            Return Me._selectTopRequests
        End Get
        Set(value As Integer)
            Me._selectTopRequests = value
        End Set
    End Property

    Public Property SelectTopReviews As Integer Implements ILibraryDataService.SelectTopReviews
        Get
            Return Me._selectTopReviews
        End Get
        Set(value As Integer)
            Me._selectTopReviews = value
        End Set
    End Property

    Public Property SelectTopTransactions As Integer Implements ILibraryDataService.SelectTopTransactions
        Get
            Return Me._selectTopTransactions
        End Get
        Set(value As Integer)
            Me._selectTopTransactions = value
        End Set
    End Property

#End Region

#Region "Public Methods : Constructor"

    Public Sub New()

        'Initialize Entity Queries Dictionary with a Default Query for each Entity of Book Properties
        Dim author_source = New EntityList(Of Author)(Me._domainContext.Authors)
        Dim author_loader = New DomainCollectionViewLoader(Of Author)(
                                        AddressOf Me.LoadBookProperties,
                                        AddressOf Me.OnLoadBookPropertiesCompleted)
        Dim author_view = New DomainCollectionView(Of Author)(author_loader, author_source)
        Me._bookProperties(GetType(Author)) = New DomainCollectionModel("Authors", GetType(Author),
                                               author_view, author_source, author_loader)

        Dim category_source = New EntityList(Of Category)(Me._domainContext.Categories)
        Dim category_loader = New DomainCollectionViewLoader(Of Category)(
                                        AddressOf Me.LoadBookProperties,
                                        AddressOf Me.OnLoadBookPropertiesCompleted)
        Dim category_view = New DomainCollectionView(Of Category)(category_loader, category_source)
        Me._bookProperties(GetType(Category)) = New DomainCollectionModel("Categories", GetType(Category),
                                               category_view, category_source, category_loader)

        Dim shelf_source = New EntityList(Of Shelf)(Me._domainContext.Shelfs)
        Dim shelf_loader = New DomainCollectionViewLoader(Of Shelf)(
                                        AddressOf Me.LoadBookProperties,
                                        AddressOf Me.OnLoadBookPropertiesCompleted)
        Dim shelf_view = New DomainCollectionView(Of Shelf)(shelf_loader, shelf_source)
        Me._bookProperties(GetType(Shelf)) = New DomainCollectionModel("Shelves", GetType(Shelf),
                                               shelf_view, shelf_source, shelf_loader)

        'Initialize Entity Queries Dictionary with a Default Query for each Entity of Request Properties
        Dim book_source = New EntityList(Of Book)(Me._domainContext.Books)
        Dim book_loader = New DomainCollectionViewLoader(Of Book)(
                                        AddressOf Me.LoadRequestProperties,
                                        AddressOf Me.OnLoadRequestPropertiesCompleted)
        Dim book_view = New DomainCollectionView(Of Book)(book_loader, book_source)
        Me._requestProperties(GetType(Book)) = New DomainCollectionModel("Books", GetType(Book),
                                               book_view, book_source, book_loader)
        Dim member_source = New EntityList(Of Member)(Me._domainContext.Members)
        Dim member_loader = New DomainCollectionViewLoader(Of Member)(
                                        AddressOf Me.LoadRequestProperties,
                                        AddressOf Me.OnLoadRequestPropertiesCompleted)
        Dim member_view = New DomainCollectionView(Of Member)(member_loader, member_source)
        Me._requestProperties(GetType(Member)) = New DomainCollectionModel("Members", GetType(Member),
                                               member_view, member_source, member_loader)

        'Initialize Entity Queries Dictionary with a Default Query for each Entity of Review Properties
        Dim book_source1 = New EntityList(Of Book)(Me._domainContext.Books)
        Dim book_loader1 = New DomainCollectionViewLoader(Of Book)(
                                        AddressOf Me.LoadReviewProperties,
                                        AddressOf Me.OnLoadReviewPropertiesCompleted)
        Dim book_view1 = New DomainCollectionView(Of Book)(book_loader1, book_source1)
        Me._reviewProperties(GetType(Book)) = New DomainCollectionModel("Books", GetType(Book),
                                               book_view1, book_source1, book_loader1)
        Dim member_source1 = New EntityList(Of Member)(Me._domainContext.Members)
        Dim member_loader1 = New DomainCollectionViewLoader(Of Member)(
                                        AddressOf Me.LoadReviewProperties,
                                        AddressOf Me.OnLoadReviewPropertiesCompleted)
        Dim member_view1 = New DomainCollectionView(Of Member)(member_loader1, member_source1)
        Me._reviewProperties(GetType(Member)) = New DomainCollectionModel("Members", GetType(Member),
                                               member_view1, member_source1, member_loader1)

        'Initialize Entity Queries Dictionary with a Default Query for each Entity of Transaction Properties
        Dim book_source2 = New EntityList(Of Book)(Me._domainContext.Books)
        Dim book_loader2 = New DomainCollectionViewLoader(Of Book)(
                                        AddressOf Me.LoadTransactionProperties,
                                        AddressOf Me.OnLoadTransactionPropertiesCompleted)
        Dim book_view2 = New DomainCollectionView(Of Book)(book_loader2, book_source2)
        Me._transactionProperties(GetType(Book)) = New DomainCollectionModel("Books", GetType(Book),
                                               book_view2, book_source2, book_loader2)
        Dim member_source2 = New EntityList(Of Member)(Me._domainContext.Members)
        Dim member_loader2 = New DomainCollectionViewLoader(Of Member)(
                                        AddressOf Me.LoadTransactionProperties,
                                        AddressOf Me.OnLoadTransactionPropertiesCompleted)
        Dim member_view2 = New DomainCollectionView(Of Member)(member_loader2, member_source2)
        Me._transactionProperties(GetType(Member)) = New DomainCollectionModel("Members", GetType(Member),
                                               member_view2, member_source2, member_loader2)

    End Sub

#End Region

#Region "Public Methods : Save, Reject, Refresh Data"

    Public Sub SubmitChanges(callback As System.Action(Of Ria.Common.ServiceSubmitChangesResult), state As Object) Implements ILibraryDataService.SubmitChanges
        Me.DomainContext.SubmitChanges(Function(so)
                                           callback(Me.CreateResult(so))
                                       End Function, state)
    End Sub

    Public Sub RejectChanges(callback As System.Action(Of Ria.Common.ServiceSubmitChangesResult), state As Object) Implements ILibraryDataService.RejectChanges
        Me.DomainContext.RejectChanges()
    End Sub

    Public Sub RefreshLibraryDataService() Implements ILibraryDataService.RefreshLibraryDataService
        Me.DomainContext.EntityContainer.Clear()
    End Sub

    Public Sub UpdateAuthorsOfBook(book_id As Integer, author_ids As System.Collections.Generic.List(Of Integer)) Implements ILibraryDataService.UpdateAuthorsOfBook
        Me.DomainContext.UpdateAuthorsOfBook(book_id, author_ids)
    End Sub

    Public Sub UpdateCategoriesOfBook(book_id As Integer, category_ids As System.Collections.Generic.List(Of Integer)) Implements ILibraryDataService.UpdateCategoriesOfBook
        Me.DomainContext.UpdateCategoriesOfBook(book_id, category_ids)
    End Sub

    Public Sub UpdateShelvesOfBook(book_id As Integer, shelf_ids As System.Collections.Generic.List(Of Integer)) Implements ILibraryDataService.UpdateShelvesOfBook
        Me.DomainContext.UpdateShelvesOfBook(book_id, shelf_ids)
    End Sub

    Public Sub RefreshBookPropertiesCollectionViews() Implements ILibraryDataService.RefreshBookPropertiesCollectionViews
        Me._book_Authors_View = Nothing
        Me._book_Categories_View = Nothing
        Me._book_Shelves_View = Nothing
        Me._book_Authors_Loader = Nothing
        Me._book_Categories_Loader = Nothing
        Me._book_Shelves_Loader = Nothing

        Me._selectTopTransactions = 6
    End Sub

    Public Sub RefreshBookFilterCollectionViews() Implements ILibraryDataService.RefreshBookFilterCollectionViews
        Me._book_Authors_Filter_View = Nothing
        Me._book_Categories_Filter_View = Nothing
    End Sub

#End Region

#Region "ICleanup Interface"
    Public Overrides Sub Cleanup()
        'There is no event handler or application message to unregister
    End Sub
#End Region

#Region "Public Methods : Load Data Operations"

    Public Sub LoadAuthors() Implements ILibraryDataService.LoadAuthors
        Me._domainCollections(GetType(Author)).Query = Me.DomainContext.GetActiveAuthorsQuery()
        Me._domainCollections(GetType(Author)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooks() Implements ILibraryDataService.LoadBooks
        Me._domainCollections(GetType(Book)).Query = Me.DomainContext.GetActiveBooksQuery()
        Me._domainCollections(GetType(Book)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadBookByBookID(book_id As Integer) Implements ILibraryDataService.LoadBooksByBookID
        Me._domainCollections(GetType(Book)).Query = Me.DomainContext.GetBook_By_Book_IDQuery(book_id)
        Me._domainCollections(GetType(Book)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooksByAuthors(author_ids As Integer()) Implements ILibraryDataService.LoadBooksByAuthors
        Me._domainCollections(GetType(Book)).Query = Me.DomainContext.GetActiveBooks_By_AuthorsQuery(author_ids)
        Me._domainCollections(GetType(Book)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooksByCategories(category_ids As Integer()) Implements ILibraryDataService.LoadBooksByCategories
        Me._domainCollections(GetType(Book)).Query = Me.DomainContext.GetActiveBooks_By_CategoriesQuery(category_ids)
        Me._domainCollections(GetType(Book)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooksByShelves(shelf_ids As Integer()) Implements ILibraryDataService.LoadBooksByShelves
        Me._domainCollections(GetType(Book)).Query = Me.DomainContext.GetActiveBooks_By_ShelvesQuery(shelf_ids)
        Me._domainCollections(GetType(Book)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadCategories() Implements ILibraryDataService.LoadCategories
        Me._domainCollections(GetType(Category)).Query = Me.DomainContext.GetActiveCategoriesQuery()
        Me._domainCollections(GetType(Category)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadMembers() Implements ILibraryDataService.LoadMembers
        Me._domainCollections(GetType(Member)).Query = Me.DomainContext.GetAllMembersQuery()
        Me._domainCollections(GetType(Member)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadRequests() Implements ILibraryDataService.LoadRequests
        Me._domainCollections(GetType(Member_Requests)).Query = Me.DomainContext.GetAllMember_RequestsQuery(Me._selectTopRequests)
        Me._domainCollections(GetType(Member_Requests)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadCurrentMemberPendingRequests() Implements ILibraryDataService.LoadCurrentMemberPendingRequests
        Me._domainCollections(GetType(Member_Requests)).Query = Me.DomainContext.Get_Current_Member_Pending_RequestsQuery(0)
        Me._domainCollections(GetType(Member_Requests)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadReviews() Implements ILibraryDataService.LoadReviews
        Me._domainCollections(GetType(Member_Reviews)).Query = Me.DomainContext.GetAllMember_ReviewsQuery(Me._selectTopRequests)
        Me._domainCollections(GetType(Member_Reviews)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadCurrentMemberReviews() Implements ILibraryDataService.LoadCurrentMemberReviews
        Me._domainCollections(GetType(Member_Reviews)).Query = Me.DomainContext.Get_Current_Member_ReviewsQuery(Me._selectTopReviews)
    End Sub

    Public Sub LoadTransactions() Implements ILibraryDataService.LoadTransactions
        Me._domainCollections(GetType(Member_Transactions)).Query = Me.DomainContext.GetAllMember_ReviewsQuery(Me._selectTopTransactions)
        Me._domainCollections(GetType(Member_Transactions)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadCurrentMemberTransactions() Implements ILibraryDataService.LoadCurrentMemberTransactions
        Me._domainCollections(GetType(Member_Transactions)).Query = Me.DomainContext.Get_Current_Member_TransactionsQuery(Me._selectTopTransactions / 2)
        Me._domainCollections(GetType(Member_Transactions)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadShelves() Implements ILibraryDataService.LoadShelves
        Me._domainCollections(GetType(Shelf)).Query = Me.DomainContext.GetActiveShelvesQuery()
        Me._domainCollections(GetType(Shelf)).Loader.Load(Nothing)
    End Sub

    Public Sub LoadCategoriesOfBook(ByVal book_id As Integer)
        If Me._bookProperties.ContainsKey(GetType(Category)) Then
            Me._bookProperties(GetType(Category)).Query = Me.DomainContext.GetCategories_Of_BookQuery(book_id)
            Me._bookProperties(GetType(Category)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadAuthorsOfBook(ByVal book_id As Integer)
        If Me._bookProperties.ContainsKey(GetType(Author)) Then
            Me._bookProperties(GetType(Author)).Query = Me.DomainContext.GetAuthors_Of_BookQuery(book_id)
            Me._bookProperties(GetType(Author)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadShelvesOfBook(ByVal book_id As Integer)
        If Me._bookProperties.ContainsKey(GetType(Shelf)) Then
            Me._bookProperties(GetType(Shelf)).Query = Me.DomainContext.GetShelves_Of_BookQuery(book_id)
            Me._bookProperties(GetType(Shelf)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadBookOfRequest(ByVal request_id As Integer)
        If Me._requestProperties.ContainsKey(GetType(Book)) Then
            Me._requestProperties(GetType(Book)).Query = Me.DomainContext.GetBook_Of_RequestQuery(request_id)
            Me._requestProperties(GetType(Book)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadMemberOfRequest(ByVal request_id As Integer)
        If Me._requestProperties.ContainsKey(GetType(Member)) Then
            Me._requestProperties(GetType(Member)).Query = Me.DomainContext.GetMember_Of_RequestQuery(request_id)
            Me._requestProperties(GetType(Member)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadBookOfReview(ByVal review_id As Integer)
        If Me._reviewProperties.ContainsKey(GetType(Book)) Then
            Me._reviewProperties(GetType(Book)).Query = Me.DomainContext.GetBook_Of_ReviewQuery(review_id)
            Me._reviewProperties(GetType(Book)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadMemberOfReview(ByVal review_id As Integer)
        If Me._reviewProperties.ContainsKey(GetType(Member)) Then
            Me._reviewProperties(GetType(Member)).Query = Me.DomainContext.GetMember_Of_ReviewQuery(review_id)
            Me._reviewProperties(GetType(Member)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadBookOfTransaction(ByVal transaction_id As Integer)
        If Me._transactionProperties.ContainsKey(GetType(Book)) Then
            Me._transactionProperties(GetType(Book)).Query = Me.DomainContext.GetBook_Of_TransactionQuery(transaction_id)
            Me._transactionProperties(GetType(Book)).Loader.Load(Nothing)
        End If
    End Sub

    Public Sub LoadMemberOfTransaction(ByVal transaction_id As Integer)
        If Me._transactionProperties.ContainsKey(GetType(Member)) Then
            Me._transactionProperties(GetType(Member)).Query = Me.DomainContext.GetMember_Of_TransactionQuery(transaction_id)
            Me._transactionProperties(GetType(Member)).Loader.Load(Nothing)
        End If
    End Sub

#End Region

#Region "Private Method : Apply Filters"

    Private Function ApplyAuthorFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Author) = entityQuery
        If Not String.IsNullOrEmpty(Me._filterAuthorCareer) Then
            returnQuery = returnQuery.Where(Function(a) a.Author_Career.Contains(Me._filterAuthorCareer))
        End If
        If Not String.IsNullOrEmpty(Me._filterAuthorName) Then
            returnQuery = returnQuery.Where(Function(a) a.Author_First_Name.Contains(Me._filterAuthorName) _
                                                        OrElse _
                                                        a.Author_Last_Name.Contains(Me._filterAuthorName))
        End If
        If Me._filterActiveAuthor Then
            returnQuery = returnQuery.Where(Function(a) a.IsActive)
        End If
        Return returnQuery
    End Function

    Private Function ApplyBookFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Book) = entityQuery
        Dim inputQuery As EntityQuery(Of Book) = entityQuery
        If Not String.IsNullOrEmpty(Me._filterBookDescription) Then
            'returnQuery = returnQuery.Where(Function(b) b.Book_Description.Contains(Me._filterBookDescription))
            Dim query = (From b In Me._domainContext.Books Where b.Book_Description.Contains(Me._filterBookDescription) _
                 Select b). _
             Union( _
                 From b In Me._domainContext.Books Where b.Book_Description.Contains(Me._filterBookDescription) _
                 Select b).OrderBy(Function(d) d.Book_Authors)

            Dim q1 = (From b In Me._domainContext.Books Where b.Book_Description.Contains(Me._filterBookDescription) _
                 Select b)
            Dim q2 = (From b In Me._domainContext.Books Where b.Book_Description.Contains(Me._filterBookDescription) _
                 Select b)
            Dim u = q1.Union(q2)
            Return u
        End If
        If Not String.IsNullOrEmpty(Me._filterBookISBN) Then
            returnQuery = returnQuery.Where(Function(b) b.Book_ISBN.Contains(Me._filterBookISBN))
        End If
        If Not String.IsNullOrEmpty(Me._filterBookLanguage) Then
            returnQuery = returnQuery.Where(Function(b) b.Book_Language.Contains(Me._filterBookLanguage))
        End If
        If Not String.IsNullOrEmpty(Me._filterBookMedia) Then
            returnQuery = returnQuery.Where(Function(b) b.Book_Media.Contains(Me._filterBookMedia))
        End If
        If Not String.IsNullOrEmpty(Me._filterBookPublished) Then
            returnQuery = returnQuery.Where(Function(b) b.Book_Published Is Nothing _
                                                        OrElse _
                                                        CType(b.Book_Published, DateTime) >= Me._filterBookPublished)
        End If
        If Not String.IsNullOrEmpty(Me._filterBookTitle) Then
            returnQuery = returnQuery.Where(Function(b) b.Book_Title.Contains(Me._filterBookTitle))
        End If
        If Me._filterActiveBook Then
            returnQuery = returnQuery.Where(Function(b) b.IsActive)
        End If
        If Not IsNothing(Me._filterBookByAuthors) Then
            returnQuery = returnQuery.Where(Function(b) b.Book_Authors.Intersect(_filterBookByAuthors).Count > 0)
        End If
        If Not IsNothing(Me._filterBookByCategories) Then
            returnQuery = returnQuery.Where(Function(b) b.Book_Categories.Intersect(_filterBookByCategories).Count > 0)
        End If
        Return returnQuery
    End Function

    Private Function ApplyCategoryFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Category) = entityQuery
        If Not String.IsNullOrEmpty(Me._filterCategoryName) Then
            returnQuery = returnQuery.Where(Function(c) c.Category_Name.Contains(Me._filterCategoryName))
        End If
        If Me._filterActiveCategory Then
            returnQuery = returnQuery.Where(Function(c) c.IsActive)
        End If
        Return returnQuery
    End Function

    Private Function ApplyMemberFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Member) = entityQuery
        If Not String.IsNullOrEmpty(Me._filterMemberAddress) Then
            returnQuery = returnQuery.Where(Function(m) m.Address_Line_1.Contains(Me._filterMemberAddress) _
                                                        OrElse _
                                                        m.Address_Line_2.Contains(Me._filterMemberAddress) _
                                                        OrElse _
                                                        m.City.Contains(Me._filterMemberAddress) _
                                                        OrElse _
                                                        m.Country.Contains(Me._filterMemberAddress) _
                                                        OrElse _
                                                        m.Province.Contains(Me._filterMemberAddress))
        End If
        If Not String.IsNullOrEmpty(Me._filterMemberEmail) Then
            returnQuery = returnQuery.Where(Function(m) m.Email.Contains(Me._filterMemberEmail))
        End If
        If Not String.IsNullOrEmpty(Me._filterMemberLogin) Then
            returnQuery = returnQuery.Where(Function(m) m.MemberLogin.Contains(Me._filterMemberLogin))
        End If
        If Not String.IsNullOrEmpty(Me._filterMemberName) Then
            returnQuery = returnQuery.Where(Function(m) m.Name.Contains(Me._filterMemberName))
        End If
        If Not String.IsNullOrEmpty(Me._filterMemberPhone) Then
            returnQuery = returnQuery.Where(Function(m) m.Phone.Contains(Me._filterMemberPhone))
        End If
        If Not String.IsNullOrEmpty(Me._filterMemberPIN) Then
            returnQuery = returnQuery.Where(Function(m) m.Library_PIN.Contains(Me._filterMemberPIN))
        End If
        If Not String.IsNullOrEmpty(Me._filterMemberRole) Then
            returnQuery = returnQuery.Where(Function(m) m.Roles.Contains(Me._filterMemberRole))
        End If
        If Me._filterActiveMember Then
            returnQuery = returnQuery.Where(Function(m) m.IsActive)
        End If
        Return returnQuery
    End Function

    Private Function ApplyRequestFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Member_Requests) = entityQuery
        Dim yearInteger As Integer = Me._filterRequestExpectedDate.Year
        If yearInteger > 2010 Then
            returnQuery = returnQuery.Where(Function(r) r.Expected_Date Is Nothing _
                                                        OrElse _
                                                        CType(r.Expected_Date, DateTime) <= Me._filterRequestExpectedDate)
        End If
        Return returnQuery
    End Function

    Private Function ApplyReviewFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Member_Reviews) = entityQuery
        If Not String.IsNullOrEmpty(Me._filterReview) Then
            returnQuery = returnQuery.Where(Function(r) r.Review.Contains(Me._filterReview))
        End If
        Return returnQuery
    End Function

    Private Function ApplyShelfFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Shelf) = entityQuery
        If Not String.IsNullOrEmpty(Me._filterShelf) Then
            returnQuery = returnQuery.Where(Function(s) s.Shelf_Code.Contains(Me._filterShelf) _
                                                        OrElse _
                                                        s.Shelf_Description.Contains(Me._filterShelf))
        End If
        If Me._filterActiveShelf Then
            returnQuery = returnQuery.Where(Function(s) s.IsActive)
        End If
        Return returnQuery
    End Function

    Private Function ApplyTransactionFilter(entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        Dim returnQuery As EntityQuery(Of Member_Transactions) = entityQuery
        Dim yearInteger As Integer = Me._filterRequestExpectedDate.Year
        If yearInteger > 2010 Then
            returnQuery = returnQuery.Where(Function(t) t.Expected_Return Is Nothing _
                                                        OrElse _
                                                        CType(t.Expected_Return, DateTime) <= Me._filterTransactionExpectedReturn)
        End If
        Return returnQuery
    End Function

    Private Function ApplyFilter(type As Type, entityQuery As System.ServiceModel.DomainServices.Client.EntityQuery) As System.ServiceModel.DomainServices.Client.EntityQuery
        If type Is GetType(Author) Then
            Return ApplyAuthorFilter(entityQuery)
        ElseIf type Is GetType(Book) Then
            Return ApplyBookFilter(entityQuery)
        ElseIf type Is GetType(Category) Then
            Return ApplyCategoryFilter(entityQuery)
        ElseIf type Is GetType(Member) Then
            Return ApplyMemberFilter(entityQuery)
        ElseIf type Is GetType(Member_Requests) Then
            Return ApplyRequestFilter(entityQuery)
        ElseIf type Is GetType(Member_Reviews) Then
            Return ApplyReviewFilter(entityQuery)
        ElseIf type Is GetType(Shelf) Then
            Return ApplyShelfFilter(entityQuery)
        ElseIf type Is GetType(Member_Transactions) Then
            Return ApplyTransactionFilter(entityQuery)
        End If
        Return entityQuery
    End Function

#End Region

#Region "Private Methods : Load Data"

    Private Function LoadEntities(Of T As Entity)() As LoadOperation(Of T)

        If Me._pendingLoads.Count > 0 Then
            If Me._pendingLoads.ContainsKey(GetType(T)) Then
                If Me._pendingLoads(GetType(T)).CanCancel Then
                    Me._pendingLoads(GetType(T)).Cancel()
                    Me._pendingLoads.Remove(GetType(T))
                Else
                    Return Nothing
                End If
            End If
        End If

        Dim entityQuery As EntityQuery(Of T) = CType(Me._domainCollections(GetType(T)), DomainCollectionModel).Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        'Apply Filters
        entityQuery = ApplyFilter(GetType(T), entityQuery)
        'If (GetType(T) Is GetType(Book)) Then
        '    Dim bookView = CType(Me._domainCollections(GetType(T)), DomainCollectionModel).View
        '    Dim modifiedQuery = entityQuery.SortAndPageBy(bookView)
        'End If
        Me._pendingLoads(GetType(T)) =
            Me.DomainContext.Load(entityQuery.SortAndPageBy(CType(Me._domainCollections(GetType(T)), DomainCollectionModel).View))

        Me.CanLoad = False
        Return Me._pendingLoads(GetType(T))

    End Function

    Private Sub OnLoadEntitiesCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        Me._pendingLoads.Remove(GetType(T))

        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Dim domainCollectionModel As DomainCollectionModel = CType(Me._domainCollections(GetType(T)), DomainCollectionModel)
            CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
            If op.AllEntities.Count <> -1 Then
                If op.TotalEntityCount <> -1 Then
                    CType(Me._domainCollections(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                End If
                If (GetType(T) Is GetType(Book)) Then
                    For Each book As Book In CType(op.Entities, IEnumerable(Of Book))
                        Me.LoadAuthorsOfBook(book.Book_id)
                        Me.LoadCategoriesOfBook(book.Book_id)
                        Me.LoadShelvesOfBook(book.Book_id)
                    Next
                ElseIf (GetType(T) Is GetType(Member_Requests)) Then
                    For Each request As Member_Requests In CType(op.Entities, IEnumerable(Of Member_Requests))
                        Me.LoadBookOfRequest(request.Request_id)
                        Me.LoadMemberOfRequest(request.Request_id)
                    Next
                ElseIf (GetType(T) Is GetType(Member_Reviews)) Then
                    For Each review As Member_Reviews In CType(op.Entities, IEnumerable(Of Member_Reviews))
                        Me.LoadBookOfReview(review.Review_id)
                        Me.LoadMemberOfReview(review.Review_id)
                    Next
                ElseIf (GetType(T) Is GetType(Member_Transactions)) Then
                    For Each transaction As Member_Transactions In CType(op.Entities, IEnumerable(Of Member_Transactions))
                        Me.LoadBookOfTransaction(transaction.Transaction_id)
                        Me.LoadMemberOfTransaction(transaction.Transaction_id)
                    Next
                ElseIf (GetType(T) Is GetType(Author)) Then
                    Me.RaisePropertyChanged("AuthorsLoaded")
                ElseIf (GetType(T) Is GetType(Category)) Then
                    Me.RaisePropertyChanged("CategoriesLoaded")
                End If
            End If
            CType(Me._domainCollections(GetType(T)), DomainCollectionModel).View.MoveCurrentToFirst()
        End If

    End Sub

    Private Function LoadBookProperties(Of T As Entity)() As LoadOperation(Of T)

        Dim collectionModel As DomainCollectionModel = CType(Me._bookProperties(GetType(T)), DomainCollectionModel)
        Dim entityQuery As EntityQuery(Of T) = collectionModel.Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        Me.CanLoad = False
        Return Me.DomainContext.Load(entityQuery.SortAndPageBy(collectionModel.View))

    End Function

    Private Sub OnLoadBookPropertiesCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Dim domainCollectionModel As DomainCollectionModel =
                    CType(Me._bookProperties(GetType(T)), DomainCollectionModel)

            Dim bookCollectionModel As DomainCollectionModel =
                CType(Me._domainCollections(GetType(Book)), DomainCollectionModel)
            CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
            If op.AllEntities.Count <> -1 Then

                If op.TotalEntityCount <> -1 Then
                    CType(Me._bookProperties(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                End If

                Dim book_id = op.EntityQuery.Parameters("book_id")
                Dim bookCollection = CType(bookCollectionModel.Source, EntityList(Of Book)).Source.AsQueryable
                Dim book = From b In bookCollection
                           Where b.Book_id = book_id
                           Select b
                Dim bookRef = book.FirstOrDefault
                If Not bookRef Is Nothing Then
                    If GetType(T) Is GetType(Category) Then
                        Dim bookCategories = New ObservableCollection(Of Category)
                        For Each c As Category In CType(domainCollectionModel.Source, EntityList(Of Category)).Source
                            bookCategories.Add(c)
                        Next
                        bookRef.Book_Categories = bookCategories
                    ElseIf GetType(T) Is GetType(Author) Then
                        Dim bookAuthors = New ObservableCollection(Of Author)
                        For Each a As Author In CType(domainCollectionModel.Source, EntityList(Of Author)).Source
                            bookAuthors.Add(a)
                        Next
                        bookRef.Book_Authors = bookAuthors
                    ElseIf GetType(T) Is GetType(Shelf) Then
                        Dim bookShelves = New ObservableCollection(Of Shelf)
                        For Each s As Shelf In CType(domainCollectionModel.Source, EntityList(Of Shelf)).Source
                            bookShelves.Add(s)
                        Next
                        bookRef.Book_Shelves = bookShelves
                    End If
                    CType(Me._bookProperties(GetType(T)), DomainCollectionModel).View.MoveCurrentToFirst()
                End If
            End If
        Else
            'If query was cancelled, do it again to get all books' properties
            bookPropertiesLoadQueue.Enqueue(op.EntityQuery)
        End If

        'If there are pending query of book properties, do it
        If bookPropertiesLoadQueue.Count > 0 Then
            Dim entityQuery As EntityQuery = bookPropertiesLoadQueue.Dequeue()
            If TypeOf (entityQuery) Is EntityQuery(Of Author) Then
                Me._bookProperties(GetType(Author)).Query = entityQuery
                Me._bookProperties(GetType(Author)).Loader.Load(Nothing)
            ElseIf TypeOf (entityQuery) Is EntityQuery(Of Category) Then
                Me._bookProperties(GetType(Category)).Query = entityQuery
                Me._bookProperties(GetType(Category)).Loader.Load(Nothing)
            ElseIf TypeOf (entityQuery) Is EntityQuery(Of Shelf) Then
                Me._bookProperties(GetType(Shelf)).Query = entityQuery
                Me._bookProperties(GetType(Shelf)).Loader.Load(Nothing)
            End If
        End If

    End Sub

    Private Function LoadTopRequests() As LoadOperation(Of Books_By_Requests)
        Me.CanLoad = False
        Dim query As EntityQuery(Of Books_By_Requests)
        query = Me._domainContext.Get_Top_Requests_BooksQuery(Me._selectTopRequests)
        Return Me._domainContext.Load(query.SortAndPageBy(Me._top_Requests))
    End Function

    Private Sub OnLoadTopRequestsCompleted(op As LoadOperation(Of Books_By_Requests))
        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Me._top_Requests_source.Source = op.Entities
            If op.TotalEntityCount <> -1 Then
                Me._top_Requests.SetTotalItemCount(op.TotalEntityCount)
            End If
            Me._top_Requests.MoveCurrentToFirst()
            Me.RaisePropertyChanged("TopRequestsLoaded")
        End If
    End Sub

    Private Function LoadTopReviews() As LoadOperation(Of Books_By_Reviews)
        Me.CanLoad = False
        Dim query As EntityQuery(Of Books_By_Reviews)
        query = Me._domainContext.Get_Top_Reviews_BooksQuery(Me._selectTopReviews)
        Return Me._domainContext.Load(query.SortAndPageBy(Me._top_Reviews))
    End Function

    Private Sub OnLoadTopReviewsCompleted(op As LoadOperation(Of Books_By_Reviews))
        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Me._top_Reviews_source.Source = op.Entities
            If op.TotalEntityCount <> -1 Then
                Me._top_Reviews.SetTotalItemCount(op.TotalEntityCount)
            End If
            Me._top_Reviews.MoveCurrentToFirst()
            Me.RaisePropertyChanged("TopReviewsLoaded")
        End If
    End Sub

    Private Function CreateResult(op As SubmitOperation) As ServiceSubmitChangesResult
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        End If
        Return New ServiceSubmitChangesResult(
                                        op.ChangeSet,
                                        op.EntitiesInError,
                                        op.Error,
                                        op.IsCanceled,
                                        op.UserState)
    End Function

    Private Function LoadRequestProperties(Of T As Entity)() As LoadOperation(Of T)

        Dim collectionModel As DomainCollectionModel = CType(Me._requestProperties(GetType(T)), DomainCollectionModel)
        Dim entityQuery As EntityQuery(Of T) = collectionModel.Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        Me.CanLoad = False
        Return Me.DomainContext.Load(entityQuery.SortAndPageBy(collectionModel.View))

    End Function

    Private Sub OnLoadRequestPropertiesCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Dim domainCollectionModel As DomainCollectionModel =
                    CType(Me._requestProperties(GetType(T)), DomainCollectionModel)

            Dim requestCollectionModel As DomainCollectionModel =
                CType(Me._domainCollections(GetType(Member_Requests)), DomainCollectionModel)
            CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
            If op.AllEntities.Count <> -1 Then

                If op.TotalEntityCount <> -1 Then
                    CType(Me._requestProperties(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                End If

                Dim request_id = op.EntityQuery.Parameters("request_id")
                Dim requestCollection = CType(requestCollectionModel.Source, EntityList(Of Member_Requests)).Source.AsQueryable
                Dim request = From r In requestCollection
                              Where r.Request_id = request_id
                              Select r
                Dim requestRef = request.FirstOrDefault
                If Not requestRef Is Nothing Then
                    If GetType(T) Is GetType(Book) Then
                        requestRef.Book = CType(domainCollectionModel.Source, EntityList(Of Book)).Source.ElementAtOrDefault(0)
                    ElseIf GetType(T) Is GetType(Member) Then
                        requestRef.Member = CType(domainCollectionModel.Source, EntityList(Of Member)).Source.ElementAtOrDefault(0)
                    End If
                End If
            Else
                Me.RaisePropertyChanged("RequestsLoaded")
            End If
        Else
            'If query was cancelled, do it again to get all books' properties
            requestPropertiesLoadQueue.Enqueue(op.EntityQuery)
        End If

        'If there are pending query of request properties, do it
        If requestPropertiesLoadQueue.Count > 0 Then
            Dim entityQuery As EntityQuery = requestPropertiesLoadQueue.Dequeue()
            If TypeOf (entityQuery) Is EntityQuery(Of Book) Then
                Me._requestProperties(GetType(Book)).Query = entityQuery
                Me._requestProperties(GetType(Book)).Loader.Load(Nothing)
            ElseIf TypeOf (entityQuery) Is EntityQuery(Of Member) Then
                Me._requestProperties(GetType(Member)).Query = entityQuery
                Me._requestProperties(GetType(Member)).Loader.Load(Nothing)
            End If
        Else
            Me.RaisePropertyChanged("RequestsLoaded")
        End If

    End Sub

    Private Function LoadReviewProperties(Of T As Entity)() As LoadOperation(Of T)

        Dim collectionModel As DomainCollectionModel = CType(Me._reviewProperties(GetType(T)), DomainCollectionModel)
        Dim entityQuery As EntityQuery(Of T) = collectionModel.Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        Me.CanLoad = False
        Return Me.DomainContext.Load(entityQuery.SortAndPageBy(collectionModel.View))

    End Function

    Private Sub OnLoadReviewPropertiesCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Dim domainCollectionModel As DomainCollectionModel =
                    CType(Me._reviewProperties(GetType(T)), DomainCollectionModel)

            Dim reviewCollectionModel As DomainCollectionModel =
                CType(Me._domainCollections(GetType(Member_Reviews)), DomainCollectionModel)
            CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
            If op.AllEntities.Count <> -1 Then

                If op.TotalEntityCount <> -1 Then
                    CType(Me._reviewProperties(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                End If

                Dim review_id = op.EntityQuery.Parameters("review_id")
                Dim reviewCollection = CType(reviewCollectionModel.Source, EntityList(Of Member_Reviews)).Source.AsQueryable
                Dim review = From r In reviewCollection
                              Where r.Review_id = review_id
                              Select r
                Dim reviewRef = review.FirstOrDefault
                If Not reviewRef Is Nothing Then
                    If GetType(T) Is GetType(Book) Then
                        reviewRef.Book = CType(domainCollectionModel.Source, EntityList(Of Book)).Source.ElementAtOrDefault(0)
                    ElseIf GetType(T) Is GetType(Member) Then
                        reviewRef.Member = CType(domainCollectionModel.Source, EntityList(Of Member)).Source.ElementAtOrDefault(0)
                    End If
                End If
            End If
        Else
            'If query was cancelled, do it again to get all books' properties
            reviewPropertiesLoadQueue.Enqueue(op.EntityQuery)
        End If

        'If there are pending query of review properties, do it
        If reviewPropertiesLoadQueue.Count > 0 Then
            Dim entityQuery As EntityQuery = reviewPropertiesLoadQueue.Dequeue()
            If TypeOf (entityQuery) Is EntityQuery(Of Book) Then
                Me._reviewProperties(GetType(Book)).Query = entityQuery
                Me._reviewProperties(GetType(Book)).Loader.Load(Nothing)
            ElseIf TypeOf (entityQuery) Is EntityQuery(Of Member) Then
                Me._reviewProperties(GetType(Member)).Query = entityQuery
                Me._reviewProperties(GetType(Member)).Loader.Load(Nothing)
            End If
        Else
            Me.RaisePropertyChanged("ReviewsLoaded")
        End If

    End Sub

    Private Function LoadTransactionProperties(Of T As Entity)() As LoadOperation(Of T)

        Dim collectionModel As DomainCollectionModel = CType(Me._transactionProperties(GetType(T)), DomainCollectionModel)
        Dim entityQuery As EntityQuery(Of T) = collectionModel.Query
        If entityQuery Is Nothing Then
            Return Nothing
        End If

        Me.CanLoad = False
        Return Me.DomainContext.Load(entityQuery.SortAndPageBy(collectionModel.View))

    End Function

    Private Sub OnLoadTransactionPropertiesCompleted(Of T As Entity)(op As LoadOperation(Of T))

        Me.CanLoad = True
        If op.HasError Then
            op.MarkErrorAsHandled()
            If (Not ErrorWindow.AskingLogin) And (Not ErrorWindow.Showing) Then
                ErrorWindow.CreateNew(op.Error)
            End If
        ElseIf Not op.IsCanceled Then
            Dim domainCollectionModel As DomainCollectionModel =
                    CType(Me._transactionProperties(GetType(T)), DomainCollectionModel)

            Dim transactionCollectionModel As DomainCollectionModel =
                CType(Me._domainCollections(GetType(Member_Transactions)), DomainCollectionModel)
            CType(domainCollectionModel.Source, EntityList(Of T)).Source = op.Entities
            If op.AllEntities.Count <> -1 Then

                If op.TotalEntityCount <> -1 Then
                    CType(Me._transactionProperties(GetType(T)), DomainCollectionModel).View.SetTotalItemCount(op.TotalEntityCount)
                End If

                Dim transaction_id = op.EntityQuery.Parameters("transaction_id")
                Dim transactionCollection = CType(transactionCollectionModel.Source, EntityList(Of Member_Transactions)).Source.AsQueryable
                Dim transaction = From tr In transactionCollection _
                                  Where tr.Transaction_id = transaction_id _
                                  Select tr
                Dim transactionRef = transaction.FirstOrDefault
                If Not transactionRef Is Nothing Then
                    If GetType(T) Is GetType(Book) Then
                        transactionRef.Book = CType(domainCollectionModel.Source, EntityList(Of Book)).Source.ElementAtOrDefault(0)
                    ElseIf GetType(T) Is GetType(Member) Then
                        transactionRef.Member = CType(domainCollectionModel.Source, EntityList(Of Member)).Source.ElementAtOrDefault(0)
                    End If
                End If
            End If
        Else
            'If query was cancelled, do it again to get all books' properties
            transactionPropertiesLoadQueue.Enqueue(op.EntityQuery)
        End If

        'If there are pending query of review properties, do it
        If transactionPropertiesLoadQueue.Count > 0 Then
            Dim entityQuery As EntityQuery = transactionPropertiesLoadQueue.Dequeue()
            If TypeOf (entityQuery) Is EntityQuery(Of Book) Then
                Me._transactionProperties(GetType(Book)).Query = entityQuery
                Me._transactionProperties(GetType(Book)).Loader.Load(Nothing)
            ElseIf TypeOf (entityQuery) Is EntityQuery(Of Member) Then
                Me._transactionProperties(GetType(Member)).Query = entityQuery
                Me._transactionProperties(GetType(Member)).Loader.Load(Nothing)
            End If
        Else
            Me.RaisePropertyChanged("TransactionsLoaded")
        End If

    End Sub

#End Region

End Class
