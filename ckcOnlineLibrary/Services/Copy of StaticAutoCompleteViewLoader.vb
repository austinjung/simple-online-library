﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common

Public Class StaticAutoCompleteViewLoader(Of TEntity As Entity)
    Inherits CollectionViewLoader

    Private _target As IQueryable(Of TEntity)
    Private _type As Type

    Private _selectedList As IEnumerable(Of TEntity)
    Private _source As IEnumerable(Of TEntity)
    Private _collectionViewSource As EntityList(Of TEntity)
    Private _filter As String
    Public Property Filter As String
        Get
            Return _filter
        End Get
        Set(value As String)
            _filter = value
            DoQuery()
            SortBySelected()
            _collectionViewSource.Source = _source
        End Set
    End Property

    Public ReadOnly Property Source As IEnumerable(Of TEntity)
        Get
            Return _source
        End Get
    End Property

    Public ReadOnly Property CollectionViewSource As EntityList(Of TEntity)
        Get
            Return _collectionViewSource
        End Get
    End Property

    Public Property SelectedList As IEnumerable(Of TEntity)
        Get
            Return _selectedList
        End Get
        Set(value As IEnumerable(Of TEntity))
            _selectedList = value
            initializeIsChecked(False, _source)
            initializeIsChecked(True, _selectedList)
            DoQuery()
            SortBySelected()
            _collectionViewSource.Source = _source
        End Set
    End Property

    Public Sub New(ByRef collectionViewSource As EntityList(Of TEntity), entities As IQueryable(Of TEntity))
        _target = entities
        _collectionViewSource = collectionViewSource
        _type = GetType(TEntity)
        DoQuery()
        initializeIsChecked(False, _source)
    End Sub

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        DoQuery()
        SortBySelected()
        _collectionViewSource.Source = _source
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

    Public Sub DoQuery()
        If _type Is GetType(Author) Then
            DoQueryAuthor(_target)
        ElseIf _type Is GetType(Category) Then
            DoQueryCategory(_target)
        ElseIf _type Is GetType(Shelf) Then
            DoQueryShelf(_target)
        End If
    End Sub

    Private Sub DoQueryAuthor(target As IQueryable(Of Author))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From a In target
                         Where a.Author_First_Name.Contains(_filter) _
                         OrElse a.Author_Last_Name.Contains(_filter) _
                         Order By a.Author_Full_Name _
                         Select a
            _source = result
        Else
            Dim result = From a In target
                         Order By a.Author_Full_Name _
                         Select a
            _source = result
        End If
    End Sub

    Private Sub DoQueryCategory(target As IQueryable(Of Category))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From c In target
                         Where c.Category_Name.Contains(_filter) _
                         Order By c.Category_Name _
                         Select c
            _source = result
        Else
            Dim result = From c In target
                         Order By c.Category_Name _
                         Select c
            _source = result
        End If
    End Sub

    Private Sub DoQueryShelf(target As IQueryable(Of Shelf))
        If Not String.IsNullOrEmpty(_filter) Then
            Dim result = From s In target
                         Where s.Shelf_Description.Contains(_filter) _
                         Order By s.Shelf_Code _
                         Select s
            _source = result
        Else
            Dim result = From s In target
                         Order By s.Shelf_Code _
                         Select s
            _source = result
        End If
    End Sub

    Private Sub SortBySelected()
        If Not IsNothing(_selectedList) Then
            Dim tmpSource = _source.ToList()
            Dim tgSource = copyEntityList(_selectedList)
            For Each e In tmpSource
                If Not _selectedList.Contains(e) Then
                    tgSource.Add(e)
                End If
            Next
            _source = tgSource
        End If
    End Sub

    Private Function copyEntityList(selectedList As IEnumerable(Of TEntity)) As List(Of TEntity)
        Dim copiedList = New List(Of TEntity)
        For Each e In selectedList
            copiedList.Add(e)
        Next
        Return copiedList
    End Function

    Private Sub initializeIsChecked(value As Boolean, list As IEnumerable(Of TEntity))
        If _type Is GetType(Author) Then
            initializeIsCheckedAuthor(value, list)
        ElseIf _type Is GetType(Category) Then
            initializeIsCheckedCategory(value, list)
        ElseIf _type Is GetType(Shelf) Then
            initializeIsCheckedShelf(value, list)
        End If
    End Sub

    Private Sub initializeIsCheckedAuthor(value As Boolean, list As IEnumerable(Of Author))
        If Not IsNothing(list) Then
            For Each author In list
                author.IsChecked = value
            Next
        End If
    End Sub

    Private Sub initializeIsCheckedCategory(value As Boolean, list As IEnumerable(Of Category))
        If Not IsNothing(list) Then
            For Each category In list
                category.IsChecked = value
            Next
        End If
    End Sub

    Private Sub initializeIsCheckedShelf(value As Boolean, list As IEnumerable(Of Shelf))
        If Not IsNothing(list) Then
            For Each shelf In list
                shelf.IsChecked = value
            Next
        End If
    End Sub

End Class

