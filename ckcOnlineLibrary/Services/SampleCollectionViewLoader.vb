﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common

Public Class SampleCollectionViewLoader(Of TEntity As Entity)
    Inherits CollectionViewLoader

    Private ReadOnly _load As Action(Of Action(Of ServiceLoadResult(Of TEntity)), Object)
    Private ReadOnly _cancelLoad As Action(Of Object)
    Private ReadOnly _onLoadCompleted As Action(Of ServiceLoadResult(Of TEntity))

    Private _currentUserState As Object

    Public Sub New(
                  load As Action(Of Action(Of ServiceLoadResult(Of TEntity)), Object),
                  cancelLoad As Action(Of Object),
                  onLoadCompleted As Action(Of ServiceLoadResult(Of TEntity)))
        If load Is Nothing Then
            Throw New ArgumentNullException("load")
        End If
        If onLoadCompleted Is Nothing Then
            Throw New ArgumentNullException("onLoadCompleted")
        End If

        Me._load = load
        Me._cancelLoad = cancelLoad
        Me._onLoadCompleted = onLoadCompleted
    End Sub

    Public Overrides ReadOnly Property CanLoad() As Boolean
        Get
            Return True
        End Get
    End Property

    Private Property CurrentUserState() As Object
        Get
            Return Me._currentUserState
        End Get

        Set(value As Object)
            If Me._currentUserState <> value Then
                If Me._cancelLoad IsNot Nothing Then
                    Me._cancelLoad(Me._currentUserState)
                End If
            End If

            Me._currentUserState = value
        End Set
    End Property

    Public Overrides Sub Load(userState As Object)
        If Not Me.CanLoad Then
            Throw New InvalidOperationException("Load cannot be called when CanLoad is false")
        End If
        Me.CurrentUserState = userState
        Me._load(AddressOf Me.OnLoadCompleted, userState)
    End Sub

    Private Overloads Sub OnLoadCompleted(result As ServiceLoadResult(Of TEntity))
        Me._onLoadCompleted(result)

        If Me.CurrentUserState = result.UserState Then
            Me.CurrentUserState = Nothing
        End If

        MyBase.OnLoadCompleted(result)
    End Sub
End Class
