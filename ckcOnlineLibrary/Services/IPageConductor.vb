﻿Imports System

Public Interface IPageConductor

    Sub DisplayError(origin As String, e As Exception, details As String)
    Sub DisplayError(origin As String, e As Exception)

    Sub GoToView(viewToken As String)
    'void GoToTweetDetailView(string viewToken, string stateKey, Tweet tweet);
    'void GoToNewTweetView(string viewToken, string stateKey, TweetType tweetType, string text, long replyToId);
    Sub GoToCheckoutView(viewToken As String, stateKey As String, book As Book)
    Sub GoBack()

    Sub PushState(key As String, value As Object)
    Function PopState(Of T As Class)(key As String) As T
    Function PeekState(Of T As Class)(key As String) As T
    'void ShowEditBook(Book selectedBook);

End Interface