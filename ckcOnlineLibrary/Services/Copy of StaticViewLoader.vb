﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common

Public Class StaticViewLoader(Of TEntity As Entity)
    Inherits CollectionViewLoader

    Private _transactionEntityList As List(Of Member_Transactions)
    Private _type As Type
    Private _source As IEnumerable(Of TEntity)
    Private _collectionViewSource As EntityList(Of TEntity)

    Public ReadOnly Property Source As IEnumerable(Of TEntity)
        Get
            Return _source
        End Get
    End Property

    Public ReadOnly Property CollectionViewSource As EntityList(Of TEntity)
        Get
            Return _collectionViewSource
        End Get
    End Property

    Public Sub New(ByRef collectionViewSource As EntityList(Of TEntity), entities As IQueryable(Of TEntity))
        _type = GetType(TEntity)
        cloneEntity(entities)
        _collectionViewSource = collectionViewSource
    End Sub

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        _collectionViewSource.Source = _source
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

    Public Sub AddEntityTransaction(t As Member_Transactions)
        If IsNothing(_transactionEntityList) Then
            _transactionEntityList = New List(Of Member_Transactions)
        End If
        Dim transaction = New Member_Transactions With {
                .Transaction_id = t.Transaction_id,
                .MemberID = t.MemberID,
                .Book_id = t.Book_id,
                .Comment = t.Comment,
                .Requested_Date = t.Requested_Date,
                .Issued_Date = t.Issued_Date,
                .Expected_Return = t.Expected_Return,
                .Returned_Date = t.Returned_Date
                }
        _transactionEntityList.Add(transaction)
        _source = _transactionEntityList
    End Sub

    Public Sub RemoveEntityTransactionAt(index As Integer)
        If IsNothing(_transactionEntityList) Then
            Return
        ElseIf _transactionEntityList.Count <= index Then
            Return
        End If
        _transactionEntityList.RemoveAt(index)
        _source = _transactionEntityList
    End Sub

    Private Sub cloneEntity(entityList As IQueryable(Of TEntity))
        If _type Is GetType(Member_Transactions) Then
            cloneEntityTransactions(entityList)
        End If
    End Sub

    Private Sub cloneEntityTransactions(entityList As IQueryable(Of Member_Transactions))
        If Not IsNothing(entityList) Then
            _transactionEntityList = New List(Of Member_Transactions)
            For Each t In entityList
                Dim transaction = New Member_Transactions With {
                    .Transaction_id = t.Transaction_id,
                    .MemberID = t.MemberID,
                    .Book_id = t.Book_id,
                    .Comment = t.Comment,
                    .Requested_Date = t.Requested_Date,
                    .Issued_Date = t.Issued_Date,
                    .Expected_Return = t.Expected_Return,
                    .Returned_Date = t.Returned_Date
                    }
                _transactionEntityList.Add(transaction)
            Next
            _source = _transactionEntityList
        End If
    End Sub

End Class

