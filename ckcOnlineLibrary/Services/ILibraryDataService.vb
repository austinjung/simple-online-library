﻿Imports System
Imports System.ServiceModel.DomainServices.Client
Imports Microsoft.Windows.Data.DomainServices
Imports Papa.Common
Imports Ria.Common
Imports System.ComponentModel

Public Interface ILibraryDataService

#Region "Properties : Domain Context & Collection Views & Loaders"

    'Domain Context & Collection Views
    ReadOnly Property DomainContext As ckcLibraryDomainContext
    Property AuthorCollectionView As ICollectionView
    Property BookCollectionView As ICollectionView
    Property CategoryCollectionView As ICollectionView
    Property MemberCollectionView As ICollectionView
    Property RequestCollectionView As ICollectionView
    ReadOnly Property RequestBookIDCollection As List(Of Integer)
    Property ReviewCollectionView As ICollectionView
    Property TransactionCollectionView As ICollectionView
    Property ExpectedAllMemberReturnCollectionView As ICollectionView
    Property ExpectedCurrentMemberReturnCollectionView As ICollectionView
    Property TransactionDetailCollectionView As ICollectionView
    Property ShelfCollectionView As ICollectionView

    Property TopRequestsCollectionView As ICollectionView
    Property TopReviewsCollectionView As ICollectionView

    Property CurrentUserInformationCollectionView As ICollectionView
    Property CurrentUserNewBookRequestCollectionView As ICollectionView
    Property NewBookRequestCollectionView As ICollectionView

    Property TemplateAnswerCollectionView As ICollectionView

    'Static AutoComplete Collection Views for a Selected/Edited Book
    Property BookAuthorCollectionView As ICollectionView
    ReadOnly Property BookAuthorCollectionLoader As StaticAutoCompleteViewLoader(Of Author)
    Property BookCategoryCollectionView As ICollectionView
    ReadOnly Property BookCategoryCollectionLoader As StaticAutoCompleteViewLoader(Of Category)
    Property BookShelfCollectionView As ICollectionView
    ReadOnly Property BookShelfCollectionLoader As StaticAutoCompleteViewLoader(Of Shelf)

    'Static AutoComplete Collection Views for filter of search Book
    Property BookAuthorFilterCollectionView As ICollectionView
    ReadOnly Property BookAuthorFilterCollectionLoader As StaticFilterAutoCompleteViewLoader(Of Author)
    Property BookCategoryFilterCollectionView As ICollectionView
    ReadOnly Property BookCategoryFilterCollectionLoader As StaticFilterAutoCompleteViewLoader(Of Category)

#End Region

#Region "Properties : Data Service Control Status"

    'Data Service Control Status
    Property CanLoad As Boolean

#End Region

#Region "Properties : Filters on Load Operations"

    'Filters on Load Operations
    Property FilterBookTitle As String
    Property FilterBookDescription As String
    Property FilterBookMedia As String
    Property FilterBookLanguage As String
    Property FilterBookISBN As String
    Property FilterBookPublished As Date
    Property FilterActiveBook As Boolean
    Property FilterBookByAuthors As List(Of Author)
    Property FilterBookByCategories As List(Of Category)

    Property FilterTransactionMember As String
    Property FilterTransactionBookTitle As String
    Property FilterTransactionBookDescription As String
    Property FilterTransactionBookMedia As String
    Property FilterTransactionBookLanguage As String
    Property FilterTransactionBookISBN As String

    Property FilterAuthorName As String
    Property FilterAuthorCareer As String
    Property FilterActiveAuthor As Boolean

    Property FilterCategoryName As String
    Property FilterActiveCategory As Boolean

    Property FilterShelf As String
    Property FilterActiveShelf As Boolean

    Property FilterMemberLogin As String
    Property FilterMemberName As String
    Property FilterMemberPIN As String
    Property FilterMemberRole As String
    Property FilterMemberEmail As String
    Property FilterMemberPhone As String
    Property FilterMemberAddress As String
    Property FilterActiveMember As Boolean

    Property FilterRequestMember As String
    Property FilterRequestBookTitle As String
    Property FilterRequestBookDescription As String

    Property FilterRequestExpectedDate As Date
    Property FilterReview As String
    Property FilterTransactionExpectedReturn As Date

#End Region

#Region "Properties : Select Top"

    'Select Top
    Property SelectTopRequests As Integer
    Property SelectTopReviews As Integer
    Property SelectTopTransactions As Integer
    Property MaxPendingRequest As Integer
    Property MaxUnreturnedTransaction As Integer

#End Region

#Region "Public Methods : Save, Reject, Refresh Data"

    'Save, Reject, Refresh Data
    Sub SubmitChanges(callback As Action(Of ServiceSubmitChangesResult), ByVal state As Object)
    Sub RejectChanges(callback As Action(Of ServiceSubmitChangesResult), ByVal state As Object)
    Sub RefreshLibraryDataService()
    Sub UpdateAuthorsOfBook(ByVal book_id As Integer, ByVal author_ids As List(Of Integer))
    Sub UpdateCategoriesOfBook(ByVal book_id As Integer, ByVal category_ids As List(Of Integer))
    Sub UpdateShelvesOfBook(ByVal book_id As Integer, ByVal shelf_ids As List(Of Integer))
    Sub RefreshBookPropertiesCollectionViews()
    Sub RefreshBookFilterCollectionViews()
    Sub RefreshMemberTransactionCollectionViews()
    Sub RefreshLibraryService()

#End Region

#Region "Public Methods : Load Data Operations"

    'Load Data Operations
    Sub LoadBooksByCategories(ByVal category_id As Integer())
    Sub LoadBooksByAuthors(ByVal author_id As Integer())
    Sub LoadBooksByShelves(ByVal shelf_id As Integer())
    Sub LoadBooksByBookID(ByVal book_id As Integer)
    Sub LoadBooks()

    Sub LoadAuthors()
    Sub LoadCategories()
    Sub LoadShelves()

    Sub LoadMembers()
    Sub LoadRequests()
    Sub LoadReviews()
    Sub LoadTransactions()

    Sub LoadCurrentMemberPendingRequests()
    Sub LoadCurrentMemberReviews()
    Sub LoadCurrentMemberTransactions()
    Sub ReloadCurrentMemberTransactions(ByVal pendingRequestCounterWhenReload As Integer)
    Sub LoadCurrentMemberAllTransactions()
    Sub LoadCurrentMemberReturnedTransactions()
    Sub LoadCurrentMemberReturnedTransactionsDetail()
    Sub LoadCurrentMemberInformation()

    Sub LoadAllPendingRequests()
    Sub LoadAllMemberTransactions()
    Sub LoadAllMemberUnreturnedTransactions()
    Sub LoadAnswerTemplates()

    Sub LoadMyOpenSuggestions()
    Sub LoadAllMemberOpenSuggestions()

#End Region

End Interface
