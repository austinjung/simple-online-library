﻿Imports System
Imports System.Collections.Generic
Imports System.ServiceModel.DomainServices.Client
Imports Microsoft.Windows.Data.DomainServices
Imports Papa.Common
Imports Ria.Common

Public Class BookDataService
    Implements IBookDataService

    Private ReadOnly _pendingLoads As IDictionary(Of Type, LoadOperation) = New Dictionary(Of Type, LoadOperation)()
    Private ReadOnly _context As ckcLibraryDomainContext = New ckcLibraryDomainContext()

    Public Sub New()

    End Sub

    Public ReadOnly Property EntityContainer As EntityContainer Implements IBookDataService.EntityContainer
        Get
            Return Me._context.EntityContainer
        End Get
    End Property

    Public Sub SubmitChanges(callback As Action(Of ServiceSubmitChangesResult), state As Object) Implements IBookDataService.SubmitChanges
        Me._context.SubmitChanges(Function(so)
                                      callback(Me.CreateResult(so))
                                  End Function, state)
    End Sub

    Public Sub LoadBooksByCategory(ByVal categoryID As Integer, query As QueryBuilder(Of Book), callback As Action(Of ServiceLoadResult), state As Object) Implements IBookDataService.LoadBooksByCategory
        Me.Load(query.ApplyTo(Me._context.GetActiveBooksQuery()), Function(lo)
                                                                      callback(Me.CreateResult(lo, True))
                                                                  End Function, state)
    End Sub

    Public Sub LoadBooksOfTheDay(callback As Action(Of ServiceLoadResult), ByVal state As Object) Implements IBookDataService.LoadBooksOfTheDay
        Me.Load(Me._context.GetBooks_By_TitleQuery("Title"), Function(lo)
                                                                 callback(Me.CreateResult(lo, True))
                                                             End Function, state)
    End Sub

    Public Sub LoadAuthors(callback As Action(Of ServiceLoadResult), ByVal state As Object) Implements IBookDataService.LoadAuthors
        Me.Load(Me._context.GetActiveAuthorsQuery(), Function(lo)
                                                         callback(Me.CreateResult(lo, True))
                                                     End Function, state)
    End Sub

    Public Sub LoadCategories(callback As Action(Of ServiceLoadResult), ByVal state As Object) Implements IBookDataService.LoadCategories
        Me.Load(Me._context.GetActiveCategoriesQuery(), Function(lo)
                                                            callback(Me.CreateResult(lo, True))
                                                        End Function, state)
    End Sub

    Public Sub LoadCheckouts(callback As Action(Of ServiceLoadResult), ByVal state As Object) Implements IBookDataService.LoadCheckouts
        Me.Load(Me._context.Get_Current_Member_Unreturned_TransactionsQuery(0), Function(lo)
                                                                                    callback(Me.CreateResult(lo, True))
                                                                                End Function, state)
    End Sub

    Private Function CreateResult(op As SubmitOperation) As ServiceSubmitChangesResult
        If op.HasError Then
            op.MarkErrorAsHandled()
        End If
        Return New ServiceSubmitChangesResult(
                                        op.ChangeSet,
                                        op.EntitiesInError,
                                        op.Error,
                                        op.IsCanceled,
                                        op.UserState)
    End Function

    Private Function CreateResult(Of T As Entity)(op As LoadOperation(Of T), Optional returnEditableCollection As Boolean = False) _
                                    As ServiceLoadResult(Of T)
        If op.HasError Then
            op.MarkErrorAsHandled()
        End If
        Return New ServiceLoadResult(Of T)(
                    If(returnEditableCollection, New EntityList(Of T)(Me.EntityContainer.GetEntitySet(Of T)(), op.Entities), op.Entities),
                    op.TotalEntityCount,
                    op.ValidationErrors,
                    op.Error,
                    op.IsCanceled,
                    op.UserState)
    End Function

    Private Sub Load(Of T As Entity)(query As EntityQuery(Of T), callback As Action(Of LoadOperation(Of T)), state As Object)
        If Me._pendingLoads.ContainsKey(GetType(T)) Then
            Me._pendingLoads(GetType(T)).Cancel()
            Me._pendingLoads.Remove(GetType(T))
        End If

        Me._pendingLoads(GetType(T)) = Me._context.Load(query, Function(lo)
                                                                   Me._pendingLoads.Remove(GetType(T))
                                                                   callback(lo)

                                                               End Function, state)
    End Sub
End Class
