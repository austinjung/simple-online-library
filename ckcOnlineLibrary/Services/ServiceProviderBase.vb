﻿Imports System.ComponentModel

Public MustInherit Class ServiceProviderBase

    Private m_PageConductor As IPageConductor
    Public Overridable Property PageConductor() As IPageConductor
        Get
            Return m_PageConductor
        End Get
        Protected Set(value As IPageConductor)
            m_PageConductor = value
        End Set
    End Property

    Private m_LibraryDataService As ILibraryDataService
    Public Overridable Property LibraryDataService() As ILibraryDataService
        Get
            Return m_LibraryDataService
        End Get
        Protected Set(value As ILibraryDataService)
            m_LibraryDataService = value
        End Set
    End Property

    Private Shared _instance As ServiceProviderBase
    Public Shared ReadOnly Property Instance() As ServiceProviderBase
        Get
            Return If(_instance, CreateInstance())
        End Get
    End Property

    Public Shared Function CreateInstance() As ServiceProviderBase
        ' TODO:  Uncomment
        'Return InlineAssignHelper(_instance, If(DesignerProperties.IsInDesignTool, DirectCast(New DesignServiceProvider(), ServiceProviderBase), New ServiceProvider()))
        Return InlineAssignHelper(_instance, If(DesignerProperties.IsInDesignTool, Nothing, New ServiceProvider()))

        ' TODO:  Comment
        'Return InlineAssignHelper(_instance, New ServiceProvider())
        'Return InlineAssignHelper(_instance, DirectCast(New DesignServiceProvider(), ServiceProviderBase))
    End Function

    Private Shared Function InlineAssignHelper(Of T)(ByRef target As T, value As T) As T
        target = value
        Return value
    End Function

    'Private m_BookDataService As IBookDataService
    'Public Overridable Property BookDataService() As IBookDataService
    '    Get
    '        Return m_BookDataService
    '    End Get
    '    Protected Set(value As IBookDataService)
    '        m_BookDataService = value
    '    End Set
    'End Property

End Class
