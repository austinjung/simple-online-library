﻿Public Class ServiceProvider
    Inherits ServiceProviderBase

    Public Sub New()
        ' Do this if you want one service for your app.
        PageConductor = New PageConductor()
        LibraryDataService = New LibraryDataService()
        'BookDataService = New BookDataService()
    End Sub

    ''Do this if you want one service per VM instance for your app.
    'Public Overrides Property PageConductor() As IPageConductor
    '    Get
    '        Return New PageConductor()
    '    End Get
    '    Protected Set(value As IPageConductor)

    '    End Set
    'End Property

    ''Do this if you want one service per VM instance for your app.
    'Public Overrides Property BookDataService() As IBookDataService
    '    Get
    '        Return New BookDataService()
    '    End Get
    '    Protected Set(value As IBookDataService)

    '    End Set
    'End Property

End Class