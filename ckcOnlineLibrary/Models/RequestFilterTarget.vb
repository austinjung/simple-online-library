﻿Imports System.Collections.ObjectModel

Public Class RequestFilterTarget

    Private _ischecked As Boolean = False
    Public Property IsChecked As Boolean
        Get
            Return _ischecked
        End Get
        Set(value As Boolean)
            _ischecked = value
        End Set
    End Property

    Private _target As String
    Public Property Target As String
        Get
            Return _target
        End Get
        Set(value As String)
            _target = value
        End Set
    End Property

    Public Sub New(targetName As String)
        _target = targetName
    End Sub

    Public Sub New(targetName As String, isChecked As Boolean)
        _target = targetName
        _ischecked = isChecked
    End Sub

End Class

Public Class RequestFilterTargets
    Inherits ObservableCollection(Of RequestFilterTarget)

    Private Shared _filterTargets As ObservableCollection(Of RequestFilterTarget)
    Public Shared ReadOnly Property GetFilterTargets As ObservableCollection(Of RequestFilterTarget)
        Get
            _filterTargets = New ObservableCollection(Of RequestFilterTarget)()
            _filterTargets.Add(New RequestFilterTarget("Member", True))
            _filterTargets.Add(New RequestFilterTarget("Book Title", True))
            _filterTargets.Add(New RequestFilterTarget("Book Description"))
            Return _filterTargets
        End Get
    End Property

End Class