﻿Imports System.Collections.ObjectModel

Public Class KeywordFilterTarget

    Private _ischecked As Boolean = False
    Public Property IsChecked As Boolean
        Get
            Return _ischecked
        End Get
        Set(value As Boolean)
            _ischecked = value
        End Set
    End Property

    Private _target As String
    Public Property Target As String
        Get
            Return _target
        End Get
        Set(value As String)
            _target = value
        End Set
    End Property

    Public Sub New(targetName As String)
        _target = targetName
    End Sub

    Public Sub New(targetName As String, isChecked As Boolean)
        _target = targetName
        _ischecked = isChecked
    End Sub

End Class

Public Class KeywordFilterTargets
    Inherits ObservableCollection(Of KeywordFilterTarget)

    Private Shared _filterTargets As ObservableCollection(Of KeywordFilterTarget)
    Public Shared ReadOnly Property GetFilterTargets As ObservableCollection(Of KeywordFilterTarget)
        Get
            _filterTargets = New ObservableCollection(Of KeywordFilterTarget)()
            _filterTargets.Add(New KeywordFilterTarget("Book Title", True))
            _filterTargets.Add(New KeywordFilterTarget("Book Description", True))
            _filterTargets.Add(New KeywordFilterTarget("Book ISBN"))
            _filterTargets.Add(New KeywordFilterTarget("Book Media"))
            _filterTargets.Add(New KeywordFilterTarget("Book Language"))
            Return _filterTargets
        End Get
    End Property

End Class