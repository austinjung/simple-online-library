﻿Imports Papa.Common

Public Class NotifiableError
    Inherits NotifiableObject

    Private _description As String
    Private _title As String

    Public Property Description As String
        Get
            Return _description
        End Get
        Set(value As String)
            _description = value
            RaisePropertyChanged("Description")
        End Set
    End Property

    Public Property Title As String
        Get
            Return _title
        End Get
        Set(value As String)
            _title = value
            RaisePropertyChanged("Title")
        End Set
    End Property

End Class
