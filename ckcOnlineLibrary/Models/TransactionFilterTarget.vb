﻿Imports System.Collections.ObjectModel

Public Class TransactionFilterTarget

    Private _ischecked As Boolean = False
    Public Property IsChecked As Boolean
        Get
            Return _ischecked
        End Get
        Set(value As Boolean)
            _ischecked = value
        End Set
    End Property

    Private _target As String
    Public Property Target As String
        Get
            Return _target
        End Get
        Set(value As String)
            _target = value
        End Set
    End Property

    Public Sub New(targetName As String)
        _target = targetName
    End Sub

    Public Sub New(targetName As String, isChecked As Boolean)
        _target = targetName
        _ischecked = isChecked
    End Sub

End Class

Public Class TransactionFilterTargets
    Inherits ObservableCollection(Of TransactionFilterTarget)

    Private Shared _filterTargets As ObservableCollection(Of TransactionFilterTarget)
    Public Shared ReadOnly Property GetFilterTargets As ObservableCollection(Of TransactionFilterTarget)
        Get
            _filterTargets = New ObservableCollection(Of TransactionFilterTarget)()
            _filterTargets.Add(New TransactionFilterTarget("Member", True))
            _filterTargets.Add(New TransactionFilterTarget("Book Title", True))
            _filterTargets.Add(New TransactionFilterTarget("Book Description"))
            Return _filterTargets
        End Get
    End Property

End Class