﻿Imports System
Imports System.ComponentModel

Namespace Web

    Partial Public Class Member
        Implements ComponentModel.INotifyPropertyChanged
#Region "Make DisplayName Bindable"

        Public Event PropertyChanged As PropertyChangedEventHandler Implements ComponentModel.INotifyPropertyChanged.PropertyChanged

        ''' <summary>
        ''' Override of the <c>OnPropertyChanged</c> method that generates
        ''' property change notifications when  changes.
        ''' </summary>
        ''' <param name="e">The property change event args.</param>
        Protected Sub OnPropertyChanged(ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            If e Is Nothing Then
                Throw New ArgumentNullException("e")
            End If

            'MyBase.OnPropertyChanged(e)

            If e.PropertyName = "FullName" Then
                RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("FullName"))
            End If
        End Sub
#End Region


    End Class
End Namespace