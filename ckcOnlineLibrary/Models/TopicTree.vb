﻿Imports System.Collections.ObjectModel

Public Class TopicTree

    Private _Title As String
    Public Property Title() As String
        Get
            Return _Title
        End Get
        Set(ByVal value As String)
            _Title = value
        End Set
    End Property
    Private _NewBookRequestID As Integer
    Public Property NewBookRequestID() As Integer
        Get
            Return _NewBookRequestID
        End Get
        Set(ByVal value As Integer)
            _NewBookRequestID = value
        End Set
    End Property
    Private childTopicsValue As New ObservableCollection(Of TopicTree)()
    Public Property ChildTopics() As ObservableCollection(Of TopicTree)
        Get
            Return childTopicsValue
        End Get
        Set(ByVal value As ObservableCollection(Of TopicTree))
            childTopicsValue = value
        End Set
    End Property
    Public Sub New()
    End Sub
    Public Sub New(ByVal title As String, ByVal newBookRequestID As Integer)
        Me._Title = title
        Me._NewBookRequestID = newBookRequestID
    End Sub
End Class
