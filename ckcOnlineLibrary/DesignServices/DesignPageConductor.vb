﻿Imports System
Imports System.Collections.Generic

Class DesignPageConductor
    Implements IPageConductor

    Protected State As Dictionary(Of String, Object) = New Dictionary(Of String, Object)

    Public Sub New()

    End Sub

    Public Sub DisplayError(origin As String, e As System.Exception) Implements IPageConductor.DisplayError

    End Sub

    Public Sub DisplayError(origin As String, e As System.Exception, details As String) Implements IPageConductor.DisplayError

    End Sub

    Public Sub GoBack() Implements IPageConductor.GoBack

    End Sub

    Public Sub GoToCheckoutView(viewToken As String, stateKey As String, book As Book) Implements IPageConductor.GoToCheckoutView

    End Sub

    Public Sub GoToView(viewToken As String) Implements IPageConductor.GoToView

    End Sub

    Public Function PeekState(Of T As Class)(key As String) As T Implements IPageConductor.PeekState

        Dim value As Object = New Object
        State.TryGetValue(key, value)

        Return value

    End Function

    Public Function PopState(Of T As Class)(key As String) As T Implements IPageConductor.PopState

        Return PeekState(Of T)(key)

    End Function

    Public Sub PushState(key As String, value As Object) Implements IPageConductor.PushState

        State(key) = value

    End Sub
End Class
