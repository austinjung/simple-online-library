﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel

Public Class ShelfDesignTimeLoader
    Inherits CollectionViewLoader

    Private Shared ReadOnly _source As IEnumerable(Of Shelf) = New DesignShelves()
    Public ReadOnly Property Source As IEnumerable(Of Shelf)
        Get
            Return ShelfDesignTimeLoader._source
        End Get
    End Property

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

End Class
