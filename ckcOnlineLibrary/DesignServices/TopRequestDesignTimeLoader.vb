﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel

Class TopRequestDesignTimeLoader
    Inherits CollectionViewLoader

    Private Shared ReadOnly _source As IEnumerable(Of Books_By_Requests) = New DesignTopRequests()
    Public ReadOnly Property Source As IEnumerable(Of Books_By_Requests)
        Get
            Return TopRequestDesignTimeLoader._source
        End Get
    End Property

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

End Class
