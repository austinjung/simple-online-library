﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel

Public Class AuthorDesignTimeLoader
    Inherits CollectionViewLoader

    Private Shared ReadOnly _source As IEnumerable(Of Author) = New DesignAuthors()
    Public ReadOnly Property Source As IEnumerable(Of Author)
        Get
            Return AuthorDesignTimeLoader._source
        End Get
    End Property

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

End Class
