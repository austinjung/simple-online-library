﻿Imports System.Collections.ObjectModel
Imports System.ServiceModel.DomainServices.Client
Imports Ria.Common
Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel
Imports System.Collections.Specialized
Imports System.Windows.Data

Public Class DesignLibraryDataService
    Implements ILibraryDataService


#Region "Private Variables"

    'Domain Context & Collection Views
    Private ReadOnly _domainContext As ckcLibraryDomainContext = New ckcLibraryDomainContext()

    Private ReadOnly _authors_View As DomainCollectionView(Of Author)
    Private ReadOnly _authors_Loader As AuthorDesignTimeLoader
    Private ReadOnly _authors_Source As EntityList(Of Author)

    'Private ReadOnly _books_View As ICollectionView
    'Private ReadOnly _books_Loader As BookDesignTimeLoader
    'Private ReadOnly _books_Source As IEnumerable(Of DesignBook)
    Private ReadOnly _books_View As DomainCollectionView(Of Book)
    Private ReadOnly _books_Loader As BookDesignTimeLoader
    Private ReadOnly _books_Source As EntityList(Of Book)
    Private _book_CollectionViewSource As CollectionViewSource

    Private ReadOnly _categories_View As DomainCollectionView(Of Category)
    Private ReadOnly _categories_Loader As CategoryDesignTimeLoader
    Private ReadOnly _categories_Source As EntityList(Of Category)

    Private ReadOnly _members_View As DomainCollectionView(Of Member)
    Private ReadOnly _members_Loader As MemberDesignTimeLoader
    Private ReadOnly _members_Source As EntityList(Of Member)

    Private ReadOnly _requests_View As DomainCollectionView(Of Member_Requests)
    Private ReadOnly _requests_Loader As RequestDesignTimeLoader
    Private ReadOnly _requests_Source As EntityList(Of Member_Requests)

    Private ReadOnly _reviews_View As DomainCollectionView(Of Member_Reviews)
    Private ReadOnly _reviews_Loader As ReviewDesignTimeLoader
    Private ReadOnly _reviews_Source As EntityList(Of Member_Reviews)

    Private ReadOnly _transactions_View As DomainCollectionView(Of Member_Transactions)
    Private ReadOnly _transactions_Loader As TransactionDesignTimeLoader
    Private ReadOnly _transactions_Source As EntityList(Of Member_Transactions)

    Private ReadOnly _shelves_View As DomainCollectionView(Of Shelf)
    Private ReadOnly _shelves_Loader As ShelfDesignTimeLoader
    Private ReadOnly _shelves_Source As EntityList(Of Shelf)

    Private ReadOnly _top_requests_View As DomainCollectionView(Of Books_By_Requests)
    Private ReadOnly _top_requests_Loader As TopRequestDesignTimeLoader
    Private ReadOnly _top_requests_Source As EntityList(Of Books_By_Requests)

    Private ReadOnly _top_reviews_View As DomainCollectionView(Of Books_By_Reviews)
    Private ReadOnly _top_reviews_Loader As TopReviewDesignTimeLoader
    Private ReadOnly _top_reviews_Source As EntityList(Of Books_By_Reviews)

#End Region

#Region "Properties : Domain Context & Collection Views"

    Public ReadOnly Property DomainContext As ckcLibraryDomainContext Implements ILibraryDataService.DomainContext
        Get
            Return Me._domainContext
        End Get
    End Property

    Public Property AuthorCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.AuthorCollectionView
        Get
            Return Me._authors_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property BookAuthorCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookAuthorCollectionView
        Get
            Return Me._authors_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property BookAuthorFilterCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookAuthorFilterCollectionView
        Get
            Return Me._authors_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property BookCollectionView As ICollectionView Implements ILibraryDataService.BookCollectionView
        Get
            Return Me._books_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property CategoryCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.CategoryCollectionView
        Get
            Return Me._categories_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property BookCategoryCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookCategoryCollectionView
        Get
            Return Me._categories_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property BookCategoryFilterCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookCategoryFilterCollectionView
        Get
            Return Me._categories_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property MemberCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.MemberCollectionView
        Get
            Return Me._members_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property RequestCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.RequestCollectionView
        Get
            Return Me._requests_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property ReviewCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.ReviewCollectionView
        Get
            Return Me._reviews_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property ShelfCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.ShelfCollectionView
        Get
            Return Me._shelves_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property BookShelfCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.BookShelfCollectionView
        Get
            Return Me._shelves_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property TransactionCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.TransactionCollectionView
        Get
            Return Me._transactions_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property ExpectedReturnCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.ExpectedReturnCollectionView
        Get
            Return Me._transactions_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public ReadOnly Property BookAuthorCollectionLoader As StaticAutoCompleteViewLoader(Of Author) Implements ILibraryDataService.BookAuthorCollectionLoader
        Get
            Throw New NotImplementedException("StaticAutoCompleteViewLoader shouldn't be called at design-time.")
        End Get
    End Property

    Public ReadOnly Property BookCategoryCollectionLoader As StaticAutoCompleteViewLoader(Of Category) Implements ILibraryDataService.BookCategoryCollectionLoader
        Get
            Throw New NotImplementedException("StaticAutoCompleteViewLoader shouldn't be called at design-time.")
        End Get
    End Property

    Public ReadOnly Property BookAuthorFilterCollectionLoader As StaticFilterAutoCompleteViewLoader(Of Author) Implements ILibraryDataService.BookAuthorFilterCollectionLoader
        Get
            Throw New NotImplementedException("StaticAutoCompleteViewLoader shouldn't be called at design-time.")
        End Get
    End Property

    Public ReadOnly Property BookCategoryFilterCollectionLoader As StaticFilterAutoCompleteViewLoader(Of Category) Implements ILibraryDataService.BookCategoryFilterCollectionLoader
        Get
            Throw New NotImplementedException("StaticAutoCompleteViewLoader shouldn't be called at design-time.")
        End Get
    End Property

    Public ReadOnly Property BookShelfCollectionLoader As StaticAutoCompleteViewLoader(Of Shelf) Implements ILibraryDataService.BookShelfCollectionLoader
        Get
            Throw New NotImplementedException("StaticAutoCompleteViewLoader shouldn't be called at design-time.")
        End Get
    End Property

    Public Property TopRequestCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.TopRequestsCollectionView
        Get
            Return Me._top_requests_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property

    Public Property TopReviewsCollectionView As System.ComponentModel.ICollectionView Implements ILibraryDataService.TopReviewsCollectionView
        Get
            Return Me._top_reviews_View
        End Get
        Set(value As System.ComponentModel.ICollectionView)
            Throw New NotImplementedException("CollectionView Reset shouldn't be called at design-time.")
        End Set
    End Property
#End Region

#Region "Properties : Data Service Control Status"

    Public Property CanLoad As Boolean Implements ILibraryDataService.CanLoad
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Boolean)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

#End Region

#Region "Properties : Filters on Load Operations"
    Public Property FilterAuthorCareer As String Implements ILibraryDataService.FilterAuthorCareer
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterAuthorName As String Implements ILibraryDataService.FilterAuthorName
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterBookDescription As String Implements ILibraryDataService.FilterBookDescription
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterBookISBN As String Implements ILibraryDataService.FilterBookISBN
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterBookLanguage As String Implements ILibraryDataService.FilterBookLanguage
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterBookMedia As String Implements ILibraryDataService.FilterBookMedia
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterBookPublished As Date Implements ILibraryDataService.FilterBookPublished
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Date)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterBookTitle As String Implements ILibraryDataService.FilterBookTitle
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterCategoryName As String Implements ILibraryDataService.FilterCategoryName
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterMemberAddress As String Implements ILibraryDataService.FilterMemberAddress
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterMemberEmail As String Implements ILibraryDataService.FilterMemberEmail
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterMemberLogin As String Implements ILibraryDataService.FilterMemberLogin
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterMemberName As String Implements ILibraryDataService.FilterMemberName
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterMemberPhone As String Implements ILibraryDataService.FilterMemberPhone
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterMemberPIN As String Implements ILibraryDataService.FilterMemberPIN
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterMemberRole As String Implements ILibraryDataService.FilterMemberRole
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterRequestExpectedDate As Date Implements ILibraryDataService.FilterRequestExpectedDate
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Date)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterReview As String Implements ILibraryDataService.FilterReview
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterShelf As String Implements ILibraryDataService.FilterShelf
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As String)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterTransactionExpectedReturn As Date Implements ILibraryDataService.FilterTransactionExpectedReturn
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Date)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterActiveAuthor As Boolean Implements ILibraryDataService.FilterActiveAuthor
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Boolean)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterActiveBook As Boolean Implements ILibraryDataService.FilterActiveBook
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Boolean)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterActiveCategory As Boolean Implements ILibraryDataService.FilterActiveCategory
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Boolean)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterActiveMember As Boolean Implements ILibraryDataService.FilterActiveMember
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Boolean)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property FilterActiveShelf As Boolean Implements ILibraryDataService.FilterActiveShelf
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Boolean)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

#End Region

#Region "Properties : Select Top"

    Public Property SelectTopRequests As Integer Implements ILibraryDataService.SelectTopRequests
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Integer)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property SelectTopReviews As Integer Implements ILibraryDataService.SelectTopReviews
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Integer)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

    Public Property SelectTopTransactions As Integer Implements ILibraryDataService.SelectTopTransactions
        Get
            Throw New NotImplementedException("Properties shouldn't be accessed at design-time.")
        End Get
        Set(value As Integer)
            Throw New NotImplementedException("Properties shouldn't be modified at design-time.")
        End Set
    End Property

#End Region

#Region "Public Methods : Constructor"

    Public Sub New()

        Me._authors_Source = New EntityList(Of Author)(Me.DomainContext.Authors)
        Me._authors_Loader = New AuthorDesignTimeLoader()
        Me._authors_View = New DomainCollectionView(Of Author)(_authors_Loader, _authors_Loader.Source)

        'Me._book_CollectionViewSource = New CollectionViewSource()
        'Me._book_CollectionViewSource.Source = New DesignBooks()
        'Me._books_View = Me._book_CollectionViewSource.View
        Me._books_Source = New EntityList(Of Book)(Me.DomainContext.Books)
        Me._books_Loader = New BookDesignTimeLoader()
        Me._books_View = New DomainCollectionView(Of Book)(_books_Loader, _books_Loader.Source)

        Me._categories_Source = New EntityList(Of Category)(Me.DomainContext.Categories)
        Me._categories_Loader = New CategoryDesignTimeLoader()
        Me._categories_View = New DomainCollectionView(Of Category)(_categories_Loader, _categories_Loader.Source)

        Me._members_Source = New EntityList(Of Member)(Me.DomainContext.Members)
        Me._members_Loader = New MemberDesignTimeLoader()
        Me._members_View = New DomainCollectionView(Of Member)(_members_Loader, _members_Loader.Source)

        Me._requests_Source = New EntityList(Of Member_Requests)(Me.DomainContext.Member_Requests)
        Me._requests_Loader = New RequestDesignTimeLoader()
        Me._requests_View = New DomainCollectionView(Of Member_Requests)(_requests_Loader, _requests_Loader.Source)

        Me._reviews_Source = New EntityList(Of Member_Reviews)(Me.DomainContext.Member_Reviews)
        Me._reviews_Loader = New ReviewDesignTimeLoader()
        Me._reviews_View = New DomainCollectionView(Of Member_Reviews)(_reviews_Loader, _reviews_Loader.Source)

        Me._transactions_Source = New EntityList(Of Member_Transactions)(Me.DomainContext.Member_Transactions)
        Me._transactions_Loader = New TransactionDesignTimeLoader()
        Me._transactions_View = New DomainCollectionView(Of Member_Transactions)(_transactions_Loader, _transactions_Loader.Source)

        Me._shelves_Source = New EntityList(Of Shelf)(Me.DomainContext.Shelfs)
        Me._shelves_Loader = New ShelfDesignTimeLoader()
        Me._shelves_View = New DomainCollectionView(Of Shelf)(_shelves_Loader, _shelves_Loader.Source)

        Me._top_requests_Source = New EntityList(Of Books_By_Requests)(Me.DomainContext.Books_By_Requests)
        Me._top_requests_Loader = New TopRequestDesignTimeLoader()
        Me._top_requests_View = New DomainCollectionView(Of Books_By_Requests)(_top_requests_Loader, _top_requests_Loader.Source)

        Me._top_reviews_Source = New EntityList(Of Books_By_Reviews)(Me.DomainContext.Books_By_Reviews)
        Me._top_reviews_Loader = New TopReviewDesignTimeLoader()
        Me._top_reviews_View = New DomainCollectionView(Of Books_By_Reviews)(_top_reviews_Loader, _top_reviews_Loader.Source)

    End Sub

#End Region

#Region "Public Methods : Save, Reject, Refresh Data"

    Public Sub SubmitChanges(callback As System.Action(Of Ria.Common.ServiceSubmitChangesResult), state As Object) Implements ILibraryDataService.SubmitChanges
        Throw New NotImplementedException("SubmitChanges shouldn't be called at design-time.")
    End Sub

    Public Sub RejectChanges(callback As System.Action(Of Ria.Common.ServiceSubmitChangesResult), state As Object) Implements ILibraryDataService.RejectChanges
        Throw New NotImplementedException("RejectChanges shouldn't be called at design-time.")
    End Sub

    Public Sub RefreshLibraryDataService() Implements ILibraryDataService.RefreshLibraryDataService
        'This should be do nothing and not throw exception
        'Throw New NotImplementedException("RefreshLibraryDataService shouldn't be called at design-time.")
    End Sub

    Public Sub UpdateAuthorsOfBook(book_id As Integer, author_ids As System.Collections.Generic.List(Of Integer)) Implements ILibraryDataService.UpdateAuthorsOfBook
        Throw New NotImplementedException("UpdateAuthorsOfBook shouldn't be called at design-time.")
    End Sub

    Public Sub UpdateCategoriesOfBook(book_id As Integer, category_ids As System.Collections.Generic.List(Of Integer)) Implements ILibraryDataService.UpdateCategoriesOfBook
        Throw New NotImplementedException("UpdateCategoriesOfBook shouldn't be called at design-time.")
    End Sub

    Public Sub UpdateShelvesOfBook(book_id As Integer, shelf_ids As System.Collections.Generic.List(Of Integer)) Implements ILibraryDataService.UpdateShelvesOfBook
        Throw New NotImplementedException("UpdateShelvesOfBook shouldn't be called at design-time.")
    End Sub

    Public Sub RefreshBookPropertiesCollectionViews() Implements ILibraryDataService.RefreshBookPropertiesCollectionViews
        Throw New NotImplementedException("RefreshBookPropertiesCollectionViews shouldn't be called at design-time.")
    End Sub

    Public Sub RefreshBookFilterCollectionViews() Implements ILibraryDataService.RefreshBookFilterCollectionViews
        Throw New NotImplementedException("RefreshBookPropertiesCollectionViews shouldn't be called at design-time.")
    End Sub

#End Region

#Region "Public Methods : Load Data Operations"

    Public Sub LoadAuthors() Implements ILibraryDataService.LoadAuthors
        Me._authors_Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooks() Implements ILibraryDataService.LoadBooks
        Me._books_Loader.Load(Nothing)
    End Sub

    Public Sub LoadBookByBookID(book_id As Integer) Implements ILibraryDataService.LoadBooksByBookID
        Me._books_Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooksByCategories(category_id As Integer()) Implements ILibraryDataService.LoadBooksByCategories
        Me._books_Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooksByAuthors(author_id() As Integer) Implements ILibraryDataService.LoadBooksByAuthors
        Me._books_Loader.Load(Nothing)
    End Sub

    Public Sub LoadBooksByShelves(shelf_id() As Integer) Implements ILibraryDataService.LoadBooksByShelves
        Me._books_Loader.Load(Nothing)
    End Sub

    Public Sub LoadCategories() Implements ILibraryDataService.LoadCategories
        Me._categories_Loader.Load(Nothing)
    End Sub

    Public Sub LoadMembers() Implements ILibraryDataService.LoadMembers
        Me._members_Loader.Load(Nothing)
    End Sub

    Public Sub LoadRequests() Implements ILibraryDataService.LoadRequests
        Me._requests_Loader.Load(Nothing)
    End Sub

    Public Sub LoadCurrentMemberPendingRequests() Implements ILibraryDataService.LoadCurrentMemberPendingRequests
        Me._requests_Loader.Load(Nothing)
    End Sub

    Public Sub LoadReviews() Implements ILibraryDataService.LoadReviews
        Me._reviews_Loader.Load(Nothing)
    End Sub

    Public Sub LoadCurrentMemberReviews() Implements ILibraryDataService.LoadCurrentMemberReviews
        Me._reviews_Loader.Load(Nothing)
    End Sub

    Public Sub LoadShelves() Implements ILibraryDataService.LoadShelves
        Me._shelves_Loader.Load(Nothing)
    End Sub

    Public Sub LoadTransactions() Implements ILibraryDataService.LoadTransactions
        Me._transactions_Loader.Load(Nothing)
    End Sub

    Public Sub LoadCurrentMemberTransactions() Implements ILibraryDataService.LoadCurrentMemberTransactions
        Me._transactions_Loader.Load(Nothing)
    End Sub

#End Region

End Class
