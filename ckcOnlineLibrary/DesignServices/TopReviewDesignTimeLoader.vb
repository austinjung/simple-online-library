﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel

Class TopReviewDesignTimeLoader
    Inherits CollectionViewLoader

    Private Shared ReadOnly _source As IEnumerable(Of Books_By_Reviews) = New DesignTopReviews()
    Public ReadOnly Property Source As IEnumerable(Of Books_By_Reviews)
        Get
            Return TopReviewDesignTimeLoader._source
        End Get
    End Property

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

End Class
