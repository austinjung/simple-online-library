﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel

Public Class MemberDesignTimeLoader
    Inherits CollectionViewLoader

    Private Shared ReadOnly _source As IEnumerable(Of Member) = New DesignMembers()
    Public ReadOnly Property Source As IEnumerable(Of Member)
        Get
            Return MemberDesignTimeLoader._source
        End Get
    End Property

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

End Class
