﻿Imports Microsoft.Windows.Data.DomainServices
Imports System.ComponentModel

Public Class RequestDesignTimeLoader
    Inherits CollectionViewLoader

    Private Shared ReadOnly _source As IEnumerable(Of Member_Requests) = New DesignMemberRequests()
    Public ReadOnly Property Source As IEnumerable(Of Member_Requests)
        Get
            Return RequestDesignTimeLoader._source
        End Get
    End Property

    Public Overrides ReadOnly Property CanLoad As Boolean
        Get
            Return True
        End Get
    End Property

    Public Overrides Sub Load(userState As Object)
        Me.OnLoadCompleted(New AsyncCompletedEventArgs(Nothing, False, userState))
    End Sub

End Class
