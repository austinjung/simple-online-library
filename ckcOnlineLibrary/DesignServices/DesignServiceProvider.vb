﻿Public Class DesignServiceProvider
    Inherits ServiceProviderBase

    Public Sub New()
        PageConductor = New DesignPageConductor()
        LibraryDataService = New DesignLibraryDataService()
        'BookDataService = New DesignBookDataService()
    End Sub

End Class
