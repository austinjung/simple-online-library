﻿Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Data
Imports System.Diagnostics
Imports System.Net.Mail
Imports System.Net
Imports System.Globalization
Imports System.Threading
Imports System.Data.Objects
Imports System.Text.RegularExpressions

Module MainModule

    Dim LibraryDB As ckLibraryEntities
    Dim ComittableRequests As IQueryable(Of vwComittableRequest)
    Dim BookRequest As Book
    Dim MemberRequest As Member
    Dim ComittableRequest As Member_Requests
    Dim ComitteOperation As ServerOperation
    Dim msg As String
    Dim todayString As String
    Dim toDay As DateTime
    Dim yesterDay As DateTime
    Dim nextBusinessDay As DateTime

    Dim EmailBody As String = String.Empty
    Dim FromEmailAddress As String = "libraryadmin@columbiacabinetslibrary.com"
    Dim ToEmailAddress As String = String.Empty
    Dim smtp As SmtpClient = Nothing

    'Email template
    Dim emailHeaderOpening As String = "<html>" & _
                                        "<head>" & _
                                            "<title></title>" & _
                                        "</head>" & _
                                        "<body>" & _
                                            "<img src='http://columbiacabinetslibrary.com/Upload/Icons/NetQuoteLogoIcon.PNG' border='0' style='display:block;' alt='Be Inspired' />" & _
                                            "<h2 align='center' style='color: #897563'>$OperationMessageOnDate</h2>"


    Dim emailHeaderClosing As String = "</body>" & _
                                    "</html>"

    'New Comitted Books
    Dim comitteTableOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                            "<thead>" & _
                                                "<tr bgcolor='LightCyan'>" & _
                                                    "<th colspan='3'>New Comitted Books</th>" & _
                                                "</tr>" & _
                                                "<tr bgcolor='LightCyan'>" & _
                                                    "<th width='70%'>Book Title</th>" & _
                                                    "<th width='15%'>Member</th>" & _
                                                    "<th width='15%'>Comitted</th>" & _
                                                "</tr>" & _
                                            "</thead>" & _
                                            "<tbody>"

    Dim comitteTableRecord As String = "<tr bgcolor='$BackgroudColor'>" & _
                                            "<td>$Book</td>" & _
                                            "<td>$Member</td>" & _
                                            "<td>$ComittedDate</td>" & _
                                       "</tr>"

    Dim comitteTableFooter As String = "<tr align='center' bgcolor='LightCyan'>" & _
                                            "<td colspan='3'>$Count new comitted book(s) today!</td>" & _
                                        "</tr>"

    'New Book Requests on Yesterday
    Dim newRequestTableOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                "<thead>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th colspan='4'>New Book Requests On Yesterday</th>" & _
                                                    "</tr>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th width='55%'>Book Title</th>" & _
                                                        "<th width='15%'>Member</th>" & _
                                                        "<th width='15%'>Expected</th>" & _
                                                        "<th width='15%'>Comitted</th>" & _
                                                    "</tr>" & _
                                                "</thead>" & _
                                                "<tbody>"

    Dim newRequestTableRecord As String = "<tr bgcolor='$BackgroudColor'>" & _
                                            "<td>$Book</td>" & _
                                            "<td>$Member</td>" & _
                                            "<td>$ExpectedDate</td>" & _
                                            "<td>$ComittedDate</td>" & _
                                          "</tr>"

    Dim newRequestTableFooter As String = "<tr align='center' bgcolor='LightCyan'>" & _
                                            "<td colspan='4'>$Count book request(s) yesterday!</td>" & _
                                          "</tr>"

    'Modified Book Requests on Yesterday
    Dim modifiedRequestTableOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                    "<thead>" & _
                                                        "<tr bgcolor='LightCyan'>" & _
                                                            "<th colspan='5'>Modified Book Requests On Yesterday</th>" & _
                                                        "</tr>" & _
                                                        "<tr bgcolor='LightCyan'>" & _
                                                            "<th width='35%'>Book Title</th>" & _
                                                            "<th width='10%'>Member Name</th>" & _
                                                            "<th width='35%'>Comment</th>" & _
                                                            "<th width='10%'>Expected</th>" & _
                                                            "<th width='10%'>Comitted</th>" & _
                                                        "</tr>" & _
                                                    "</thead>" & _
                                                    "<tbody>"

    Dim modifiedRequestTableRecord As String = "<tr bgcolor='$BackgroudColor'>" & _
                                                    "<td>$Book</td>" & _
                                                    "<td>$Member</td>" & _
                                                    "<td>$Comment</td>" & _
                                                    "<td>$ExpectedDate</td>" & _
                                                    "<td>$ComittedDate</td>" & _
                                               "</tr>"

    Dim modifiedRequestTableFooter As String = "<tr align='center' bgcolor='LightCyan'>" & _
                                                  "<td colspan='5'>$Count modified book request(s) yesterday!</td>" & _
                                               "</tr>"

    'Modified Book Transactions on Yesterday
    Dim modifiedTransactionTableOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                        "<thead>" & _
                                                            "<tr bgcolor='LightCyan'>" & _
                                                                "<th colspan='4'>Modified Book Transactions On Yesterday</th>" & _
                                                            "</tr>" & _
                                                            "<tr bgcolor='LightCyan'>" & _
                                                                "<th width='40%'>Book Title</th>" & _
                                                                "<th width='10%'>Member</th>" & _
                                                                "<th width='40%'>Comment</th>" & _
                                                                "<th width='10%'>Expected</th>" & _
                                                            "</tr>" & _
                                                        "</thead>" & _
                                                        "<tbody>"

    Dim modifiedTransactionTableRecord As String = "<tr bgcolor='$BackgroudColor'>" & _
                                                        "<td>$Book</td>" & _
                                                        "<td>$Member</td>" & _
                                                        "<td>$Comment</td>" & _
                                                        "<td>$ExpectedDate</td>" & _
                                                   "</tr>"

    Dim modifiedTransactionTableFooter As String = "<tr align='center' bgcolor='LightCyan'>" & _
                                                      "<td colspan='4'>$Count modified book transaction(s) yesterday!</td>" & _
                                                   "</tr>"

    'Today's check out schedule
    Dim todayCheckOutTableOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                "<thead>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th colspan='4'>Today's Check Out Schedule</th>" & _
                                                    "</tr>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th width='40%'>Book Title</th>" & _
                                                        "<th width='10%'>Member</th>" & _
                                                        "<th width='40%'>Comment</th>" & _
                                                        "<th width='10%'>Comitted</th>" & _
                                                    "</tr>" & _
                                                "</thead>" & _
                                                "<tbody>"

    Dim todayCheckOutTableRecord As String = "<tr bgcolor='$BackgroudColor'>" & _
                                                "<td>$Book</td>" & _
                                                "<td>$Member</td>" & _
                                                "<td>$Comment</td>" & _
                                                "<td style='color: $foreColor'>$ComittedDate</td>" & _
                                             "</tr>"

    Dim todayCheckOutTableFooter As String = "<tr align='center' bgcolor='LightCyan'>" & _
                                               "<td colspan='4'>$Count book(s) scheduled to be checked out today!</td>" & _
                                             "</tr>"

    'Today's return schedule
    Dim todayReturnBookTableOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                    "<thead>" & _
                                                        "<tr bgcolor='LightCyan'>" & _
                                                            "<th colspan='4'>Today's Return Schedule</th>" & _
                                                        "</tr>" & _
                                                        "<tr bgcolor='LightCyan'>" & _
                                                            "<th width='40%'>Book Title</th>" & _
                                                            "<th width='10%'>Member</th>" & _
                                                            "<th width='40%'>Comment</th>" & _
                                                            "<th width='10%'>Expected</th>" & _
                                                        "</tr>" & _
                                                    "</thead>" & _
                                                    "<tbody>"

    Dim todayReturnBookTableRecord As String = "<tr bgcolor='$BackgroudColor'>" & _
                                                    "<td>$Book</td>" & _
                                                    "<td>$Member</td>" & _
                                                    "<td>$Comment</td>" & _
                                                    "<td style='color: $foreColor'>$ExpectedReturnDate</td>" & _
                                               "</tr>"

    Dim todayReturnBookTableFooter As String = "<tr align='center' bgcolor='LightCyan'>" & _
                                                  "<td colspan='4'>$Count book(s) scheduled to be returned today!</td>" & _
                                               "</tr>"

    Dim tableClosing As String = "</tbody>" & _
                                "</table>" & _
                                "<br /><br />"

    Dim comitteBookToMemberOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                    "<thead>" & _
                                                        "<tr bgcolor='LightCyan'>" & _
                                                            "<th>New Comitted Book</th>" & _
                                                        "</tr>" & _
                                                    "</thead>" & _
                                                    "<tbody>"

    Dim comitteBookToMemberRecord As String = "<tr bgcolor='Cornsilk'>" & _
                                                "<td>$ComittedMsg</td>" & _
                                              "</tr>"

    Dim alertCheckOutToMemberOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                    "<thead>" & _
                                                        "<tr bgcolor='LightCyan'>" & _
                                                            "<th>Scheduled Check out Book Notification</th>" & _
                                                        "</tr>" & _
                                                    "</thead>" & _
                                                    "<tbody>"

    Dim alertCheckOutToMemberRecord As String = "<tr bgcolor='Cornsilk'>" & _
                                                    "<td style='color: $foreColor'>$AlertCheckOutMsg</td>" & _
                                                "</tr>"

    Dim alertReturnBookMemberOpening As String = "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                    "<thead>" & _
                                                        "<tr bgcolor='LightCyan'>" & _
                                                            "<th>Scheduled Return Book Notification</th>" & _
                                                        "</tr>" & _
                                                    "</thead>" & _
                                                    "<tbody>"

    Dim alertReturnBookMemberRecord As String = "<tr bgcolor='Cornsilk'>" & _
                                                    "<td style='color: $foreColor'>$AlertReturnBookMsg</td>" & _
                                                "</tr>"

    Sub Main()

        If Not (Date.Today.DayOfWeek = DayOfWeek.Saturday) And Not (Date.Today.DayOfWeek = DayOfWeek.Sunday) Then
            'set current UI culture to "en-US"
            Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy"

            todayString = Date.Today.ToString("MM/dd/yyyy")
            toDay = New DateTime(Date.Today.Year, Date.Today.Month, Date.Today.Day, 0, 0, 0)
            yesterDay = toDay.AddDays(-1)
            While yesterDay.DayOfWeek = DayOfWeek.Saturday Or yesterDay.DayOfWeek = DayOfWeek.Sunday
                yesterDay = yesterDay.AddDays(-1)
            End While
            nextBusinessDay = toDay.AddDays(1)
            While nextBusinessDay.DayOfWeek = DayOfWeek.Saturday Or nextBusinessDay.DayOfWeek = DayOfWeek.Sunday
                nextBusinessDay = nextBusinessDay.AddDays(1)
            End While

            msg = "Start Library Server Operation at " & Date.Now.ToString & "."
            'Console.WriteLine(msg)
            WriteToEventLog(msg, EventLogEntryType.Information)
            EmailBody &= emailHeaderOpening.Replace("$OperationMessageOnDate", "Start Library Server Operation at " & Date.Now.ToString)

            LibraryDB = New ckLibraryEntities()
            ComitteOperation = (From o In LibraryDB.ServerOperations
                                Where o.OperationStamp >= toDay _
                                And o.OperationStamp <= nextBusinessDay _
                                And o.OperationType = "Daily Batch Comitte" _
                                And o.OperationResult = True
                                Select o).FirstOrDefault()
            If IsNothing(ComitteOperation) Then
                msg = "Start Library Daily Batch Comitte Operation On " & todayString & "."
                'Console.WriteLine(msg)
                WriteToEventLog(msg, EventLogEntryType.Information)

                Try
                    ProcessBatchComitteToPendingRequests()
                    ReportYesterdayNewRequest()
                    ReportYesterdayModifiedRequest()
                    ReportYesterdayModifiedTransaction()
                    ReportTodayScheduledIssues()
                    ReportTodayScheduledReturns()
                    Dim comitteOperation = New ServerOperation With {
                        .OperationType = "Daily Batch Comitte", _
                        .OperationResult = True, _
                        .OperationResultDetail = "Success", _
                        .OperationStamp = Date.Now()
                    }
                    LibraryDB.ServerOperations.AddObject(comitteOperation)
                    LibraryDB.SaveChanges()
                    msg = "Batch Comitte Processing done successfully at " & Date.Now.ToString & "."
                    'Console.WriteLine(msg)
                    WriteToEventLog(msg, EventLogEntryType.Information)
                    EmailBody &= emailHeaderClosing
                    SendEmailReportToAdmin()
                Catch ex As Exception
                    msg = "Error while process batch comitte [ see error detail below ]" & vbCrLf & ex.Message
                    'Console.WriteLine(msg)
                    WriteToEventLog(msg, EventLogEntryType.Information)
                End Try

            Else
                msg = "Find Another Library Daily Batch Comitte Operation Result On " & todayString & "."
                'Console.WriteLine(msg)
                WriteToEventLog(msg, EventLogEntryType.Information)
            End If

            msg = "End Library Server Operation at " & Date.Now.ToString & "."
            'Console.WriteLine(msg)
            WriteToEventLog(msg, EventLogEntryType.Information)

            'Console.WriteLine()
            'Console.WriteLine("Hit enter key to exit.")
            'Console.ReadLine()
        End If

    End Sub

    Public Function WriteToEventLog(ByVal Entry As String, ByVal inEntryType As EventLogEntryType)

        Dim appName As String = "ckcOnlineLibrary Server Operation"
        '        Dim eventType As EventLogEntryType = EventLogEntryType.Error
        '        inEntryType = "Information" or "Error"
        Dim logName = "Application"

        Dim objEventLog As New EventLog()

        Try
            'Register the App as an Event Source
            If Not EventLog.SourceExists(appName) Then
                EventLog.CreateEventSource(appName, logName)
            End If


            objEventLog.Source = appName


            'WriteEntry is overloaded; this is one
            'of 10 ways to call it
            objEventLog.WriteEntry(Entry, inEntryType)
            Return True
        Catch Ex As Exception
            Return False

        End Try

    End Function

    Private Sub ProcessBatchComitteToPendingRequests()

        ComittableRequests = From r In LibraryDB.vwComittableRequests
                             Order By r.Request_Date, r.Request_id
                             Select r

        'Compose Email Body For Admin
        EmailBody &= comitteTableOpening
        Dim comitteCounter As Integer = 0

        For Each cr As vwComittableRequest In ComittableRequests

            Dim request_id = cr.Request_id
            Dim member_id = cr.MemberID
            Dim book_id = cr.Book_id
            MemberRequest = (From m In LibraryDB.Members
                             Where m.MemberID = member_id
                             Select m).FirstOrDefault

            BookRequest = (From b In LibraryDB.Books
                           Where b.Book_id = book_id
                           Select b).FirstOrDefault

            If IsNothing(BookRequest) Then
                Continue For
            End If
            If BookRequest.Book_Quantity_Onhand > 0 Then
                If Count_Unreturned_Transactions_Of_Member(cr.MemberID) < 3 Then
                    Dim changedBook = Allocate_Book(BookRequest)
                    LibraryDB.Refresh(RefreshMode.StoreWins, BookRequest)
                    If Not IsNothing(changedBook) Then
                        Try
                            Dim tmpLibraryDB As ckLibraryEntities = New ckLibraryEntities()
                            Using (tmpLibraryDB)
                                ComittableRequest = (From r In tmpLibraryDB.Member_Requests
                                                     Where r.Request_id = request_id
                                                     Select r).FirstOrDefault

                                If ComittableRequest.Expected_Date < nextBusinessDay Then
                                    ComittableRequest.Comitted_Date = nextBusinessDay
                                Else
                                    ComittableRequest.Comitted_Date = ComittableRequest.Expected_Date
                                End If
                                tmpLibraryDB.SaveChanges(Objects.SaveOptions.DetectChangesBeforeSave)
                            End Using
                            'Compose email for admin
                            Dim tmpEmailBody As String = comitteTableRecord.Replace("$Book", BookRequest.Book_Title)
                            If ((comitteCounter Mod 2) = 0) Then
                                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Cornsilk")
                            Else
                                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Beige")
                            End If
                            tmpEmailBody = tmpEmailBody.Replace("$Member", MemberRequest.FullName)
                            EmailBody &= tmpEmailBody.Replace("$ComittedDate", CType(ComittableRequest.Comitted_Date, Date).ToString("MM/dd/yyyy"))
                            comitteCounter += 1
                        Catch ex As Exception
                            'Console.WriteLine(ex.Message)
                            WriteToEventLog(ex.Message, EventLogEntryType.Error)
                            Dim tmpEmailBody As String = comitteTableRecord.Replace("$Book", BookRequest.Book_Title)
                            tmpEmailBody = tmpEmailBody.Replace("$Member", MemberRequest.FullName)
                            EmailBody &= tmpEmailBody.Replace("$ComittedDate", ex.Message)
                        End Try
                        If Not String.IsNullOrEmpty(MemberRequest.Email) Then
                            SendComittedEmailToMember(MemberRequest.Email, BookRequest.Book_Title, ComittableRequest.Comitted_Date)
                        End If
                    Else
                        'Console.WriteLine(BookRequest.Book_Title & " comitting error")
                        WriteToEventLog(BookRequest.Book_Title & " comitting error", EventLogEntryType.Error)
                        Dim tmpEmailBody As String = comitteTableRecord.Replace("$Book", BookRequest.Book_Title)
                        tmpEmailBody = tmpEmailBody.Replace("$Member", MemberRequest.FullName)
                        EmailBody &= tmpEmailBody.Replace("$ComittedDate", "Comitte Error")
                    End If
                End If
            End If
        Next

        'Compose Email Body For Admin
        If comitteCounter = 0 Then
            EmailBody &= comitteTableFooter.Replace("$Count", "No")
        Else
            EmailBody &= comitteTableFooter.Replace("$Count", comitteCounter.ToString)
        End If
        EmailBody &= tableClosing

    End Sub

    Public Sub SendComittedEmailToMember(ByVal toAddress As String, ByVal book_Title As String, ByVal comittedDate As Date)
        If IsNothing(smtp) Then
            InitialzeSmtpClient()
        End If
        If Not IsNothing(smtp) Then
            Try
                Dim msg As MailMessage = New MailMessage()

                ' Send email 
                msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
                msg.To.Add(New MailAddress(toAddress))
                msg.Subject = "[" & book_Title & "] Comitted."

                msg.Body &= emailHeaderOpening.Replace("$OperationMessageOnDate", msg.Subject)
                msg.Body &= comitteBookToMemberOpening
                Dim thanksMessage = "Thanks for your using Columbia Kitchen Cabinets' library.<br />[" & book_Title & _
                           "] will be ready for you on " & comittedDate.ToString("MM/dd/yyyy") & "."
                msg.Body &= comitteBookToMemberRecord.Replace("$ComittedMsg", thanksMessage)
                msg.Body &= tableClosing
                msg.Body &= emailHeaderClosing
                msg.IsBodyHtml = True

                smtp.Send(msg)
            Catch ex As Exception
                'Console.WriteLine(ex.Message)
                WriteToEventLog(ex.Message, EventLogEntryType.Error)
            End Try

        End If
    End Sub

    Public Sub InitialzeSmtpClient()

        smtp = New SmtpClient("columbiacabinetslibrary.com")

        smtp.EnableSsl = False
        smtp.UseDefaultCredentials = False
        smtp.Credentials = New NetworkCredential("libraryadmin@columbiacabinetslibrary.com", "qwaszx12")

    End Sub

    Public Sub SendEmailReportToAdmin()
        If IsNothing(smtp) Then
            InitialzeSmtpClient()
        End If
        If Not IsNothing(smtp) Then
            Try
                'Compose email
                Dim msg As MailMessage = New MailMessage()
                'from
                msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")

                Dim admins = From a In LibraryDB.Members
                             Where a.Role.Contains("Manager")
                             Select a
                'to
                For Each a As Member In admins
                    If Not String.IsNullOrEmpty(a.Email) Then
                        msg.To.Add(New MailAddress(a.Email))
                    End If
                Next
                'subject and body
                msg.Subject = "Columbia Kitchen Cabinets Library's Daily Report"
                msg.Body = EmailBody
                msg.IsBodyHtml = True

                ' Send email 
                smtp.Send(msg)
            Catch ex As Exception
                'Console.WriteLine(ex.Message)
                WriteToEventLog(ex.Message, EventLogEntryType.Error)
            End Try
        End If
    End Sub

    Public Function Allocate_Book(ByVal currentBook As Book) As Book

        'Using Concurrency control with RowVersion timestamp
        'Check Book_id
        Dim bookID As Integer = currentBook.Book_id
        Dim tmpLibraryDB As ckLibraryEntities = New ckLibraryEntities()
        Using (tmpLibraryDB)
            Dim booksInDB = From b In tmpLibraryDB.Books _
                            Where b.Book_id = bookID _
                            Select b

            'Check book
            If booksInDB.Count < 1 Then
                Return Nothing
            End If
            Dim bookInDB = booksInDB.FirstOrDefault()

            If bookInDB.Book_Quantity_Onhand <= 0 Then
                Return Nothing
            Else
                Try
                    'Detach it first
                    tmpLibraryDB.Detach(bookInDB)
                    'Update the book
                    bookInDB.Book_Title = currentBook.Book_Title
                    bookInDB.Book_Image_File = currentBook.Book_Image_File
                    bookInDB.Book_Description = currentBook.Book_Description
                    bookInDB.Book_Media = currentBook.Book_Media
                    bookInDB.Book_Language = currentBook.Book_Language
                    bookInDB.Book_ISBN = currentBook.Book_ISBN
                    bookInDB.Book_Published = currentBook.Book_Published
                    bookInDB.Book_Quantity_Avail = currentBook.Book_Quantity_Avail
                    bookInDB.Book_Quantity_Onhand -= 1 'increase quantity by 1
                    bookInDB.IsActive = currentBook.IsActive
                    bookInDB.RowVersion = currentBook.RowVersion
                    'Attach it
                    tmpLibraryDB.Attach(bookInDB)
                    'Change object state
                    tmpLibraryDB.ObjectStateManager.ChangeObjectState(bookInDB, EntityState.Modified)
                    tmpLibraryDB.SaveChanges(SaveOptions.DetectChangesBeforeSave)
                    Return bookInDB
                Catch ex As Exception
                    Return Nothing
                End Try
            End If
        End Using

    End Function

    Public Function Count_Unreturned_Transactions_Of_Member(ByVal member_id As Integer) As Integer
        Dim returnCount As Integer = 0

        Dim transactionQuery As IQueryable(Of Member_Transactions)
        transactionQuery = From mt In LibraryDB.Member_Transactions
                           Where mt.MemberID = member_id _
                            And mt.Returned_Date Is Nothing
                           Select mt


        Dim noDate As Date = #12:00:00 AM#
        Dim comittedRequestQuery As IQueryable(Of Member_Requests)
        comittedRequestQuery = From mr In LibraryDB.Member_Requests
                               Where mr.MemberID = member_id _
                                And mr.Request_Status = False _
                                And mr.Comitted_Date IsNot Nothing _
                                And mr.Comitted_Date > noDate _
                               Select mr

        returnCount = transactionQuery.Count() + comittedRequestQuery.Count()
        Return returnCount
    End Function

    Private Sub ReportYesterdayNewRequest()
        Dim newRequests = From r In LibraryDB.Member_Requests
                          Where r.Request_Date >= yesterDay _
                            And r.Request_Date < toDay
                          Select r

        'Compose Email Body For Admin
        EmailBody &= newRequestTableOpening
        Dim Counter As Integer = 0

        For Each nr As Member_Requests In newRequests

            'Compose email for admin
            Dim tmpEmailBody As String = newRequestTableRecord.Replace("$Book", nr.Book.Book_Title)
            If ((Counter Mod 2) = 0) Then
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Cornsilk")
            Else
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Beige")
            End If
            tmpEmailBody = tmpEmailBody.Replace("$Member", nr.Member.FullName)
            tmpEmailBody = tmpEmailBody.Replace("$ExpectedDate", CType(nr.Expected_Date, Date).ToString("MM/dd/yyyy"))
            If IsNothing(nr.Comitted_Date) Then
                EmailBody &= tmpEmailBody.Replace("$ComittedDate", "Not comitted")
            Else
                Dim dayString As String = CType(nr.Comitted_Date, Date).ToString("MM/dd/yyyy")
                If dayString.Contains("01/01/0001") Then
                    EmailBody &= tmpEmailBody.Replace("$ComittedDate", "Not comitted")
                Else
                    EmailBody &= tmpEmailBody.Replace("$ComittedDate", CType(nr.Comitted_Date, Date).ToString("MM/dd/yyyy"))
                End If
            End If

            Counter += 1
        Next

        'Compose Email Body For Admin
        If Counter = 0 Then
            EmailBody &= newRequestTableFooter.Replace("$Count", "No")
        Else
            EmailBody &= newRequestTableFooter.Replace("$Count", Counter.ToString)
        End If
        EmailBody &= tableClosing
    End Sub

    Private Function GetYesterdayComment(ByVal comment As String) As String
        Dim quote As Char = ChrW(34)
        Dim regexString As String = _
            "[a-zA-Z0-9" & quote.ToString & "_:;'< >,?/`~{}()+=@#$%^&*\.\\\!\[\]\-\w\n\r\t]+"
        Dim regex As Regex
        Dim match As Match
        regex = New Regex("^" & regexString & yesterDay.ToString("MM/dd/yyyy") & "\r\n")
        match = regex.Match(comment)

        Return match.Value.Replace(vbNewLine, "<br />")
    End Function

    Private Sub ReportYesterdayModifiedRequest()
        Dim modifiedRequests = From r In LibraryDB.Member_Requests
                               Where (r.AdminCommented >= yesterDay And r.AdminCommented < toDay) _
                                 Or (r.UserCommented >= yesterDay And r.UserCommented < toDay)
                               Select r

        'Compose Email Body For Admin
        EmailBody &= modifiedRequestTableOpening
        Dim Counter As Integer = 0

        For Each nr As Member_Requests In modifiedRequests

            'Compose email for admin
            Dim tmpEmailBody As String = modifiedRequestTableRecord.Replace("$Book", nr.Book.Book_Title)
            If ((Counter Mod 2) = 0) Then
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Cornsilk")
            Else
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Beige")
            End If
            tmpEmailBody = tmpEmailBody.Replace("$Member", nr.Member.FullName)
            tmpEmailBody = tmpEmailBody.Replace("$Comment", GetYesterdayComment(nr.Comment))
            tmpEmailBody = tmpEmailBody.Replace("$ExpectedDate", CType(nr.Expected_Date, Date).ToString("MM/dd/yyyy"))
            EmailBody &= tmpEmailBody.Replace("$ComittedDate", CType(nr.Comitted_Date, Date).ToString("MM/dd/yyyy"))

            Counter += 1
        Next

        'Compose Email Body For Admin
        If Counter = 0 Then
            EmailBody &= modifiedRequestTableFooter.Replace("$Count", "No")
        Else
            EmailBody &= modifiedRequestTableFooter.Replace("$Count", Counter.ToString)
        End If
        EmailBody &= tableClosing
    End Sub

    Private Sub ReportYesterdayModifiedTransaction()
        Dim modifiedTransactions = From t In LibraryDB.Member_Transactions
                                   Where (t.AdminCommented >= yesterDay And t.AdminCommented < toDay) _
                                      Or (t.UserCommented >= yesterDay And t.UserCommented < toDay)
                                   Select t

        'Compose Email Body For Admin
        EmailBody &= modifiedTransactionTableOpening
        Dim Counter As Integer = 0

        For Each nt As Member_Transactions In modifiedTransactions

            'Compose email for admin
            Dim tmpEmailBody As String = modifiedTransactionTableRecord.Replace("$Book", nt.Book.Book_Title)
            If ((Counter Mod 2) = 0) Then
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Cornsilk")
            Else
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Beige")
            End If
            tmpEmailBody = tmpEmailBody.Replace("$Member", nt.Member.FullName)
            tmpEmailBody = tmpEmailBody.Replace("$Comment", GetYesterdayComment(nt.Comment))
            EmailBody &= tmpEmailBody.Replace("$ExpectedDate", nt.Expected_Return.ToString("MM/dd/yyyy"))

            Counter += 1
        Next

        'Compose Email Body For Admin
        If Counter = 0 Then
            EmailBody &= modifiedTransactionTableFooter.Replace("$Count", "No")
        Else
            EmailBody &= modifiedTransactionTableFooter.Replace("$Count", Counter.ToString)
        End If
        EmailBody &= tableClosing
    End Sub

    Private Sub ReportTodayScheduledIssues()
        Dim checkoutRequests = From r In LibraryDB.Member_Requests
                               Where (r.Request_Status = False And r.Comitted_Date IsNot Nothing And r.Comitted_Date <= toDay)
                               Select r

        'Compose Email Body For Admin
        EmailBody &= todayCheckOutTableOpening
        Dim Counter As Integer = 0

        For Each nr As Member_Requests In checkoutRequests

            'Compose email for admin
            Dim tmpEmailBody As String = todayCheckOutTableRecord.Replace("$Book", nr.Book.Book_Title)
            If ((Counter Mod 2) = 0) Then
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Cornsilk")
            Else
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Beige")
            End If
            tmpEmailBody = tmpEmailBody.Replace("$Member", nr.Member.FullName)
            tmpEmailBody = tmpEmailBody.Replace("$Comment", nr.Comment.Replace(vbNewLine, "<br />"))
            If nr.Comitted_Date < toDay Then
                tmpEmailBody = tmpEmailBody.Replace("$foreColor", "#FF0000")
                SendDelayedCheckOutAlertToMember(nr.Member.Email, nr.Book.Book_Title, nr.Comitted_Date)
            Else
                tmpEmailBody = tmpEmailBody.Replace("$foreColor", "#000000")
                SendCheckOutAlertToMember(nr.Member.Email, nr.Book.Book_Title, nr.Comitted_Date)
            End If
            EmailBody &= tmpEmailBody.Replace("$ComittedDate", CType(nr.Comitted_Date, Date).ToString("MM/dd/yyyy"))

            Counter += 1
        Next

        'Compose Email Body For Admin
        If Counter = 0 Then
            EmailBody &= todayCheckOutTableFooter.Replace("$Count", "No")
        Else
            EmailBody &= todayCheckOutTableFooter.Replace("$Count", Counter.ToString)
        End If
        EmailBody &= tableClosing
    End Sub

    Public Sub SendDelayedCheckOutAlertToMember(ByVal toAddress As String, ByVal book_Title As String, ByVal comittedDate As Date)
        If IsNothing(smtp) Then
            InitialzeSmtpClient()
        End If
        If Not IsNothing(smtp) Then
            Try
                Dim msg As MailMessage = New MailMessage()

                ' Send email 
                msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
                msg.To.Add(New MailAddress(toAddress))
                msg.Subject = "[" & book_Title & "] is waiting for you today."

                msg.Body &= emailHeaderOpening.Replace("$OperationMessageOnDate", msg.Subject)
                msg.Body &= alertCheckOutToMemberOpening
                Dim alertMessage = "Thanks for your using Columbia Kitchen Cabinets' library.<br />[" & book_Title & _
                           "] has been waiting for you since " & comittedDate.ToString("MM/dd/yyyy") & ".<br />" & _
                           "Please pick it up today."
                Dim tmpBody = alertCheckOutToMemberRecord.Replace("$AlertCheckOutMsg", alertMessage)
                msg.Body &= tmpBody.Replace("$foreColor", "#FF0000")
                msg.Body &= tableClosing
                msg.Body &= emailHeaderClosing
                msg.IsBodyHtml = True

                smtp.Send(msg)
            Catch ex As Exception
                'Console.WriteLine(ex.Message)
                WriteToEventLog(ex.Message, EventLogEntryType.Error)
            End Try

        End If
    End Sub

    Public Sub SendCheckOutAlertToMember(ByVal toAddress As String, ByVal book_Title As String, ByVal comittedDate As Date)
        If IsNothing(smtp) Then
            InitialzeSmtpClient()
        End If
        If Not IsNothing(smtp) Then
            Try
                Dim msg As MailMessage = New MailMessage()

                ' Send email 
                msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
                msg.To.Add(New MailAddress(toAddress))
                msg.Subject = "[" & book_Title & "] is ready for you today."

                msg.Body &= emailHeaderOpening.Replace("$OperationMessageOnDate", msg.Subject)
                msg.Body &= alertCheckOutToMemberOpening
                Dim alertMessage = "Thanks for your using Columbia Kitchen Cabinets' library.<br />[" & book_Title & _
                           "] is ready for you today.<br />Please pick it up today."
                Dim tmpBody = alertCheckOutToMemberRecord.Replace("$AlertCheckOutMsg", alertMessage)
                msg.Body &= tmpBody.Replace("$foreColor", "#000000")
                msg.Body &= tableClosing
                msg.Body &= emailHeaderClosing
                msg.IsBodyHtml = True

                smtp.Send(msg)
            Catch ex As Exception
                'Console.WriteLine(ex.Message)
                WriteToEventLog(ex.Message, EventLogEntryType.Error)
            End Try

        End If
    End Sub

    Private Sub ReportTodayScheduledReturns()
        Dim expectedTransactions = From t In LibraryDB.Member_Transactions
                               Where (t.Returned_Date Is Nothing And t.Expected_Return <= toDay)
                               Select t

        'Compose Email Body For Admin
        EmailBody &= todayReturnBookTableOpening
        Dim Counter As Integer = 0

        For Each tr As Member_Transactions In expectedTransactions

            'Compose email for admin
            Dim tmpEmailBody As String = todayReturnBookTableRecord.Replace("$Book", tr.Book.Book_Title)
            If ((Counter Mod 2) = 0) Then
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Cornsilk")
            Else
                tmpEmailBody = tmpEmailBody.Replace("$BackgroudColor", "Beige")
            End If
            tmpEmailBody = tmpEmailBody.Replace("$Member", tr.Member.FullName)
            tmpEmailBody = tmpEmailBody.Replace("$Comment", tr.Comment.Replace(vbNewLine, "<br />"))
            If tr.Expected_Return < toDay Then
                tmpEmailBody = tmpEmailBody.Replace("$foreColor", "#FF0000")
                SendOverdueReturnAlertToMember(tr.Member.Email, tr.Book.Book_Title, tr.Expected_Return)
            Else
                tmpEmailBody = tmpEmailBody.Replace("$foreColor", "#000000")
                SendReturnAlertToMember(tr.Member.Email, tr.Book.Book_Title, tr.Expected_Return)
            End If
            EmailBody &= tmpEmailBody.Replace("$ExpectedReturnDate", tr.Expected_Return.ToString("MM/dd/yyyy"))

            Counter += 1
        Next

        'Compose Email Body For Admin
        If Counter = 0 Then
            EmailBody &= todayReturnBookTableFooter.Replace("$Count", "No")
        Else
            EmailBody &= todayReturnBookTableFooter.Replace("$Count", Counter.ToString)
        End If
        EmailBody &= tableClosing
    End Sub

    Private Sub SendOverdueReturnAlertToMember(ByVal toAddress As String, ByVal book_Title As String, ByVal expectedDate As Date)
        If IsNothing(smtp) Then
            InitialzeSmtpClient()
        End If
        If Not IsNothing(smtp) Then
            Try
                Dim msg As MailMessage = New MailMessage()

                ' Send email 
                msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
                msg.To.Add(New MailAddress(toAddress))
                msg.Subject = "[" & book_Title & "] is waiting for you today."

                msg.Body &= emailHeaderOpening.Replace("$OperationMessageOnDate", msg.Subject)
                msg.Body &= alertReturnBookMemberOpening
                Dim alertMessage = "Thanks for your using Columbia Kitchen Cabinets' library.<br />[" & book_Title & _
                           "]'s return due date was " & expectedDate.ToString("MM/dd/yyyy") & ".<br />" & _
                           "Please return it today."
                Dim tmpBody = alertCheckOutToMemberRecord.Replace("$AlertCheckOutMsg", alertMessage)
                msg.Body &= tmpBody.Replace("$foreColor", "#FF0000")
                msg.Body &= tableClosing
                msg.Body &= emailHeaderClosing
                msg.IsBodyHtml = True

                smtp.Send(msg)
            Catch ex As Exception
                'Console.WriteLine(ex.Message)
                WriteToEventLog(ex.Message, EventLogEntryType.Error)
            End Try

        End If
    End Sub

    Private Sub SendReturnAlertToMember(ByVal toAddress As String, ByVal book_Title As String, ByVal expectedDate As Date)
        If IsNothing(smtp) Then
            InitialzeSmtpClient()
        End If
        If Not IsNothing(smtp) Then
            Try
                Dim msg As MailMessage = New MailMessage()

                ' Send email 
                msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
                msg.To.Add(New MailAddress(toAddress))
                msg.Subject = "[" & book_Title & "] is scheduled to be returned today."

                msg.Body &= emailHeaderOpening.Replace("$OperationMessageOnDate", msg.Subject)
                msg.Body &= alertReturnBookMemberOpening
                Dim alertMessage = "Thanks for your using Columbia Kitchen Cabinets' library.<br />[" & book_Title & _
                           "] is scheduled to be returned today.<br />Please return it today."
                Dim tmpBody = alertReturnBookMemberRecord.Replace("$AlertReturnBookMsg", alertMessage)
                msg.Body &= tmpBody.Replace("$foreColor", "#000000")
                msg.Body &= tableClosing
                msg.Body &= emailHeaderClosing
                msg.IsBodyHtml = True

                smtp.Send(msg)
            Catch ex As Exception
                'Console.WriteLine(ex.Message)
                WriteToEventLog(ex.Message, EventLogEntryType.Error)
            End Try

        End If
    End Sub

End Module
