﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью 
// набора атрибутов. Измените значения этих атрибутов для изменения сведений,
// связанных со сборкой.
[assembly: AssemblyTitle("FileManager")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kbyte.Ru, Aleksey S Nemiro")]
[assembly: AssemblyProduct("FileManager")]
[assembly: AssemblyCopyright("Copyright © Aleksey S Nemiro, 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения false атрибута ComVisible делает типы этой сборки невидимыми 
// для компонентов COM.  Если необходимо обратиться к типу в этой сборке через 
// COM, задайте атрибуту ComVisible значение true для требуемого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект видим для COM
[assembly: Guid("fa2e49be-6ab7-4a25-95a6-b6eaecd88cff")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер построения
//      Редакция
//
// Можно задать все значения или принять номер построения и номер редакции по умолчанию, 
// с помощью знака "*", как показано ниже:
[assembly: AssemblyVersion("1.2012.1.8")]
[assembly: AssemblyFileVersion("1.2012.1.8")]
