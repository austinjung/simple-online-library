﻿/*
 * This is an example for article «Silverlight File Manager»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=68&mode=art
 * (russian language only)
 * Translated into English special for CodeProject
 * http://www.codeproject.com/
 * Author: Aleksey S Nemiro
 * http://aleksey.nemiro.ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2012
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;

namespace Nemiro.FileManager
{
  public partial class PleaseWait : ChildWindow
  {
    public PleaseWait()
    {
      InitializeComponent();
    }

    private void OKButton_Click(object sender, RoutedEventArgs e)
    {
      this.DialogResult = true;
    }

    private void CancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.DialogResult = false;
    }

    private void ChildWindow_Loaded(object sender, RoutedEventArgs e)
    {
      new Timer(new TimerCallback(Timer_OnTick), null, 50, 50);
    }

    private void Timer_OnTick(object sender)
    {
      this.Dispatcher.BeginInvoke(() =>
      {
        progressBar1.Value++;
        if (progressBar1.Value == progressBar1.Maximum) progressBar1.Value = 0;
      });
    }
  }
}

