﻿/*
 * This is an example for article «Silverlight File Manager»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=68&mode=art
 * (russian language only)
 * Translated into English special for CodeProject
 * http://www.codeproject.com/
 * Author: Aleksey S Nemiro
 * http://aleksey.nemiro.ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2012
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Browser;
using System.Text;
using System.Json;

namespace Nemiro.FileManager
{
  /// <summary>
  /// The helper class for uploading files
  /// </summary>
  /// <remarks>The class created for simply code. You can unite the UploadItem class and FileItem class.</remarks>
  public class UploadItem
  {
    /// <summary>
    /// The event occurs after the request is sent
    /// </summary>
    public event EventHandler Complete;

    /// <summary>
    /// State code list
    /// </summary>
    public enum StateList
    {
      /// <summary>
      /// File sent successfully
      /// </summary>
      OK,
      /// <summary>
      /// Error
      /// </summary>
      Error,
      /// <summary>
      /// File is waiting
      /// </summary>
      Wait
    }

    private StateList _State = StateList.Wait;
    private string _Message = String.Empty;
    private int _Index = 0;
    private string _FileName = String.Empty;
    private Stream _FileStream = null;
    private string _Path = String.Empty;
    private string _Url = String.Empty;

    /// <summary>
    /// Upload state
    /// </summary>
    public StateList State
    {
      get { return _State; }
    }

    /// <summary>
    /// Error message
    /// </summary>
    public string Message
    {
      get { return _Message; }
    }

    /// <summary>
    /// File name
    /// </summary>
    public string FileName
    {
      get { return _FileName; }
    }

    /// <summary>
    /// File index
    /// </summary>
    public int Index
    {
      get { return _Index; }
    }

    /// <param name="f">File</param>
    /// <param name="idx">File index</param>
    /// <param name="url">Url for uploading</param>
    /// <param name="path">Server path</param>
    public UploadItem(int idx, FileInfo f, string url, string path)
    {
      _Index = idx;
      
      _Path = path;
      _Url = url;

      _FileName = f.Name; // set file name
      _FileStream = f.OpenRead(); // open file stream
    }

    public void Run()
    {
      try
      {
        // send request for check server
        WebHelper w = new WebHelper(_Url);
        w.Queries.Add("cmd", "check");
        w.Queries.Add("path", _Path);
        w.Queries.Add("name", _FileName);
        w.Execute(CheckNameResult);
      }
      catch (Exception ex)
      {
        _State = StateList.Error;
        _Message = ex.Message;
        if(Complete != null) Complete(this, null);
      }
    }
    private void CheckNameResult(string stat, string msg, bool allowUp, JsonValue data, object tag)
    {
      try
      {
        if (stat == "ok")
        {
          // send file
          WebHelper w = new WebHelper(_Url);
          w.Queries.Add("cmd", "upload");
          w.Queries.Add("path", _Path);
          w.Queries.Add("file1", _FileName, _FileStream);//add file to request
          w.Execute(UploadResult);
        }
        else
        {
          // error
          _State = StateList.Error;
          _Message = msg;
          if(Complete != null) Complete(this, null);
        }
      }
      catch (Exception ex)
      {
        _State = StateList.Error;
        _Message = ex.Message;
        if(Complete != null) Complete(this, null);
      }
    }
    private void UploadResult(string stat, string msg, bool allowUp, JsonValue data, object tag)
    {
      if (stat == "ok")
      {
        _State = StateList.OK;
      }
      else
      {
        // error
        _State = StateList.Error;
        _Message = msg;
      }
      if(Complete != null) Complete(this, null);
    }

  }
}