﻿/*
 * This is an example for article «Silverlight File Manager»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=68&mode=art
 * (russian language only)
 * Translated into English special for CodeProject
 * http://www.codeproject.com/
 * Author: Aleksey S Nemiro
 * http://aleksey.nemiro.ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2012
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.Text;
using System.Windows.Browser;
using System.Json;
using System.ComponentModel;
using System.ServiceModel.DomainServices.Client;

namespace Nemiro.FileManager
{
  /// <summary>
  /// FileList control
  /// </summary>
  public class FileList : ListBox
  {

    string myUrl = string.Empty;

    /// <summary>
    /// The event occurs before the request is sent
    /// </summary>
    public event EventHandler Process;
    /// <summary>
    /// The event occurs after the request is sent
    /// </summary>
    public event EventHandler Complete;

    private string _Url = "";
    private string _Path = "/";
    private List<UploadItem> _UploadFiles = null; // the file list to upload 
    private StringBuilder _UploadErrorMessages = null; // upload error messages

    /// <summary>
    /// The path of the directory on the server
    /// </summary>
    public string Path
    {
      get
      {
        return _Path;
      }
      set
      {
        _Path = value;
        if (String.IsNullOrEmpty(_Path)) _Path = "/";
      }
    }

    /// <summary>
    /// The file list to upload 
    /// </summary>
    public List<UploadItem> UploadFiles
    {
      get
      {
        return _UploadFiles;
      }
    }

    /// <summary>
    /// Gateway url
    /// </summary>
    public string Url
    {
      get
      {
        return _Url;
      }
      set
      {
        _Url = value;
        if (!System.ComponentModel.DesignerProperties.IsInDesignTool)// is not Visual Studio designer
        { //update file list
          UpdateFileList();
        }
      }
    }

    public FileList() : base()
    {
        if (!DesignerProperties.IsInDesignTool)
        {
            string myUrl = Application.Current.Host.Source.Scheme.ToString() +
                           "://" + Application.Current.Host.Source.Host.ToString() +
                           ":" + Application.Current.Host.Source.Port.ToString() + "/FileManagerIcons/";
            //ImageBrush ib = new ImageBrush();
            //ib.ImageSource = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + "3.png", UriKind.Absolute));
            //ib.Stretch = Stretch.Uniform;
            //this.Background = ib;
            this.AllowDrop = true;
            this.KeyUp += new KeyEventHandler(FileList_KeyUp); // add KeyUp handler
            this.Drop += new DragEventHandler(FileList_Drop); // add Drop handler
        }
    }

    /// <summary>
    /// FileList KeyUp handler
    /// </summary>
    private void FileList_KeyUp(object sender, KeyEventArgs e)
    {
      if (((FileList)sender).SelectedItem == null) return;
      FileItem itm = ((FileList)sender).SelectedItem as FileItem;
      if (e.Key == Key.Enter && !itm.IsEdit)
      { // open file or go into the directory
        itm.Open();
      }
      else if (e.Key == Key.Enter && itm.IsEdit)
      { // finish editing
        itm.EditComplete();
      }
      else if (e.Key == Key.F2 && itm.CanEdit && !itm.IsEdit)
      { // start editing
        itm.EditStart();
      }
      else if (e.Key == Key.Escape && itm.IsEdit)
      { // cancel editing
        itm.EditCancel();
      }
      else if (e.Key == Key.Delete && !itm.IsEdit)
      { // delete file or directory
        itm.Delete();
      }
      else if (e.Key == Key.F5)
      { // update file list
        UpdateFileList();
      }
    }

    #region update file list
    /// <summary>
    /// Send request to the server for a list of files
    /// </summary>
    public void UpdateFileList()
    {
      if (Process!=null) Process(this, null);; // start Process

      // send request
      WebHelper w = new WebHelper(this.Url);
      w.Queries.Add("cmd", "get");
      w.Queries.Add("path", _Path);
      w.Execute(UpdateFileListResult); // the server response we can found in the UpdateFileListResult method
    }
    private void UpdateFileListResult(string stat, string msg, bool allowUp, JsonValue data, object tag)
    {
      if (stat == "ok")
      {
        // clear FileList items
        this.Dispatcher.BeginInvoke(() =>
        {
          this.Items.Clear();
        });

        // add top-level directory
        if (allowUp)
        {
          AddItem(-1, "...", "", 0);
        }

        // add files and directories
        if (data != null && data.Count > 0)
        {
          foreach (JsonValue itm in data)
          {
            AddItem(itm["type"], itm["name"], itm["url"], itm["size"]);
          }
        }

        // set focus to the first item
        this.Dispatcher.BeginInvoke(() =>
        {
          this.SelectedIndex = -1;
          this.Focus();
          if (this.Items.Count > 0) this.SelectedIndex = 0;
        });
      }
      else
      {
        // show error message
        ShowError("Error. " + msg);
      }

      if (Complete != null) Complete(this, null); // start Complete
    }
    
    /// <summary>
    /// The method adds an item to the list
    /// </summary>
    /// <param name="type">Item type: -1 - top-level directory, 0 - directory, 1 - file</param>
    /// <param name="name">Name</param>
    /// <param name="size">File size (kb)</param>
    private void AddItem(int type, string name, string url, double size)
    {
      this.Dispatcher.BeginInvoke(() =>
      {
        FileItem itm = new FileItem(type, name, url, size);
        this.Items.Add(itm);
      });
    }
    #endregion
    #region rename file or directory
    /// <summary>
    /// Send request to the server for rename item
    /// </summary>
    public void SetNewName(FileItem itm)
    {
      if (itm == null || !itm.IsEdit)
      {
          var messageBox = new MessageBoxPopUp();
          messageBox.Show("Error",
                          "The item can not be changed!",
                          Buttons.Ok);
          return;
      }

      if (Process!=null) Process(this, null);

      // send request
      WebHelper w = new WebHelper(this.Url);
      w.Queries.Add("cmd", "rename");
      w.Queries.Add("path", _Path);
      w.Queries.Add("oldName", itm.FileName);
      w.Queries.Add("newName", itm.NewName);
      w.Tag = itm; //pass the item to instance of the WebHelper 
      w.Execute(SetNewNameResult);
    }
    private void SetNewNameResult(string stat, string msg, bool allowUp, JsonValue data, object tag)
    {
      if (stat == "ok")
      {
        // rename item in the FileList
        this.Dispatcher.BeginInvoke(() =>
        {
          FileItem itm = tag as FileItem;
          itm.FileName = itm.NewName; 
          itm.FileUrl = itm.FileUrl.Substring(0, itm.FileUrl.LastIndexOf("/") + 1) + itm.FileName;
          itm.EditCancel(); // change TextBox to TextBlock
        });
      }
      else
      {
        ShowError("Error. " + msg);
      }

      if (Complete != null) Complete(this, null);
    }
    #endregion
    #region delete file or directory
    /// <summary>
    /// Send request to the server for delete item
    /// </summary>
    public void DeleteItem(FileItem itm)
    {
      if (Process!=null) Process(this, null);

      // send request
      WebHelper w = new WebHelper(this.Url);
      w.Queries.Add("cmd", "delete"); 
      w.Queries.Add("path", _Path);
      w.Queries.Add("name", itm.FileName);
      w.Tag = itm;  //pass the item to instance of the WebHelper 
      w.Execute(DeleteItemResult);
    }
    private void DeleteItemResult(string stat, string msg, bool allowUp, JsonValue data, object tag)
    {
      if (stat == "ok")
      {
        // delete item from the FileList
        this.Dispatcher.BeginInvoke(() =>
        {
          FileItem itm = tag as FileItem;
          this.Items.Remove(itm);
        });

      }
      else
      {
        ShowError("Error. " + msg);
      }

      if (Complete != null) Complete(this, null);
    }
    #endregion
    #region create new directory
    /// <summary>
    /// Send request to the server for create a new directory
    /// </summary>
    /// <param name="name">Directory name</param>
    public void CreateDirectory(string name)
    {
      if (Process!=null) Process(this, null);

      // send request
      WebHelper w = new WebHelper(this.Url);
      w.Queries.Add("cmd", "newdir");
      w.Queries.Add("path", _Path);
      w.Queries.Add("name", name);
      w.Execute(CreateDirectoryResult);
    }
    private void CreateDirectoryResult(string stat, string msg, bool allowUp, JsonValue data, object tag)
    {
      if (Complete != null) Complete(this, null);

      if (stat == "ok")
      {
        // update file list
        UpdateFileList();
      }
      else
      {
        ShowError("Error. " + msg);
      }
    }
    #endregion
    #region upload file

    /// <summary>
    /// Add file to the upload list
    /// </summary>
    public void AddUploadItem(FileInfo f)
    {
      if (_UploadFiles == null) _UploadFiles = new List<UploadItem>();
      UploadItem itm = new UploadItem(_UploadFiles.Count, f, this.Url, this.Path);
      itm.Complete += new EventHandler(UploadItem_Complete);
      _UploadFiles.Add(itm);
    }

    /// <summary>
    /// Send upload list to the server
    /// </summary>
    public void Upload()
    {
      if (_UploadFiles == null || _UploadFiles.Count <= 0) return; // upload list is empty
      
      _UploadErrorMessages = new StringBuilder();

      if (Process!=null) Process(this, null);;

      foreach (UploadItem itm in _UploadFiles)
      {
        itm.Run();//start upload file
      }
    }
    /// <summary>
    /// Upload file complete handler
    /// </summary>
    private void UploadItem_Complete(object sender, EventArgs e)
    {
      this.Dispatcher.BeginInvoke(() =>
      {
        UploadItem itm = sender as UploadItem;
        if (itm.State == UploadItem.StateList.Error)
        {
          _UploadErrorMessages.AppendLine(itm.Message);
        }
        // remove file from upload list
        _UploadFiles.Remove(itm);
        if (_UploadFiles.Count == 0)
        {
          // upload list is empty
          // start Complete
          if (Complete != null) Complete(this, null);
          // has error?
          if (_UploadErrorMessages.Length <= 0)
          {
            // no error, update file list
            UpdateFileList();
          }
          else
          {
            // show error message
            ShowError(_UploadErrorMessages.ToString());
          }
        }
      });
    }

    /// <summary>
    /// FileList Drop handler
    /// </summary>
    private void FileList_Drop(object sender, DragEventArgs e)
    {
      // add selected files to upload list
      FileInfo[] files = e.Data.GetData(DataFormats.FileDrop) as FileInfo[];
      foreach (FileInfo f in files)
      {
        this.AddUploadItem(f); 
      }

      // upload files
      this.Upload();
    }
    #endregion

    /// <summary>
    /// The method show error messages
    /// </summary>
    /// <param name="msg">Error text</param>
    private void ShowError(string msg)
    {
      this.Dispatcher.BeginInvoke(() =>
      {
          var messageBox = new MessageBoxPopUp();
          messageBox.Show("Error",
                          msg,
                          Buttons.Ok);
      });
    }

  }
}
