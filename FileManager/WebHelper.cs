﻿/*
 * This is an example for article «Silverlight File Manager»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=68&mode=art
 * (russian language only)
 * Translated into English special for CodeProject
 * http://www.codeproject.com/
 * Author: Aleksey S Nemiro
 * http://aleksey.nemiro.ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2012
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.IO;
using System.Windows.Browser;
using System.Text;
using System.Json;

namespace Nemiro.FileManager
{
  /// <summary>
  /// The helper class for executing async HTTP requests
  /// </summary>
  public class WebHelper
  {

    /// <summary>
    /// The delegate  for callback function
    /// </summary>
    /// <param name="stat">Server response code (ok, err)</param>
    /// <param name="msg">Error message  (only for stat = err)</param>
    /// <param name="allowUp">Has top-level directory</param>
    /// <param name="data">Array of file list</param>
    public delegate void WebCallback(string stat, string msg, bool allowUp, JsonValue data, object tag);
    // public delegate void WebCallback(HttpWebResponse resp);

    private string _Method = "POST";
    private QueryItemCollection _Queries = new QueryItemCollection();
    private string _Url = String.Empty;

    private string _Boundary = String.Empty;
    private WebCallback _Callback = null;

    /// <summary>
    /// GET or POST
    /// </summary>
    public string Method
    {
      get
      {
        return _Method;
      }
      set
      {
        _Method = value;
        if (String.IsNullOrEmpty(_Method) || _Method.ToUpper() != "GET" || _Method.ToUpper() != "POST") _Method = "POST";
      }
    }

    /// <summary>
    /// Parameters of Request
    /// </summary>
    public QueryItemCollection Queries
    {
      get
      {
        return _Queries;
      }
      set
      {
        _Queries = value;
      }
    }

    /// <summary>
    /// Url for sending request
    /// </summary>
    public string Url
    {
      get
      {
        return _Url;
      }
      set
      {
        _Url = value;
      }
    }

    /// <summary>
    /// Additional custom property
    /// </summary>
    public object Tag { get; set; }

    public WebHelper(string url) : this (url, "POST") { }
    public WebHelper(string url, string method)
    {
      this.Url = url;
      this.Method = method;
    }

    /// <summary>
    /// Execute the Request
    /// </summary>
    /// <param name="callback">The callback function</param>
    public void Execute(WebCallback callback)
    {
      if (String.IsNullOrEmpty(_Url))
      {
        // url is empty
        return;
      }

      _Callback = callback;
      string url = _Url;

      #region add parameters to url for GET requests
      if (_Method == "GET")
      {
        string qs = _Queries.GetQueryString();
        if (url.EndsWith("?"))
        {
          url += "&" + qs;
        }
        else
        {
          url += "?" + qs;
        }
      }
      #endregion

      HttpWebRequest myReq = (HttpWebRequest)HttpWebRequest.Create(_Url);
      myReq.Method = _Method;

      #region Content-Type for POST requests
      if (_Method == "POST")
      {
        if (_Queries.HasFiles())
        {
          // has files, this is multipart/form-data content type
          _Boundary = "----------" + DateTime.Now.Ticks.ToString("x"); // random boundary
          myReq.ContentType = "multipart/form-data; boundary=" + _Boundary;
        }
        else
        {
          // has not files, this is application/x-www-form-urlencoded content type
          myReq.ContentType = "application/x-www-form-urlencoded";
        }
      }
      #endregion

      // start requests
      myReq.BeginGetRequestStream(Execute_BeginGetRequestStream, myReq);
    }
    private void Execute_BeginGetRequestStream(IAsyncResult result)
    {
      HttpWebRequest r = result.AsyncState as HttpWebRequest; // get request

      #region write parameters to request (only for POST)
      if (_Queries.Count > 0 && _Method == "POST")
      {
        Stream myStream = r.EndGetRequestStream(result);

        // no the boundary
        if (String.IsNullOrEmpty(_Boundary))
        {
          // write parameters as string
          byte[] buffer = Encoding.UTF8.GetBytes(_Queries.GetQueryString());
          myStream.Write(buffer, 0, buffer.Length);

        }
        // has the boundary
        else
        {
          // write parameters with headers
          byte[] buffer = null;
          foreach (QueryItem itm in _Queries)
          {
            if (!itm.IsFile)
            {
              // the text parameter
              string q = String.Format("\r\n--{0}\r\nContent-Disposition: form-data; name=\"{1}\";\r\n\r\n{2}", _Boundary, itm.Name, itm.ValueAsString());
              buffer = Encoding.UTF8.GetBytes(q);
              myStream.Write(buffer, 0, buffer.Length);
            }
            else
            {
              // the file
              string q = String.Format("\r\n--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n", _Boundary, itm.Name, itm.FileName, itm.GetContentType());
              buffer = Encoding.UTF8.GetBytes(q);
              // file headers
              myStream.Write(buffer, 0, buffer.Length);
              // file body
              buffer = new byte[4096]; // 4 Kb
              int bytesRead = 0; int totalSize = 0;
              while ((bytesRead = ((Stream)itm.Value).Read(buffer, 0, buffer.Length)) != 0) // read file data
              {
                myStream.Write(buffer, 0, buffer.Length); // write file to request
                totalSize += bytesRead;
              }
            }
          }

          // close the boundary
          buffer = Encoding.UTF8.GetBytes(String.Format("\r\n--{0}--\r\n", _Boundary));
          myStream.Write(buffer, 0, buffer.Length);

        }

        myStream.Close();
      }
      #endregion

      // get response
      r.BeginGetResponse(Execute_Complete, r);
    }
    private void Execute_Complete(IAsyncResult result)
    {
      HttpWebRequest myReq = (HttpWebRequest)result.AsyncState;
      HttpWebResponse myResp = (HttpWebResponse)myReq.EndGetResponse(result);

      string stat = "", msg = "";
      bool allowUp = false;
      JsonValue data = null;

      if (myResp.StatusCode == HttpStatusCode.OK) //HTTP 200 - OK
      {
        // read response
        StreamReader reader = new StreamReader(myResp.GetResponseStream(), Encoding.UTF8);
        string page = reader.ReadToEnd();
        // parse JSON
        JsonValue json = System.Json.JsonObject.Parse(page);
        if (json.ContainsKey("stat")) stat = json["stat"];
        if (json.ContainsKey("msg")) msg = json["msg"];
        if (json.ContainsKey("allowUp")) allowUp = json["allowUp"];
        if (json.ContainsKey("data")) data = json["data"];
      }
      else
      {
        stat = "err";
        msg = String.Format("Server error {0}", myResp.StatusCode);
      }

      // callback
      if (_Callback != null)
      {
        _Callback(stat, msg, allowUp, data, this.Tag);
        // _Callback(myResp);
      }
    }

    #region additional classes
    /// <summary>
    /// Collection parameters of request
    /// </summary>
    public class QueryItemCollection : List<QueryItem>
    {

      /// <summary>
      /// Add text parameter
      /// </summary>
      /// <param name="name">Parameter name</param>
      /// <param name="value">Parameter value</param>
      public void Add(string name, string value)
      {
        this.Add(new QueryItem(name, value));
      }

      /// <summary>
      /// Add file
      /// </summary>
      /// <param name="name">Parameter name</param>
      /// <param name="fileName">File name</param>
      /// <param name="stream">File stream</param>
      public void Add(string name, string fileName, Stream stream)
      {
        this.Add(new QueryItem(name, fileName, stream));
      }

      /// <summary>
      /// The function return parameters as string (par1=val1&par2=val2&par3=val3 etc.)
      /// </summary>
      public string GetQueryString()
      {
        string qs = "";
        foreach (QueryItem itm in this)
        {
          if (!String.IsNullOrEmpty(qs)) qs += "&";
          qs += String.Format("{0}={1}", itm.Name, itm.ValueForUrl());
        }
        return qs;
      }

      /// <summary>
      /// The function search files. If has files, function returned "true".
      /// </summary>
      public bool HasFiles()
      {
        foreach (QueryItem itm in this)
        {
          if (itm.IsFile) return true;
        }
        return false;
      }

    }

    /// <summary>
    /// Parameter of request
    /// </summary>
    public class QueryItem
    {

      public string Name { get; set; }
      public object Value{get;set;}
      public string FileName { get; set; }

      /// <summary>
      /// Is file or is not file
      /// </summary>
      public bool IsFile
      {
        get
        {
          return this.Value != null && this.Value.GetType() == typeof(FileStream);
        }
      }

      public QueryItem(string name, string value)
      {
        this.Name = name;
        this.Value = value;
      }

      public QueryItem(string name, string fileName, Stream stream)
      {
        this.Name = name;
        this.FileName = fileName;
        this.Value = stream;
      }

      /// <summary>
      /// UrlEncode value
      /// </summary>
      public string ValueForUrl()
      {
        return HttpUtility.UrlEncode(this.Value.ToString());
      }
      /// <summary>
      /// Value as string
      /// </summary>
      public string ValueAsString()
      {
        return this.Value.ToString();
      }

      /// <summary>
      /// Content-Type by file extension
      /// </summary>
      /// <returns></returns>
      public string GetContentType()
      {
        string ext = System.IO.Path.GetExtension(this.FileName).ToLower().Substring(1, System.IO.Path.GetExtension(this.FileName).Length - 1);
        if (WebHelper.MIME.ContainsKey(ext))
        {
          return WebHelper.MIME[ext];
        }
        return "application/data";
      }
    }
    #endregion
    #region mime 
    public static readonly Dictionary<string, string> MIME = new Dictionary<string, string>
    {
      {"ai", "application/postscript"},
      {"aif", "audio/x-aiff"},
      {"aifc", "audio/x-aiff"},
      {"aiff", "audio/x-aiff"},
      {"asc", "text/plain"},
      {"atom", "application/atom+xml"},
      {"au", "audio/basic"},
      {"avi", "video/x-msvideo"},
      {"bcpio", "application/x-bcpio"},
      {"bin", "application/octet-stream"},
      {"bmp", "image/bmp"},
      {"cdf", "application/x-netcdf"},
      {"cgm", "image/cgm"},
      {"class", "application/octet-stream"},
      {"cpio", "application/x-cpio"},
      {"cpt", "application/mac-compactpro"},
      {"csh", "application/x-csh"},
      {"css", "text/css"},
      {"dcr", "application/x-director"},
      {"dif", "video/x-dv"},
      {"dir", "application/x-director"},
      {"djv", "image/vnd.djvu"},
      {"djvu", "image/vnd.djvu"},
      {"dll", "application/octet-stream"},
      {"dmg", "application/octet-stream"},
      {"dms", "application/octet-stream"},
      {"doc", "application/msword"},
      {"dtd", "application/xml-dtd"},
      {"dv", "video/x-dv"},
      {"dvi", "application/x-dvi"},
      {"dxr", "application/x-director"},
      {"eps", "application/postscript"},
      {"etx", "text/x-setext"},
      {"exe", "application/octet-stream"},
      {"ez", "application/andrew-inset"},
      {"gif", "image/gif"},
      {"gram", "application/srgs"},
      {"grxml", "application/srgs+xml"},
      {"gtar", "application/x-gtar"},
      {"hdf", "application/x-hdf"},
      {"hqx", "application/mac-binhex40"},
      {"htm", "text/html"},
      {"html", "text/html"},
      {"ice", "x-conference/x-cooltalk"},
      {"ico", "image/vnd.microsoft.icon"},
      {"ics", "text/calendar"},
      {"ief", "image/ief"},
      {"ifb", "text/calendar"},
      {"iges", "model/iges"},
      {"igs", "model/iges"},
      {"jnlp", "application/x-java-jnlp-file"},
      {"jp2", "image/jp2"},
      {"jpe", "image/jpeg"},
      {"jpeg", "image/jpeg"},
      {"jpg", "image/jpeg"},
      {"js", "application/x-javascript"},
      {"kar", "audio/midi"},
      {"latex", "application/x-latex"},
      {"lha", "application/octet-stream"},
      {"lzh", "application/octet-stream"},
      {"m3u", "audio/x-mpegurl"},
      {"m4a", "audio/mp4a-latm"},
      {"m4b", "audio/mp4a-latm"},
      {"m4p", "audio/mp4a-latm"},
      {"m4u", "video/vnd.mpegurl"},
      {"m4v", "video/x-m4v"},
      {"mac", "image/x-macpaint"},
      {"man", "application/x-troff-man"},
      {"mathml", "application/mathml+xml"},
      {"me", "application/x-troff-me"},
      {"mesh", "model/mesh"},
      {"mid", "audio/midi"},
      {"midi", "audio/midi"},
      {"mif", "application/vnd.mif"},
      {"mov", "video/quicktime"},
      {"movie", "video/x-sgi-movie"},
      {"mp2", "audio/mpeg"},
      {"mp3", "audio/mpeg"},
      {"mp4", "video/mp4"},
      {"mpe", "video/mpeg"},
      {"mpeg", "video/mpeg"},
      {"mpg", "video/mpeg"},
      {"mpga", "audio/mpeg"},
      {"ms", "application/x-troff-ms"},
      {"msh", "model/mesh"},
      {"mxu", "video/vnd.mpegurl"},
      {"nc", "application/x-netcdf"},
      {"oda", "application/oda"},
      {"ogg", "application/ogg"},
      {"pbm", "image/x-portable-bitmap"},
      {"pct", "image/pict"},
      {"pdb", "chemical/x-pdb"},
      {"pdf", "application/pdf"},
      {"pgm", "image/x-portable-graymap"},
      {"pgn", "application/x-chess-pgn"},
      {"pic", "image/pict"},
      {"pict", "image/pict"},
      {"png", "image/png"}, 
      {"pnm", "image/x-portable-anymap"},
      {"pnt", "image/x-macpaint"},
      {"pntg", "image/x-macpaint"},
      {"ppm", "image/x-portable-pixmap"},
      {"ppt", "application/vnd.ms-powerpoint"},
      {"ps", "application/postscript"},
      {"qt", "video/quicktime"},
      {"qti", "image/x-quicktime"},
      {"qtif", "image/x-quicktime"},
      {"ra", "audio/x-pn-realaudio"},
      {"ram", "audio/x-pn-realaudio"},
      {"ras", "image/x-cmu-raster"},
      {"rdf", "application/rdf+xml"},
      {"rgb", "image/x-rgb"},
      {"rm", "application/vnd.rn-realmedia"},
      {"roff", "application/x-troff"},
      {"rtf", "text/rtf"},
      {"rtx", "text/richtext"},
      {"sgm", "text/sgml"},
      {"sgml", "text/sgml"},
      {"sh", "application/x-sh"},
      {"shar", "application/x-shar"},
      {"silo", "model/mesh"},
      {"sit", "application/x-stuffit"},
      {"skd", "application/x-koan"},
      {"skm", "application/x-koan"},
      {"skp", "application/x-koan"},
      {"skt", "application/x-koan"},
      {"smi", "application/smil"},
      {"smil", "application/smil"},
      {"snd", "audio/basic"},
      {"so", "application/octet-stream"},
      {"spl", "application/x-futuresplash"},
      {"src", "application/x-wais-source"},
      {"sv4cpio", "application/x-sv4cpio"},
      {"sv4crc", "application/x-sv4crc"},
      {"svg", "image/svg+xml"},
      {"swf", "application/x-shockwave-flash"},
      {"t", "application/x-troff"},
      {"tar", "application/x-tar"},
      {"tcl", "application/x-tcl"},
      {"tex", "application/x-tex"},
      {"texi", "application/x-texinfo"},
      {"texinfo", "application/x-texinfo"},
      {"tif", "image/tiff"},
      {"tiff", "image/tiff"},
      {"tr", "application/x-troff"},
      {"tsv", "text/tab-separated-values"},
      {"txt", "text/plain"},
      {"ustar", "application/x-ustar"},
      {"vcd", "application/x-cdlink"},
      {"vrml", "model/vrml"},
      {"vxml", "application/voicexml+xml"},
      {"wav", "audio/x-wav"},
      {"wbmp", "image/vnd.wap.wbmp"},
      {"wbmxl", "application/vnd.wap.wbxml"},
      {"wml", "text/vnd.wap.wml"},
      {"wmlc", "application/vnd.wap.wmlc"},
      {"wmls", "text/vnd.wap.wmlscript"},
      {"wmlsc", "application/vnd.wap.wmlscriptc"},
      {"wrl", "model/vrml"},
      {"xbm", "image/x-xbitmap"},
      {"xht", "application/xhtml+xml"},
      {"xhtml", "application/xhtml+xml"},
      {"xls", "application/vnd.ms-excel"},                        
      {"xml", "application/xml"},
      {"xpm", "image/x-xpixmap"},
      {"xsl", "application/xml"},
      {"xslt", "application/xslt+xml"},
      {"xul", "application/vnd.mozilla.xul+xml"},
      {"xwd", "image/x-xwindowdump"},
      {"xyz", "chemical/x-xyz"},
      {"zip", "application/zip"}
    };
    #endregion
  }
}
