﻿/*
 * This is an example for article «Silverlight File Manager»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=68&mode=art
 * (russian language only)
 * Translated into English special for CodeProject
 * http://www.codeproject.com/
 * Author: Aleksey S Nemiro
 * http://aleksey.nemiro.ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2012
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Nemiro.FileManager
{
  public partial class Prompt : ChildWindow
  {

    public string InputValue
    {
      get
      {
        return textBox1.Text;
      }
      set
      {
        textBox1.Text = value;
      }
    }

    public Prompt(string title, string promptText)
    {
      InitializeComponent();
      
      this.Title = title;
      label1.Content = promptText;
    }

    private void OKButton_Click(object sender, RoutedEventArgs e)
    {
      if (String.IsNullOrEmpty(textBox1.Text))
      {
        var messageBox = new MessageBoxPopUp();
        messageBox.Show("Error",
                        "Value is required.",
                        Buttons.Ok);
        return;
      }
      this.DialogResult = true;
    }

    private void CancelButton_Click(object sender, RoutedEventArgs e)
    {
      this.DialogResult = false;
    }
  }
}

