﻿/*
 * This is an example for article «Silverlight File Manager»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=68&mode=art
 * (russian language only)
 * Translated into English special for CodeProject
 * http://www.codeproject.com/
 * Author: Aleksey S Nemiro
 * http://aleksey.nemiro.ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2012
 */
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Browser;
//using SilverlightHtmlHost;

namespace Nemiro.FileManager
{
  /// <summary>
  /// FileItem control (for FileList)
  /// </summary>
  public class FileItem : StackPanel
  {

    string myUrl = Application.Current.Host.Source.Scheme.ToString() +
                   "://" + Application.Current.Host.Source.Host.ToString() +
                   ":" + Application.Current.Host.Source.Port.ToString() + "/FileManagerIcons/";

    /// <summary>
    /// Item type: -1 - top-level directory, 0 - directory, 1 - file
    /// </summary>
    public int ItemType { get; set; }
    /// <summary>
    /// Directory/File name
    /// </summary>
    public string FileName { get; set; }
    /// <summary>
    /// File size (Kb)
    /// </summary>
    public double FileSize { get; set; }
    /// <summary>
    /// File url
    /// </summary>
    public string FileUrl { get; set; }
    /// <summary>
    /// True - item is in edit mode. 
    /// False - item is not in edit mode.
    /// </summary>
    public bool IsEdit { get; set; }
    /// <summary>
    /// Can edit the Item
    /// </summary>
    public bool CanEdit { get; set; }

    private string _NewName = "";
    /// <summary>
    /// New Directory/File name
    /// </summary>
    public string NewName
    {
      get
      {
        return _NewName;
      }
    }

    private int _ItemIndex = 0;

    public FileItem(int type, string name, string url, double size, bool isEditable)
    {
      this.ItemType = type;
      this.FileName = name;
      this.FileUrl = url;
      this.FileSize = size;

      if (isEditable)
      {
          this.CanEdit = type != -1; // top-level directory cannot be editable
      }
      else
      {
          this.CanEdit = false;
      }

      this.Orientation = Orientation.Horizontal;

      // item icon
      Image myImg = new Image() { Width = 16, Height = 16 };
      if (type == -1)
      {
        // top-level directory
        myImg.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + "folder2.png", UriKind.Absolute));
      }
      else if (type == 0)
      {
        // directory
          myImg.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + "folder.png", UriKind.Absolute));
      }
      else
      {
        // file
        // set icon by extension
        string fileExtension = System.IO.Path.GetExtension(name).ToLower();
        string[] fileType = { ".exe", ".bat", ".cmd", ".asp", ".aspx", ".html", ".htm", ".cs", ".txt", ".doc", ".docx", ".php", ".gif", ".png", ".jpg", ".jpeg", ".bmp", ".js", ".xls", "xlsx", ".zip" };
        string[] fileIcon = { "exe.png", "cmd.png", "cmd.png", "aspx.png", "aspx.png", "html.png", "html.png", "csharp.png", "txt.png", "doc.png", "doc.png", "php.png", "image.png", "image.png", "image.png", "image.png", "bmp.png", "script.png", "xls.png", "xls.png", "zip.png" };
        int idx = Array.IndexOf(fileType, fileExtension);
        if (idx != -1)
        {
            myImg.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + fileIcon[idx], UriKind.Absolute));
        }
        else
        {
          // default file icon
            myImg.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + "unknown.png", UriKind.Absolute));
        }
      }
      myImg.Margin = new Thickness(2, 0, 0, 0);
      this.Children.Add(myImg);

      // file/directory name
      this.Children.Add(new TextBlock() { Text = name, Margin = new Thickness(2, 0, 0, 0) });

      // control buttons
      // open file or go into directory
      Image myImg2 = new Image() { Width = 9, Height = 9, Cursor = Cursors.Hand };
      myImg2.Margin = new Thickness(4, 0, 0, 0);
      myImg2.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + "open.png", UriKind.Absolute));
      myImg2.MouseLeftButtonUp += (sender, e) =>
      {
        Open();
      };
      this.Children.Add(myImg2);

      // is not top-level directory
      if (type != -1)
      {
          if (isEditable)
          {
              // rename directory/file 
              Image myImg4 = new Image() { Width = 9, Height = 9, Cursor = Cursors.Hand };
              myImg4.Margin = new Thickness(4, 0, 0, 0);
              myImg4.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + "edit.png", UriKind.Absolute));
              myImg4.MouseLeftButtonUp += (sender, e) =>
              {
                  EditStart();
              };
              this.Children.Add(myImg4);

              // delete directory/file 
              Image myImg3 = new Image() { Width = 9, Height = 9, Cursor = Cursors.Hand };
              myImg3.Margin = new Thickness(4, 0, 0, 0);
              myImg3.Source = new System.Windows.Media.Imaging.BitmapImage(new Uri(myUrl + "del.png", UriKind.Absolute));
              myImg3.MouseLeftButtonUp += (sender, e) =>
              {
                  Delete();
              };
              this.Children.Add(myImg3);
          }
      }

      // file size
      if (type == 1) // only for files
      {
        this.Children.Add(new TextBlock() { Text = String.Format("{0:##,###,##0.00} Kb", size / 1024), HorizontalAlignment = System.Windows.HorizontalAlignment.Right, Margin = new Thickness(8, 0, 0, 0), FontSize = 9, Foreground = new SolidColorBrush(Color.FromArgb(255, 128, 128, 128)) });
      }

      this.MouseLeftButtonUp += new MouseButtonEventHandler(FileItem_MouseLeftButtonUp);
    }

    private DateTime _lastClick = DateTime.Now;
    private bool _firstClickDone = false;
    private void FileItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
      DateTime clickTime = DateTime.Now;
      TimeSpan span = clickTime - _lastClick;
      if (span.TotalMilliseconds > 350 || !_firstClickDone)//350 ms
      {
        // first click
        _firstClickDone = true;
        _lastClick = clickTime;
      }
      else
      {
        // second click
        _firstClickDone = false;
        // open file or go into directory
        Open();
      }
    }

    /// <summary>
    /// Start editing (change TextBlock to TextBox and set IsEdit = true)
    /// </summary>
    public void EditStart()
    {
      if (this.IsEdit) return;
      // remove TextBlock
      this.Children.RemoveAt(1);
      // add TextBox
      this.Children.Insert(1, new TextBox() { Text = this.FileName, Margin = new Thickness(2, 0, 0, 0) });
      // set TextBox LostFocus handler
      ((TextBox)this.Children[1]).LostFocus += new RoutedEventHandler(EditTextBox_LostFocus);
      // select all text for directories
      if (this.ItemType == 0)
      {
        ((TextBox)this.Children[1]).SelectAll();
      }
      else
      {
        // select only file name for files (excluding file extension)
        ((TextBox)this.Children[1]).SelectionStart = 0;
        ((TextBox)this.Children[1]).SelectionLength = this.FileName.LastIndexOf(".");
      }
      // remember item index
      _ItemIndex = ((FileList)this.Parent).SelectedIndex;
      // set focus to TextBox
      ((TextBox)this.Children[1]).Focus();
      this.IsEdit = true;
    }
    /// <summary>
    /// TextBox LostFocus handler
    /// </summary>
    private void EditTextBox_LostFocus(object sender, RoutedEventArgs e)
    {
      EditComplete();
    }
    /// <summary>
    /// Cancel editing (change TextBox to TextBlock and set IsEdit = false)
    /// </summary>
    public void EditCancel()
    {
      // remove TextBox
      this.Children.RemoveAt(1);
      // add TextBlock
      this.Children.Insert(1, new TextBlock() { Text = this.FileName, Margin = new Thickness(2, 0, 0, 0) });
      this.IsEdit = false;
      ((FileList)this.Parent).Focus();
    }
    /// <summary>
    /// Finish editing and send request to server for rename item
    /// </summary>
    public void EditComplete()
    {
      // remove TextBox LostFocus handler
      ((TextBox)this.Children[1]).LostFocus -= EditTextBox_LostFocus;
      // get new name
      _NewName = ((TextBox)this.Children[1]).Text;
      // send request for rename item
      ((FileList)this.Parent).SetNewName(this);
    }

    /// <summary>
    /// Open file or go into the directory
    /// </summary>
    public void Open()
    {
      if (this.ItemType == 1)
      {
        // open file in new window
          HtmlDocument doc = HtmlPage.Document;
          bool _isNavigator = HtmlPage.BrowserInformation.Name.Contains("Netscape");
          int _screenWidth = (int)((double)doc.Body.GetProperty("clientWidth"));
          int _screenHeight = (int)((double)doc.Body.GetProperty("clientHeight"));
          int _screenTop = 0;
          int _screenLeft = 0;
          if (_isNavigator)
          {
              _screenTop = (_screenHeight - 600) / 2;
              if (_screenTop < 0)
              {
                  _screenTop = 50;
              }
              _screenLeft = (int)((double)HtmlPage.Window.GetProperty("screenX"));
          }
          else
          {
              _screenLeft = (int)((double)HtmlPage.Window.GetProperty("screenLeft"));
              _screenTop = (int)((double)HtmlPage.Window.GetProperty("screenTop"));
          }
          int _myCenteredLeft = (_screenWidth - 800) / 2 + _screenLeft;
          if (_myCenteredLeft < 0)
          {
              _myCenteredLeft = 0;
          }
          int _htmlControlTop = _screenTop;
          var popupOption = new HtmlPopupWindowOptions { Left = _myCenteredLeft, Top = _htmlControlTop, Width = 800, Height = 600, 
                                                         Menubar = false, Toolbar = false, Directories = false, Status = false,
                                                         Scrollbars = true, Resizeable = true, Location = false };

          if (this.FileUrl.ToLower().Contains(".pdf"))
          {
              HtmlPage.PopupWindow(new Uri(this.FileUrl + "?#toolbar=1"), "new", popupOption);
             // HtmlPage.Window.Invoke("DialogWindow", this.FileUrl + "?#toolbar=1");
          }
          else
          {
              HtmlPage.PopupWindow(new Uri(this.FileUrl), "new", popupOption);
              //HtmlPage.Window.Invoke("DialogWindow", this.FileUrl);
          }

        //var popupWindow = new HtmlHostChildWindow(this.FileUrl);
        //popupWindow.Title = this.FileName;
        //popupWindow.Show();
      }
      else if (this.ItemType == 0)
      {
        // this is directory, 
        // append current item to patch
        if (!((FileList)this.Parent).Path.EndsWith("/")) ((FileList)this.Parent).Path += "/";
        ((FileList)this.Parent).Path += this.FileName;
        // update file list
        ((FileList)this.Parent).UpdateFileList();
      }
      else if (this.ItemType == -1)
      {
        // this is top-level directory, 
        // remove last item from path
        string[] arr = ((FileList)this.Parent).Path .Split("/".ToCharArray());
        Array.Resize(ref arr, arr.Length - 1);
        ((FileList)this.Parent).Path  = String.Join("/", arr);
        // update file list
        ((FileList)this.Parent).UpdateFileList();
      }
    }

    /// <summary>
    /// Delete item
    /// </summary>
    public void Delete()
    {
      var messageBox = new MessageBoxPopUp();
      if (this.ItemType == 0)
      {
        messageBox.Show("Confirm",
                        String.Format("Are you want delete the directory \"{0}\"?", this.FileName),
                        Buttons.YesNo);
        messageBox.Closed += (sender, eventArg) =>
            {
                var result = messageBox.DialogResult;
                if (result == true)
                {
                    ((FileList)this.Parent).DeleteItem(this);
                }
            };
      }
      else if (this.ItemType == 1)
      {
        messageBox.Show("Confirm",
                        String.Format("Are you want delete the file \"{0}\"?", this.FileName),
                        Buttons.YesNo);
        messageBox.Closed += (sender, eventArg) =>
        {
            var result = messageBox.DialogResult;
            if (result == true)
            {
                ((FileList)this.Parent).DeleteItem(this);
            }
        };
      }
    }

  }
}
