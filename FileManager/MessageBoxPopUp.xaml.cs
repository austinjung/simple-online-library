﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Nemiro.FileManager
{
    public enum Buttons
    {
        Ok,
        YesNo,
        OkCancel,
        YesNoCancel
    }

    public partial class MessageBoxPopUp : ChildWindow
    {
        private Button _okButton;
        private Button _cancelButton;
        private Button _yesButton;
        private Button _noButton;

        public string Message
        {
            get { return MessageTextBlock.Text; }
            set { MessageTextBlock.Text = value; }
        }

        public MessageBoxPopUp()
        {  InitializeComponent(); }

        public MessageBoxPopUp(double opacity)
        { 
            InitializeComponent();
            Opacity = opacity;
        }

        public MessageBoxPopUp(double width, double height, double opacity)
        {
            InitializeComponent();
            Width = width;
            Height = height;
            Opacity = opacity;
        }

        public void Show(string title, string message, Buttons buttons)
        {
            generateButtons(buttons);
            titleLabel.Content = title;
            Message = message;
            Show();
        }

        public void Show(string title, string message, Buttons buttons, double width, double height, double opacity)
        {
            generateButtons(buttons);
            titleLabel.Content = title;
            Message = message;
            Width = width;
            Height = height;
            Opacity = opacity;
            Show();
        }

        private void generateButtons(Buttons buttons)
        {
            switch (buttons)
            {
                case Buttons.Ok:
                    {
                        createOkButton();
                        createOkButtonGrid();
                        break;   
                    }
                case Buttons.YesNo:
                    {
                        createYesAndNoButtons();
                        createYesNoGrid();
                        break;
                    }
                case Buttons.OkCancel:
                    {
                        createOkAndCancelButtons();
                        createOkCancelGrid();
                        break;
                    }
                case Buttons.YesNoCancel:
                    {
                        createYesAndNoAndCancelButtons();
                        createYesNoCancelGrid();
                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException("buttons");
            }
        }

        private void createOkButtonGrid()
        {
            resetAndClearGrid(ButtonsGrid);
            addColumnDifinitionsToGrid(ButtonsGrid, 1);
            addButtonToGrid(ButtonsGrid, _okButton, 0);
        }

        private void createOkCancelGrid()
        {
            resetAndClearGrid(ButtonsGrid);
            addColumnDifinitionsToGrid(ButtonsGrid, 2);
            addButtonToGrid(ButtonsGrid, _okButton, 0);
            addButtonToGrid(ButtonsGrid, _cancelButton, 1);
        }

        private void createYesNoGrid()
        {
            resetAndClearGrid(ButtonsGrid);
            addColumnDifinitionsToGrid(ButtonsGrid, 2);
            addButtonToGrid(ButtonsGrid, _yesButton, 0);
            addButtonToGrid(ButtonsGrid, _noButton, 1);
        }

        private void createYesNoCancelGrid()
        {
            resetAndClearGrid(ButtonsGrid);
            addColumnDifinitionsToGrid(ButtonsGrid, 3);
            addButtonToGrid(ButtonsGrid, _yesButton, 0);
            addButtonToGrid(ButtonsGrid, _noButton, 1);
            addButtonToGrid(ButtonsGrid, _cancelButton, 2);
        }

        private static void resetAndClearGrid(Grid grid)
        {
            grid.Children.Clear();
            grid.ColumnDefinitions.Clear();
        }

        private static void addColumnDifinitionsToGrid(Grid grid, int columns)
        {
            for (var i = 0; i < columns; i++)
            { grid.ColumnDefinitions.Add(new ColumnDefinition()); }
        }

        private static void addButtonToGrid(Panel grid, UIElement button, int columnIndex)
        {
            grid.Children.Add(button);
            button.SetValue(Grid.ColumnProperty, columnIndex);
        }

        private void createOkButton()
        {
            if (_okButton != null) return;

            _okButton = new Button
                            {
                                Content = "Ok",
                                Margin = new Thickness(2)
                            };
            _okButton.Click += (sender, args) => DialogResult = true;
        }

        private void createCancelButton()
        {
            if (_cancelButton != null) return;

            _cancelButton = new Button
            {
                Content = "Cancel",
                Margin = new Thickness(2)
            };
            _cancelButton.Click += (sender, args) => DialogResult = false;
        }

        private void createYesButton()
        {
            if (_yesButton != null) return;

            _yesButton = new Button
            {
                Content = "Yes",
                Margin = new Thickness(2)
            };
            _yesButton.Click += (sender, args) => DialogResult = true;
        }

        private void createNoButton()
        {
            if (_noButton != null) return;

            _noButton = new Button
            {
                Content = "No",
                Margin = new Thickness(2)
            };
            _noButton.Click += (sender, args) => DialogResult = false;
        }

        private void createYesAndNoButtons()
        {
            createYesButton();
            createNoButton();
        }

        private void createOkAndCancelButtons()
        {
            createOkButton();
            createCancelButton();
        }

        private void createYesAndNoAndCancelButtons()
        {
            createYesButton();
            createNoButton();
            createCancelButton();
        }
    }
}
