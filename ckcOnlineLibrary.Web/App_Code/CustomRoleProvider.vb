﻿Imports System.Web.Security
Imports System.Configuration.Provider
Imports System.Collections.Specialized
Imports System
Imports System.Data
Imports System.Data.Odbc
Imports System.Configuration
Imports System.Diagnostics
Imports System.Web
Imports System.Globalization
Imports Microsoft.VisualBasic
Imports System.Security.Principal
Imports System.Web.SessionState

Public Class CustomRoleProvider
    Inherits RoleProvider

    Public Overrides Function GetRolesForUser(ByVal username As String) As String()

        Dim cookieName As String = FormsAuthentication.FormsCookieName
        Dim authCookie As HttpCookie = HttpContext.Current.Request.Cookies(cookieName)

        If authCookie Is Nothing Then
            ' There is no authentication cookie.
            Return {}
        End If
        Dim authTicket As FormsAuthenticationTicket = Nothing
        Try
            authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        Catch ex As Exception
            ' Log exception details (omitted for simplicity)
            Return {}
        End Try

        If authTicket Is Nothing Then
            ' Cookie failed to decrypt.
            Return {}
        End If
        ' When the ticket was created, the UserData property was assigned a
        ' comma delimited string of role names.
        Dim roles As String() = authTicket.UserData.Split(New Char() {","c})

        Return roles

    End Function

    Public Overrides Function IsUserInRole(ByVal username As String, ByVal roleName As String) As Boolean
        Return If(Array.BinarySearch(GetRolesForUser(username), roleName) >= 0, True, False)
    End Function

#Region "Not Implemented"
    Public Overrides Sub AddUsersToRoles(ByVal usernames() As String, ByVal roleNames() As String)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Property ApplicationName() As String
        Get
            Throw New NotImplementedException()
        End Get
        Set(ByVal value As String)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Sub CreateRole(ByVal roleName As String)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Function DeleteRole(ByVal roleName As String, ByVal throwOnPopulatedRole As Boolean) As Boolean
        Throw New NotImplementedException()
    End Function

    Public Overrides Function FindUsersInRole(ByVal roleName As String, ByVal usernameToMatch As String) As String()
        Throw New NotImplementedException()
    End Function

    Public Overrides Function GetAllRoles() As String()
        Throw New NotImplementedException()
    End Function

    Public Overrides Function GetUsersInRole(ByVal roleName As String) As String()
        Throw New NotImplementedException()
    End Function

    Public Overrides Sub RemoveUsersFromRoles(ByVal usernames() As String, ByVal roleNames() As String)
        Throw New NotImplementedException()
    End Sub

    Public Overrides Function RoleExists(ByVal roleName As String) As Boolean
        Throw New NotImplementedException()
    End Function
#End Region

End Class

