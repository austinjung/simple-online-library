﻿Imports System
Imports System.Web.Security
Imports System.Security.Principal
Imports System.Web.SessionState
Imports System.Web

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
        ' Extract the forms authentication cookie
        Dim cookieName As String = FormsAuthentication.FormsCookieName
        Dim authCookie As HttpCookie = Context.Request.Cookies(cookieName)

        If authCookie Is Nothing Then
            ' There is no authentication cookie.
            Return
        End If
        Dim authTicket As FormsAuthenticationTicket = Nothing
        Try
            authTicket = FormsAuthentication.Decrypt(authCookie.Value)
        Catch ex As Exception
            ' Log exception details (omitted for simplicity)
            Return
        End Try

        If authTicket Is Nothing Then
            ' Cookie failed to decrypt.
            Return
        End If
        ' When the ticket was created, the UserData property was assigned a
        ' comma delimited string of role names.
        Dim roles As String() = authTicket.UserData.Split(New Char() {","c})
        ' Create an Identity object
        Dim id As New FormsIdentity(authTicket)

        ' This principal will flow throughout the request.
        Dim principal = New CustomPrincipal(id, roles)
        ' Attach the new principal object to the current HttpContext object
        Context.User = principal

    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class