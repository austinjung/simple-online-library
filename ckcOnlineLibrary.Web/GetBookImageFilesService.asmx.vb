﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Collections.Generic

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class GetBookImageFilesService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function GetBookImageFiles(ByRef Path As String) As List(Of String)

        ' Change the location to Path.
        Dim uploadPath = Context.Server.MapPath("~/Upload/Book_Images")

        'Get the files in sub directories
        Dim fileList As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = Nothing
        Dim myFileList As List(Of String)
        myFileList = New List(Of String)

        fileList = My.Computer.FileSystem.GetFiles(uploadPath, Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "*.(bmp | jpg | png)")
        For Each foundFile As String In fileList
            myFileList.Add(foundFile)
        Next

        Return myFileList

    End Function

End Class