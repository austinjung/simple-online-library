﻿Imports System.Runtime.CompilerServices
Imports System

Module DateTimeExtensions

    <Extension()>
    Public Function CalculateBusinessDaysFromInputDate(ByVal StartDate As Date, ByVal NumberOfBusinessDays As Integer) As Date
        'Knock the start date down one day if it is on a weekend.
        If StartDate.DayOfWeek = DayOfWeek.Saturday Or StartDate.DayOfWeek = DayOfWeek.Sunday Then
            NumberOfBusinessDays -= 1
        End If

        For index = 1 To NumberOfBusinessDays
            Select Case StartDate.DayOfWeek
                Case DayOfWeek.Sunday
                    StartDate = StartDate.AddDays(2)
                Case DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, _
                    DayOfWeek.Thursday, DayOfWeek.Friday
                    StartDate = StartDate.AddDays(1)
                Case DayOfWeek.Saturday
                    StartDate = StartDate.AddDays(3)

            End Select

        Next

        'check to see if the end date is on a weekend.
        'If so move it ahead to Monday.
        'You could also bump it back to the Friday before if you desired to. 
        'Just change the code to -2 and -1.
        If StartDate.DayOfWeek = DayOfWeek.Saturday Then
            StartDate = StartDate.AddDays(2)
        ElseIf StartDate.DayOfWeek = DayOfWeek.Sunday Then
            StartDate = StartDate.AddDays(1)
        End If

        Return StartDate

    End Function

    <Extension()>
    Public Sub PrintAndPunctuate(ByVal aString As String,
                             ByVal punc As String)
        Console.WriteLine(aString & punc)
    End Sub

End Module
