﻿Imports System.Web
Imports System.Web.Services

Public Class FileManagerGateway
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        If context.Request.Form.Get("cmd") = "get" And context.Request.Form.Get("path") = "/Book_Images/" Then
            Dim myGateway = New Nemiro.FileManager.Common.Gateway()
            context.Response.ContentType = "application/json"
            Dim gatewayResponse = myGateway.GetResult()
            context.Response.Write(gatewayResponse)
        Else
            If context.ApplicationInstance.User.Identity.IsAuthenticated Then
                Dim myGateway = New Nemiro.FileManager.Common.Gateway()
                context.Response.ContentType = "application/json"
                Dim gatewayResponse = myGateway.GetResult()
                context.Response.Write(gatewayResponse)
            Else
                context.Response.ContentType = "application/json"
                Dim gatewayResponse = "{'stat':'err','msg':'You have no privilege or your session has been terminated.'}"
                context.Response.Write(gatewayResponse)
            End If
        End If
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class