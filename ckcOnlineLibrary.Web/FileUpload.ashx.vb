﻿Imports System.Web
Imports System.Web.Services
Imports DC.SilverlightFileUpload

Public Class FileUpload
    Implements System.Web.IHttpHandler

    Private ctx As HttpContext
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        If context.ApplicationInstance.User.IsInRole("Manager") Then
            ctx = context
            Dim uploadPath = context.Server.MapPath("~/Upload/Book_Images")
            Dim fileUpload As FileUploadProcess = New FileUploadProcess()
            AddHandler fileUpload.FileUploadCompleted, New FileUploadCompletedEvent(AddressOf fileUpload_FileUpLoadCompleted)
            fileUpload.ProcessRequest(context, uploadPath)
        End If

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Sub fileUpload_FileUpLoadCompleted(sender As Object, args As FileUploadCompletedEventArgs)
        Dim id As String = ctx.Request.QueryString("id")
    End Sub

End Class