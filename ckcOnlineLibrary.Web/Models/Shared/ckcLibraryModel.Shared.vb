﻿Imports System
Imports System.Collections.ObjectModel

Partial Public Class Book

    Private Const _format As String = "/Assets/Images/{0}.jpg"

    Public ReadOnly Property ImageSource As String
        Get
            If String.IsNullOrEmpty(Me.Book_Image_File) Then
                Return String.Format(_format, Me._Book_id)
            Else
                Return Me.Book_Image_File
            End If
        End Get
    End Property

    Private _book_Categiries As ObservableCollection(Of Category)
    Public Property Book_Categories As ObservableCollection(Of Category)
        Get
            Return _book_Categiries
        End Get
        Set(value As ObservableCollection(Of Category))
            _book_Categiries = value
            Me.ReportPropertyChanged("Book_Categories")
            Me.ReportPropertyChanged("Book_Categories_List")
        End Set
    End Property
    Private _categories_list As String
    Public ReadOnly Property Book_Categories_List As String
        Get
            If String.IsNullOrEmpty(_categories_list) Then
                _categories_list = String.Empty
                If Me.Book_Categories IsNot Nothing Then
                    For Each category In Me.Book_Categories
                        _categories_list &= category.Category_Name & ","
                    Next
                    _categories_list = _categories_list.Substring(0, _categories_list.Length - 1)
                End If
            End If
            Return _categories_list
        End Get
    End Property

End Class

Partial Public Class Member

    Private _isActive As Boolean
    Public Property IsActive As Boolean
        Get
            If Me.MemberActive Then
                If Me.MemberDeactivatedFrom Is Nothing Then
                    _isActive = True
                Else
                    If DateTime.Today >= Me.MemberDeactivatedFrom Then
                        _isActive = False
                        Me.IsActive = False
                    Else
                        _isActive = True
                    End If
                End If
            Else
                _isActive = False
            End If
            Return _isActive
        End Get
        Set(value As Boolean)
            If Me.MemberActive <> value Then
                Me.MemberActive = value
                If Me.MemberActive Then
                    If Me.MemberDeactivatedFrom IsNot Nothing Then
                        Me.MemberDeactivatedFrom = Nothing
                    End If
                Else
                    Me.MemberDeactivatedFrom = DateTime.Today
                End If
                Me.ReportPropertyChanged("MemberActive")
            End If
        End Set
    End Property

End Class

Partial Public Class Author

    Public ReadOnly Property Author_Full_Name As String
        Get
            Return Me.Author_First_Name & " " & Me.Author_Last_Name
        End Get
    End Property

End Class
