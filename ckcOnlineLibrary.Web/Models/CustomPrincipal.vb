﻿Imports System.Security.Principal
Imports System

Public Class CustomPrincipal
    Implements IPrincipal

    Private _identity As IIdentity
    Private _roles As String()

    Public Sub New(identity As IIdentity, roles As String())
        _identity = identity
        _roles = New String(roles.Length - 1) {}
        roles.CopyTo(_roles, 0)
        Array.Sort(_roles)
    End Sub

    Public ReadOnly Property Identity As System.Security.Principal.IIdentity Implements System.Security.Principal.IPrincipal.Identity
        Get
            Return _identity
        End Get
    End Property

    Public Function IsInRole(role As String) As Boolean Implements System.Security.Principal.IPrincipal.IsInRole
        Return If(Array.BinarySearch(_roles, role) >= 0, True, False)
    End Function

    Public Function IsInAllRoles(ParamArray roles As String()) As Boolean
        For Each searchrole As String In roles
            If Array.BinarySearch(_roles, searchrole) < 0 Then
                Return False
            End If
        Next
        Return True
    End Function

    Public Function IsInAnyRoles(ParamArray roles As String()) As Boolean
        For Each searchrole As String In roles
            If Array.BinarySearch(_roles, searchrole) > 0 Then
                Return True
            End If
        Next
        Return False
    End Function

End Class
