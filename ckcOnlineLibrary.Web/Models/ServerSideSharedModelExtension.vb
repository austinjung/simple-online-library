﻿Imports System
Imports System.Collections.ObjectModel
Imports System.ComponentModel.DataAnnotations
Imports System.Collections
Imports System.Web
Imports System.Web.Services

'Public Class Book_Rank_By_Request

'    Private _book_id As Integer
'    Public Property Book_id As Integer
'        Get
'            Return _book_id
'        End Get
'        Set(value As Integer)
'            _book_id = value
'        End Set
'    End Property

'    Private _count As Integer
'    Public Property Count As Integer
'        Get
'            Return _count
'        End Get
'        Set(value As Integer)
'            _count = value
'        End Set
'    End Property

'    Private _book As Book
'    Public Property Book As Book
'        Get
'            Return _book
'        End Get
'        Set(value As Book)
'            _book = value
'        End Set
'    End Property

'    Private _book_Requests As ObservableCollection(Of Member_Requests)
'    Public Property Book_Requests As ObservableCollection(Of Member_Requests)
'        Get
'            Return _book_Requests
'        End Get
'        Set(value As ObservableCollection(Of Member_Requests))
'            _book_Requests = value
'        End Set
'    End Property

'End Class

Partial Public Class Transactions_Detail

    Private Const _format As String = "/Upload/Book_Images/{0}.jpg"

    Public ReadOnly Property ImageSource As String
        Get
            If String.IsNullOrEmpty(Me.Book_Image_File) Then
                'Return String.Format(_format, Me._Book_id)
                Return "/Upload/Book_Images/NoImage.png"
            ElseIf Me.Book_Image_File.Contains("file:") Then
                Return Me.Book_Image_File
            Else
                Return "/Upload/Book_Images/" & Me.Book_Image_File
            End If
        End Get
    End Property

End Class

Partial Public Class Books_With_Statistics

    Private Const _format As String = "/Upload/Book_Images/{0}.jpg"

    Public ReadOnly Property ImageSource As String
        Get
            If String.IsNullOrEmpty(Me.Book_Image_File) Then
                'Return String.Format(_format, Me._Book_id)
                Return "/Upload/Book_Images/NoImage.png"
            Else
                Return "/Upload/Book_Images/" & Me.Book_Image_File
            End If
        End Get
    End Property

End Class

Partial Public Class Books_By_Requests

    Private Const _format As String = "/Upload/Book_Images/{0}.jpg"

    Public ReadOnly Property ImageSource As String
        Get
            If String.IsNullOrEmpty(Me.Book_Image_File) Then
                'Return String.Format(_format, Me._Book_id)
                Return "/Upload/Book_Images/NoImage.png"
            Else
                Return "/Upload/Book_Images/" & Me.Book_Image_File
            End If
        End Get
    End Property

End Class

Partial Public Class Books_By_Reviews

    Private Const _format As String = "/Upload/Book_Images/{0}.jpg"

    Public ReadOnly Property ImageSource As String
        Get
            If String.IsNullOrEmpty(Me.Book_Image_File) Then
                'Return String.Format(_format, Me._Book_id)
                Return "/Upload/Book_Images/NoImage.png"
            Else
                Return "/Upload/Book_Images/" & Me.Book_Image_File
            End If
        End Get
    End Property

End Class

Partial Public Class Book

    Private Const _format As String = "/Upload/Book_Images/{0}.jpg"

    Public ReadOnly Property ImageSource As String
        Get
            If String.IsNullOrEmpty(Me.Book_Image_File) Then
                'Return String.Format(_format, Me._Book_id)
                Return "/Upload/Book_Images/NoImage.png"
            Else
                Return "/Upload/Book_Images/" & Me.Book_Image_File
            End If
        End Get
    End Property

    Private _book_Categiries As ObservableCollection(Of Category)
    Public Property Book_Categories As ObservableCollection(Of Category)
        Get
            Return _book_Categiries
        End Get
        Set(value As ObservableCollection(Of Category))
            'If Not _book_Categiries.Equals(value) Then
            _book_Categiries = value
            Me.ReportPropertyChanged("Book_Categories")
            Me.ReportPropertyChanged("Book_Categories_List")
            'End If
        End Set
    End Property
    Private _categories_list As String
    Public ReadOnly Property Book_Categories_List As String
        Get
            _categories_list = String.Empty
            If Me.Book_Categories IsNot Nothing Then
                For Each category In Me.Book_Categories
                    _categories_list &= category.Category_Name & ", "
                Next
                If Not String.IsNullOrEmpty(_categories_list) Then
                    _categories_list = _categories_list.Substring(0, _categories_list.Length - 2)
                End If
            End If
            Return _categories_list
        End Get
    End Property

    Private _book_Authors As ObservableCollection(Of Author)
    Public Property Book_Authors As ObservableCollection(Of Author)
        Get
            Return _book_Authors
        End Get
        Set(value As ObservableCollection(Of Author))
            'If Not _book_Authors.Equals(value) Then
            _book_Authors = value
            Me.ReportPropertyChanged("Book_Authors")
            Me.ReportPropertyChanged("Book_Authors_List")
            'End If
        End Set
    End Property
    Private _authors_list As String
    Public ReadOnly Property Book_Authors_List As String
        Get
            _authors_list = String.Empty
            If Me.Book_Authors IsNot Nothing Then
                For Each author In Me.Book_Authors
                    _authors_list &= author.Author_Full_Name & ", "
                Next
                If Not String.IsNullOrEmpty(_authors_list) Then
                    _authors_list = _authors_list.Substring(0, _authors_list.Length - 2)
                End If
            End If
            Return _authors_list
        End Get
    End Property

    Private _book_Shelves As ObservableCollection(Of Shelf)
    Public Property Book_Shelves As ObservableCollection(Of Shelf)
        Get
            Return _book_Shelves
        End Get
        Set(value As ObservableCollection(Of Shelf))
            'If _book_Shelves.Equals(value) Then
            _book_Shelves = value
            Me.ReportPropertyChanged("Book_Shelves")
            Me.ReportPropertyChanged("Book_Shelves_List")
            'End If
        End Set
    End Property
    Private _shelves_list As String
    Public ReadOnly Property Book_Shelves_List As String
        Get
            _shelves_list = String.Empty
            If Me.Book_Shelves IsNot Nothing Then
                For Each shelf In Me.Book_Shelves
                    _shelves_list &= shelf.Shelf_Code & ", "
                Next
                If Not String.IsNullOrEmpty(_shelves_list) Then
                    _shelves_list = _shelves_list.Substring(0, _shelves_list.Length - 2)
                End If
            End If
            Return _shelves_list
        End Get
    End Property

End Class

Partial Public Class Member

    Private _isActive As Boolean
    Public Property IsActive As Boolean
        Get
            If Me.MemberActive Then
                If Me.MemberDeactivatedFrom Is Nothing Then
                    _isActive = True
                Else
                    If DateTime.Today >= Me.MemberDeactivatedFrom Then
                        _isActive = False
                        Me.IsActive = False
                    Else
                        _isActive = True
                    End If
                End If
            Else
                _isActive = False
            End If
            Return _isActive
        End Get
        Set(value As Boolean)
            If Me.MemberActive <> value Then
                Me.MemberActive = value
                If Me.MemberActive Then
                    If Me.MemberDeactivatedFrom IsNot Nothing Then
                        Me.MemberDeactivatedFrom = Nothing
                    End If
                Else
                    Me.MemberDeactivatedFrom = DateTime.Today
                End If
                Me.ReportPropertyChanged("MemberActive")
            End If
        End Set
    End Property

End Class

Partial Public Class Author

    Private _isChecked As Boolean
    Public Property IsChecked As Boolean
        Get
            Return _isChecked
        End Get
        Set(value As Boolean)
            _isChecked = value
            Me.ReportPropertyChanged("IsChecked")
        End Set
    End Property

    Public ReadOnly Property Author_Full_Name As String
        Get
            Return Me.Author_First_Name & " " & Me.Author_Last_Name
        End Get
    End Property

End Class

Partial Public Class Category

    Private _isChecked As Boolean
    Public Property IsChecked As Boolean
        Get
            Return _isChecked
        End Get
        Set(value As Boolean)
            _isChecked = value
            Me.ReportPropertyChanged("IsChecked")
        End Set
    End Property

End Class

Partial Public Class Shelf

    Private _isChecked As Boolean
    Public Property IsChecked As Boolean
        Get
            Return _isChecked
        End Get
        Set(value As Boolean)
            _isChecked = value
            Me.ReportPropertyChanged("IsChecked")
        End Set
    End Property

End Class
