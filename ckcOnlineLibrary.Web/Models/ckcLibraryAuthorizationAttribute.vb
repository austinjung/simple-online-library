﻿Imports System.ComponentModel.DataAnnotations
Imports System.Web.Configuration

Public Class ckcLibraryAuthorizationAttribute
    Inherits AuthorizationAttribute

    Protected Overrides Function IsAuthorized(ByVal principal As System.Security.Principal.IPrincipal,
                                              ByVal authorizationContext As System.ComponentModel.DataAnnotations.AuthorizationContext) As System.ComponentModel.DataAnnotations.AuthorizationResult

        '        If (selectedEmployee.ManagerID = authenticatedUser.EmployeeID) Then
        Return AuthorizationResult.Allowed
        'Else
        'Return New AuthorizationResult("Only the authenticated manager for the employee can add a new record.")
        'End If
    End Function

End Class
