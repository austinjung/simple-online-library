﻿
Option Compare Binary
Option Infer On
Option Strict On
Option Explicit On

Imports ckcOnlineLibrary
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Data.Objects.DataClasses
Imports System.Linq
Imports System.ServiceModel.DomainServices.Hosting
Imports System.ServiceModel.DomainServices.Server


'The MetadataTypeAttribute identifies AuthorMetadata as the class
' that carries additional metadata for the Author class.
<MetadataTypeAttribute(GetType(Author.AuthorMetadata))> _
Partial Public Class Author

    'This class allows you to attach custom attributes to properties
    ' of the Author class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class AuthorMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Author_Career As String

        <Required(ErrorMessage:="First Name is required.")>
        <StringLength(50, ErrorMessage:="First Name must be a string with a maximum length of 50.")>
        Public Property Author_First_Name As String

        Public Property Author_id As Integer

        <Required(ErrorMessage:="Last Name is required.")>
        <StringLength(50, ErrorMessage:="Last Name must be a string with a maximum length of 50.")>
        Public Property Author_Last_Name As String

        Public Property Books As EntityCollection(Of Book)

        Public Property IsActive As Boolean
    End Class
End Class

'The MetadataTypeAttribute identifies BookMetadata as the class
' that carries additional metadata for the Book class.
<MetadataTypeAttribute(GetType(Book.BookMetadata))> _
Partial Public Class Book

    'This class allows you to attach custom attributes to properties
    ' of the Book class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class BookMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Authors As EntityCollection(Of Author)

        Public Property Book_Description As String

        Public Property Book_id As Integer

        Public Property Book_Image_File As String

        Public Property Book_ISBN As String

        Public Property Book_Language As String

        Public Property Book_Media As String

        Public Property Book_Media_Set As String

        Public Property Book_Published As Nullable(Of DateTime)

        <Required(ErrorMessage:="Available quantity is required.")>
        <Range(1, 1000, ErrorMessage:="Quantity must be a number between 1 to 1000.")>
        Public Property Book_Quantity_Avail As Integer

        Public Property Book_Quantity_Onhand As Integer

        <Required(ErrorMessage:="Book title is required.")>
        <StringLength(100, ErrorMessage:="Book title must be a string with a maximum length of 100.")>
        Public Property Book_Title As String

        Public Property Categories As EntityCollection(Of Category)

        Public Property ImageSource As String

        Public Property IsActive As Boolean

        Public Property Member_Requests As EntityCollection(Of Member_Requests)

        Public Property Member_Reviews As EntityCollection(Of Member_Reviews)

        Public Property Member_Transactions As EntityCollection(Of Member_Transactions)

        Public Property Shelves As EntityCollection(Of Shelf)
    End Class
End Class

'The MetadataTypeAttribute identifies CategoryMetadata as the class
' that carries additional metadata for the Category class.
<MetadataTypeAttribute(GetType(Category.CategoryMetadata))> _
Partial Public Class Category

    'This class allows you to attach custom attributes to properties
    ' of the Category class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class CategoryMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Books As EntityCollection(Of Book)

        Public Property Category_id As Integer

        <Required(ErrorMessage:="Category Name is required.")>
        <StringLength(50, ErrorMessage:="Category Name must be a string with a maximum length of 50.")>
        Public Property Category_Name As String

        Public Property IsActive As Boolean
    End Class
End Class

'The MetadataTypeAttribute identifies MemberMetadata as the class
' that carries additional metadata for the Member class.
<MetadataTypeAttribute(GetType(Member.MemberMetadata))> _
Partial Public Class Member

    'This class allows you to attach custom attributes to properties
    ' of the Member class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class MemberMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Address_Line_1 As String

        Public Property Address_Line_2 As String

        <DefaultValue(True)>
        Public Property AllowEmails As Nullable(Of Boolean)

        Public Property City As String

        <DefaultValue("Canada")>
        Public Property Country As String

        <StringLength(50)>
        <RegularExpression("\b[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}\b", ErrorMessage:="Invalid Email Address.")>
        Public Property Email As String

        <Required(ErrorMessage:="Full Name is required.")>
        <StringLength(50, ErrorMessage:="Full Name must be a string with a maximum length of 50.")>
        Public Property FullName As String

        Public Property Library_PIN As String

        Public Property Member_Requests As EntityCollection(Of Member_Requests)

        Public Property Member_Reviews As EntityCollection(Of Member_Reviews)

        Public Property Member_Transactions As EntityCollection(Of Member_Transactions)

        <DefaultValue(True)>
        Public Property MemberActive As Nullable(Of Boolean)

        Public Property MemberDeactivatedFrom As Nullable(Of DateTime)

        Public Property MemberID As Integer

        <Required(ErrorMessage:="Login Name is required.")>
        <StringLength(50, ErrorMessage:="Login Name must be a string with a maximum length of 50.")>
        Public Property MemberLogin As String

        '<DefaultValue(TypeOf(DateTime), DateTime.Today)>
        Public Property MemberSince As Nullable(Of DateTime)

        Public Property Name As String

        <Required(ErrorMessage:="Password is required.")>
        <StringLength(20, MinimumLength:=6, ErrorMessage:="Password must be a string with a minimum length of 6 and a maximum length of 20.")>
        Public Property Password As String

        <RegularExpression("\b^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$\b", ErrorMessage:="Invalid Phone number. Ex: 1 (604) 850-0371 354")>
        <StringLength(25, ErrorMessage:="Phone number must be a string with a minimum length of 10 and a maximum length of 25.")>
        Public Property Phone As String

        Public Property PostalCode As String

        <DefaultValue("BC")>
        Public Property Province As String

        <Required(ErrorMessage:="Role is required.")>
        <StringLength(100, ErrorMessage:="Role must be a string with a maximum length of 100.")>
        <DefaultValue("User")>
        Public Property Role As String

        Public Property Roles As IEnumerable(Of String)
    End Class
End Class

'The MetadataTypeAttribute identifies Member_RequestsMetadata as the class
' that carries additional metadata for the Member_Requests class.
<MetadataTypeAttribute(GetType(Member_Requests.Member_RequestsMetadata))> _
Partial Public Class Member_Requests

    'This class allows you to attach custom attributes to properties
    ' of the Member_Requests class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class Member_RequestsMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Book As Book

        Public Property Book_id As Integer

        Public Property Comitted_Date As Nullable(Of DateTime)

        Public Property Comment As String

        Public Property Expected_Date As Nullable(Of DateTime)

        Public Property Member As Member

        Public Property MemberID As Integer

        Public Property Request_id As Integer

        Public Property Request_Status As Nullable(Of Boolean)
    End Class
End Class

'The MetadataTypeAttribute identifies Member_ReviewsMetadata as the class
' that carries additional metadata for the Member_Reviews class.
<MetadataTypeAttribute(GetType(Member_Reviews.Member_ReviewsMetadata))> _
Partial Public Class Member_Reviews

    'This class allows you to attach custom attributes to properties
    ' of the Member_Reviews class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class Member_ReviewsMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Book As Book

        Public Property Book_id As Integer

        Public Property Member As Member

        Public Property MemberID As Integer

        <Required()>
        Public Property Rating As Nullable(Of Double)

        Public Property Review As String

        Public Property Review_Date As Nullable(Of DateTime)

        Public Property Review_id As Integer
    End Class
End Class

'The MetadataTypeAttribute identifies Member_TransactionsMetadata as the class
' that carries additional metadata for the Member_Transactions class.
<MetadataTypeAttribute(GetType(Member_Transactions.Member_TransactionsMetadata))> _
Partial Public Class Member_Transactions

    'This class allows you to attach custom attributes to properties
    ' of the Member_Transactions class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class Member_TransactionsMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Book As Book

        Public Property Book_id As Integer

        Public Property Comment As String

        Public Property Expected_Return As Nullable(Of DateTime)

        Public Property Issued_Date As Nullable(Of DateTime)

        Public Property Member As Member

        Public Property MemberID As Integer

        Public Property Requested_Date As Nullable(Of DateTime)

        Public Property Returned_Date As Nullable(Of DateTime)

        Public Property Transaction_id As Integer
    End Class
End Class

'The MetadataTypeAttribute identifies Books_By_RequestsMetadata as the class
' that carries additional metadata for the Books_By_Requests class.
<MetadataTypeAttribute(GetType(Books_By_Requests.Books_By_RequestsMetadata))> _
Partial Public Class Books_By_Requests

    'This class allows you to attach custom attributes to properties
    ' of the Books_By_Requests class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class Books_By_RequestsMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Book_Description As String

        Public Property Book_id As Integer

        Public Property Book_Image_File As String

        Public Property Book_Quantity_Onhand As Integer

        Public Property Book_Title As String

        Public Property Request_Count As Nullable(Of Integer)
    End Class
End Class

'The MetadataTypeAttribute identifies Books_By_ReviewsMetadata as the class
' that carries additional metadata for the Books_By_Reviews class.
<MetadataTypeAttribute(GetType(Books_By_Reviews.Books_By_ReviewsMetadata))> _
Partial Public Class Books_By_Reviews

    'This class allows you to attach custom attributes to properties
    ' of the Books_By_Reviews class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class Books_By_ReviewsMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Average_Rate As Nullable(Of Double)

        Public Property Book_Description As String

        Public Property Book_id As Integer

        Public Property Book_Image_File As String

        Public Property Book_Quantity_Onhand As Integer

        Public Property Book_Title As String
    End Class
End Class

'The MetadataTypeAttribute identifies ShelfMetadata as the class
' that carries additional metadata for the Shelf class.
<MetadataTypeAttribute(GetType(Shelf.ShelfMetadata))> _
Partial Public Class Shelf

    'This class allows you to attach custom attributes to properties
    ' of the Shelf class.
    '
    'For example, the following marks the Xyz property as a
    ' required property and specifies the format for valid values:
    '    <Required()>
    '    <RegularExpression("[A-Z][A-Za-z0-9]*")>
    '    <StringLength(32)>
    '    Public Property Xyz As String
    Friend NotInheritable Class ShelfMetadata

        'Metadata classes are not meant to be instantiated.
        Private Sub New()
            MyBase.New()
        End Sub

        Public Property Books As EntityCollection(Of Book)

        Public Property IsActive As Boolean

        Public Property IsChecked As Boolean

        <Required(ErrorMessage:="Shelf code is required.")>
        <StringLength(50, ErrorMessage:="Shelf code must be a string with a maximum length of 50.")>
        Public Property Shelf_Code As String

        Public Property Shelf_Description As String

        Public Property Shelf_id As Integer
    End Class
End Class

