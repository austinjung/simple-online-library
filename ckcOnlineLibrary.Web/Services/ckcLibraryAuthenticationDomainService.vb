﻿Option Strict Off
Option Explicit On

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Linq
Imports System.ServiceModel.DomainServices.Hosting
Imports System.ServiceModel.DomainServices.Server
Imports System.ServiceModel.DomainServices.Server.ApplicationServices
Imports System.ServiceModel.DomainServices.EntityFramework
Imports System.Web.Security
Imports System.Web
Imports System.Xml

<EnableClientAccess()> _
Public Class ckcLibraryAuthenticationService
    Inherits LinqToEntitiesDomainService(Of ckLibraryDataEntities)
    Implements IAuthentication(Of Member)

    'To enable Forms/Windows Authentication for the Web Application, 
    'edit the appropriate section of web.config file.

    Private Shared DefaultMember As Member = New Member() With {
            .MemberLogin = String.Empty,
            .Password = String.Empty,
            .FullName = String.Empty,
            .Role = String.Empty
   }

    Public Function GetUser() As Member Implements System.ServiceModel.DomainServices.Server.ApplicationServices.IAuthentication(Of Member).GetUser
        If Not (Me.ServiceContext Is Nothing) And Not (Me.ServiceContext.User Is Nothing) And Me.ServiceContext.User.Identity.IsAuthenticated Then
            Return Me.GetUser(Me.ServiceContext.User.Identity.Name)
        End If
        Return DefaultMember
    End Function

    Private Function GetUser(ByVal userName As String) As Member
        Return Me.ObjectContext.Members.First(Function(u) u.MemberLogin = userName)
    End Function

    Public Function Login(ByVal userName As String, ByVal password As String, ByVal isPersistent As Boolean, ByVal customData As String) As Member Implements System.ServiceModel.DomainServices.Server.ApplicationServices.IAuthentication(Of Member).Login
        If (Me.ValidateUser(userName, password)) Then

            'FormsAuthentication.SetAuthCookie(userName, isPersistent)
            ''''''''''''''''''''''''''''''''''''''''''''''''
            Dim user = Me.GetUser(userName)
            SetAuthCookieMain(userName, user.Role, isPersistent, "/")
            ''''''''''''''''''''''''''''''''''''''''''''''''

            Return user
        Else
            Return Nothing
        End If
    End Function

    ' Austin Jung : Customized
    Private Function ValidateUser(ByVal userName As String, ByVal password As String) As Boolean
        Dim currentMember = Me.ObjectContext.Members.Where(Function(u) _
                                                           u.MemberLogin = userName And u.Password = password And u.MemberActive = True).SingleOrDefault
        Dim _isActive As Boolean
        If currentMember Is Nothing Then
            _isActive = False
        ElseIf currentMember.MemberDeactivatedFrom Is Nothing Then
            _isActive = True
        ElseIf DateTime.Today >= currentMember.MemberDeactivatedFrom Then
            _isActive = False
            currentMember.MemberActive = False
            Me.ObjectContext.SaveChanges()
        Else
            _isActive = True
        End If
        Return _isActive
    End Function

    Public Function Logout() As Member Implements System.ServiceModel.DomainServices.Server.ApplicationServices.IAuthentication(Of Member).Logout
        FormsAuthentication.SignOut()
        Return DefaultMember
    End Function

    Public Sub UpdateUser(ByVal user As Member) Implements System.ServiceModel.DomainServices.Server.ApplicationServices.IAuthentication(Of Member).UpdateUser
        If ((Me.ServiceContext.User Is Nothing) Or (Me.ServiceContext.User.Identity Is Nothing) Or Not (String.Equals(Me.ServiceContext.User.Identity.Name, user.Name, System.StringComparison.Ordinal))) Then
            Throw New UnauthorizedAccessException("You are only authorized to modify your own profile!")
        Else
            Me.ObjectContext.Members.AttachAsModified(user, Me.ChangeSet.GetOriginal(user))
        End If

    End Sub

    Protected Overrides Sub OnError(errorInfo As System.ServiceModel.DomainServices.Server.DomainServiceErrorInfo)
        MyBase.OnError(errorInfo)
    End Sub




    ''' <summary>
    ''' Creates Forms authentication ticket and writes it in URL or embeds it within Cookie
    ''' </summary>
    ''' <param name="userName">User name</param>
    ''' <param name="commaSeperatedRoles">Comma separated roles for the users</param>
    ''' <param name="createPersistentCookie">True or false whether
    '''          to create persistant cookie</param>
    ''' <param name="strCookiePath">Path for which
    '''         the authentication ticket is valid</param>
    Private Shared Sub SetAuthCookieMain(userName As String, commaSeperatedRoles As String, createPersistentCookie As Boolean, strCookiePath As String)
        Dim ticket As FormsAuthenticationTicket = CreateAuthenticationTicket(userName, commaSeperatedRoles, createPersistentCookie, strCookiePath)
        'Encrypt the authentication ticket
        Dim encrypetedTicket As String = FormsAuthentication.Encrypt(ticket)
        If Not FormsAuthentication.CookiesSupported Then
            'If the authentication ticket is specified not to use cookie, set it in the URL
            FormsAuthentication.SetAuthCookie(encrypetedTicket, False)
        Else
            'If the authentication ticket is specified to use a cookie,
            'wrap it within a cookie.
            'The default cookie name is .ASPXAUTH if not specified
            'in the <forms> element in web.config
            Dim authCookie As New HttpCookie(FormsAuthentication.FormsCookieName, encrypetedTicket)
            'Set the cookie's expiration time to the tickets expiration time
            authCookie.Expires = ticket.Expiration
            'Set the cookie in the Response
            HttpContext.Current.Response.Cookies.Add(authCookie)
        End If
    End Sub

    ''' <summary> 
    ''' Creates and returns the Forms authentication ticket
    ''' </summary>
    ''' <param name="userName">User name</param>
    ''' <param name="commaSeperatedRoles">Comma separated roles for the users</param>
    ''' <param name="createPersistentCookie">True or false
    '''        whether to create persistant cookie</param>
    ''' <param name="strCookiePath">Path for which the authentication ticket is valid</param>
    Private Shared Function CreateAuthenticationTicket(userName As String, commaSeperatedRoles As String, createPersistentCookie As Boolean, strCookiePath As String) As FormsAuthenticationTicket
        Dim cookiePath As String = If(strCookiePath Is Nothing, FormsAuthentication.FormsCookiePath, strCookiePath)
        'Determine the cookie timeout value from web.config if specified
        Dim expirationMinutes As Integer = GetCookieTimeoutValue()
        'Create the authentication ticket
        'A dummy ticket version
        'User name for whom the ticket is issued
        'Current date and time
        'Expiration date and time
        'Whether to persist cookie on client side. If true,
        'The authentication ticket will be issued for new sessions from the same client
        'PC
        'Comma separated user roles
        Dim ticket As New FormsAuthenticationTicket(1, userName, DateTime.Now, DateTime.Now.AddMinutes(expirationMinutes), createPersistentCookie, commaSeperatedRoles, _
         cookiePath)
        'Path cookie valid for
        Return ticket
    End Function

    ''' <summary>
    ''' Retrieves cookie timeout value in the <forms></forms>
    ''' section in the web.config file as this
    ''' value is not accessible via the FormsAuthentication or any other built in class
    ''' </summary>
    ''' <returns></returns>
    Private Shared Function GetCookieTimeoutValue() As Integer
        Dim timeout As Integer = 60
        'Default timeout is 30 minutes
        Dim webConfig As New XmlDocument()
        webConfig.Load(HttpContext.Current.Server.MapPath("../web.config"))
        Dim node As XmlNode = webConfig.SelectSingleNode("/configuration/" & "system.web/authentication/forms")
        If node IsNot Nothing AndAlso node.Attributes("timeout") IsNot Nothing Then
            timeout = Integer.Parse(node.Attributes("timeout").Value)
        End If
        Return timeout
    End Function

End Class
