﻿
Option Compare Binary
Option Infer On
Option Strict On
Option Explicit On

Imports ckcOnlineLibrary
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Data
Imports System.Linq
Imports System.ServiceModel.DomainServices.EntityFramework
Imports System.ServiceModel.DomainServices.Hosting
Imports System.ServiceModel.DomainServices.Server
Imports System.Data.Objects
Imports System.Linq.Expressions
Imports System.Security.Principal
Imports System.Data.Objects.DataClasses
Imports System.Net.Mail
Imports System.Net
Imports ckcOnlineLibrary.DateTimeExtensions
Imports System.IO
Imports System.Web
Imports Ria.Common
Imports Microsoft.VisualBasic

'Implements application logic using the ckLibraryDataEntities context.
' TODO: Add your application logic to these methods or in additional methods.
' TODO: Wire up authentication (Windows/ASP.NET Forms) and uncomment the following to disable anonymous access
' Also consider adding roles to restrict access as appropriate.
'<RequiresAuthentication> _
<EnableClientAccess()> _
Public Class ckcLibraryDomainService
    Inherits LinqToEntitiesDomainService(Of ckLibraryDataEntities)

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Authors' query.

    '============== Paging ===============
    'Overridden to support paging over DataEntities.
    'If you're extending a derived DomainService, implementation this is already done for you.
    'Protected Overrides Function Count(Of T)(query As System.Linq.IQueryable(Of T)) As Integer
    '    Return MyBase.Count(Of T)(query)
    'End Function

    Private Function _getAllAuthors() As IQueryable(Of Author)
        Return Me.ObjectContext.Authors
    End Function

    Private Function _getAuthorsByActive(ByVal active As Boolean) As IQueryable(Of Author)
        Return Me.ObjectContext.Authors.Where(Function(a) a.IsActive = active)
    End Function

    Private Function _orderAuthors(query As IQueryable(Of Author)) As IQueryable(Of Author)
        Return query.OrderBy(Function(a) a.Author_First_Name & " " & a.Author_Last_Name)
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveAuthors() As IQueryable(Of Author)
        Return _orderAuthors(_getAuthorsByActive(True))
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllAuthors() As IQueryable(Of Author)
        Return _orderAuthors(_getAllAuthors())
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveAuthors_By_Name(ByVal name_Segment As String) As IQueryable(Of Author)
        'Dim query = From a In Me.ObjectContext.Authors
        '            Where a.Author_First_Name.Contains(name_Segment) OrElse a.Author_Last_Name.Contains(name_Segment)
        '            Order By a.Author_First_Name, a.Author_Last_Name
        '            Select a
        'Return query
        Return _orderAuthors(_getAuthorsByActive(True).Where(Function(a) a.Author_First_Name.Contains(name_Segment) _
                                                                      OrElse a.Author_Last_Name.Contains(name_Segment)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetAuthors_By_Name(ByVal name_Segment As String) As IQueryable(Of Author)
        Return _orderAuthors(_getAllAuthors().Where(Function(a) a.Author_First_Name.Contains(name_Segment) _
                                                             OrElse a.Author_Last_Name.Contains(name_Segment)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveAuthors_By_First_Name(ByVal name_Segment As String) As IQueryable(Of Author)
        Return _orderAuthors(_getAuthorsByActive(True).Where(Function(a) a.Author_First_Name.Contains(name_Segment)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetAuthors_By_First_Name(ByVal name_Segment As String) As IQueryable(Of Author)
        Return _orderAuthors(_getAllAuthors().Where(Function(a) a.Author_First_Name.Contains(name_Segment)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveAuthors_By_Last_Name(ByVal name_Segment As String) As IQueryable(Of Author)
        Return _orderAuthors(_getAuthorsByActive(True).Where(Function(a) a.Author_Last_Name.Contains(name_Segment)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetAuthors_By_Last_Name(ByVal name_Segment As String) As IQueryable(Of Author)
        Return _orderAuthors(_getAllAuthors().Where(Function(a) a.Author_Last_Name.Contains(name_Segment)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveAuthors_By_Career(ByVal keywords As String) As IQueryable(Of Author)
        Return _orderAuthors(_getAuthorsByActive(True).Where(Function(a) a.Author_Career.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetAuthors_By_Career(ByVal keywords As String) As IQueryable(Of Author)
        Return _orderAuthors(_getAllAuthors().Where(Function(a) a.Author_Career.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetAuthors_Of_Book(ByVal book_id As Integer) As IQueryable(Of Author)
        'Return _orderAuthors(_getAllAuthors().Where(Function(a) a.Books.Any(Function(b) b.Book_id = book_id)))
        Return _orderAuthors(_getAuthorsByActive(True).Where(Function(a) a.Books.Any(Function(b) b.Book_id = book_id)))
    End Function

    <RequiresRole("Manager")>
    Public Sub InsertAuthor(ByVal author As Author)
        If ((author.EntityState = EntityState.Detached) _
                    = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(author, EntityState.Added)
        Else
            Me.ObjectContext.Authors.AddObject(author)
        End If
    End Sub

    <RequiresRole("Manager")>
    Public Sub UpdateAuthor(ByVal currentAuthor As Author)
        Me.ObjectContext.Authors.AttachAsModified(currentAuthor, Me.ChangeSet.GetOriginal(currentAuthor))
    End Sub

    <RequiresRole("Manager")>
    Public Sub DeleteAuthor(ByVal author As Author)
        'If ((author.EntityState = EntityState.Detached) _
        '            = False) Then
        '    Me.ObjectContext.ObjectStateManager.ChangeObjectState(author, EntityState.Deleted)
        'Else
        '    Me.ObjectContext.Authors.Attach(author)
        '    Me.ObjectContext.Authors.DeleteObject(author)
        'End If
        Me.ObjectContext.Delete_Author_Books(author.Author_id)
        author.IsActive = False
        UpdateAuthor(author)
    End Sub

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Books' query.
    '                        .Book_Categories = CType(b.Categories, EntityList(Of Category)).Source,

    Private Function _getAllBooks() As IQueryable(Of Book)
        'Dim books = From b In Me.ObjectContext.Books
        '            Select New Book With {
        '                .Book_id = b.Book_id,
        '                .Book_Title = b.Book_Title,
        '                .Book_Categories = _get_Categories_of_Book(b.Categories, .Book_Categories),
        '                .Book_Description = b.Book_Description,
        '                .Book_Image_File = b.Book_Image_File,
        '                .Book_ISBN = b.Book_ISBN,
        '                .Book_Language = b.Book_Language,
        '                .Book_Media = b.Book_Media,
        '                .Book_Published = b.Book_Published,
        '                .Book_Quantity_Avail = b.Book_Quantity_Avail,
        '                .Book_Quantity_Onhand = b.Book_Quantity_Onhand,
        '                .IsActive = b.IsActive
        '            }
        'Return CType(books, Global.System.Linq.IQueryable(Of Global.ckcOnlineLibrary.Book))
        Return Me.ObjectContext.Books
    End Function

    'Private Function _get_Categories_of_Book(ByVal entities As EntityCollection(Of Category),
    '                                         ByRef categories As Category()) As Category()
    '    categories = {}
    '    entities.CopyTo(categories, 0)
    '    Return categories
    'End Function

    Private Function _getBooksByActive(ByVal active As Boolean) As IQueryable(Of Book)
        Return Me.ObjectContext.Books.Where(Function(b) b.IsActive = active)
    End Function

    Private Function _orderBooks(query As IQueryable(Of Book)) As IQueryable(Of Book)
        Return query.OrderBy(Function(b) b.Book_Title)
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks() As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True))
    End Function

    <RequiresAuthentication()>
    Public Function GetBook_Of_Request(ByVal request_id As Integer) As IQueryable(Of Book)
        Dim query = From b In Me.ObjectContext.Books _
                    Join r In Me.ObjectContext.Member_Requests _
                    On b.Book_id Equals r.Book_id _
                    Where r.Request_id = request_id
                    Select b
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function GetBook_Of_Review(ByVal review_id As Integer) As IQueryable(Of Book)
        Dim query = From b In Me.ObjectContext.Books _
                    Join r In Me.ObjectContext.Member_Reviews _
                    On b.Book_id Equals r.Book_id _
                    Where r.Review_id = review_id
                    Select b
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function GetBook_Of_Transaction(ByVal transaction_id As Integer) As IQueryable(Of Book)
        Dim query = From b In Me.ObjectContext.Books _
                    Join t In Me.ObjectContext.Member_Transactions _
                    On b.Book_id Equals t.Book_id _
                    Where t.Transaction_id = transaction_id
                    Select b
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllBooks() As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks())
    End Function

    <RequiresAuthentication()>
    Public Function GetBook_By_Book_ID(ByVal book_id As Integer) As IQueryable(Of Book)
        Return _getAllBooks().Where(Function(b) b.Book_id = book_id)
    End Function

    <RequiresAuthentication()>
    Public Function GetBook_With_Statics_By_Book_ID(ByVal book_id As Integer) As IQueryable(Of Books_With_Statistics)
        Return Me.ObjectContext.Books_With_Statistics.Where(Function(b) b.Book_id = book_id)
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Title(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Book_Title.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Title(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Book_Title.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Description(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Book_Description.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Description(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Book_Description.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Media(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Book_Media.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Media(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Book_Media.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Language(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Book_Language.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Language(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Book_Language.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_ISBN(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Book_ISBN.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_ISBN(ByVal keywords As String) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Book_ISBN.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Author(ByVal author_id As Integer) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Authors.Any(Function(a) a.Author_id = author_id)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Authors(ByVal author_ids As Integer()) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Authors.Any(Function(a) author_ids.Contains(a.Author_id))))
    End Function

    <RequiresAuthentication()>
    Public Function FilterBooksBasic(ByVal author_ids As Integer(),
                                     ByVal category_ids As Integer(),
                                     ByVal shelf_ids As Integer()) As IQueryable(Of Book)

        Dim query As IQueryable(Of Book) = _getAllBooks()
        If author_ids.Count > 0 Then
            query = query.Where(Function(b) b.Authors.Any(Function(a) author_ids.Contains(a.Author_id)))
        End If
        If category_ids.Count > 0 Then
            query = query.Where(Function(b) b.Categories.Any(Function(c) category_ids.Contains(c.Category_id)))
        End If
        If shelf_ids.Count > 0 Then
            query = query.Where(Function(b) b.Shelves.Any(Function(s) shelf_ids.Contains(s.Shelf_id)))
        End If

        Return _orderBooks(query)
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Author(ByVal author_id As Integer) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Authors.Any(Function(a) a.Author_id = author_id)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Authors(ByVal author_ids As Integer()) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Authors.Any(Function(a) author_ids.Contains(a.Author_id))))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Category(ByVal category_id As Integer) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Categories.Any(Function(c) c.Category_id = category_id)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Categories(ByVal category_ids As Integer()) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Categories.Any(Function(c) category_ids.Contains(c.Category_id))))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Category(ByVal category_id As Integer) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Categories.Any(Function(c) c.Category_id = category_id)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Categories(ByVal category_ids As Integer()) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Categories.Any(Function(c) category_ids.Contains(c.Category_id))))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Shelf(ByVal shelf_id As Integer) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Shelves.Any(Function(s) s.Shelf_id = shelf_id)))
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveBooks_By_Shelves(ByVal shelf_ids As Integer()) As IQueryable(Of Book)
        Return _orderBooks(_getBooksByActive(True).Where(Function(b) b.Shelves.Any(Function(s) shelf_ids.Contains(s.Shelf_id))))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Shelf(ByVal shelf_id As Integer) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Shelves.Any(Function(s) s.Shelf_id = shelf_id)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetBooks_By_Shelves(ByVal shelf_ids As Integer()) As IQueryable(Of Book)
        Return _orderBooks(_getAllBooks().Where(Function(b) b.Shelves.Any(Function(s) shelf_ids.Contains(s.Shelf_id))))
    End Function

    <RequiresRole("Manager")>
    Public Sub InsertBook(ByVal book As Book)
        If ((book.EntityState = EntityState.Detached) _
                    = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(book, EntityState.Added)
        Else
            Me.ObjectContext.Books.AddObject(book)
        End If
    End Sub

    <RequiresRole("Manager")>
    Public Function InsertAuthorOfBook(book_id As Integer, author_id As Integer) As Integer
        Me.ObjectContext.Insert_Author_Of_Book(book_id, author_id)
        Return 1
    End Function

    <RequiresRole("Manager")>
    Public Function InsertCategoryOfBook(book_id As Integer, category_id As Integer) As Integer
        Me.ObjectContext.Insert_Category_Of_Book(book_id, category_id)
        Return 1
    End Function

    <RequiresRole("Manager")>
    Public Function InsertShelfOfBook(book_id As Integer, shelf_id As Integer) As Integer
        Me.ObjectContext.Insert_Shelf_Of_Book(book_id, shelf_id)
        Return 1
    End Function

    <RequiresAuthentication()>
    Public Sub UpdateBook(ByVal currentBook As Book)
        'Without Concurrency control in Entity Framework
        'Me.ObjectContext.Books.AttachAsModified(currentBook, Me.ChangeSet.GetOriginal(currentBook))

        'Using Concurrency control with RowVersion timestamp
        'Check Book_id
        Dim bookID As Integer = currentBook.Book_id
        Dim booksInDB = From b In ObjectContext.Books _
                        Where b.Book_id = bookID _
                        Select b

        'Check book
        If booksInDB.Count < 1 Then
            Throw New Exception("No book with ID " & currentBook.Book_id)
        End If
        Dim bookInDB = booksInDB.FirstOrDefault()
        Try
            'Detach it first
            ObjectContext.Detach(bookInDB)
            'Update the book
            bookInDB.Book_Title = currentBook.Book_Title
            bookInDB.Book_Image_File = currentBook.Book_Image_File
            bookInDB.Book_Description = currentBook.Book_Description
            bookInDB.Book_Media = currentBook.Book_Media
            bookInDB.Book_Language = currentBook.Book_Language
            bookInDB.Book_ISBN = currentBook.Book_ISBN
            bookInDB.Book_Published = currentBook.Book_Published
            bookInDB.Book_Quantity_Avail = currentBook.Book_Quantity_Avail
            bookInDB.Book_Quantity_Onhand = currentBook.Book_Quantity_Onhand
            bookInDB.IsActive = currentBook.IsActive
            bookInDB.RowVersion = currentBook.RowVersion
            'Attach it
            ObjectContext.Attach(bookInDB)
            'Change object state
            ObjectContext.ObjectStateManager.ChangeObjectState(bookInDB, EntityState.Modified)
            ObjectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave)
            ObjectContext.Refresh(RefreshMode.StoreWins, bookInDB)
        Catch ex As Exception
            ObjectContext.Refresh(RefreshMode.StoreWins, bookInDB)
            bookInDB.Book_Title = currentBook.Book_Title
            bookInDB.Book_Image_File = currentBook.Book_Image_File
            bookInDB.Book_Description = currentBook.Book_Description
            bookInDB.Book_Media = currentBook.Book_Media
            bookInDB.Book_Language = currentBook.Book_Language
            bookInDB.Book_ISBN = currentBook.Book_ISBN
            bookInDB.Book_Published = currentBook.Book_Published
            bookInDB.Book_Quantity_Avail = currentBook.Book_Quantity_Avail
            bookInDB.Book_Quantity_Onhand = currentBook.Book_Quantity_Onhand
            bookInDB.IsActive = currentBook.IsActive
            ObjectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave)
            ObjectContext.Refresh(RefreshMode.StoreWins, bookInDB)
        End Try

    End Sub

    <RequiresRole("Manager")>
    Public Function UpdateAuthorsOfBook(book_id As Integer, author_ids As List(Of Integer)) As Integer
        Dim retVal = 0
        Me.ObjectContext.Delete_Authors_Of_Book(book_id)
        For Each author_id In author_ids
            Me.ObjectContext.Insert_Author_Of_Book(book_id, author_id)
            retVal += 1
        Next
        Return retVal
    End Function

    <RequiresRole("Manager")>
    Public Function UpdateCategoriesOfBook(book_id As Integer, category_ids As List(Of Integer)) As Integer
        Dim retVal = 0
        Me.ObjectContext.Delete_Categories_Of_Book(book_id)
        For Each category_id In category_ids
            Me.ObjectContext.Insert_Category_Of_Book(book_id, category_id)
            retVal += 1
        Next
        Return retVal
    End Function

    <RequiresRole("Manager")>
    Public Function UpdateShelvesOfBook(book_id As Integer, shelf_ids As List(Of Integer)) As Integer
        Dim retVal = 0
        Me.ObjectContext.Delete_Shelves_Of_Book(book_id)
        For Each shelf_id In shelf_ids
            Me.ObjectContext.Insert_Shelf_Of_Book(book_id, shelf_id)
            retVal += 1
        Next
        Return retVal
    End Function

    <RequiresRole("Manager")>
    Public Sub DeleteBook(ByVal book As Book)
        'If ((book.EntityState = EntityState.Detached) _
        '            = False) Then
        '    Me.ObjectContext.ObjectStateManager.ChangeObjectState(book, EntityState.Deleted)
        'Else
        '    Me.ObjectContext.Books.Attach(book)
        '    Me.ObjectContext.Books.DeleteObject(book)
        'End If
        Me.ObjectContext.Delete_Authors_Of_Book(book.Book_id)
        Me.ObjectContext.Delete_Categories_Of_Book(book.Book_id)
        Me.ObjectContext.Delete_Shelves_Of_Book(book.Book_id)
        book.IsActive = False
        UpdateBook(book)
    End Sub

    <RequiresRole("Manager")>
    Public Function DeleteAuthorsOfBook(book_id As Integer) As Integer
        Me.ObjectContext.Delete_Authors_Of_Book(book_id)
        Return 1
    End Function

    <RequiresRole("Manager")>
    Public Function DeleteCategoriesOfBook(book_id As Integer) As Integer
        Me.ObjectContext.Delete_Categories_Of_Book(book_id)
        Return 1
    End Function

    <RequiresRole("Manager")>
    Public Function DeleteShelvesOfBook(book_id As Integer) As Integer
        Me.ObjectContext.Delete_Shelves_Of_Book(book_id)
        Return 1
    End Function

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Categories' query.
    Private Function _getAllCategories() As IQueryable(Of Category)
        Return Me.ObjectContext.Categories
    End Function

    Private Function _getCategoriesByActive(ByVal active As Boolean) As IQueryable(Of Category)
        Return Me.ObjectContext.Categories.Where(Function(c) c.IsActive = active)
    End Function

    Private Function _orderCategories(query As IQueryable(Of Category)) As IQueryable(Of Category)
        Return query.OrderBy(Function(c) c.Category_Name)
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveCategories() As IQueryable(Of Category)
        Return _orderCategories(_getCategoriesByActive(True))
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllCategories() As IQueryable(Of Category)
        Return _orderCategories(_getAllCategories())
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveCategories_By_Category_Name(ByVal keywords As String) As IQueryable(Of Category)
        Return _orderCategories(_getCategoriesByActive(True).Where(Function(c) c.Category_Name.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetCategories_By_Category_Name(ByVal keywords As String) As IQueryable(Of Category)
        Return _orderCategories(_getAllCategories().Where(Function(c) c.Category_Name.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetCategories_Of_Book(ByVal book_id As Integer) As IQueryable(Of Category)
        'Return _orderCategories(_getAllCategories().Where(Function(c) c.Books.Any(Function(b) b.Book_id = book_id)))
        Return _orderCategories(_getCategoriesByActive(True).Where(Function(c) c.Books.Any(Function(b) b.Book_id = book_id)))
    End Function

    <RequiresRole("Manager")>
    Public Sub InsertCategory(ByVal category As Category)
        If ((category.EntityState = EntityState.Detached) _
                    = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(category, EntityState.Added)
        Else
            Me.ObjectContext.Categories.AddObject(category)
        End If
    End Sub

    <RequiresRole("Manager")>
    Public Sub UpdateCategory(ByVal currentCategory As Category)
        Me.ObjectContext.Categories.AttachAsModified(currentCategory, Me.ChangeSet.GetOriginal(currentCategory))
    End Sub

    <RequiresRole("Manager")>
    Public Sub DeleteCategory(ByVal category As Category)
        'If ((category.EntityState = EntityState.Detached) _
        '            = False) Then
        '    Me.ObjectContext.ObjectStateManager.ChangeObjectState(category, EntityState.Deleted)
        'Else
        '    Me.ObjectContext.Categories.Attach(category)
        '    Me.ObjectContext.Categories.DeleteObject(category)
        'End If
        Me.ObjectContext.Delete_Category_Books(category.Category_id)
        category.IsActive = False
        UpdateCategory(category)
    End Sub

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Members' query.
    Private Function _getAllMembers() As IQueryable(Of Member)
        Return Me.ObjectContext.Members
    End Function

    ' Austin Jung : Should check from the 
    Private Function _getMembersByActiveStatus(ByVal active As Boolean) As IQueryable(Of Member)
        Dim query = From m In Me.ObjectContext.Members
                    Where m.MemberActive = active _
                      And (m.MemberDeactivatedFrom Is Nothing _
                         OrElse m.MemberDeactivatedFrom > DateTime.Today)
                    Select m
        Return query
        'Return Me.ObjectContext.Members.Where(Function(m) m.MemberActive = active)
    End Function

    Private Function _orderMembers(query As IQueryable(Of Member)) As IQueryable(Of Member)
        Return query.OrderBy(Function(m) m.FullName)
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member() As IQueryable(Of Member)
        Dim query = From m In Me.ObjectContext.Members _
                    Where (m.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Select m
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMembers() As IQueryable(Of Member)
        Return _orderMembers(_getAllMembers())
    End Function

    <RequiresAuthentication()>
    Public Function GetMember_Of_Request(ByVal request_id As Integer) As IQueryable(Of Member)
        Dim query = From m In Me.ObjectContext.Members _
                    Join r In Me.ObjectContext.Member_Requests _
                    On m.MemberID Equals r.MemberID _
                    Where r.Request_id = request_id
                    Select m
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function GetMember_Of_Review(ByVal review_id As Integer) As IQueryable(Of Member)
        Dim query = From m In Me.ObjectContext.Members _
                    Join r In Me.ObjectContext.Member_Reviews _
                    On m.MemberID Equals r.MemberID _
                    Where r.Review_id = review_id
                    Select m
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function GetMember_Of_Transaction(ByVal transaction_id As Integer) As IQueryable(Of Member)
        Dim query = From m In Me.ObjectContext.Members _
                    Join t In Me.ObjectContext.Member_Transactions _
                    On m.MemberID Equals t.MemberID _
                    Where t.Transaction_id = transaction_id
                    Select m
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function GetMember_Of_NewBookRequest(ByVal newBookRequestID As Integer) As IQueryable(Of Member)
        Dim query = From m In Me.ObjectContext.Members _
                    Join r In Me.ObjectContext.NewBookRequests _
                    On m.MemberID Equals r.MemberID _
                    Where r.NewBookRequestID = newBookRequestID
                    Select m
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetMembers_By_Active_Status(ByVal active As Boolean) As IQueryable(Of Member)
        Return _orderMembers(_getMembersByActiveStatus(active))
    End Function

    <RequiresRole("Manager")>
    Public Function GetMembers_By_Login(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getAllMembers().Where(Function(m) m.MemberLogin.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetActiveMembers_By_Login(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getMembersByActiveStatus(True).Where(Function(m) m.MemberLogin.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetMembers_By_FullName(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getAllMembers().Where(Function(m) m.FullName.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetActiveMembers_By_FullName(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getMembersByActiveStatus(True).Where(Function(m) m.FullName.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetMembers_By_Library_PIN(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getAllMembers().Where(Function(m) m.Library_PIN.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetActiveMembers_By_Library_PIN(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getMembersByActiveStatus(True).Where(Function(m) m.Library_PIN.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetMembers_By_Role(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getAllMembers().Where(Function(m) m.Roles.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetActiveMembers_By_Role(ByVal keyword As String) As IQueryable(Of Member)
        Return _orderMembers(_getMembersByActiveStatus(True).Where(Function(m) m.Roles.Contains(keyword)))
    End Function

    <RequiresRole("Manager")>
    Public Sub InsertMember(ByVal member As Member)
        If ((member.EntityState = EntityState.Detached) _
                    = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(member, EntityState.Added)
        Else
            Me.ObjectContext.Members.AddObject(member)
        End If
    End Sub

    <RequiresAuthentication()>
    Public Sub UpdateMember(ByVal currentMember As Member)
        Me.ObjectContext.Members.AttachAsModified(currentMember, Me.ChangeSet.GetOriginal(currentMember))
    End Sub

    <RequiresRole("Manager")>
    Public Sub DeleteMember(ByVal member As Member)
        'If ((member.EntityState = EntityState.Detached) _
        '            = False) Then
        '    Me.ObjectContext.ObjectStateManager.ChangeObjectState(member, EntityState.Deleted)
        'Else
        '    Me.ObjectContext.Members.Attach(member)
        '    Me.ObjectContext.Members.DeleteObject(member)
        'End If
        member.MemberActive = False
        UpdateMember(member)
    End Sub

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Member_Requests' query.
    Private Function _getAllMembers_Requests() As IQueryable(Of Member_Requests)
        Return Me.ObjectContext.Member_Requests
    End Function

    Private Function _getAllMembers_Pending_Requests() As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                Where mr.Request_Status = False
                Select mr
        Return query
    End Function

    Private Function _get_Requests_By_Member(ByVal member_id As Integer) As IQueryable(Of Member_Requests)
        Return Me.ObjectContext.Member_Requests.Where(Function(mr) mr.MemberID = member_id)
    End Function

    Private Function _get_Pending_Requests_By_Member(ByVal member_id As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                Where mr.MemberID = member_id And mr.Request_Status = False
                Select mr
        Return query
    End Function

    Private Function _get_Requests_By_Book(ByVal book_id As Integer) As IQueryable(Of Member_Requests)
        Return Me.ObjectContext.Member_Requests.Where(Function(mr) mr.Book_id = book_id)
    End Function

    Private Function _get_Pending_Requests_By_Book(ByVal book_id As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                Where mr.Book_id = book_id And mr.Request_Status = False
                Select mr
        Return query
    End Function

    Private Function _orderMembers_Requests(query As IQueryable(Of Member_Requests)) As IQueryable(Of Member_Requests)
        Return query.OrderBy(Function(mr) mr.Expected_Date)
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Requests(ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = From mr In Me.ObjectContext.Member_Requests
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mr.Request_Status Descending, mr.Expected_Date Ascending
                    Select mr
                    Take top
        Else
            query = From mr In Me.ObjectContext.Member_Requests
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mr.Request_Status Descending, mr.Expected_Date Ascending
                    Select mr
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Pending_Requests(ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = From mr In Me.ObjectContext.Member_Requests
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
                           And mr.Request_Status = False)
                    Order By mr.Expected_Date Ascending, mr.Comitted_Date Ascending, mr.Request_id Descending
                    Select mr
                    Take top
        Else
            query = From mr In Me.ObjectContext.Member_Requests
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
                           And mr.Request_Status = False)
                    Order By mr.Expected_Date Ascending, mr.Comitted_Date Ascending, mr.Request_id Descending
                    Select mr
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Member_Pending_Requests_On_Book(ByVal member_id As Integer, ByVal book_id As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                    Where (mr.MemberID = member_id _
                           And mr.Request_Status = False _
                           And mr.Book_id = book_id)
                    Order By mr.Expected_Date Ascending, mr.Comitted_Date Ascending, mr.Request_id Descending
                    Select mr
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Pending_Requests_On_Book(ByVal book_id As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
                           And mr.Request_Status = False _
                           And mr.Book_id = book_id)
                    Order By mr.Expected_Date Ascending, mr.Comitted_Date Ascending, mr.Request_id Descending
                    Select mr
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMember_Requests(ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = _orderMembers_Requests(_getAllMembers_Requests()).Take(top)
        Else
            query = _orderMembers_Requests(_getAllMembers_Requests())
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMember_Pending_Requests(ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = _orderMembers_Requests(_getAllMembers_Pending_Requests()).Take(top)
        Else
            query = _orderMembers_Requests(_getAllMembers_Pending_Requests())
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMember_Pending_Requests_On_Book(ByVal book_id As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                    Where (mr.Request_Status = False _
                           And mr.Book_id = book_id)
                    Order By mr.Expected_Date Descending, mr.Comitted_Date Ascending, mr.Request_id Descending
                    Select mr
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Requests_By_Member(ByVal member_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = _orderMembers_Requests(_get_Requests_By_Member(member_id)).Take(top)
        Else
            query = _orderMembers_Requests(_get_Requests_By_Member(member_id))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Pending_Requests_By_Member(ByVal member_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = _orderMembers_Requests(_get_Pending_Requests_By_Member(member_id)).Take(top)
        Else
            query = _orderMembers_Requests(_get_Pending_Requests_By_Member(member_id))
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Requests_By_Book(ByVal book_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = _orderMembers_Requests(_get_Requests_By_Book(book_id)).Take(top)
        Else
            query = _orderMembers_Requests(_get_Requests_By_Book(book_id))
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Top_Reviews_Books(ByVal top As Integer) As IQueryable(Of Books_By_Reviews)
        Return Me.ObjectContext.Books_By_Reviews.OrderByDescending(Function(b) b.Average_Rate).Take(top)
    End Function

    <RequiresAuthentication()>
    Public Function Get_Top_Requests_Books(ByVal top As Integer) As IQueryable(Of Books_By_Requests)
        Return Me.ObjectContext.Books_By_Requests.OrderByDescending(Function(b) b.Request_Count).Take(top)
    End Function
    '<RequiresAuthentication()>
    'Public Function Get_Top_Requests_Books(ByVal top As Integer) As IQueryable(Of Book)
    '    Dim query = From b In Me.ObjectContext.Books _
    '                Join r In Me.Get_Top_Requests_Book_Ids(top) _
    '                On b.Book_id Equals r.Book_id _
    '                Select b

    '    Return query
    'End Function

    '<RequiresAuthentication()>
    'Public Function Get_Top_Requests_Book_Ids(ByVal top As Integer) As IEnumerable(Of Book_Rank_By_Request)

    '    Dim query As IEnumerable(Of Book_Rank_By_Request)
    '    If top > 0 Then
    '        query = From r In Me.ObjectContext.Member_Requests _
    '                Group r By r.Book_id Into g = Group _
    '                Order By g.Count() _
    '                Select New Book_Rank_By_Request _
    '                       With {.Book_id = g.First().Book_id, .Count = g.Count()}

    '    Else
    '        query = From r In Me.ObjectContext.Member_Requests _
    '                Group r By r.Book_id Into g = Group _
    '                Order By g.Count() _
    '                Select New Book_Rank_By_Request _
    '                       With {.Book_id = g.First().Book_id, .Count = g.Count()} _
    '                Take (top)
    '    End If
    '    Return query

    'End Function

    '<RequiresAuthentication()>
    'Public Function Get_Book_Rank_By_Request(ByVal top As Integer) As IQueryable(Of Book_Rank_By_Request)

    '    Dim query As IQueryable(Of Book_Rank_By_Request)
    '    If top > 0 Then
    '        query = From r In Me.ObjectContext.Member_Requests _
    '                Group r By r.Book_id Into g = Group _
    '                Order By g.Count() _
    '                Select New Book_Rank_By_Request _
    '                       With {.Book_id = g.First().Book_id, .Count = g.Count()}

    '    Else
    '        query = From r In Me.ObjectContext.Member_Requests _
    '                Group r By r.Book_id Into g = Group _
    '                Order By g.Count() _
    '                Select New Book_Rank_By_Request _
    '                       With {.Book_id = g.First().Book_id, .Count = g.Count()} _
    '                Take (top)
    '    End If
    '    Return query

    'End Function

    <RequiresRole("Manager")>
    Public Function Get_Pending_Requests_By_Book(ByVal book_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        If top > 0 Then
            query = _orderMembers_Requests(_get_Pending_Requests_By_Book(book_id)).Take(top)
        Else
            query = _orderMembers_Requests(_get_Pending_Requests_By_Book(book_id))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Member_Requests_Expected_Today() As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                Where (mr.Expected_Date <= DateTime.Today _
                        And mr.Request_Status = False)
                Order By mr.Expected_Date Ascending
                Select mr
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Member_Requests_Comitted_Today() As IQueryable(Of Member_Requests)
        Dim query As IQueryable(Of Member_Requests)
        query = From mr In Me.ObjectContext.Member_Requests
                Where (mr.Comitted_Date <= DateTime.Today _
                        And mr.Request_Status = False)
                Order By mr.Comitted_Date Ascending
                Select mr
        Return query
    End Function

    <RequiresAuthentication()>
    Public Sub InsertMember_Requests_For_Oneself(ByVal member_Requests As Member_Requests)
        Dim requestMember = From m In Me.ObjectContext.Members
                            Where m.MemberID = member_Requests.MemberID
                            Select m
        Dim memberLogin = requestMember.FirstOrDefault.MemberLogin

        If Me.ServiceContext.User.Identity.Name = memberLogin Then
            If ((member_Requests.EntityState = EntityState.Detached) _
                        = False) Then
                Me.ObjectContext.ObjectStateManager.ChangeObjectState(member_Requests, EntityState.Added)
            Else
                Me.ObjectContext.Member_Requests.AddObject(member_Requests)
            End If
        End If
    End Sub

    '<RequiresRole("Manager")>
    'Public Sub UpdateMember_Requests(ByVal currentMember_Requests As Member_Requests)
    '    Me.ObjectContext.Member_Requests.AttachAsModified(currentMember_Requests, Me.ChangeSet.GetOriginal(currentMember_Requests))
    'End Sub

    <RequiresAuthentication()>
    Public Sub UpdateMember_Requests(ByVal currentMember_Requests As Member_Requests)
        If Me.ServiceContext.User.IsInRole("Manager") Then
            Me.ObjectContext.Member_Requests.AttachAsModified(currentMember_Requests, Me.ChangeSet.GetOriginal(currentMember_Requests))
        Else
            Dim requestMember = From m In Me.ObjectContext.Members
                                Where m.MemberID = currentMember_Requests.MemberID
                                Select m
            Dim memberLogin = requestMember.FirstOrDefault.MemberLogin
            If Me.ServiceContext.User.Identity.Name = memberLogin Then
                Me.ObjectContext.Member_Requests.AttachAsModified(currentMember_Requests, Me.ChangeSet.GetOriginal(currentMember_Requests))
            End If
        End If
    End Sub

    <RequiresAuthentication()>
    Public Sub DeleteMember_Requests_For_Oneself(ByVal member_Requests As Member_Requests)
        Dim requestMember = From m In Me.ObjectContext.Members
                            Where m.MemberID = member_Requests.MemberID
                            Select m
        Dim memberLogin = requestMember.FirstOrDefault.MemberLogin

        If Me.ServiceContext.User.Identity.Name = memberLogin Then
            If ((member_Requests.EntityState = EntityState.Detached) _
                        = False) Then
                Me.ObjectContext.ObjectStateManager.ChangeObjectState(member_Requests, EntityState.Deleted)
            Else
                Me.ObjectContext.Member_Requests.Attach(member_Requests)
                Me.ObjectContext.Member_Requests.DeleteObject(member_Requests)
            End If
        End If
    End Sub

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Member_Reviews' query.
    Private Function _getAllMembers_Reviews() As IQueryable(Of Member_Reviews)
        Return Me.ObjectContext.Member_Reviews
    End Function

    Private Function _get_Reviews_By_Member(ByVal member_id As Integer) As IQueryable(Of Member_Reviews)
        Return Me.ObjectContext.Member_Reviews.Where(Function(mr) mr.MemberID = member_id)
    End Function

    Private Function _get_Reviews_By_Book(ByVal book_id As Integer) As IQueryable(Of Member_Reviews)
        Return Me.ObjectContext.Member_Reviews.Where(Function(mr) mr.Book_id = book_id)
    End Function

    Private Function _orderMembers_Reviews(query As IQueryable(Of Member_Reviews)) As IQueryable(Of Member_Reviews)
        Return query.OrderByDescending(Function(mr) mr.Review_Date)
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Reviews(ByVal top As Integer) As IQueryable(Of Member_Reviews)
        Dim query As IQueryable(Of Member_Reviews)
        If top > 0 Then
            query = From mr In Me.ObjectContext.Member_Reviews
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mr.Review_id Descending
                    Select mr
                    Take top
        Else
            query = From mr In Me.ObjectContext.Member_Reviews
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mr.Review_id Descending
                    Select mr
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function GetAllMember_Reviews(ByVal top As Integer) As IQueryable(Of Member_Reviews)
        If top > 0 Then
            Return _orderMembers_Reviews(_getAllMembers_Reviews()).Take(top)
        Else
            Return _orderMembers_Reviews(_getAllMembers_Reviews())
        End If
    End Function

    <RequiresAuthentication()>
    Public Function Get_Reviews_By_Member(ByVal member_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Reviews)
        If top > 0 Then
            Return _orderMembers_Reviews(_get_Reviews_By_Member(member_id)).Take(top)
        Else
            Return _orderMembers_Reviews(_get_Reviews_By_Member(member_id))
        End If
    End Function

    <RequiresAuthentication()>
    Public Function Get_Reviews_By_Book(ByVal book_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Reviews)
        If top > 0 Then
            Return _orderMembers_Reviews(_get_Reviews_By_Book(book_id)).Take(top)
        Else
            Return _orderMembers_Reviews(_get_Reviews_By_Book(book_id))
        End If
    End Function

    <RequiresAuthentication()>
    Public Sub InsertMember_Reviews_For_Oneself(ByVal member_Reviews As Member_Reviews)
        Dim requestMember = From m In Me.ObjectContext.Members
                            Where m.MemberID = member_Reviews.MemberID
                            Select m
        Dim memberLogin = requestMember.FirstOrDefault.MemberLogin

        If Me.ServiceContext.User.Identity.Name = memberLogin Then
            If ((member_Reviews.EntityState = EntityState.Detached) _
                            = False) Then
                Me.ObjectContext.ObjectStateManager.ChangeObjectState(member_Reviews, EntityState.Added)
            Else
                Me.ObjectContext.Member_Reviews.AddObject(member_Reviews)
            End If
        End If
    End Sub

    <RequiresAuthentication()>
    Public Sub UpdateMember_Reviews(ByVal currentMember_Reviews As Member_Reviews)
        If Me.ServiceContext.User.Identity.Name = currentMember_Reviews.Member.MemberLogin Then
            Me.ObjectContext.Member_Reviews.AttachAsModified(currentMember_Reviews, Me.ChangeSet.GetOriginal(currentMember_Reviews))
        End If
    End Sub

    <RequiresAuthentication()>
    Public Sub DeleteMember_Reviews(ByVal member_Reviews As Member_Reviews)
        Dim requestMember = From m In Me.ObjectContext.Members
                            Where m.MemberID = member_Reviews.MemberID
                            Select m
        Dim memberLogin = requestMember.FirstOrDefault.MemberLogin

        If Me.ServiceContext.User.Identity.Name = memberLogin Then
            If ((member_Reviews.EntityState = EntityState.Detached) _
                        = False) Then
                Me.ObjectContext.ObjectStateManager.ChangeObjectState(member_Reviews, EntityState.Deleted)
            Else
                Me.ObjectContext.Member_Reviews.Attach(member_Reviews)
                Me.ObjectContext.Member_Reviews.DeleteObject(member_Reviews)
            End If
        End If
    End Sub

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Member_Transactions' query.
    Private Function _getAllMembers_Transactions() As IQueryable(Of Member_Transactions)
        Return Me.ObjectContext.Member_Transactions
    End Function

    Private Function _getAllMembers_Unreturned_Excluding_OverDue_Transactions() As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where mt.Issued_Date IsNot Nothing _
        '          And mt.Expected_Return IsNot Nothing _
        '          And mt.Expected_Return >= DateTime.Today _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where mt.Expected_Return >= DateTime.Today _
                  And mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _getAllMembers_Unreturned_Transactions() As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where mt.Issued_Date IsNot Nothing _
        '          And mt.Expected_Return IsNot Nothing _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _getAllMembers_Overdue_Transactions(ByVal overdueDays As Integer) As IQueryable(Of Member_Transactions)
        Dim overdueDate As DateTime = DateTime.Today.AddDays(0 - overdueDays)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where Not mt.Issued_Date IsNot Nothing _
        '          And mt.Expected_Return IsNot Nothing _
        '          And mt.Expected_Return < overdueDate _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where mt.Expected_Return < overdueDate _
                  And mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _getAllMembers_Overdue_Transaction_History(ByVal overdueDays As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where mt.Expected_Return IsNot Nothing _
        '          And mt.Returned_Date IsNot Nothing _
        '          And CType(mt.Expected_Return, DateTime).AddDays(overdueDays) < mt.Returned_Date _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where CType(mt.Expected_Return, DateTime).AddDays(overdueDays) < mt.Returned_Date _
                  And mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _get_Transactions_By_Member(ByVal member_id As Integer) As IQueryable(Of Member_Transactions)
        Return Me.ObjectContext.Member_Transactions.Where(Function(mt) mt.MemberID = member_id)
    End Function

    Private Function _get_Unreturned_Transactions_By_Member(ByVal member_id As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where mt.MemberID = member_id _
        '          And mt.Issued_Date IsNot Nothing _
        '          And mt.Expected_Return IsNot Nothing _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where mt.MemberID = member_id _
                  And mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _get_Overdue_Transactions_By_Member(ByVal member_id As Integer, ByVal overdueDays As Integer) As IQueryable(Of Member_Transactions)
        Dim overdueDate As DateTime = DateTime.Today.AddDays(0 - overdueDays)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where mt.MemberID = member_id _
        '          And mt.Issued_Date IsNot Nothing _
        '          And mt.Expected_Return IsNot Nothing _
        '          And mt.Expected_Return < overdueDate _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where mt.MemberID = member_id _
                  And mt.Expected_Return < overdueDate _
                  And mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _get_Transactions_By_Book(ByVal book_id As Integer) As IQueryable(Of Member_Transactions)
        Return Me.ObjectContext.Member_Transactions.Where(Function(mt) mt.Book_id = book_id)
    End Function

    Private Function _get_Unreturned_Transactions_By_Book(ByVal book_id As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where mt.Book_id = book_id _
        '          And mt.Issued_Date IsNot Nothing _
        '          And mt.Expected_Return IsNot Nothing _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where mt.Book_id = book_id _
                  And mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _get_Overdue_Transactions_By_Book(ByVal book_id As Integer, ByVal overdueDays As Integer) As IQueryable(Of Member_Transactions)
        Dim overdueDate As DateTime = DateTime.Today.AddDays(0 - overdueDays)
        Dim query As IQueryable(Of Member_Transactions)
        'query = From mt In Me.ObjectContext.Member_Transactions
        '        Where mt.Book_id = book_id _
        '          And mt.Issued_Date IsNot Nothing _
        '          And mt.Expected_Return IsNot Nothing _
        '          And mt.Expected_Return < overdueDate _
        '          And mt.Returned_Date Is Nothing
        '        Select mt
        query = From mt In Me.ObjectContext.Member_Transactions
                Where mt.Book_id = book_id _
                  And mt.Expected_Return < overdueDate _
                  And mt.Returned_Date Is Nothing
                Select mt
        Return query
    End Function

    Private Function _orderMembers_Transactions(query As IQueryable(Of Member_Transactions)) As IQueryable(Of Member_Transactions)
        Return query.OrderByDescending(Function(mt) mt.Transaction_id)
    End Function

    Private Function _orderMembers_Transactions_By_Expected_Return(query As IQueryable(Of Member_Transactions)) As IQueryable(Of Member_Transactions)
        Return query.OrderBy(Function(mt) mt.Expected_Return)
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Returned_Transactions(ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            'query = From mt In Me.ObjectContext.Member_Transactions
            '        Where mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
            '           And mt.Issued_Date IsNot Nothing _
            '           And mt.Expected_Return IsNot Nothing _
            '           And mt.Returned_Date IsNot Nothing
            '        Order By mt.Transaction_id Descending
            '        Select mt
            '        Take (top)
            query = From mt In Me.ObjectContext.Member_Transactions
                    Where mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
                       And mt.Returned_Date IsNot Nothing
                    Order By mt.Transaction_id Descending
                    Select mt
                    Take (top)
        Else
            query = From mt In Me.ObjectContext.Member_Transactions
                    Where mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
                       And mt.Returned_Date IsNot Nothing
                    Order By mt.Transaction_id Descending
                    Select mt
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Returned_Transactions_Detail(ByVal top As Integer) As IQueryable(Of Transactions_Detail)
        Dim query As IQueryable(Of Transactions_Detail)
        If top > 0 Then
            query = From mt In Me.ObjectContext.Transactions_Detail
                    Where mt.MemberLogin = Me.ServiceContext.User.Identity.Name _
                       And mt.Issued_Date IsNot Nothing _
                       And mt.Returned_Date IsNot Nothing
                    Order By mt.Transaction_id Descending
                    Select mt
                    Take (top)
        Else
            query = From mt In Me.ObjectContext.Transactions_Detail
                    Where mt.MemberLogin = Me.ServiceContext.User.Identity.Name _
                       And mt.Issued_Date IsNot Nothing _
                       And mt.Returned_Date IsNot Nothing
                    Order By mt.Transaction_id Descending
                    Select mt
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_All_Transactions(ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = From mt In Me.ObjectContext.Member_Transactions
                    Where (mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mt.Transaction_id Descending
                    Select mt
                    Take top
        Else
            query = From mt In Me.ObjectContext.Member_Transactions
                    Where (mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mt.Transaction_id Descending
                    Select mt
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Unreturned_Transactions(ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            'query = From mt In Me.ObjectContext.Member_Transactions
            '        Where mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
            '          And mt.Issued_Date IsNot Nothing _
            '          And mt.Expected_Return IsNot Nothing _
            '          And mt.Returned_Date Is Nothing
            '        Order By mt.Expected_Return Ascending
            '        Select mt
            '        Take top
            query = From mt In Me.ObjectContext.Member_Transactions
                    Where mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
                      And mt.Returned_Date Is Nothing
                    Order By mt.Expected_Return Ascending
                    Select mt
                    Take top
        Else
            'query = From mt In Me.ObjectContext.Member_Transactions
            '        Where mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
            '          And mt.Issued_Date IsNot Nothing _
            '          And mt.Expected_Return IsNot Nothing _
            '          And mt.Returned_Date Is Nothing
            '        Order By mt.Expected_Return Ascending
            '        Select mt
            query = From mt In Me.ObjectContext.Member_Transactions
                    Where mt.Member.MemberLogin = Me.ServiceContext.User.Identity.Name _
                      And mt.Returned_Date Is Nothing
                    Order By mt.Expected_Return Ascending
                    Select mt
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMember_Transactions(ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions(_getAllMembers_Transactions()).Take(top)
        Else
            query = _orderMembers_Transactions(_getAllMembers_Transactions())
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMember_Unreturned_Excluding_OverDue_Transactions(ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions_By_Expected_Return(_getAllMembers_Unreturned_Excluding_OverDue_Transactions()).Take(top)
        Else
            query = _orderMembers_Transactions_By_Expected_Return(_getAllMembers_Unreturned_Transactions())
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMember_Unreturned_Transactions(ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions_By_Expected_Return(_getAllMembers_Unreturned_Transactions()).Take(top)
        Else
            query = _orderMembers_Transactions_By_Expected_Return(_getAllMembers_Unreturned_Transactions())
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMembers_Overdue_Transactions(ByVal overdueDays As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions_By_Expected_Return(_getAllMembers_Overdue_Transactions(overdueDays)).Take(top)
        Else
            query = _orderMembers_Transactions_By_Expected_Return(_getAllMembers_Overdue_Transactions(overdueDays))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllMembers_Overdue_Transaction_History(ByVal overdueDays As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions(_getAllMembers_Overdue_Transaction_History(overdueDays)).Take(top)
        Else
            query = _orderMembers_Transactions(_getAllMembers_Overdue_Transaction_History(overdueDays))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Transactions_By_Member(ByVal member_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions(_get_Transactions_By_Member(member_id)).Take(top)
        Else
            query = _orderMembers_Transactions(_get_Transactions_By_Member(member_id))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Unreturned_Transactions_By_Member(ByVal member_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions_By_Expected_Return(_get_Unreturned_Transactions_By_Member(member_id)).Take(top)
        Else
            query = _orderMembers_Transactions_By_Expected_Return(_get_Unreturned_Transactions_By_Member(member_id))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Overdue_Transactions_By_Member(ByVal member_id As Integer, ByVal overdueDays As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions_By_Expected_Return(_get_Overdue_Transactions_By_Member(member_id, overdueDays)).Take(top)
        Else
            query = _orderMembers_Transactions_By_Expected_Return(_get_Overdue_Transactions_By_Member(member_id, overdueDays))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Transactions_By_Book(ByVal book_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions(_get_Transactions_By_Book(book_id)).Take(top)
        Else
            query = _orderMembers_Transactions(_get_Transactions_By_Book(book_id))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Unreturned_Transactions_By_Book(ByVal book_id As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions_By_Expected_Return(_get_Unreturned_Transactions_By_Book(book_id)).Take(top)
        Else
            query = _orderMembers_Transactions_By_Expected_Return(_get_Unreturned_Transactions_By_Book(book_id))
        End If
        Return query
    End Function

    <RequiresRole("Manager")>
    Public Function Get_Overdue_Transactions_By_Book(ByVal book_id As Integer, ByVal overdueDays As Integer, ByVal top As Integer) As IQueryable(Of Member_Transactions)
        Dim query As IQueryable(Of Member_Transactions)
        If top > 0 Then
            query = _orderMembers_Transactions_By_Expected_Return(_get_Overdue_Transactions_By_Book(book_id, overdueDays)).Take(top)
        Else
            query = _orderMembers_Transactions_By_Expected_Return(_get_Overdue_Transactions_By_Book(book_id, overdueDays))
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Sub InsertMember_Transactions(ByVal member_Transactions As Member_Transactions)
        Dim requestMember = From m In Me.ObjectContext.Members
                            Where m.MemberID = member_Transactions.MemberID
                            Select m
        Dim memberLogin = requestMember.FirstOrDefault.MemberLogin

        If Me.ServiceContext.User.Identity.Name = memberLogin Then
            If ((member_Transactions.EntityState = EntityState.Detached) _
                            = False) Then
                Me.ObjectContext.ObjectStateManager.ChangeObjectState(member_Transactions, EntityState.Added)
            Else
                Me.ObjectContext.Member_Transactions.AddObject(member_Transactions)
            End If
        End If
    End Sub

    <RequiresAuthentication()>
    Public Sub UpdateMember_Transactions(ByVal currentMember_Transactions As Member_Transactions)
        If Me.ServiceContext.User.IsInRole("Manager") Then
            Me.ObjectContext.Member_Transactions.AttachAsModified(currentMember_Transactions, Me.ChangeSet.GetOriginal(currentMember_Transactions))
        Else
            Dim requestMember = From m In Me.ObjectContext.Members
                                Where m.MemberID = currentMember_Transactions.MemberID
                                Select m
            Dim memberLogin = requestMember.FirstOrDefault.MemberLogin

            If Me.ServiceContext.User.Identity.Name = memberLogin Then
                Me.ObjectContext.Member_Transactions.AttachAsModified(currentMember_Transactions, Me.ChangeSet.GetOriginal(currentMember_Transactions))
            End If
        End If
    End Sub

    <RequiresAuthentication()>
    Public Sub DeleteMember_Transactions(ByVal member_Transactions As Member_Transactions)
        Dim requestMember = From m In Me.ObjectContext.Members
                            Where m.MemberID = member_Transactions.MemberID
                            Select m
        Dim memberLogin = requestMember.FirstOrDefault.MemberLogin

        If Me.ServiceContext.User.Identity.Name = memberLogin Then
            If ((member_Transactions.EntityState = EntityState.Detached) _
                        = False) Then
                Me.ObjectContext.ObjectStateManager.ChangeObjectState(member_Transactions, EntityState.Deleted)
            Else
                Me.ObjectContext.Member_Transactions.Attach(member_Transactions)
                Me.ObjectContext.Member_Transactions.DeleteObject(member_Transactions)
            End If
        End If
    End Sub

    'TODO:
    ' Consider constraining the results of your query method.  If you need additional input you can
    ' add parameters to this method or create additional query methods with different names.
    'To support paging you will need to add ordering to the 'Shelves' query.
    Private Function _getAllShelves() As IQueryable(Of Shelf)
        Return Me.ObjectContext.Shelves
    End Function

    Private Function _getShelvesByActive(ByVal active As Boolean) As IQueryable(Of Shelf)
        Return Me.ObjectContext.Shelves.Where(Function(s) s.IsActive = active)
    End Function

    Private Function _orderShelves(query As IQueryable(Of Shelf)) As IQueryable(Of Shelf)
        Return query.OrderBy(Function(s) s.Shelf_Code)
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveShelves() As IQueryable(Of Shelf)
        Return _orderShelves(_getShelvesByActive(True))
    End Function

    <RequiresRole("Manager")>
    Public Function GetAllShelves() As IQueryable(Of Shelf)
        Return _orderShelves(_getAllShelves())
    End Function

    <RequiresAuthentication()>
    Public Function GetActiveShelves_By_Shelf_Name(ByVal keywords As String) As IQueryable(Of Shelf)
        Return _orderShelves(_getShelvesByActive(True).Where(Function(s) s.Shelf_Code.Contains(keywords)))
    End Function

    <RequiresRole("Manager")>
    Public Function GetShelves_By_Shelf_Name(ByVal keywords As String) As IQueryable(Of Shelf)
        Return _orderShelves(_getAllShelves().Where(Function(s) s.Shelf_Code.Contains(keywords)))
    End Function

    <RequiresAuthentication()>
    Public Function GetShelves_Of_Book(ByVal book_id As Integer) As IQueryable(Of Shelf)
        'Return _orderShelves(_getAllShelves().Where(Function(s) s.Books.Any(Function(b) b.Book_id = book_id)))
        Return _orderShelves(_getShelvesByActive(True).Where(Function(s) s.Books.Any(Function(b) b.Book_id = book_id)))
    End Function

    <RequiresRole("Manager")>
    Public Sub InsertShelf(ByVal shelf As Shelf)
        If ((shelf.EntityState = EntityState.Detached) _
                    = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(shelf, EntityState.Added)
        Else
            Me.ObjectContext.Shelves.AddObject(shelf)
        End If
    End Sub

    <RequiresRole("Manager")>
    Public Sub UpdateShelf(ByVal currentShelf As Shelf)
        Me.ObjectContext.Shelves.AttachAsModified(currentShelf, Me.ChangeSet.GetOriginal(currentShelf))
    End Sub

    <RequiresRole("Manager")>
    Public Sub DeleteShelf(ByVal shelf As Shelf)
        'If ((shelf.EntityState = EntityState.Detached) _
        '            = False) Then
        '    Me.ObjectContext.ObjectStateManager.ChangeObjectState(shelf, EntityState.Deleted)
        'Else
        '    Me.ObjectContext.Shelves.Attach(shelf)
        '    Me.ObjectContext.Shelves.DeleteObject(shelf)
        'End If
        Me.ObjectContext.Delete_Shelf_Books(shelf.Shelf_id)
        shelf.IsActive = False
        UpdateShelf(shelf)
    End Sub


    <RequiresAuthentication()>
    Public Function Get_Current_Member_NewBookRequests(ByVal top As Integer) As IQueryable(Of NewBookRequest)
        Dim query As IQueryable(Of NewBookRequest)
        If top > 0 Then
            query = From mr In Me.ObjectContext.NewBookRequests
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mr.NewBookRequestID Descending
                    Select mr
                    Take top
        Else
            query = From mr In Me.ObjectContext.NewBookRequests
                    Where (mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name)
                    Order By mr.NewBookRequestID Descending
                    Select mr
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Function Get_Current_Member_Opened_NewBookRequests(ByVal top As Integer) As IQueryable(Of NewBookRequest)
        Dim query As IQueryable(Of NewBookRequest)
        If top > 0 Then
            query = From mr In Me.ObjectContext.NewBookRequests
                    Where ((mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name) _
                        And ((mr.RequestClosed = False) Or (mr.ReviewViewed = False))) _
                    Order By mr.NewBookRequestID
                    Select mr
                    Take top
        Else
            query = From mr In Me.ObjectContext.NewBookRequests
                    Where ((mr.Member.MemberLogin = Me.ServiceContext.User.Identity.Name) _
                        And ((mr.RequestClosed = False) Or (mr.ReviewViewed = False))) _
                    Order By mr.NewBookRequestID
                    Select mr
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    <RequiresRole("Manager")>
    Public Function Get_All_Member_NewBookRequests(ByVal top As Integer) As IQueryable(Of NewBookRequest)
        Dim query As IQueryable(Of NewBookRequest)
        If top > 0 Then
            query = From mr In Me.ObjectContext.NewBookRequests
                    Order By mr.NewBookRequestID Descending
                    Select mr
                    Take top
        Else
            query = From mr In Me.ObjectContext.NewBookRequests
                    Order By mr.NewBookRequestID Descending
                    Select mr
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    <RequiresRole("Manager")>
    Public Function Get_All_Member_Opened_NewBookRequests(ByVal top As Integer) As IQueryable(Of NewBookRequest)
        Dim query As IQueryable(Of NewBookRequest)
        If top > 0 Then
            query = From mr In Me.ObjectContext.NewBookRequests
                    Where (mr.RequestClosed = False)
                    Order By mr.NewBookRequestID
                    Select mr
                    Take top
        Else
            query = From mr In Me.ObjectContext.NewBookRequests
                    Where (mr.RequestClosed = False)
                    Order By mr.NewBookRequestID
                    Select mr
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    Public Sub InsertNewBookRequest(ByVal newBookRequest As NewBookRequest)
        If ((newBookRequest.EntityState = EntityState.Detached) _
                            = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(newBookRequest, EntityState.Added)
        Else
            Me.ObjectContext.NewBookRequests.AddObject(newBookRequest)
        End If
    End Sub

    <RequiresAuthentication()>
    Public Sub UpdateNewBookRequest(ByVal newBookRequest As NewBookRequest)
        Me.ObjectContext.NewBookRequests.AttachAsModified(newBookRequest, Me.ChangeSet.GetOriginal(newBookRequest))
    End Sub

    <RequiresAuthentication()>
    Public Sub DeleteNewBookRequest(ByVal newBookRequest As NewBookRequest)
        If ((newBookRequest.EntityState = EntityState.Detached) _
                        = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(newBookRequest, EntityState.Deleted)
        Else
            Me.ObjectContext.NewBookRequests.Attach(newBookRequest)
            Me.ObjectContext.NewBookRequests.DeleteObject(newBookRequest)
        End If
    End Sub

    <RequiresAuthentication()>
    <RequiresRole("Manager")>
    Public Function Get_Answer_Template_To_Suggestion(ByVal top As Integer) As IQueryable(Of AnswerTemplate)
        Dim query As IQueryable(Of AnswerTemplate)
        If top > 0 Then
            query = From at In Me.ObjectContext.AnswerTemplates
                    Order By at.AnswerTemplate1
                    Select at
                    Take top
        Else
            query = From at In Me.ObjectContext.AnswerTemplates
                    Order By at.AnswerTemplate1
                    Select at
        End If
        Return query
    End Function

    <RequiresAuthentication()>
    <RequiresRole("Manager")>
    Public Sub InsertNewAnswerTemplate(ByVal newAnswerTemplate As AnswerTemplate)
        If ((newAnswerTemplate.EntityState = EntityState.Detached) _
                            = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(newAnswerTemplate, EntityState.Added)
        Else
            Me.ObjectContext.AnswerTemplates.AddObject(newAnswerTemplate)
        End If
    End Sub

    <RequiresAuthentication()>
    <RequiresRole("Manager")>
    Public Sub UpdateNewAnswerTemplate(ByVal newAnswerTemplate As AnswerTemplate)
        Me.ObjectContext.AnswerTemplates.AttachAsModified(newAnswerTemplate, Me.ChangeSet.GetOriginal(newAnswerTemplate))
    End Sub

    <RequiresAuthentication()>
    <RequiresRole("Manager")>
    Public Sub DeleteNewAnswerTemplate(ByVal newAnswerTemplate As AnswerTemplate)
        If ((newAnswerTemplate.EntityState = EntityState.Detached) _
                        = False) Then
            Me.ObjectContext.ObjectStateManager.ChangeObjectState(newAnswerTemplate, EntityState.Deleted)
        Else
            Me.ObjectContext.AnswerTemplates.Attach(newAnswerTemplate)
            Me.ObjectContext.AnswerTemplates.DeleteObject(newAnswerTemplate)
        End If
    End Sub

    <Invoke()>
    <RequiresAuthentication()>
    Public Function SendMailToAdmin(ByVal fromAddress As String, ByVal subject As String, ByVal body As String) As String
        Try
            Dim msg As MailMessage = New MailMessage()
            msg.From = New MailAddress(fromAddress)
            'msg.To.Add(New MailAddress("weblibadmin@columbiacabinets.com"))
            msg.To.Add(New MailAddress("libraryadmin@columbiacabinetslibrary.com"))
            msg.Subject = subject
            msg.Body = body
            msg.IsBodyHtml = False

            'Dim smtp As SmtpClient = New SmtpClient("Colsvr03")
            'Dim smtp As SmtpClient = New SmtpClient("vps.columbiacabinets.com", 587)

            Dim smtp As SmtpClient = New SmtpClient("columbiacabinetslibrary.com")
            smtp.EnableSsl = False
            smtp.UseDefaultCredentials = False
            'smtp.Credentials = New NetworkCredential("weblibadmin@columbiacabinets.com", "qwaszx12")
            smtp.Credentials = New NetworkCredential("libraryadmin@columbiacabinetslibrary.com", "qwaszx12")

            smtp.Send(msg)
            Return (subject & " was sent to administrator successfully.")
        Catch ex As Exception
            Return "Fail to send " & subject & " to administrator." & vbNewLine & "[ErrorDetails] : " & vbNewLine & ex.Message
        End Try
    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function SendMailFromAdmin(ByVal toAddress As String, ByVal subject As String, ByVal body As String, ByVal subTitle As String) As String
        Dim emailTemplate As String = "<html>" & _
                                        "<head>" & _
                                            "<title></title>" & _
                                        "</head>" & _
                                        "<body>" & _
                                            "<img src='http://columbiacabinetslibrary.com/Upload/Icons/NetQuoteLogoIcon.PNG' border='0' style='display:block;' alt='Be Inspired' />" & _
                                            "<h2 align='center' style='color: #897563'>$OperationMessageOnDate</h2>" & _
                                            "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                "<thead>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th>$Subject</th>" & _
                                                    "</tr>" & _
                                                "</thead>" & _
                                                "<tbody>" & _
                                                    "<tr bgcolor='Cornsilk'>" & _
                                                        "<td>$Body</td>" & _
                                                    "</tr>" & _
                                                "</tbody>" & _
                                            "</table>" & _
                                        "</body>" & _
                                      "</html>"

        Try
            Dim msg As MailMessage = New MailMessage()
            'msg.From = New MailAddress("weblibadmin@columbiacabinets.com")
            msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
            msg.To.Add(New MailAddress(toAddress))
            msg.Subject = subject
            'msg.Body = body
            emailTemplate = emailTemplate.Replace("$OperationMessageOnDate", subTitle)
            emailTemplate = emailTemplate.Replace("$Subject", subject)
            Dim newBody = body.Replace(vbNewLine, "<br />")
            msg.Body = emailTemplate.Replace("$Body", newBody)
            'msg.IsBodyHtml = False
            msg.IsBodyHtml = True

            'Dim smtp As SmtpClient = New SmtpClient("Colsvr03")
            'Dim smtp As SmtpClient = New SmtpClient("vps.columbiacabinets.com", 587)
            Dim smtp As SmtpClient = New SmtpClient("columbiacabinetslibrary.com")

            smtp.EnableSsl = False
            smtp.UseDefaultCredentials = False
            'smtp.Credentials = New NetworkCredential("weblibadmin@columbiacabinets.com", "qwaszx12")
            smtp.Credentials = New NetworkCredential("libraryadmin@columbiacabinetslibrary.com", "qwaszx12")

            smtp.Send(msg)
            Return (subject & " was sent to " & toAddress & " successfully.")
        Catch ex As Exception
            Return "Fail to send " & subject & " to " & toAddress & "." & vbNewLine & "[ErrorDetails] : " & vbNewLine & ex.Message
        End Try
    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function ProcessReceiveBook(ByVal toAddress As String, ByVal book_Title As String, ByVal book_id As Integer) As String
        Dim msg As MailMessage = New MailMessage()
        Dim noDay As Date = Date.Parse("01/01/0001")
        Dim requestComitteMaxDate As Date = CalculateBusinessDaysFromInputDate(Date.Today, 5) ' Maximu day to be comitted : 5 business days
        Dim smtp As SmtpClient = Nothing

        Dim emailTemplate As String = "<html>" & _
                                        "<head>" & _
                                            "<title></title>" & _
                                        "</head>" & _
                                        "<body>" & _
                                            "<img src='http://columbiacabinetslibrary.com/Upload/Icons/NetQuoteLogoIcon.PNG' border='0' style='display:block;' alt='Be Inspired' />" & _
                                            "<h2 align='center' style='color: #897563'>$OperationMessageOnDate</h2>" & _
                                            "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                "<thead>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th>$Subject</th>" & _
                                                    "</tr>" & _
                                                "</thead>" & _
                                                "<tbody>" & _
                                                    "<tr bgcolor='Cornsilk'>" & _
                                                        "<td>$Body</td>" & _
                                                    "</tr>" & _
                                                "</tbody>" & _
                                            "</table>" & _
                                        "</body>" & _
                                      "</html>"

        Try
            'Initialize smtp client
            'smtp = New SmtpClient("vps.columbiacabinets.com", 587)
            smtp = New SmtpClient("columbiacabinetslibrary.com")
            smtp.EnableSsl = False
            smtp.UseDefaultCredentials = False
            'smtp.Credentials = New NetworkCredential("weblibadmin@columbiacabinets.com", "qwaszx12")
            smtp.Credentials = New NetworkCredential("libraryadmin@columbiacabinetslibrary.com", "qwaszx12")

            ' Send thanks email for receiving
            If Not String.IsNullOrEmpty(toAddress) Then
                'msg.From = New MailAddress("weblibadmin@columbiacabinets.com")
                msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
                msg.To.Add(New MailAddress(toAddress))
                msg.Subject = "[" & book_Title & "] Returned."
                Dim newBody = "Thanks for your returning [" & book_Title & "]."
                emailTemplate = emailTemplate.Replace("$OperationMessageOnDate", "[" & book_Title & "] is returned.")
                emailTemplate = emailTemplate.Replace("$Subject", "Returned Book")
                msg.Body = emailTemplate.Replace("$Body", newBody)
                'msg.IsBodyHtml = False
                msg.IsBodyHtml = True

                smtp.Send(msg)
            End If
        Catch ex As Exception
            Return "Fail to send " & msg.Subject & " to " & toAddress & "." & vbNewLine & "[ErrorDetails] : " & vbNewLine & ex.Message
        End Try

        Try
            ' Allocate received book to next request
            Dim nextRequest As Member_Requests = Nothing
            Dim nextWaitingRequests = From mr In Me.ObjectContext.Member_Requests
                                      Where mr.Book_id = book_id And mr.Request_Status = False _
                                       And (mr.Comitted_Date Is Nothing Or mr.Comitted_Date = noDay) _
                                       And (mr.Expected_Date <= requestComitteMaxDate)
                                      Order By mr.Request_Date Ascending, mr.Expected_Date Ascending
                                      Select mr

            'Get a waiting request of member who has unreturned book less than 3
            For Each wr As Member_Requests In nextWaitingRequests
                Dim unreturnedBooks As Integer = Count_Unreturned_Transactions_Of_Member(wr.MemberID)
                If unreturnedBooks < 3 Then
                    nextRequest = wr
                    Exit For
                End If
            Next

            If (nextRequest Is Nothing) Then
                Dim receivedBook As Book = Nothing
                receivedBook = (From b In Me.ObjectContext.Books
                                Where b.Book_id = book_id
                                Select b).FirstOrDefault

                receivedBook.Book_Quantity_Onhand += 1
                Me.ObjectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave)
            Else
                nextRequest.Comitted_Date = nextRequest.Expected_Date
                nextRequest.Comment &= Me.ServiceContext.User.Identity.Name & " : Comitted on " _
                            & CType(nextRequest.Comitted_Date, DateTime).ToString("MM/dd/yyyy") & "." & vbNewLine
                Me.ObjectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave)

                'send new comitted date email
                If Not String.IsNullOrEmpty(nextRequest.Member.Email) Then
                    emailTemplate = "<html>" & _
                                        "<head>" & _
                                            "<title></title>" & _
                                        "</head>" & _
                                        "<body>" & _
                                            "<img src='http://columbiacabinetslibrary.com/Upload/Icons/NetQuoteLogoIcon.PNG' border='0' style='display:block;' alt='Be Inspired' />" & _
                                            "<h2 align='center' style='color: #897563'>$OperationMessageOnDate</h2>" & _
                                            "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                "<thead>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th>$Subject</th>" & _
                                                    "</tr>" & _
                                                "</thead>" & _
                                                "<tbody>" & _
                                                    "<tr bgcolor='Cornsilk'>" & _
                                                        "<td>$Body</td>" & _
                                                    "</tr>" & _
                                                "</tbody>" & _
                                            "</table>" & _
                                        "</body>" & _
                                      "</html>"

                    Try
                        msg.To.Add(New MailAddress(nextRequest.Member.Email))
                        msg.Subject = "[" & book_Title & "] Comitted."
                        Dim newBody = "Thanks for your using Columbia Kitchen Cabinets' library.<br />[" & book_Title & _
                                      "] will be ready for you on " & CType(nextRequest.Comitted_Date, DateTime).ToString("MM/dd/yyyy") & "."
                        emailTemplate = emailTemplate.Replace("$Subject", "New Comitted Book")
                        msg.Body = emailTemplate.Replace("$Body", newBody)
                        'msg.IsBodyHtml = False
                        msg.IsBodyHtml = True

                        smtp.Send(msg)
                    Catch exSendMail As Exception
                        Return "Fail to send a comitted date of " & book_Title & " to " & nextRequest.Member.Email & "." & vbNewLine & "[ErrorDetails] : " & vbNewLine & exSendMail.Message
                    End Try
                End If
            End If

            Return "Receive book successfully."
        Catch ex As Exception
            Return "Fail to allocate " & book_Title & " to a waiting request." & vbNewLine & "[ErrorDetails] : " & vbNewLine & ex.Message
        End Try

    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function ProcessIssueBook(ByVal toAddress As String, ByVal book_Title As String, ByVal request_id As Integer) As String
        Dim msg As MailMessage = New MailMessage()
        Dim noDay As Date = Date.Parse("01/01/0001")
        Dim returnDueDate As Date = Date.Today.AddDays(14) ' Expected Return Date : 14 days later
        Dim issuedRequest As Member_Requests = Nothing
        Try

            issuedRequest = (From mr In Me.ObjectContext.Member_Requests
                             Where mr.Request_id = request_id
                             Select mr).FirstOrDefault

            ' create a transaction from issued request
            Dim newTransaction = New Member_Transactions() With {
                                 .MemberID = issuedRequest.MemberID,
                                 .Book_id = issuedRequest.Book_id,
                                 .Requested_Date = issuedRequest.Request_Date,
                                 .Issued_Date = Date.Today,
                                 .Expected_Return = returnDueDate,
                                 .Comment = "Issued on " & Date.Today.ToString("MM/dd/yyyy") & vbNewLine,
                                 .AdminCommented = Date.Now,
                                 .AdminCommentViewed = False,
                                 .UserCommentViewed = False
                                }

            Me.ObjectContext.Member_Transactions.AddObject(newTransaction)
            Me.ObjectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave)

            ' Send email 
            If Not String.IsNullOrEmpty(toAddress) Then
                Try
                    Dim emailTemplate As String = "<html>" & _
                                                    "<head>" & _
                                                        "<title></title>" & _
                                                    "</head>" & _
                                                    "<body>" & _
                                                        "<img src='http://columbiacabinetslibrary.com/Upload/Icons/NetQuoteLogoIcon.PNG' border='0' style='display:block;' alt='Be Inspired' />" & _
                                                        "<h2 align='center' style='color: #897563'>$OperationMessageOnDate</h2>" & _
                                                        "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                            "<thead>" & _
                                                                "<tr bgcolor='LightCyan'>" & _
                                                                    "<th>$Subject</th>" & _
                                                                "</tr>" & _
                                                            "</thead>" & _
                                                            "<tbody>" & _
                                                                "<tr bgcolor='Cornsilk'>" & _
                                                                    "<td>$Body</td>" & _
                                                                "</tr>" & _
                                                            "</tbody>" & _
                                                        "</table>" & _
                                                    "</body>" & _
                                                  "</html>"

                    'msg.From = New MailAddress("weblibadmin@columbiacabinets.com")
                    msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
                    msg.To.Add(New MailAddress(toAddress))
                    msg.Subject = "[" & book_Title & "] is checked out."
                    Dim newBody = "Thanks for your using Columbia Cabinets' library.<br />[" & book_Title & _
                                  "] is expected to be returned by " & returnDueDate.ToString("MM/dd/yyyy") & "."
                    emailTemplate = emailTemplate.Replace("$OperationMessageOnDate", "[" & book_Title & "] is checked out.")
                    emailTemplate = emailTemplate.Replace("$Subject", "Book Checked out")
                    msg.Body = emailTemplate.Replace("$Body", newBody)
                    'msg.IsBodyHtml = False
                    msg.IsBodyHtml = True

                    'Dim smtp As SmtpClient = New SmtpClient("vps.columbiacabinets.com", 587)
                    Dim smtp As SmtpClient = New SmtpClient("columbiacabinetslibrary.com")
                    smtp.EnableSsl = False
                    smtp.UseDefaultCredentials = False
                    'smtp.Credentials = New NetworkCredential("weblibadmin@columbiacabinets.com", "qwaszx12")
                    smtp.Credentials = New NetworkCredential("libraryadmin@columbiacabinetslibrary.com", "qwaszx12")

                    smtp.Send(msg)
                Catch exMail As Exception
                    Return "Fail to send " & msg.Subject & " to " & toAddress & "." & vbNewLine & "[ErrorDetails] : " & vbNewLine & exMail.Message
                End Try
            End If

            Return "Receive book successfully."
        Catch ex As Exception
            Return "Fail to issue " & msg.Subject & " to " & toAddress & "." & vbNewLine & "[ErrorDetails] : " & vbNewLine & ex.Message
        End Try

    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function ProcessExtendReturnBook(ByVal toAddress As String, ByVal bookTitle As String, ByVal newReturnDate As String) As String
        Dim emailTemplate As String = "<html>" & _
                                        "<head>" & _
                                            "<title></title>" & _
                                        "</head>" & _
                                        "<body>" & _
                                            "<img src='http://columbiacabinetslibrary.com/Upload/Icons/NetQuoteLogoIcon.PNG' border='0' style='display:block;' alt='Be Inspired' />" & _
                                            "<h2 align='center' style='color: #897563'>$OperationMessageOnDate</h2>" & _
                                            "<table width='100%' cellpadding='1' cellspacing='1' border='1'>" & _
                                                "<thead>" & _
                                                    "<tr bgcolor='LightCyan'>" & _
                                                        "<th>$Subject</th>" & _
                                                    "</tr>" & _
                                                "</thead>" & _
                                                "<tbody>" & _
                                                    "<tr bgcolor='Cornsilk'>" & _
                                                        "<td>$Body</td>" & _
                                                    "</tr>" & _
                                                "</tbody>" & _
                                            "</table>" & _
                                        "</body>" & _
                                      "</html>"

        Try
            Dim msg As MailMessage = New MailMessage()
            'msg.From = New MailAddress("weblibadmin@columbiacabinets.com")
            msg.From = New MailAddress("libraryadmin@columbiacabinetslibrary.com")
            msg.To.Add(New MailAddress(toAddress))
            msg.Subject = "[" & bookTitle & "]'s new return due date."
            Dim newBody = "[" & bookTitle & "]'s new return due date is " & newReturnDate
            emailTemplate = emailTemplate.Replace("$OperationMessageOnDate", "[" & bookTitle & "]'s new return due date.")
            emailTemplate = emailTemplate.Replace("$Subject", "New Return Due Date")
            msg.Body = emailTemplate.Replace("$Body", newBody)
            'msg.IsBodyHtml = False
            msg.IsBodyHtml = True

            'Dim smtp As SmtpClient = New SmtpClient("vps.columbiacabinets.com", 587)
            Dim smtp As SmtpClient = New SmtpClient("columbiacabinetslibrary.com")
            smtp.EnableSsl = False
            smtp.UseDefaultCredentials = False
            'smtp.Credentials = New NetworkCredential("weblibadmin@columbiacabinets.com", "qwaszx12")
            smtp.Credentials = New NetworkCredential("libraryadmin@columbiacabinetslibrary.com", "qwaszx12")

            smtp.Send(msg)
            Return (msg.Subject & " was sent to " & toAddress & " successfully.")
        Catch ex As Exception
            Return "Fail to send a new return due date to " & toAddress & "." & vbNewLine & "[ErrorDetails] : " & vbNewLine & ex.Message
        End Try
    End Function

    <Invoke()>
    Public Function GetNotificationUrl() As String
        Try
            Dim Server As HttpServerUtility = HttpContext.Current.Server
            Dim DI As DirectoryInfo = New DirectoryInfo(Server.MapPath("~/Upload"))
            If DI.Exists Then
                If File.Exists(Path.Combine(DI.FullName, "notification.pdf")) Then
                    'Return "Upload/notification.pdf?#toolbar=1"
                    Return "Upload/notification.pdf"
                End If
            End If
            Return String.Empty
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function Count_Unreturned_Transactions_Of_Member(ByVal member_id As Integer) As Integer
        Dim returnCount As Integer = 0

        Dim transactionQuery As IQueryable(Of Member_Transactions)
        'transactionQuery = From mt In Me.ObjectContext.Member_Transactions
        '                   Where mt.MemberID = member_id _
        '                    And mt.Issued_Date IsNot Nothing _
        '                    And mt.Expected_Return IsNot Nothing _
        '                    And mt.Returned_Date Is Nothing
        '                   Select mt
        transactionQuery = From mt In Me.ObjectContext.Member_Transactions
                           Where mt.MemberID = member_id _
                            And mt.Returned_Date Is Nothing
                           Select mt


        Dim noDate As Date = #12:00:00 AM#
        Dim comittedRequestQuery As IQueryable(Of Member_Requests)
        comittedRequestQuery = From mr In Me.ObjectContext.Member_Requests
                               Where mr.MemberID = member_id _
                                And mr.Request_Status = False _
                                And mr.Comitted_Date IsNot Nothing _
                                And mr.Comitted_Date > noDate _
                               Select mr

        returnCount = transactionQuery.Count() + comittedRequestQuery.Count()
        Return returnCount
    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function Allocate_Book(ByVal currentBook As Book) As Book

        'Using Concurrency control with RowVersion timestamp
        'Check Book_id
        Dim bookID As Integer = currentBook.Book_id
        Dim booksInDB = From b In ObjectContext.Books _
                        Where b.Book_id = bookID _
                        Select b

        'Check book
        If booksInDB.Count < 1 Then
            Return Nothing
        End If
        Dim bookInDB = booksInDB.FirstOrDefault()

        If bookInDB.Book_Quantity_Onhand <= 0 Then
            Return Nothing
        Else
            Try
                'Detach it first
                ObjectContext.Detach(bookInDB)
                'Update the book
                bookInDB.Book_Title = currentBook.Book_Title
                bookInDB.Book_Image_File = currentBook.Book_Image_File
                bookInDB.Book_Description = currentBook.Book_Description
                bookInDB.Book_Media = currentBook.Book_Media
                bookInDB.Book_Language = currentBook.Book_Language
                bookInDB.Book_ISBN = currentBook.Book_ISBN
                bookInDB.Book_Published = currentBook.Book_Published
                bookInDB.Book_Quantity_Avail = currentBook.Book_Quantity_Avail
                bookInDB.Book_Quantity_Onhand -= 1 'decrease quantity by 1
                bookInDB.IsActive = currentBook.IsActive
                bookInDB.RowVersion = currentBook.RowVersion
                'Attach it
                ObjectContext.Attach(bookInDB)
                'Change object state
                ObjectContext.ObjectStateManager.ChangeObjectState(bookInDB, EntityState.Modified)
                ObjectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave)
                ObjectContext.Refresh(RefreshMode.StoreWins, bookInDB)
                Return bookInDB
            Catch ex As Exception
                ObjectContext.Refresh(RefreshMode.StoreWins, bookInDB)
                Return Nothing
            End Try
        End If

    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function Deallocate_Book(ByVal currentBook As Book) As Book

        'Using Concurrency control with RowVersion timestamp
        'Check Book_id
        Dim bookID As Integer = currentBook.Book_id
        Dim booksInDB = From b In ObjectContext.Books _
                        Where b.Book_id = bookID _
                        Select b

        'Check book
        If booksInDB.Count < 1 Then
            Return Nothing
        End If
        Dim bookInDB = booksInDB.FirstOrDefault()

        If bookInDB.Book_Quantity_Onhand >= bookInDB.Book_Quantity_Avail Then
            Return Nothing
        Else
            Try
                'Detach it first
                ObjectContext.Detach(bookInDB)
                'Update the book
                bookInDB.Book_Title = currentBook.Book_Title
                bookInDB.Book_Image_File = currentBook.Book_Image_File
                bookInDB.Book_Description = currentBook.Book_Description
                bookInDB.Book_Media = currentBook.Book_Media
                bookInDB.Book_Language = currentBook.Book_Language
                bookInDB.Book_ISBN = currentBook.Book_ISBN
                bookInDB.Book_Published = currentBook.Book_Published
                bookInDB.Book_Quantity_Avail = currentBook.Book_Quantity_Avail
                bookInDB.Book_Quantity_Onhand += 1 'increase quantity by 1
                bookInDB.IsActive = currentBook.IsActive
                bookInDB.RowVersion = currentBook.RowVersion
                'Attach it
                ObjectContext.Attach(bookInDB)
                'Change object state
                ObjectContext.ObjectStateManager.ChangeObjectState(bookInDB, EntityState.Modified)
                ObjectContext.SaveChanges(SaveOptions.DetectChangesBeforeSave)
                ObjectContext.Refresh(RefreshMode.StoreWins, bookInDB)
                Return bookInDB
            Catch ex As Exception
                ObjectContext.Refresh(RefreshMode.StoreWins, bookInDB)
                Return Nothing
            End Try
        End If

    End Function

    <Invoke()>
    <RequiresAuthentication()>
    Public Function Count_Unreviewed_Transactions_Of_Member(ByVal member_id As Integer) As Integer
        Dim returnCount As Integer = 0
        Dim datePastTwoMonth As Date = Date.Today.AddMonths(-2)

        Dim unreviewedTransactionQuery As IQueryable(Of Transactions_Detail)
        unreviewedTransactionQuery = From mt In Me.ObjectContext.Transactions_Detail
                                     Where mt.MemberID = member_id _
                                        And mt.Returned_Date > datePastTwoMonth _
                                        And mt.Review_id Is Nothing
                                     Select mt

        returnCount = unreviewedTransactionQuery.Count()
        Return returnCount
    End Function

End Class

