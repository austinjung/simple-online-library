﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.Linq
Imports System.Runtime.Serialization
Imports System.ServiceModel.DomainServices.EntityFramework
Imports System.ServiceModel.DomainServices.Hosting
Imports System.ServiceModel.DomainServices.Server
Imports System.ServiceModel.DomainServices.Server.ApplicationServices
Imports System.Web.Security

Partial Public Class Member
    Implements IUser


    <DataMember(), Key()>
    Public Property Name As String Implements System.ServiceModel.DomainServices.Server.ApplicationServices.IUser.Name
        Get
            Return Me.MemberLogin
        End Get
        Set(ByVal value As String)
            Me.MemberLogin = value
        End Set
    End Property

    <DataMember()>
    Public Property Roles As System.Collections.Generic.IEnumerable(Of String) Implements System.ServiceModel.DomainServices.Server.ApplicationServices.IUser.Roles
        Get
            Return Me.Role.Split(",")
        End Get
        Set(ByVal value As System.Collections.Generic.IEnumerable(Of String))
            Me.Role = String.Join(",", value.ToArray())
        End Set
    End Property

End Class
