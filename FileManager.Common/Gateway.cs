﻿/*
 * This is an example for article «Silverlight File Manager»
 * http://kbyte.ru/ru/Programming/Articles.aspx?id=68&mode=art
 * (russian language only)
 * Translated into English special for CodeProject
 * http://www.codeproject.com/
 * Author: Aleksey S Nemiro
 * http://aleksey.nemiro.ru
 * http://kbyte.ru
 * Copyright © Aleksey S Nemiro, 2012
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using System.Collections;
using System.Web.Script.Serialization;

namespace Nemiro.FileManager.Common
{
  /// <summary>
  /// Universal requests handler
  /// </summary>
  public class Gateway
  {

    private string _Root = "Upload"; // root directory

    public Gateway() { }

    public string GetResult()
    {
      if (HttpContext.Current == null) throw new Exception("HTTP request is required.");

      HttpRequest Request = HttpContext.Current.Request;
      HttpServerUtility Server = HttpContext.Current.Server;

      StringBuilder result = new StringBuilder();

      try
      {
        // ...
        // here you can make authorization
        // ..

        string cmd = "", path = "";
        if (!String.IsNullOrEmpty(Request.Form["cmd"])) { cmd = Request.Form["cmd"].ToLower(); }
        if (!String.IsNullOrEmpty(Request.Form["path"])) { path = Request.Form["path"]; } else { path = "/"; }
        if (!path.EndsWith("/")) path += "/";
        
        DirectoryInfo DI = new DirectoryInfo(Server.MapPath(String.Format("~/{0}{1}", _Root, path)));
        if (!DI.Exists)
        {
          result = GetError(String.Format("Error. The directory \"{0}\" not found.", String.Format("~/{0}{1}", _Root, path)));
          return result.ToString();
        }

        if (cmd == "check")
        {
          #region check file name
          if (File.Exists(Path.Combine(DI.FullName, Request.Form["name"])))
          {
            result = GetJsonString(new { stat = "err", msg = String.Format("Sorry, file \"{0}\" is exists on the directory \"{1}\".", Request.Form["name"], path) });
          }
          else
          {
            result = GetJsonString(new { stat = "ok" });
          }
          #endregion
        }
        else if (cmd == "upload")
        {
          #region save file
          if (Request.Files["file1"] == null || Request.Files["file1"].ContentLength <= 0)
          {
            result = GetError("Error. File is required.");
          }
          else
          {
            // check file name
            if (File.Exists(Path.Combine(DI.FullName, Request.Files["file1"].FileName)))
            { 
              result = GetJsonString(new { stat = "err", msg = String.Format("Sorry, file \"{0}\" is exists on the directory \"{1}\".", Request.Files["file1"].FileName, path) });
            }
            else
            { 
              // save
              using (FileStream fs = System.IO.File.Create(Path.Combine(DI.FullName, Request.Files["file1"].FileName)))
              {
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = Request.Files["file1"].InputStream.Read(buffer, 0, buffer.Length)) != 0)
                {
                  fs.Write(buffer, 0, bytesRead);
                }
              }
              result = GetJsonString(new { stat = "ok" });
            }
          }
          #endregion
        }
        else if (cmd == "newdir")
        {
          #region create a new directory
          if (String.IsNullOrEmpty(Request.Form["name"]))
          {
            result = GetError("Error. Directory name is required.");
          }
          else
          {
            // check name
            DirectoryInfo d = new DirectoryInfo(Path.Combine(DI.FullName, Request.Form["name"]));
            if (d.Exists)
            {
              result = GetError("Sorry, directory is exists.");
            }
            else
            {
              // create directory
              d.Create();
              // is ok
              result = GetJsonString(new { stat = "ok" });
            }
          }
          #endregion
        }
        else if (cmd == "delete")
        {
          #region delete file/directory
          if (String.IsNullOrEmpty(Request.Form["name"]))
          {
            result = GetError("Error. Name is required.");
          }
          else
          {
            if (File.GetAttributes(Path.Combine(DI.FullName, Request.Form["name"])) == FileAttributes.Directory)
            {
              // is directory, 
              Directory.Delete(Path.Combine(DI.FullName, Request.Form["name"]), true);
            }
            else
            {
              // is file
              File.Delete(Path.Combine(DI.FullName, Request.Form["name"]));
            }
            result = GetJsonString(new { stat = "ok" });
          }
          #endregion
        }
        else if (cmd == "rename")
        {
          #region rename file/directory
          string oldName = Request.Form["oldName"], newName = Request.Form["newName"];
          if (String.IsNullOrEmpty(oldName) || String.IsNullOrEmpty(newName))
          {
            result = GetError("Error. Name is required.");
          }
          else
          {
            if (newName != oldName)
            {
              if (File.GetAttributes(Path.Combine(DI.FullName, oldName)) == FileAttributes.Directory)
              {
                // rename directory
                Directory.Move(Path.Combine(DI.FullName, oldName), Path.Combine(DI.FullName, newName));
              }
              else
              {
                // rename file
                File.Move(Path.Combine(DI.FullName, oldName), Path.Combine(DI.FullName, newName));
              }
            }
            result  = GetJsonString(new { stat = "ok" });
          }
          #endregion
        }
        else
        {
          #region file list
          ArrayList files = new ArrayList();
          // dicrectories
          foreach (DirectoryInfo d in DI.GetDirectories())
          {
            files.Add(new
            {
              name = d.Name,
              size = 0,
              type = 0, // type = 0 - is directory
              url = String.Format("http://{0}/{1}{2}{3}", Request.Url.Host + (Request.Url.Port > 80 ? ":" + Request.Url.Port.ToString() : ""), _Root, path, d.Name) 
            }); 
          }
          // files
          foreach (FileInfo f in DI.GetFiles())
          {
            files.Add(new
            {
              name = f.Name,
              size = f.Length,
              type = 1,// type = 1 - is file
              url = String.Format("http://{0}/{1}{2}{3}", Request.Url.Host + (Request.Url.Port > 80 ? ":" + Request.Url.Port.ToString() : ""), _Root, path, f.Name)
            }); 
          }
          // check top-level directory
          bool allowUp = !String.IsNullOrEmpty(path.Trim("/".ToCharArray()));
          // create JSON
          result = GetJsonString(new { stat = "ok", allowUp = allowUp, data = files });
          #endregion
        }
      }
      catch (Exception ex)
      {
        // error
        result = GetError(ex.Message);
      }

      // result
      return result.ToString();
    }

    /// <summary>
    /// The helper function returning error in the JSON
    /// </summary>
    /// <param name="msg">Error message</param>
    private StringBuilder GetError(string msg)
    {
      return GetJsonString(new { stat = "err", msg = msg });
    }

    /// <summary>
    /// The helper function for converting object to JSON
    /// </summary>
    /// <param name="source">Object for converting JSON</param>
    private StringBuilder GetJsonString(object source)
    {
      StringBuilder result = new StringBuilder();
      JavaScriptSerializer myJSON = new JavaScriptSerializer();
      myJSON.Serialize(source, result);
      return result;
    }

  }
}
