-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Insert_Author_Of_Book] 
	-- Add the parameters for the stored procedure here
	@Book_id int,
	@Author_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF ( NOT EXISTS ( SELECT Author_id
                  FROM   [dbo].[Books_by_Author]
                  WHERE  @Book_id = Book_id AND @Author_id = Author_id) )
    BEGIN
        INSERT INTO [dbo].[Books_by_Author] 
			(Book_id, Author_id)
        VALUES (@Book_id, @Author_id)
    END
END
GO
