USE [ckLibrary]
GO

/****** Object:  StoredProcedure [dbo].[Delete_Authors_Of_Book]    Script Date: 03/06/2012 11:33:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Delete_Authors_Of_Book] 
	-- Add the parameters for the stored procedure here
	@Book_id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Books_by_Author] WHERE @Book_id = Book_id
END

GO


